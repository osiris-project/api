﻿namespace Osiris.Api.Modules.ProductEp
{
    public class ProductQuery
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
