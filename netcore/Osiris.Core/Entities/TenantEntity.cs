﻿namespace Osiris.Core.Entities
{
    public abstract class TenantEntity
        : CompanyEntity
    {
        #region Properties

        public virtual Tenant Tenant
        {
            get { return (Tenant)Company; }
            set { Company = value; }
        }

        #endregion

        protected TenantEntity() { }

        protected TenantEntity(Tenant tenant)
            : base(tenant)
        {
        }
    }
}
