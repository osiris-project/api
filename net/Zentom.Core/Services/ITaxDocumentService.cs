﻿using System.IO;
using System.Threading.Tasks;

namespace Zentom.Core.Services
{
    public interface ITaxDocumentService
    {
        Task<IFileUploadResult> HandleFileUpload(string fileName, Stream stream);
    }
}