﻿namespace Zentom.Core.Entities
{
    public class TaxDocumentReceptor
        : AbstractTaxDocumentEnterprise
    {
        public virtual string BankAccount { get; set; }

        public virtual string ReceptionAddressStreet { get; set; }

        public virtual string ReceptionAddressPart { get; set; }

        public virtual string ReceptionAdminDivisionLevel2 { get; set; }

        public virtual string ReceptionAdminDivisionLevel3 { get; set; }

        public virtual string AddressPart { get; set; }

        public virtual string PhoneNumber { get; set; }

        public TaxDocumentReceptor() { }
    }
}
