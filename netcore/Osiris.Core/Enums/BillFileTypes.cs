﻿namespace Osiris.Core.Enums
{
    public enum BillFileTypes
    {
        //[Display(Name = "ElectronicInvoice", ResourceType = typeof(Text))]
        EInvoice,
        //[Display(Name = "ManualInvoice", ResourceType = typeof(Text))]
        ManualInvoice,
        //[Display(Name = "ElectronicBill", ResourceType = typeof(Text))]
        EBill,
        //[Display(Name = "ManualBill", ResourceType = typeof(Text))]
        ManualBill
    }
}
