﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class AccountRoleMap
        : ClassMap<AccountRole>
    {
        public AccountRoleMap()
        {
            Schema("system");
            Table("account_role");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            
            References(i => i.AuthorizedAccount, "authorized_account_id")
                .UniqueKey("account_role_un")
                .Cascade.SaveUpdate();

            References(i => i.Role, "role_id")
                .UniqueKey("account_role_un")
                .Cascade.SaveUpdate();
        }
    }
}
