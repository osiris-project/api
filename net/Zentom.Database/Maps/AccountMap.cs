﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class AccountMap
        : ClassMap<Account>
    {
        public AccountMap()
        {
            Schema("system");
            Table("account");

            Id(i => i.Id).GeneratedBy.Foreign("Person").Not.Nullable().UniqueKey("account_un");
            Version(i => i.RowVersion);

            Map(i => i.Login).Length(50).UniqueKey("account_un");
            Map(i => i.Password).Length(128);
            Map(i => i.Salt).Length(500);
            Map(i => i.Email).Length(1000);

            HasMany(i => i.AuthorizedBy)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("account_id");

            HasOne(i => i.Person)
                .Not.LazyLoad()
                .Fetch.Join()
                .Cascade.SaveUpdate()
                .Constrained();

            HasMany(i => i.Templates)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("account_id");

            HasMany(i => i.PasswordRequests)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("account_id");
        }
    }
}
