﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Osiris.Core.Entities
{
    public class Account
        : Entity
    {
        #region Fields

        private string login;

        private string email;

        private byte[] password;

        private byte[] salt;

        //private string password;

        private Person person;

        private ISet<TaxDocumentTemplate> templates = new HashSet<TaxDocumentTemplate>();

        private ISet<PasswordRequest> passwordRequests = new HashSet<PasswordRequest>();

        private ISet<AuthorizedAccount> authorizedBy = new HashSet<AuthorizedAccount>();

        #endregion

        #region Properties

        public virtual string Login
        {
            get { return login; }
            set { login = value; }
        }

        //public virtual string Password
        //{
        //    get { return password; }
        //    set { password = value;  }
        //}

        public virtual byte[] Password
        {
            get { return password; }
            set { password = value; }
        }

        public virtual byte[] Salt
        {
            get { return salt; }
            set { salt = value; }
        }

        public virtual Person Person
        {
            get { return person; }
            set { person = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }

        public virtual ISet<TaxDocumentTemplate> Templates
        {
            get { return templates; }
            set { templates = value; }
        }

        public virtual ISet<PasswordRequest> PasswordRequests
        {
            get { return passwordRequests; }
            set { passwordRequests = value; }
        }

        public virtual ISet<AuthorizedAccount> AuthorizedBy
        {
            get { return authorizedBy; }
            set { authorizedBy = value; }
        }

        #endregion

        public Account() { }

        public Account(string login, string email, byte[] password, byte[] salt, Person person)
        {
            this.login = login;
            this.email = email;
            this.password = password;
            this.salt = salt;
            this.person = person;
        }

        //public Account(string login, string email, string password, Person person)
        //{
        //    this.login = login;
        //    this.email = email;
        //    this.password = password;
        //    this.person = person;
        //}

        #region Templates

        public virtual void AddTemplate(string name, string filePath, string documentType, string jsonContents)
        {
            if (templates.Any(i => i.Name == name))
                return;
            var template = new TaxDocumentTemplate(filePath, name, documentType, DateTime.UtcNow, this);
            templates.Add(template);
            template.Save(jsonContents);
        }

        public virtual void RemoveTemplate(string name)
        {
            var template = templates.SingleOrDefault(i => i.Name == name);
            if (template == null)
                return;
            template.RemoveFile();
            templates.Remove(template);
        }

        #endregion

        public virtual void AddPasswordRequest(DateTime expireDate, DateTime requestDate, string ipv4, string ipv6 = null)
        {
            var passReq = new PasswordRequest(expireDate, requestDate, ipv4, ipv6, this);
            passwordRequests.Add(passReq);
        }
    }
}
