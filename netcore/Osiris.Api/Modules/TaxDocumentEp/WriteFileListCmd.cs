﻿using System;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class WriteFileListCmd
    {
        public Guid[] Documents { get; set; } 
    }
}
