﻿namespace Zentom.Core.Entities
{
    public enum ContactType : byte
    {
        AccountEmail, Phone, MobilePhone
    }

    public abstract class ContactInfo
        : Entity
    {
        #region Fields

        private byte priority;

        private byte contactType;

        private string value;


        #endregion

        #region Properties

        public virtual byte Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        public virtual byte ContactType
        {
            get { return contactType; }
            set { contactType = value; }
        }

        public virtual string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        #endregion

        protected ContactInfo() { }

        protected ContactInfo(byte priority, byte contactType, string value)
        {
            this.contactType = contactType;
            this.value = value;
            this.priority = priority;
        }
    }
}
