# Osiris Invoice Manager

This is the port of Zentom that will run on .NET Core.

- **09-28-2020**: The project's original API library has been deprecated (NancyFX) so I cannot continue for now (it may take a lot of time to migrate to another library) and I don't have access to a Windows Server testing environment (even on docker it's heavy for my current machine).
