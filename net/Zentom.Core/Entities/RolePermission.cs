﻿namespace Zentom.Core.Entities
{
    public class RolePermission
        : Entity
    {
        #region Fields

        private Role _role;

        private Permission _permission;

        #endregion
        
        #region Properties

        public virtual Role Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public virtual Permission Permission
        {
            get { return _permission; }
            set { _permission = value; }
        }

        #endregion

        public RolePermission() { }

        public RolePermission(Role role, Permission permission)
        {
            _role = role;
            _permission = permission;
        }
    }
}
