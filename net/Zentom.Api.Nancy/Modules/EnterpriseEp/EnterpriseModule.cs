﻿using System;
using System.Linq;
using Dobelik.Utils.Identity;
using log4net;
using Nancy.ModelBinding;
using NHibernate;
using NHibernate.Transform;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Modules.EconomicActivityEp;
using Zentom.Api.Security;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.EnterpriseEp
{
    public class EnterpriseModule
        : CRUDLModule<Command, EnterpriseCmd, EnterpriseUpdateCmd>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EnterpriseModule));

        private readonly INationalIdentity identity;

        public EnterpriseModule(ISessionFactory factory, IdentityLocator locator)
            : base("enterprises", factory)
        {
            identity = locator.Get();

            //this.Patch(BasePath + "/{id:guid}/addresses/{addressId:guid}/");

            // Commercial Activities
            this.Post<CommercialActivityCmd>(BasePath + "/{id:guid}/commercial-activities/", AddCommercialActivity);
            this.Get<Command>(BasePath + "/{id:guid}/comercial-activities/", GetCommercialActivities);
            this.Get<Command>(BasePath + "/{id:guid}/commercial-activities/", GetCommercialActivities);

            // Economic Activities
            this.Get<Command>(BasePath + "/{id:guid}/economic-activities/", GetEconomicActivities);
            this.Put<UpdateCostsCenterCmd>(BasePath + "/{id:guid}/costs-centers/", UpdateCostsCenter);

            // Addresses
            this.Post<EnterpriseAddressCreateCmd>(BasePath + "/{id:guid}/addresses/", AddAddress);
            this.Get<Command>(BasePath + "/{id:guid}/addresses/{addressType}/", GetAddress);
        }

        [Permission(Permission = "enterprise-update")]
        public object UpdateCostsCenter(UpdateCostsCenterCmd cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Relationship == EnterpriseRelationship.Provider && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(string.Format(I18n.Error.EnterpriseNotFound, cmd.Id));
            }
            var costsCenter = db.QueryOver<CostsCenter>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.CostsCenterId)
                .Take(1)
                .SingleOrDefault();
            if (costsCenter == null)
            {
                return Negotiate.WithNotFound(I18n.Error.CostsCenterNotFound);
            }
            costsCenter.Enterprises.Add(entity);
            entity.CostsCenter = costsCenter;
            return Negotiate.WithNoContent();
        }

        #region Overrides of CRUDModule<Command,object,object>

        [Permission(Permission = "enterprise-read")]
        public override object Read(Command cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.EnterpriseNotFound);
            }

            Commune commune = null;
            City city = null;
            var addresses = db.QueryOver<EnterpriseAddress>()
                .Where(i => i.Enterprise == entity)
                .Left.JoinAlias(i => i.Commune, () => commune)
                .Left.JoinAlias(i => commune.City, () => city)
                .SelectList(i => i.Select(j => j.Id)
                    .Select(j => j.Line1)
                    .Select(j => j.Line2)
                    .Select(j => commune.Id)
                    .Select(j => commune.Name)
                    .Select(j => city.Name))
                .Future<object[]>();

            ComercialActivity comercialActivity = null;
            var comercialActivities = db.QueryOver<EnterpriseComercialActivity>()
                .Where(i => i.Enterprise == entity)
                .Left.JoinAlias(i => i.ComercialActivity, () => comercialActivity)
                .SelectList(i => i
                    .Select(j => comercialActivity.Id)
                    .Select(j => comercialActivity.Name))
                .Future<object[]>();

            object dto = new
            {
                id = entity.Id,
                taxId = entity.TaxId,
                legalName = entity.LegalName,
                displayName = entity.DisplayName,
                email = entity.Email,
                phone = entity.Phone,
                bankAccountNumber = entity.BankAccountNumber,
                addresses = addresses.ToList().Select(i => new
                {
                    id = (Guid)i[0],
                    line1 = (string)i[1],
                    line2 = (string)i[2],
                    commune = new
                    {
                        id = (Guid)i[3],
                        name = (string)i[4]
                    },
                    city = new
                    {
                        name = (string)i[5]
                    }
                }),
                comercialActivities = comercialActivities.ToList().Select(i => new
                {
                    id = (Guid)i[0],
                    name = (string)i[1]
                })
            };
            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "enterprise-list")]
        public override object List(dynamic args)
        {
            var bound = this.BindTo(new EnterpriseQuery());
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var page = Settings.PaginationSettings.Page;
            var query = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Relationship == bound.RelationFilter);

            log.DebugFormat("RelationFilter: {0}", bound.RelationFilter);

            if (!string.IsNullOrWhiteSpace(bound.TaxId))
            {
                log.DebugFormat("Searching for tax id: {0}", bound.TaxId);
                if (identity.Validate(bound.TaxId))
                {
                    bound.TaxId = identity.CleanId(bound.TaxId);
                    query.Where(i => i.TaxId == bound.TaxId);
                }
            }

            if (!string.IsNullOrWhiteSpace(bound.LegalName))
            {
                log.DebugFormat("Searching for legal name: {0}", bound.LegalName);
                bound.LegalName = bound.LegalName.Trim();
                query.Where(i => i.LegalName == bound.LegalName);
            }

            EnterpriseDto dto = null;
            var items = query
                .SelectList(i => i.Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.TaxId).WithAlias(() => dto.TaxId)
                    .Select(j => j.LegalName).WithAlias(() => dto.LegalName)
                    .Select(j => j.Relationship).WithAlias(() => dto.Relationship))
                .ProjectInto<Enterprise, EnterpriseDto>()
                .PaginateResults<Enterprise, Enterprise, EnterpriseDto>(page, rpp);
            log.DebugFormat("Result count: {0}", items.Items.Count);
            return Negotiate.WithOkAndList(items, Context.Request.Url);
        }

        /// <summary>
        /// Allows to create the full graph enterprise entity.
        /// </summary>
        /// <param name="cmd">The command to create the enterprise entity.</param>
        /// <param name="args"></param>
        /// <returns>A negotiated response.</returns>
        [Permission(Permission = "enterprise-create")]
        public override object Create(EnterpriseCmd cmd, dynamic args)
        {
            cmd.TaxId = identity.CleanId(cmd.TaxId);
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && (i.TaxId == cmd.TaxId || i.LegalName == cmd.LegalName))
                .Take(1)
                .SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithExistingItem("duplicate", entity.TaxId == cmd.TaxId ? "taxId" : "legalName");
            }

            entity = new Enterprise
            {
                TaxId = cmd.TaxId,
                DisplayName = cmd.DisplayName,
                LegalName = cmd.LegalName,
                BankAccountNumber = cmd.BankAccountNumber,
                Email = cmd.Email,
                Phone = cmd.Phone,
                Relationship = cmd.Relationship,
                Tenant = currentTenant
            };

            if (cmd.Addresses != null)
            {
                foreach (var address in cmd.Addresses)
                {
                    var types = address.AddressTypes.Select(i => db.Load<AddressType>(i));
                    entity.AddAddress(address.Line1, address.Line2, db.Load<Commune>(address.CommuneId), types);
                }
            }

            if (cmd.EconomicActivities != null)
            {
                entity.SetEconomicActivities(cmd.EconomicActivities.Select(i => db.Load<EconomicActivity>(i)).ToList());
            }

            if (cmd.ComercialActivities != null)
            {
                //var comercialActivities = db.QueryOver<EnterpriseComercialActivity>()
                //    .Where(i => i.Enterprise == entity)
                //    .List();

                //var x = comercialActivities.Count - 1;
                //for (; x >= 0; x--)
                //{
                //    var el = comercialActivities.ElementAt(x);
                //    if(cmd.ComercialActivities.All(i => i != el.ComercialActivity.Name)) continue;
                //    comercialActivities.Remove(el);
                //}

                foreach (var comercialActivity in cmd.ComercialActivities.Select(name => db.QueryOver<ComercialActivity>()
                    .Where(i => i.Name == name)
                    .Take(1)
                    .SingleOrDefault() ?? new ComercialActivity(name)))
                {
                    entity.EnterpriseComercialActivities.Add(new EnterpriseComercialActivity(entity, comercialActivity));
                }
            }

            db.Save(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        [Permission(Permission = "enterprise-update")]
        public override object Update(EnterpriseUpdateCmd cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.EnterpriseNotFound);
            }

            cmd.TaxId = identity.CleanId(cmd.TaxId);

            log.DebugFormat("Entity.TaxId '{0}', cmd.TaxId '{1}', Are equal? {2}", entity.TaxId, cmd.TaxId, entity.TaxId == cmd.TaxId);

            if (cmd.TaxId != entity.TaxId)
            {
                var found = db.QueryOver<Enterprise>()
                    .Where(i => i.TaxId == cmd.TaxId && i.Tenant == currentTenant)
                    .Take(1)
                    .SingleOrDefault();

                if (found != null)
                {
                    return Negotiate.WithDuplicateItem(I18n.Error.ExistingTaxId, "taxId");
                }
            }

            if (cmd.LegalName != entity.LegalName)
            {
                var found = db.QueryOver<Enterprise>()
                    .Where(i => i.LegalName == cmd.LegalName && i.Tenant == currentTenant)
                    .Take(1)
                    .SingleOrDefault();

                if (found != null)
                {
                    return Negotiate.WithDuplicateItem(I18n.Error.DuplicateLegalName, "legalName");
                }
            }
            //var j = comercialActivities.Count - 1;

            //for (; j >= 0; j--)
            //{
            //    comercialActivities.RemoveAt(j);
            //}

            var comercialActivities =
                cmd.ComercialActivities.Select(
                    name =>
                        db.QueryOver<ComercialActivity>().Where(i => i.Name == name).Take(1).SingleOrDefault() ??
                        new ComercialActivity(name)).ToList();
            var index = entity.EnterpriseComercialActivities.Count - 1;

            for (; index >= 0; index--)
            {
                var item = entity.EnterpriseComercialActivities.ElementAt(index);

                if (comercialActivities.Any(i => i.Id == item.ComercialActivity.Id))
                {
                    comercialActivities.Remove(item.ComercialActivity);
                }
                else
                {
                    entity.EnterpriseComercialActivities.Remove(item);
                }
            }

            foreach (var comercialActivity in comercialActivities)
            {
                entity.EnterpriseComercialActivities.Add(new EnterpriseComercialActivity(entity, comercialActivity));
            }

            var addresses = db.QueryOver<EnterpriseAddress>()
                .Where(i => i.Enterprise == entity)
                .List();
            addresses.Clear();

            foreach (var item in cmd.Addresses)
            {
                var types = item.AddressTypes.Select(i => db.Load<AddressType>(i));
                entity.AddAddress(item.Line1, item.Line2, db.Load<Commune>(item.CommuneId), types);
            }

            entity.Relationship = cmd.Relationship;
            entity.TaxId = cmd.TaxId;
            entity.BankAccountNumber = cmd.BankAccountNumber;
            entity.DisplayName = cmd.DisplayName;
            entity.LegalName = cmd.LegalName;
            entity.Email = cmd.Email;
            entity.Phone = cmd.Phone;

            return Negotiate.WithOk();
        }

        [Permission(Permission = "enterprise-delete")]
        public override object Remove(Command cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.EnterpriseNotFound);
            }

            db.Delete(entity);
            return Negotiate.WithOk();
        }

        #endregion

        #region Enterprise Addresses

        [Permission(Permission = "enterprise-update")]
        public object AddAddress(EnterpriseAddressCreateCmd cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Id == cmd.EnterpriseId && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.EnterpriseId, I18n.Error.EnterpriseNotFound);
            }

            var addressEntity =
                entity.EnterpriseAddresses.SingleOrDefault(i => i.Line1 == cmd.Line1 && i.Commune.Id == cmd.CommuneId);

            if (addressEntity != null)
            {
                return Negotiate.WithOk();
            }

            entity.AddAddress(cmd.Line1, cmd.Line2, db.Load<Commune>(cmd.CommuneId), cmd.AddressTypes.Select(i => db.Load<AddressType>(i)));
            return Negotiate.WithOk();
        }

        [Permission(Permission = "enterprise-update")]
        public object UpdateAddress(EnterpriseAddressCmd cmd)
        {

            return null;
        }

        [Permission(Permission = "enterprise-update")]
        public object RemoveAddresses(Command cmd)
        {
            var entity = db.QueryOver<EnterpriseAddress>()
                .Where(i => i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithOk();
            }

            entity = null;
            return Negotiate.WithOk();
        }

        [Permission(RequiresLogin = true)]
        public object GetAddress(Command cmd)
        {
            var bound = this.BindTo(new AddressQuery());
            AddressType addressType = null;
            Enterprise enterprise = null;
            EnterpriseAddressType enterpriseAddressTypes = null;
            Commune commune = null;
            City city = null;
            AddressDto dto = null;
            var query = db.QueryOver<EnterpriseAddress>()
                .Left.JoinAlias(i => i.Enterprise, () => enterprise)
                .Left.JoinAlias(i => i.EnterpriseAddressTypes, () => enterpriseAddressTypes)
                .Left.JoinAlias(i => enterpriseAddressTypes.AddressType, () => addressType)
                .Left.JoinAlias(i => i.Commune, () => commune)
                .Left.JoinAlias(i => commune.City, () => city)
                .SelectList(i => i
                    .Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.Line1).WithAlias(() => dto.Line1)
                    .Select(j => j.Line2).WithAlias(() => dto.Line2)
                    .Select(j => commune.Id).WithAlias(() => dto.CommuneId)
                    .Select(j => commune.Name).WithAlias(() => dto.CommuneName)
                    .Select(j => city.Id).WithAlias(() => dto.CityId)
                    .Select(j => city.Name).WithAlias(() => dto.CityName))
                .Where(i => enterprise.Id == cmd.Id && enterprise.Tenant == currentTenant);
            if(!string.IsNullOrWhiteSpace(bound.AddressType))
            {
                query.Where(i => addressType.Name == bound.AddressType);
            }
            var items = query.OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<AddressDto>())
                .PaginateResults<EnterpriseAddress, AddressDto>();
            return Negotiate.WithOkAndList(items, Request.Url);
        }

        #endregion

        #region Commercial and Economic Activities

        [Permission(RequiresLogin = true)]
        public object GetCommercialActivities(Command cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.EnterpriseNotFound);
            }

            ComercialActivity comercialActivity = null;
            ComercialActivityDto dto = null;
            var items = db.QueryOver<EnterpriseComercialActivity>()
                .Where(i => i.Enterprise == entity)
                .Left.JoinAlias(i => i.ComercialActivity, () => comercialActivity)
                .SelectList(i => i.Select(j => comercialActivity.Name).WithAlias(() => dto.Name)
                    .Select(() => comercialActivity.Id).WithAlias(() => dto.Id))
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<ComercialActivityDto>())
                .PaginateResults<EnterpriseComercialActivity, ComercialActivityDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(items, Context.Request.Url);
        }

        [Permission(RequiresLogin = true)]
        public object GetEconomicActivities(Command cmd)
        {
            var entity = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.EnterpriseNotFound);
            }

            EconomicActivity economicActivity = null;
            EconomicActivityDto dto = null;
            var items = db.QueryOver<EnterpriseEconomicActivity>()
                .Where(i => i.Enterprise == entity)
                .Left.JoinAlias(i => i.EconomicActivity, () => economicActivity)
                .SelectList(i => i.Select(j => economicActivity.Name).WithAlias(() => dto.Name)
                    .Select(j => economicActivity.Code).WithAlias(() => dto.Code)
                    .Select(() => economicActivity.Id).WithAlias(() => dto.Id))
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<EconomicActivityDto>())
                .PaginateResults<EnterpriseEconomicActivity, EconomicActivityDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(items, Context.Request.Url);
        }

        [Permission(Permission = "enterprise-update")]
        public object AddCommercialActivity(CommercialActivityCmd cmd)
        {
            var commercialActivity = db.QueryOver<ComercialActivity>()
                .Where(i => i.Name == cmd.Name)
                .Take(1)
                .SingleOrDefault();

            if(commercialActivity == null)
            {
                commercialActivity = new ComercialActivity
                {
                    Name = cmd.Name
                };
                db.Save(commercialActivity);
            }

            ComercialActivity comercialActivity = null;
            var enterprise = db.QueryOver<Enterprise>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if(enterprise == null)
            {
                return Negotiate.WithNotFound(string.Format(I18n.Error.EnterpriseNotFound, cmd.Id));
            }

            var entity = db.QueryOver<EnterpriseComercialActivity>()
                .Left.JoinAlias(i => i.ComercialActivity, () => comercialActivity)
                .Where(i => i.Enterprise == enterprise && comercialActivity.Name == cmd.Name)
                .Take(1)
                .SingleOrDefault();

            if(entity != null)
            {
                return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
            }

            entity = new EnterpriseComercialActivity
            {
                Enterprise = enterprise,
                ComercialActivity = commercialActivity
            };
            db.Save(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        #endregion
    }
}