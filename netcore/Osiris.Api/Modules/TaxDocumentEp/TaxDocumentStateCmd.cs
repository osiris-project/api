﻿using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentStateCmd
        : Command
    {
        public TaxDocumentStatus Status { get; set; }
    }
}
