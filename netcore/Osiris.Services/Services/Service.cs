﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NHibernate;
using Osiris.Core.Entities;
using Osiris.Core.Interfaces.Validation;
using Osiris.Core.Services;
using Osiris.I18n;

namespace Osiris.Services
{
    public abstract class Service
        : IService
    {
        private readonly ISessionFactory factory;

        private IList<IModelValidation> errorList;

        protected ISession session => factory.GetCurrentSession();

        #region Implementation of IService

        public bool IsValid => ErrorList?.Count == 0;

        public IList<IModelValidation> ErrorList
        {
            get { return errorList ?? (errorList = new List<IModelValidation>()); }
        }

        #endregion

        protected Service(ISessionFactory factory)
        {
            this.factory = factory;
            errorList = new List<IModelValidation>();
        }

        protected bool IsNull(object obj)
        {
            if (obj != null) return false;
            ErrorList.Add(new ErrorModel("", Error.Null));
            return true;
        }

        protected bool Exists<T>(Expression<Func<T, bool>> checkExpression)
            where T : Entity
        {
            return session.QueryOver<T>()
                .Where(checkExpression)
                .Take(1)
.SingleOrDefault() != null;
        }
    }
}
