/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core

import com.valorti.osiris.core.organization.Tenant

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface Service {
    List<ValidationError> getErrors()

    Tenant getTenant()

    void setTenant(Tenant tenant)
}
