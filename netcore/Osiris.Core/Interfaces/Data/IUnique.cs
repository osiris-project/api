﻿namespace Osiris.Core.Interfaces.Data
{
    public interface IUnique<out T>
    {
        T Id { get; }
    }
}
