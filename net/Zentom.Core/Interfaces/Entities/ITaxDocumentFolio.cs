﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocumentFolio
        : IVersioned
    {
        IDocumentType DocumentType { get; set; }

        IEnterprise Enterprise { get; set; }
    }

    public interface ITaxDocumentFolioId
    {
        IDocumentType DocumentType { get; set; }

        IEnterprise Enterprise { get; set; }
    }
}
