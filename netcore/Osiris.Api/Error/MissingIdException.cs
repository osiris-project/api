﻿using System;

namespace Osiris.Api.Error
{
    public class MissingIdException
        : Exception
    {
        public MissingIdException(string propertyName)
            : base(string.Format(I18n.Error.MissingId, propertyName))
        { }
    }
}
