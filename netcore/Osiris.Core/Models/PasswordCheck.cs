namespace Osiris.Core.Models
{
    /// <summary>
    /// Model containing a user's login and password to be checked.
    /// </summary>
    public class PasswordCheck
    {
        /// <summary>
        /// Builds a Password Check model.
        /// </summary>
        /// <param name="login">The user's login whose password is checked.</param>
        /// <param name="password">The password being checked.</param>
        public PasswordCheck (string login, string password)
        {
            Login = login;
            Password = password;
        }

        public string Login { get; }

        public string Password { get; }
    }
}
