﻿namespace Zentom.Api.Modules.SectorEp
{
    public class SectorQuery
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public bool EconomicGroupsOnly { get; set; }

        public bool OnlyExemptSectors { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Code: {Code}, EconomicGroupsOnly: {EconomicGroupsOnly}, OnlyExemptSectors: {OnlyExemptSectors}";
        }
    }
}