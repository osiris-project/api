﻿using System.Text;
using log4net;
using Nancy;
using Zentom.Api.Extensions.Nancy;
using Zentom.Core.Entities;

namespace Zentom.Api.Security
{
    public static class SecurityHelpers
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SecurityHelpers));

        public static Response CheckAccount(this NancyContext context, Account account)
        {
            if (account == null)
            {
                return new Response
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    ReasonPhrase = I18n.Error.InvalidAccount,
                    Contents = stream =>
                    {
                        var error = Encoding.UTF8.GetBytes(I18n.Error.InvalidAccount);
                        stream.Write(error, 0, error.Length);
                    }
                };
            }
            context.CurrentUser = new NancyUser(account.Login, account);
            Log.InfoFormat("[User Session]: {{ Login = {0}, IP = {1} }}", account.Login, context.RemoteAddress());
            return null;
        }
    }
}