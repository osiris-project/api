﻿namespace Osiris.Api.Modules.EnterpriseEp
{
    public class CommercialActivityCmd
        : Command
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
