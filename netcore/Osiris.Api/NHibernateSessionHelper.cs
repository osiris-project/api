﻿using System;
using System.Data;
using Slf4Net;
using LightInject;
using Nancy;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Context;
using Osiris.Core.Exceptions;

namespace Osiris.Api
{
    public static class NHibernateSessionHelper
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(NHibernateSessionHelper));

        //public static void Configure(IServiceFactory container, IPipelines pipelines)
        //{
        //    pipelines.BeforeRequest.AddItemToStartOfPipeline(ctx => OpenSession(container));
        //    pipelines.AfterRequest.AddItemToStartOfPipeline(ctx => Commit(container));
        //    pipelines.OnError += (ctx, ex) => Rollback(container);
        //}

        public static Response OpenSession(IServiceFactory container)
        {
            Log.Debug("Entering OpenSession.");
            var sessionFactory = container.GetInstance<ISessionFactory>();
            var session = sessionFactory.OpenSession();
            CurrentSessionContext.Bind(session);
            Log.Debug("Begginning session '{0}'.", session.GetSessionImplementation().SessionId);
            session.BeginTransaction(IsolationLevel.ReadCommitted);
            Log.Debug("Leaving OpenSession.");
            return null;
        }

        public static Response Commit(NancyContext context, IServiceFactory container)
        {
            Log.Debug("Entering Commit.");
            var sessionFactory = container.GetInstance<ISessionFactory>();
            var responseNegotiator = container.GetInstance<IResponseNegotiator>();

            if (!CurrentSessionContext.HasBind(sessionFactory))
                return null;

            var session = sessionFactory.GetCurrentSession();
            Log.Debug("Commiting session '{0}'.", session.GetSessionImplementation().SessionId);
            Negotiator negotiator;
            try
            {
                session.Transaction.Commit();
                CurrentSessionContext.Unbind(sessionFactory);
                DisposeSession(session);
            }
            catch (UniqueKeyException ex)
            {
                Log.Error("Failed to commit NH. transaction.", ex);
                Rollback(container);
                negotiator = new Negotiator(context);
                negotiator.WithModel(new
                {
                    error = string.Format(I18n.Error.ExistingValue, ex.Value)
                }).WithStatusCode(HttpStatusCode.Conflict);
                context.Response = responseNegotiator.NegotiateResponse(negotiator, context);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to commit NH. transaction.", ex);
                Rollback(container);
                negotiator = new Negotiator(context);
                negotiator.WithModel(new
                {
                    error = string.Format(I18n.Error.Exception)
                }).WithStatusCode(HttpStatusCode.Conflict);
                context.Response = responseNegotiator.NegotiateResponse(negotiator, context);
            }

            Log.Debug("Leaving Commit.");
            return null;
        }

        public static void Rollback(IServiceFactory container)
        {
            Log.Debug("Entering Rollback.");
            var sessionFactory = container.GetInstance<ISessionFactory>();
            if (!CurrentSessionContext.HasBind(sessionFactory))
                return;
            var session = sessionFactory.GetCurrentSession();
            Log.Debug("Rolling back session '{0}'.", session.GetSessionImplementation().SessionId);
            session.Transaction.Rollback();
            CurrentSessionContext.Unbind(sessionFactory);
            DisposeSession(session);
            Log.Debug("Leaving Rollback.");
        }

        private static void DisposeSession(ISession session)
        {
            if (session == null)
                return;
            Log.Debug("Disposing of session '{0}'.", session.GetSessionImplementation().SessionId);
            if (session.IsOpen)
            {
                Log.Debug("Closing session.");
                session.Close();
            }
            session.Dispose();
            Log.Debug("ISession correctly disposed.");
        }
    }
}
