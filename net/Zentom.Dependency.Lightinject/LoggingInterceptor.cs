﻿using log4net;
using LightInject.Interception;

namespace Zentom.Dependency
{
    public class LoggingInterceptor
        : IInterceptor
    {
        public object Invoke(IInvocationInfo invocationInfo)
        {
            var log = LogManager.GetLogger(invocationInfo.Proxy.ToString());
            log.DebugFormat("Entering {1}.{0}()", invocationInfo.Method.Name, invocationInfo.Proxy);

            //if (log.IsDebugEnabled)
            //{
            //    foreach (object t in invocationInfo.Arguments)
            //    {
            //        log.Debug("Parameter: {0}", t);
            //    }
            //}

            var retval = invocationInfo.Proceed();
            log.DebugFormat("Leaving {1}.{0}()", invocationInfo.Method.Name, invocationInfo.Proxy);
            return retval;
        }
    }
}
