﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class CountryLocaleMap 
        : ClassMap<CountryLocale>
    {
        public CountryLocaleMap()
        {
            Schema("i18n");
            Table("country_locale");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            
            References(i => i.Country, "country_id")
                .UniqueKey("country_locale_un")
                .Cascade.SaveUpdate();

            References(i => i.Locale, "locale_id")
                .UniqueKey("country_locale_un")
                .Cascade.SaveUpdate();
        }
    }
}
