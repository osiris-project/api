﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IContactData
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Value { get; set; }

        string ValueType { get; set; }

        bool IsMain { get; set; }
    }
}
