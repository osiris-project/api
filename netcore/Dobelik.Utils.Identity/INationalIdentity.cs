﻿namespace Dobelik.Utils.Identity {
    /// <summary>
    /// Interface to implement national identity validation and formatting methods.
    /// </summary>
    public interface INationalIdentity {
        /// <summary>
        /// Verifies that the National identity is valid.
        /// </summary>
        /// <param name="id">The National Identity. It can be normalized or not.</param>
        /// <returns>True if is valid, false otherwise.</returns>
        bool Validate (string id);

        /// <summary>
        /// From a partial National Id (without the verifier part),
        /// this function calculates the verifier part of the id.
        /// </summary>
        /// /// <param name="partialId">The id without the verifier part.</param>
        /// <returns>The verifier part of the National Id.</returns>
        object CalculateVerifierPart (string partialId);

        /// <summary>
        /// Normalizes the string for display or storage.
        /// </summary>
        /// <param name="rawId">The raw identity.</param>
        /// <returns>The normalized id.</returns>
        string Normalize (string rawId);

        /// <summary>
        /// Removes every character that is not required for validation.
        /// </summary>
        /// <param name="id">The national identity.</param>
        /// <returns>A clean representation of the Id.</returns>
        string CleanId (string id);
    }
}
