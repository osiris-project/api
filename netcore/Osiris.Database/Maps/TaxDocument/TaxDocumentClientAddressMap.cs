﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class TaxDocumentClientAddressMap
        : ClassMap<TaxDocumentClientAddress>
    {
        public TaxDocumentClientAddressMap()
        {
            Schema("taxation");
            Table("taxdoc_client_address");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.City).Length(15);
            Map(i => i.Commune).Length(20);
            Map(i => i.Line1).Length(70);
            Map(i => i.Line2).Length(150);
            
            References(i => i.TaxDocumentClient, "taxdoc_client_id")
                .Cascade.None()
                .UniqueKey("taxdoc_client_address_type_un");

            HasMany(i => i.AddressTypes)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("taxdoc_client_address_id");
        }
    }
}
