/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.taxation.documents.DocumentReference

import groovy.util.slurpersupport.GPathResult
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DefaultReferenceParser
    extends AbstractParser<DocumentReference> {
    private final Logger log = LoggerFactory.getLogger(DefaultReferenceParser.class)

    /**
     * Parses a single element
     * @param path
     * @return A document reference object.
     */
    @Override
    DocumentReference parse(GPathResult path) {
        this.errors.clear()
        def refLine = path.NroLinRef?.toInteger()
        log.debug('Parsing reference node {}', refLine)
        def refDocType = path.TpoDocRef?.text()?.trim() as String
        this.log.debug('Referenced document type: {}', refDocType)

        def isGlobal = path.IndGlobal.isEmpty() ? false : path.IndGlobal.text() == '1'
        this.log.debug('Is global? {}', isGlobal)

        def refFolio = path.FolioRef?.text()?.trim() as String
        this.log.debug('Referenced document folio: {}', refFolio)

        def date = XmlUtils.dateFromString(path.FchRef?.text()?.trim() as String)
        this.log.debug('Reference date: {}', date)

        def type = DocumentReference.ReferenceType.fromInt(!path.CodRef.isEmpty() ? path.CodRef.toInteger() : 0)
        this.log.debug('Reference type: {}', type)
        def reason = path.RazonRef?.text()?.trim() as String
        this.log.debug('Reference reason: {}', reason)
        DocumentReference reference
        try {
            reference = new DocumentReference(refDocType, isGlobal, refFolio, type, date, reason)
        } catch (ValidationException ex) {
            log.error('Validation failed for a document reference. ', ex)
            this.errors.addAll(ex.errors)
            reference = null
        }
        return reference
    }
}
