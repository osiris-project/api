﻿using Dobelik.Utils.Identity;
using log4net;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Security;
using Zentom.Core.Entities;
using Zentom.Core.Services;

namespace Zentom.Api.Modules.ManagementEp
{
    [Module(Name = "management")]
    public class ManagementModule
        : TenantApi
    {
        private readonly IAuthenticationService authenticationService;
        private readonly INationalIdentity identity;
        //private static readonly ILog log = LogManager.GetLogger(typeof(ManagementModule));

        public ManagementModule(ISessionFactory factory, IAuthenticationService authenticationService, INationalIdentity identity)
            : base("management", factory)
        {
            this.authenticationService = authenticationService;
            this.identity = identity;
            this.Post<AccountCmd>(BasePath + "/accounts/", CreateAccount);
        }

        [Permission(Permission = "account-create")]
        public object CreateAccount(AccountCmd cmd)
        {
            var entity = db.QueryOver<Account>()
                .Where(i => i.Login == cmd.Login && i.Email == cmd.Email)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                entity = new Account
                {
                    Email = cmd.Email,
                    Login = cmd.Login,
                    Person = new Person
                    {
                        FirstName = cmd.FirstName,
                        LastName = cmd.LastName,
                        NationalId = identity.CleanId(cmd.NationalId),
                        BirthDate = cmd.BirthDate,
                        Gender = cmd.Gender
                    }
                };
                authenticationService.HashPassword(entity, cmd.Password);
                db.Save(entity);
            }
            return Negotiate.WithOkAndModel(new
            {
                id = entity.Id,
                login = entity.Login,
                email = entity.Email,
                person = new
                {
                    id = entity.Person.Id,
                    nationalId = entity.Person.NationalId,
                    firstName = entity.Person.FirstName,
                    lastName = entity.Person.LastName,
                    birthDate = entity.Person.BirthDate
                }
            });
        }
    }
}