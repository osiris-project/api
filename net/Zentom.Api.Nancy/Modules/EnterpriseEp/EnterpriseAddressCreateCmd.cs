﻿using System;
using System.Collections.Generic;

namespace Zentom.Api.Modules.EnterpriseEp
{
    public class EnterpriseAddressCreateCmd
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public Guid CommuneId { get; set; }

        public IEnumerable<Guid> AddressTypes { get; set; }

        public Guid EnterpriseId { get; set; }
    }
}