﻿using System.Resources;
using Osiris.I18n;

namespace Osiris.Api.Extensions
{
    public static class ResourceHelper
    {
        private static ResourceManager errorResourceManager;

        private static ResourceManager textResourceManager;

        public static ResourceManager ErrorResourceManager =>
            errorResourceManager ?? (errorResourceManager = new ResourceManager(typeof (I18n.Error)));

        public static ResourceManager TextResourceManager =>
            textResourceManager ?? (textResourceManager = new ResourceManager(typeof (Text)));

        public static string GetValue<T>(this ResourceManager manager)
        {
            return manager.GetString(typeof(T).Name);
        }

        public static string ExistingValue(this ResourceManager resourceManager, string nameOfProperty)
        {
            return string.Format(I18n.Error.ExistingValue, resourceManager.GetString(nameOfProperty));
        }
    }
}
