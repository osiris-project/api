﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Zentom.Api.Nancy.Tests
{
    [TestClass]
    public class RegexTests
    {
        [TestMethod]
        public void TestRegex()
        {
            var message = "Violation of UNIQUE KEY constraint 'product_un'. Cannot insert duplicate key in object 'organization.product'. The duplicate key value is (Test Product XX, dae0b638-f67b-e511-80c4-080027018225).\r\nThe statement has been terminated.";
            var match = Regex.Match(message, @"('[a-z0-9_\-]*')|\((.*?)\)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var constraint = match.Groups[1].Value;
            match = match.NextMatch();
            var value = match.Groups[2].Value;
            Console.WriteLine($"Constraint: {constraint}, Value: {value}");
        }

        [TestMethod]
        public void TestString()
        {
            var empty = "";
            var space = " ";
            Console.WriteLine("IsNullOrEmpty 'empty': {0}", string.IsNullOrEmpty(empty));
            Console.WriteLine("IsNullOrWhiteSpace 'empty': {0}", string.IsNullOrWhiteSpace(empty));
            Console.WriteLine("IsNullOrEmpty 'space': {0}", string.IsNullOrEmpty(space));
            Console.WriteLine("IsNullOrWhiteSpace 'space': {0}", string.IsNullOrWhiteSpace(space));
        }

        [TestMethod]
        public void TestKebabConverter()
        {
            var s = typeof(TaxDocument).Name;
            Assert.AreEqual("tax-document", ToKebabCase(s));
            s = typeof(TaxDocumentReference).Name;
            Assert.AreEqual("tax-document-reference", ToKebabCase(s));
            s = typeof(EnterpriseSettings).Name;
            Assert.AreEqual("enterprise-settings", ToKebabCase(s));
            s = typeof(AResourceRpm).Name;
            Assert.AreEqual("a-resource-rpm", ToKebabCase(s));
            s = typeof (APlusOne).Name;
            Assert.AreEqual("a-plus-one", ToKebabCase(s));
            var b = new BPlusTwo();
            s = GetName(b);
            Assert.AreEqual("b-plus-two", ToKebabCase(s));
        }

        private static string ToKebabCase(string input)
        {
            return
                string.IsNullOrWhiteSpace(input) ? "" :
                string.Concat(input.Select((x, i) => i > 0 && char.IsUpper(x) ? "-" + x.ToString() : x.ToString())).ToLower();
        }

        private static string GetName(APlusOne obj)
        {
            return obj.GetType().Name;
        }

        public class TaxDocument
        {
            
        }

        public class TaxDocumentReference 
        {
            
        }

        public class AResourceRpm
        {
            
        }

        public class EnterpriseSettings
        {
            
        }

        public class APlusOne
        {
            
        }

        public class BPlusTwo : APlusOne
        {
            
        }
    }
}
