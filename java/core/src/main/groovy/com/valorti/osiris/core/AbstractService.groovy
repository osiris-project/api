/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core

import com.valorti.osiris.core.organization.Tenant

/**
 * The base service for all other services.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
abstract class AbstractService
    implements Service {
    final List<ValidationError> errors
    Tenant tenant

    protected AbstractService() {
        this.errors = []
    }
}
