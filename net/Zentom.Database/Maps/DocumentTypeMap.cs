﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class DocumentTypeMap
        : ClassMap<DocumentType>
    {
        public DocumentTypeMap()
        {
            Schema("taxation");
            Table("document_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(6).UniqueKey("document_type_code_un");
            Map(p => p.Name).Length(70).UniqueKey("document_type_code_un");
            Map(p => p.TaxRate).Precision(4).Scale(3);

            HasMany(x => x.TaxDocuments)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("document_type_id");
        }
    }
}
