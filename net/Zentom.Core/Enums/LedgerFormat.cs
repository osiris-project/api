﻿namespace Zentom.Core.Enums
{
    public enum LedgerFormat
    {
        Total,
        Partial,
        Final,
        Adjustment
    }
}
