﻿using Osiris.Core.Entities;

namespace Osiris.Core.Services
{
    public interface IFolioManagerService
    {
        long GetNextFolioNumber(string documentType, Tenant tenant);
    }
}
