﻿namespace Zentom.Core.Entities
{
    public class Option
        : Entity
    {
        #region Fields

        private string _name;

        private string _value;

        private string _valueType;

        private string _description;

        #endregion
        
        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public virtual string ValueType
        {
            get { return _valueType; }
            set { _valueType = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        public Option() { }

        public Option(string value, string valueType, string description, string name)
        {
            _value = value;
            _valueType = valueType;
            _description = description;
            _name = name;
        }

        public override string ToString()
        {
            var str = string.Empty;
            str = string.Concat(str, "Id = ", Id, "\r\n");
            str = string.Concat(str, "RowVersion = ", RowVersion, "\r\n");
            str = string.Concat(str, "Name = ", _name, "\r\n");
            str = string.Concat(str, "Value = ", _value, "\r\n");
            str = string.Concat(str, "ValueType = ", _valueType, "\r\n");
            str = string.Concat(str, "Description = ", _description, "\r\n");
            str = string.Concat(str, "IsTransient = ", IsTransient, "\r\n");
            return str;
        }
    }
}