﻿using System;
using System.Collections.Generic;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Enums;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentCmd
    {
        public long? FolioNumber { get; set; }

        public string DocumentType { get; set; }

        public DateTime AccountingDate { get; set; }

        public DateTime? ExpireDate { get; set; }

        public decimal PreviousDebt { get; set; }

        public string Comments { get; set; }

        public decimal TaxRate { get; set; }

        public decimal ExtraTaxRate { get; set; }

        public bool IsRetainedTax { get; set; }

        public int ExtraTaxCode { get; set; }

        public PaymentType PaymentType { get; set; }

        public int ServiceType { get; set; }

        public bool CreditNoteDiscount { get; set; }

        public CorrectionCode CorrectionCode { get; set; }

        public string VatOwnerTaxId { get; set; }

        public ClientCmd Client { get; set; }

        public TenantCmd Tenant { get; set; }

        public IEnumerable<TaxDocumentLineCmd> Lines { get; set; }

        public IEnumerable<TaxDocumentReferenceCmd> ReferencedDocuments { get; set; }

        public bool GenerateFile { get; set; }
    }

    public class TenantCmd
    {
        public string ComercialActivity { get; set; }

        public EconomicActivityCmd EconomicActivity { get; set; }

        public class EconomicActivityCmd 
        {
            public string Name { get; set; }

            public string Code { get; set; }
        }
    }

    public class ClientCmd
        : Command
    {
        public string ComercialActivity { get; set; }
    }

    public class TaxDocumentLineCmd
    {
        public string Text { get; set; }

        public decimal Quantity { get; set; }

        public string AdditionalGloss { get; set; }

        public decimal UnitPrice { get; set; }

        public string MeasureUnit { get; set; }

        public TaxType TaxType { get; set; }
    }

    public class TaxDocumentReferenceCmd
    {
        public Guid ReferencedDocumentId { get; set; }
        
        public long Folio { get; set; }

        public string DocumentType { get; set; }

        public CorrectionCode ReferenceReason { get; set; }

        public string Comment { get; set; }

        public DateTime ReferenceDate { get; set; }
    }
}
