﻿namespace Zentom.Core.Entities
{
    public class EconomicActivity
        : Entity
    {
        #region Fields

        private string name;

        private string code;

        private bool isSubjectToTax;

        private sbyte? tributaryCategory;

        private EconomicActivity parent;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual bool IsSubjectToTax
        {
            get { return isSubjectToTax; }
            set { isSubjectToTax = value; }
        }

        public virtual sbyte? TributaryCategory
        {
            get { return tributaryCategory; }
            set { tributaryCategory = value; }
        }

        public virtual EconomicActivity Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        #endregion

        public EconomicActivity()
        {
        }

        public EconomicActivity(string name, string code, bool isSubjectToTax, sbyte? tributaryCategory, EconomicActivity parent)
        {
            this.name = name;
            this.code = code;
            this.isSubjectToTax = isSubjectToTax;
            this.tributaryCategory = tributaryCategory;
            this.parent = parent;
        }
    }
}