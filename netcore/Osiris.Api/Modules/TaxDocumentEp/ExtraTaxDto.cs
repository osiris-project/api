﻿using System;
using System.Collections.Generic;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class ExtraTaxDto
    {
        public Guid SelectedSpecificTax { get; set; }
        
        public IDictionary<string, string> SpecificTaxList { get; set; }
    }
}
