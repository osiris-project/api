﻿using System;
using System.Linq;
using log4net;
using Nancy;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Security;
using Zentom.Core.Services;

namespace Zentom.Api.Modules.FileProcessorEp
{
    public class FileProcessorModule
        : TenantApi
    {
        private readonly ILog log = LogManager.GetLogger(typeof (FileProcessorModule));

        private readonly IBulkImportTaxDocumentService bulkImportSvc;

        public FileProcessorModule(ISessionFactory factory, IBulkImportTaxDocumentService bulkImportSvc)
            : base("file-processors", factory)
        {
            this.bulkImportSvc = bulkImportSvc;
            this.Post(BasePath + "/bulk-import-tax-documents/", BulkImportTaxDocuments);
        }

        [Permission(RequiresLogin = true)]
        public object BulkImportTaxDocuments()
        {
            if (!Request.Files.Any())
            {
                return Negotiate.WithStatusCode(HttpStatusCode.BadRequest);
            }

            var file = Request.Files.FirstOrDefault();

            if (file == null)
            {
                return Negotiate.WithStatusCode(HttpStatusCode.BadRequest);
            }

            try
            {
                var list = bulkImportSvc.Process(file.Value, file.ContentType);
                return Negotiate.WithOkAndModel(new
                {
                    list,
                    errors = bulkImportSvc.ErrorList
                });
            }
            catch (Exception ex)
            {
                log.Error("Failed to process the uploaded bulk file.", ex);
            }

            return Negotiate.WithStatusCode(HttpStatusCode.BadRequest).WithModel(new
            {
                error = I18n.Error.BulkImportFailed
            });
        }
    }
}