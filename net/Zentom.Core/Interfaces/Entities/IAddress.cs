﻿using System;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IAddress : IUnique<long>, IVersioned, IRowGuid
    {
        string StreetName { get; set; }

        int StreetNumber { get; set; }

        bool IsMain { get; set; }

        string PostalCode { get; set; }

        Nullable<int> Floor { get; set; }

        ICommune Commune { get; set; }
    }
}
