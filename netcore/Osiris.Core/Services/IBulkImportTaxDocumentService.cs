﻿using System.Collections.Generic;
using System.IO;

namespace Osiris.Core.Services {
    /// <summary>
    /// Service to import tax documents from a datasheet file.
    /// </summary>
    public interface IBulkImportTaxDocumentService : IService {
        /// <summary>
        /// Process a file to import the tax document data.
        /// </summary>
        /// <param name="filePath">The path to the file.</param>
        /// <returns>The operation result containing the rows to import and, 
        /// if there were errors in the rows, a list of messages with the failing rows.</returns>
        IOperationResult<IList<IBulkImportRow>, string> Process (string filePath);

        /// <summary>
        /// Process a file to import the tax document data.
        /// </summary>
        /// <param name="fileInfo">The file information of the file to import.</param>
        /// <returns>The operation result containing the rows to import and, 
        /// if there were errors in the rows, a list of messages with the failing rows.</returns>
        IOperationResult<IList<IBulkImportRow>, string> Process (FileInfo fileInfo);

        /// <summary>
        /// Process a file to import the tax document data.
        /// </summary>
        /// <param name="stream">The stream of data to import.</param>
        /// <returns>The operation result containing the rows to import and, 
        /// if there were errors in the rows, a list of messages with the failing rows.</returns>
        IOperationResult<IList<IBulkImportRow>, string> Process (Stream stream);
    }
}
