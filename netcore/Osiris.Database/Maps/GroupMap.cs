﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class GroupMap
        : ClassMap<Group>
    {
        public GroupMap()
        {
            Schema("organization");
            Table("group");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            HasOne(i => i.Headquarters)
                .Cascade.SaveUpdate()
                .Fetch.Join();

            Map(i => i.Name).Length(100).UniqueKey("group_un");

            HasMany(i => i.Tenants)
                .KeyColumn("group_id")
                .Cascade.AllDeleteOrphan()
                .Inverse();
        }
    }
}
