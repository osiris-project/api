﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class ReceivedTaxDocumentReferenceMap
        : ClassMap<ReceivedTaxDocumentReference>
    {
        public ReceivedTaxDocumentReferenceMap()
        {
            Schema("taxation");
            Table("received_taxdoc_reference");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Parent, "parent_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("received_taxdoc_reference_un");

            References(i => i.ReferencedDocument, "reference_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("received_taxdoc_reference_un");

            Map(i => i.ReferenceComment).Length(90);
            Map(i => i.ReferenceDate);
            Map(i => i.ReferenceReason).Length(15);
        }
    }
}
