﻿using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public class PaymentType
        : Entity
    {
        #region Fields

        private string _code;

        private string _description;

        private ISet<Payment> _payments;

        private ISet<TaxDocument.TaxDocument> _taxDocuments;

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual ISet<Payment> Payments
        {
            get { return _payments; }
            set { _payments = value; }
        }

        public virtual ISet<TaxDocument.TaxDocument> TaxDocuments
        {
            get { return _taxDocuments; }
            set { _taxDocuments = value; }
        }

        #endregion

        public PaymentType()
        {
            _payments = new HashSet<Payment>();
            _taxDocuments = new HashSet<TaxDocument.TaxDocument>();
        }

        public PaymentType(string code, string description, ISet<Payment> payments, ISet<TaxDocument.TaxDocument> taxDocuments)
            : this()
        {
            _code = code;
            _description = description;
            if (payments != null) _payments = payments;
            if (taxDocuments != null) _taxDocuments = taxDocuments;
        }
    }
}
