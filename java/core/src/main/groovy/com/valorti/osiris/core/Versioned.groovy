/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import java.time.Instant

/**
 * Adds a version to the object.
 *
 * @author Nicolas Mancilla Vergara <nmancilla@valorti.com>
 * @since 0.0.1
 */
interface Versioned {
    /**
     * The object version
     * @since 0.0.1
     */
    Instant version
}
