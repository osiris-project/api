﻿using System.Collections.Generic;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api.Modules.RoleEp {
    public class RoleModule : CRUDLModule<Command, RoleCommand, RoleCommand> {
        private readonly ILogger log;

        public RoleModule (ISessionFactory factory) : base ("roles", factory) {
            log = LoggerFactory.GetLogger (GetType ());
            log.Debug ("RoleModule CTOR.");
            this.Put<PermissionCommand> (BasePath + "/{roleId:guid}/permissions", SetPermissions);
        }

        #region Overrides of CRUDModule<RoleDto>

        [Permission (Permission = "role-read")]
        public override Negotiator Read (Command cmd) {
            log.Debug ("Trying to locate role '{0}'.", cmd.Id);
            var role = db.QueryOver<Role> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant && i.Name != Constants.ADMIN_ROLE_NAME)
                .Take (1)
                .SingleOrDefault ();

            if (role == null) {
                return Negotiate.WithNotFound (I18n.Text.Role, cmd.Id);
            }

            var permissions = db.QueryOver<RolePermission> ()
                .Where (i => i.Role == role)
                .JoinQueryOver (i => i.Permission)
                .List ();

            var dto = new {
                id = role.Id,
                name = role.Name,
                displayName = role.DisplayName,
                description = role.Description,
                permissions = permissions.Select (i => new {
                id = i.Permission.Id,
                name = i.Permission.Name,
                description = i.Permission.Description
                })
            };

            return Negotiate.WithOkAndModel (dto);
        }

        [Permission (Permission = "role-list")]
        public override Negotiator List (dynamic args) {
            log.Debug ("Entering ListRoles.");
            var bound = this.BindTo (new RoleQuery ());
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var query = db.QueryOver<Role> ();

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike (bound.Name);
            } else if (!string.IsNullOrWhiteSpace (bound.DisplayName)) {
                query.WhereRestrictionOn (i => i.DisplayName)
                    .IsInsensitiveLike (bound.DisplayName);
            }

            var roles = query
                .Where (i => i.Tenant == currentTenant && i.Name != Constants.ADMIN_ROLE_NAME)
                .ProjectInto<Role, RoleDto> ()
                .PaginateResults<Role, RoleDto> (page, rpp)
                .Items;
            return roles == null ? Negotiate.WithOk () : Negotiate.WithOkAndModel (roles);
        }

        [Permission (Permission = "role-create")]
        public override Negotiator Create (RoleCommand cmd, dynamic args) {
            log.Debug ("Permissions: {0}", args.permissions);
            var exists = db.Exists<Role> (i => i.Name == cmd.Name && i.Tenant == currentTenant);
            if (exists) {
                return Negotiate.WithExistingItem (I18n.Text.Role, "name");
            }
            var role = new Role {
                Name = cmd.Name,
                DisplayName = cmd.DisplayName,
                Description = cmd.Description,
                Tenant = currentTenant
            };

            if (cmd.Permissions.Any ()) {
                foreach (var rolePermission in cmd.Permissions.Select (id => new RolePermission (role, db.Load<Permission> (id)))) {
                    role.RolePermissions.Add (rolePermission);
                }
            }

            db.Save (role);
            return Negotiate.WithCreated (Context.GetResourceUrl (role.Id), role.Id);
        }

        [Permission (Permission = "role-update")]
        public override Negotiator Update (RoleCommand cmd) {
            var entity = db.QueryOver<Role> ()
                .Where (i => (i.Id == cmd.Id) && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithNotFound (cmd.Id, I18n.Error.RoleNotFound);
            }

            if (entity.Name != cmd.Name) {
                var found = db.QueryOver<Role> ()
                    .Where (i => i.Name == cmd.Name && i.Tenant == currentTenant)
                    .Take (1)
                    .SingleOrDefault ();

                if (found != null) {
                    return Negotiate.WithDuplicateItem ("duplicate", "name");
                }
            }
            entity.Name = cmd.Name;
            entity.DisplayName = cmd.DisplayName;
            entity.Description = cmd.Description;

            if (cmd.Permissions.Any ()) {
                var index = entity.RolePermissions.Count - 1;

                for (; index >= 0; index--) {
                    var item = entity.RolePermissions.ElementAt (index);
                    log.Debug ("Role.Id = {0}, Permission.Id = {1}", entity.Id, item.Permission.Id);
                    if (cmd.Permissions.Any (i => i == item.Permission.Id)) {
                        cmd.Permissions.Remove (item.Permission.Id);
                    } else {
                        entity.RolePermissions.Remove (item);
                    }
                }

                foreach (var item in cmd.Permissions.Select (id => new RolePermission (entity, db.Load<Permission> (id)))) {
                    entity.RolePermissions.Add (item);
                }
            } else {
                entity.RolePermissions.Clear ();
            }

            return Negotiate.WithNoContent ();
        }

        [Permission (Permission = "role-delete")]
        public override Negotiator Remove (Command cmd) {
            var role = db.QueryOver<Role> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();
            if (role == null)
                return Negotiate.RespondWithValidationFailure (string.Format (I18n.Error.ProductNotFound, cmd.Id));

            db.Delete (role);
            return Negotiate.WithStatusCode (HttpStatusCode.OK);
        }

        #endregion

        #region Permission Management

        [Permission (Permission = "role-update")]
        public Negotiator SetPermissions (PermissionCommand cmd) {
            log.Info ("Adding permissions to role '{0}'.", cmd.RoleId);
            var entity = db.QueryOver<Role> ()
                .Where (i => i.Id == cmd.RoleId && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                log.Debug ("Role {0} not found.", cmd.RoleId);
                return Negotiate.WithNotFound (cmd.RoleId, I18n.Error.RoleNotFound);
            }

            entity.AddPermissions (cmd.Permissions.Any () ?
                cmd.Permissions.Select (i => db.Load<Permission> (i)).ToList () :
                new List<Permission> ());
            return Negotiate.WithNoContent ();
        }

        #endregion
    }
}
