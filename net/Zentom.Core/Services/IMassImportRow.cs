﻿namespace Zentom.Core.Services
{
    public interface IBulkImportRow
    {
        string Gloss { get; }

        double NetAmount { get; }
    }
}