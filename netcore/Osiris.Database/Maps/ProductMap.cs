﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class ProductMap
        : SubclassMap<Product>
    {
        public ProductMap()
        {
            Schema("organization");
            Table("product");
            Abstract();
        }
    }
}
