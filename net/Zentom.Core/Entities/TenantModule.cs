﻿using CsQuery.Utility;

namespace Zentom.Core.Entities
{
    public class TenantModule
        : TenantEntity
    {
        #region Fields

        private Module module;

        private bool isEnabled;

        private string settings;

        private string version;

        #endregion

        #region Properties

        public virtual Module Module
        {
            get { return module; }
            set { module = value; }
        }

        public virtual bool IsEnabled
        {
            get { return isEnabled; }
            set { isEnabled = value; }
        }

        public virtual string Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        public virtual string Version
        {
            get { return version; }
            set { version = value; }
        }

        #endregion

        #region Ctors.

        public TenantModule()
        {
        }

        public TenantModule(Tenant tenant, Module module, bool isEnabled, string settings = "", string version = "")
            : base(tenant)
        {
            this.isEnabled = isEnabled;
            this.settings = settings;
            this.version = version;
            this.module = module;
        }

        #endregion
    }
}