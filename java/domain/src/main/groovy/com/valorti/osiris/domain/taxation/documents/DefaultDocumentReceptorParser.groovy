/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.dobelik.utils.nationalId.NationalIdentity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.taxation.documents.Address
import com.valorti.osiris.core.taxation.documents.DocumentReceiver
import com.valorti.osiris.core.taxation.documents.EnterpriseData
import groovy.util.slurpersupport.GPathResult

/**
 * Parses the <Receptor> section of a taxation document.
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DefaultDocumentReceptorParser
    extends AbstractParser<DocumentReceiver> {

    private final NationalIdentity identity

    protected DefaultDocumentReceptorParser(NationalIdentity identity) {
        this.identity = identity
    }

    DocumentReceiver parse(GPathResult path) {
        def taxId = path.RUTRecep.toString().trim()
        if (!this.identity.isValid(taxId)) {
            this.errors << ValidationBuilder.build('', 'El rut del emisor no puede estar vacío')
        }
        taxId = this.identity.clean(taxId)
        def name = path.RznSocRecep.toString().trim()
        if (!name) {
            this.errors << ValidationBuilder.build('', 'La razón social del emisor no es válida')
        }
        def commercialActivity = path.GiroRecep.toString().trim()
        def email = path.CorreoRecep.toString().trim()
        def legalAddress = new Address(
            path.DirRecep?.text()?.trim() as String,
            path.CmnaRecep?.text()?.trim() as String,
            path.CiudadRecep?.text()?.trim() as String)

        def postalAddress = buildPostalAddress(path)
        def receiver = new DocumentReceiver(
            new EnterpriseData(taxId, name, commercialActivity, legalAddress),
            postalAddress, '', email
        )
        return receiver
    }

    private static Address buildPostalAddress(GPathResult path) {
        def line = path.DirPostal?.text()?.trim() as String
        def commune = path.CmnaPostal?.text()?.trim() as String
        def city = path.CiudadPostal?.text()?.trim() as String
        if (line && commune) {
            return new Address(line, commune, city)
        }
        return null
    }
}
