﻿using System;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api.Modules.ProductEp {
    public class ProductModule : CRUDLModule<Command, ProductCommand, ProductCommand> {
        private readonly ILogger log = LoggerFactory.GetLogger (typeof (ProductModule));

        public ProductModule (ISessionFactory factory) : base ("products", factory) {
            this.Get (BasePath + "/query", ProductBaseQuery);
            this.Post<ProductCommand> (BasePath + "/force-exception", ForceUniqueKeyException);
        }

        [Permission (Permission = "product-read")]
        public override Negotiator Read (Command cmd) {
            log.Debug ("Trying to locate product '{0}'.", cmd.Id);
            var prod = db.QueryOver<Product> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .ProjectToObject<Product, ProductDto> ();

            if (prod == null)
                return Negotiate.RespondWithValidationFailure (
                    string.Format (I18n.Error.ProductNotFound, cmd.Id));

            return Negotiate.WithModel (prod)
                .WithStatusCode (HttpStatusCode.OK);
        }

        [Permission (Permission = "product-list")]
        public override Negotiator List (dynamic args) {
            log.Debug ("Entering GetProducts.");
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var bound = this.BindTo (new ProductQuery ());
            var query = db.QueryOver<Product> ();

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike (bound.Name);
            }

            var items = query
                .Where (i => i.Tenant == currentTenant)
                .ProjectInto<Product, ProductDto> ()
                .PaginateResults<Product, ProductDto> (page, rpp);
            log.Debug ("Leaving GetProducts.");
            return Negotiate.WithOkAndList (items, Request.Url);
        }

        [Permission (Permission = "product-create")]
        public override Negotiator Create (ProductCommand cmd, dynamic args) {
            var exists = db.Exists<Product> (i =>
                i.Name == cmd.Name && i.Tenant == currentTenant);
            if (exists)
                return
            Negotiate.RespondWithValidationFailure (string.Format (I18n.Error.ExistingItemName, I18n.Text.Product));
            var product = new Product (currentTenant, cmd.Name, cmd.UnitPrice, cmd.IsDiscontinued);
            db.Save (product);
            return Negotiate.WithCreated (Context.GetResourceUrl (product.Id), product.Id);
        }

        [Permission (Permission = "product-delete")]
        public override Negotiator Remove (Command cmd) {
            var found = db.QueryOver<Product> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();
            if (found?.Id == Guid.Empty)
                return Negotiate.WithNotFound ();
            db.Delete (found);
            return Negotiate.WithStatusCode (HttpStatusCode.OK);
        }

        [Permission (Permission = "product-update")]
        public override Negotiator Update (ProductCommand product) {
            var found = db.QueryOver<Product> ()
                .Where (i => i.Id == product.Id && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();
            if (found?.Id == Guid.Empty)
                return Negotiate.WithNotFound ();
            db.Update (product);
            return Negotiate.WithStatusCode (HttpStatusCode.OK);
        }

        [Permission (Permission = "product-read")]
        public Negotiator ProductBaseQuery (dynamic args) {
            var bound = this.BindTo (new ProductQuery ());
            var query = db.QueryOver<ProductBase> ()
                .Where (i => i.Tenant == currentTenant);

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike ("%" + bound.Name + "%");
            }

            var items = query.ProjectInto<ProductBase, ProductDto> ()
                .PaginateResults<ProductBase, ProductDto> ();
            return Negotiate.WithOkAndList (items, Request.Url);
        }

        [Permission (Permission = "product-create")]
        public Negotiator ForceUniqueKeyException (ProductCommand cmd) {
            var product = new Product (currentTenant, cmd.Name, cmd.UnitPrice, cmd.IsDiscontinued);
            db.Save (product);
            return Negotiate.WithCreated (Context.GetResourceUrl (product.Id), product.Id);
        }
    }
}
