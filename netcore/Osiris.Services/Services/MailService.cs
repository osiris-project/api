﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Slf4Net;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Osiris.Services
{
    internal class MailService
    {
        private static readonly ILogger log = LoggerFactory.GetLogger(typeof (MailService));

        private const string NoReplyEmail = "no-reply@valorti.com";

        private readonly SendGridClient sendgrid = new SendGridClient(Environment.GetEnvironmentVariable("SENDGRID_APIKEY"));

        public bool SendResetLink(string toEmail, string firstName, string resetUrl, string ipv4, string supportEmail = "support@valorti.com")
        {
            log.Debug("Entering SendResetLink.");
            const string subject = "[Osiris - Password Reset]";
            var templates = GetMailTemplates(firstName, resetUrl, ipv4, "reset-password", supportEmail);

            try
            {
                var mail = new SendGridMessage()
                {
                    Subject = subject,
                    From = new EmailAddress(NoReplyEmail, "Osiris")
                };
                mail.AddTo(new EmailAddress(toEmail, firstName));
                mail.AddContent("text/plain", templates[0]);
                mail.AddContent("text/html", templates[1]);
                ParseResponse(mail);
            }
            catch (Exception ex)
            {
                log.Error("Failed to send reset password mail.", ex);
            }
            
            log.Debug("Leaving SendResetLink.");
            return true;
        }

        public bool SendChangeSuccess(string toEmail, string firstName, string resetUrl, string ipv4, string supportEmail = "support@valorti.com")
        {
            log.Debug("Entering SendResetLink.");
            const string subject = "[Osiris - Password Change]";
            var templates = GetMailTemplates(firstName, resetUrl, ipv4, "password-reset", supportEmail);

            try
            {
                var mail = new SendGridMessage()
                {
                    Subject = subject,
                    From = new EmailAddress(NoReplyEmail, "Osiris")
                };
                mail.AddTo(new EmailAddress(toEmail, firstName));
                mail.AddContent("text/plain", templates[0]);
                mail.AddContent("text/html", templates[1]);
                ParseResponse(mail);
            }
            catch (Exception ex)
            {
                log.Error("Failed to send reset password mail.", ex);
            }

            log.Debug("Leaving SendResetLink.");
            return true;
        }

        private async void ParseResponse(SendGridMessage msg)
        {
            var result = await sendgrid.SendEmailAsync(msg);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                log.Info("EmailAddress sent.");
            }
            else {
                log.Error("Failed to send email: {0}", result.StatusCode);
            }
        }
       
        private static string[] GetMailTemplates(string firstName, string resetUrl, string ipv4,
            string templatePrefix = "reset-password", string supportEmail = "support@valorti.com")
        {
            log.Debug("Current Thread: {0}", Thread.CurrentThread.ManagedThreadId);
            log.Debug("Current IsoCode: {0}", Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
            var templates = new[] { resetUrl, resetUrl };
            var isocode = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            var folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "templates");
            // HTML template
            var templateName = Path.Combine(folderPath, templatePrefix + "_" + isocode + ".html");
            string result;

            if (!File.Exists(templateName))
            {
                log.Error("The HTML reset password template wasn't found at '{0}'.", templateName);
            }
            else
            {
                result = File.ReadAllText(templateName);
                result =
                    result.Replace("${user}", firstName)
                        .Replace("${resetUrl}", resetUrl)
                        .Replace("${ip}", ipv4)
                        .Replace("${support}", supportEmail);
                templates[1] = result;
            }


            // TXT template
            templateName = Path.Combine(folderPath, templatePrefix + "_" + isocode + ".txt");

            if (!File.Exists(templateName))
            {
                log.Error("The raw reset password template wasn't found at '{0}'.", templateName);
            }
            else
            {
                result = File.ReadAllText(templateName);
                result =
                    result.Replace("${user}", firstName)
                        .Replace("${resetUrl}", resetUrl)
                        .Replace("${ip}", ipv4)
                        .Replace("${support}", supportEmail);
                templates[0] = result;
            }

            return templates;
        }
    }
}
