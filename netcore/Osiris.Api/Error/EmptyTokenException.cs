﻿using System;

namespace Osiris.Api.Error
{
    public class EmptyTokenException
        : Exception
    {
        public EmptyTokenException()
            : base(I18n.Error.EmptyToken)
        {
        }
    }
}
