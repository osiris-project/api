﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class RoleMap
        : ClassMap<Role>
    {
        public RoleMap()
        {
            Schema("system");
            Table("role");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(100).UniqueKey("role_un");
            Map(i => i.DisplayName).Length(100);
            Map(p => p.Description).Length(300).Default("''").Nullable();

            HasMany(p => p.RolePermissions)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("role_id");

            HasMany(i => i.AccountRoles)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("role_id");

            References(i => i.Tenant)
                .UniqueKey("role_un")
                .Cascade.SaveUpdate();
        }
    }
}
