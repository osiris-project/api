﻿using System;

namespace Osiris.Api.Modules.ReceivedTaxDocumentsEp
{
    public class ReceivedTaxDocumentDto
    {
        public Guid Id { get; set; }

        public long FolioNumber { get; set; }

        public DateTime AccountingDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public DateTime ReceptionDate { get; set; }

        // Totals

        public decimal TotalNetAmount { get; set; }

        public decimal TotalTaxes { get; set; }

        public decimal GrossTotal { get; set; }

        public decimal ExemptAmount { get; set; }

        public string DocTypeCode { get; set; }
        
        public Guid ProviderId { get; set; }

        public string ProviderTaxId { get; set; }

        public string ProviderLegalName { get; set; }

        public Guid CostsCenterId { get; set; }

        public string CostsCenterCode { get; set; }

        public string CostsCenterName { get; set; }

        public bool AcceptedForAccounting { get; set; }
    }
}
