﻿using System.Collections.Generic;
using System.IO;
using Zentom.Core.Interfaces.Integration.TributaryDocuments;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Integration.Artikos.TributaryDocuments
{
    internal abstract class AbstractTributaryDocumentParser<TEntity>
        : ITributaryDocumentParser<TEntity>
    {
        #region Fields

        private bool _disposedValue; // To detect redundant calls

        protected StreamReader StreamReader;

        protected TEntity Entity;

        #endregion

        #region Properties

        public IList<IError> Errors { get; protected set; }

        #endregion

        protected AbstractTributaryDocumentParser()
        {
            Errors = new List<IError>();
        }

        #region ITributaryDocumentParser

        public TEntity Parse(string path)
        {
            StreamReader = new StreamReader(path);
            Setup();
            return Parse();
        }

        #endregion

        /// <summary>
        /// Performs initialization tasks.
        /// </summary>
        protected abstract void Setup();

        protected abstract TEntity Parse();

        #region IDisposable Support
        
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    StreamReader?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                _disposedValue = true;
            }
        }

        // override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AbtractTributaryDocumentParser() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion
    }
}
