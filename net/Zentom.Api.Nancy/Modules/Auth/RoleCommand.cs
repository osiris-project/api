﻿using System;

namespace Zentom.Api.Modules.Auth
{
    public class RoleCommand
    {
        public Guid AccountId { get; set; }

        public Guid TenantId { get; set; }
    }
}