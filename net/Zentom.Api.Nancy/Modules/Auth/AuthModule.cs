﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using log4net;
using Nancy;
using Nancy.Security;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Security;
using Zentom.Core.Services;

namespace Zentom.Api.Modules.Auth
{
    [Permission(Permission = Constants.ANONYMOUS)]
    public class AuthModule
        : ApiModule
    {
        private readonly IAuthenticationService authService;

        private readonly ILog log = LogManager.GetLogger(typeof(AuthModule));

        public AuthModule(IAuthenticationService authService)
        {
            this.authService = authService;
            this.Post<AuthCmd>("/auth", Authenticate);
            this.Post<RecoverPasswordCmd>("/auth/password-reset/", RecoverPassword);
            this.Post<PasswordResetCmd>("/auth/password-reset/{id:guid}/", ResetPassword);
            this.Get<Command>("/auth/password-reset/{id:guid}", ValidateResetStatus);
        }

        public object RecoverPassword(RecoverPasswordCmd cmd)
        {
            log.DebugFormat("Entering RecoverPassword.");
            log.DebugFormat("Command: {0}", cmd);
            authService.RequestResetLink(cmd.Login, cmd.ReturnUri, Context.RemoteAddress());
            if (authService.IsValid) return Negotiate.WithOk();
            log.DebugFormat("The password request failed.");
            return Negotiate.WithValidationFailure(authService.ErrorList);
        }

        public object ResetPassword(PasswordResetCmd cmd)
        {
            return !authService.ChangePassword(cmd.Id, cmd.Password, Context.RemoteAddress())
                ? Negotiate.WithValidationFailure(authService.ErrorList)
                : Negotiate.WithOk();
        }

        public object ValidateResetStatus(Command cmd)
        {
            return !authService.IsResetAvailable(cmd.Id)
                ? Negotiate.WithStatusCode(HttpStatusCode.ExpectationFailed)
                : Negotiate.WithOk();
        }

        /// <summary>
        /// Authenticates an account in the system.
        /// </summary>
        /// <param name="cmd">The authentication data (login and password)</param>
        /// <returns>
        /// An authentication token in the headers (Authorization) and the first tenant (if any) for the account on the body.
        /// If the login and password don't match any account, then an Unauthorized (401) code is returned with a list of
        /// errors (if any).
        /// </returns>
        public object Authenticate(AuthCmd cmd)
        {
            log.DebugFormat("Entering Authenticate.");

            if (Context.CurrentUser.IsAuthenticated())
                return Negotiate.WithStatusCode(HttpStatusCode.OK).WithModel(I18n.Text.AlreadyAuthorized);

            log.DebugFormat("Passed command: {0}", cmd);
            var account = authService.ValidateCredentials(cmd.LoginName, cmd.Password, cmd.EnterpriseTaxId);

            if (account == null || !authService.IsValid)
            {
                // The user does not exist or the password is wrong; either way they are unauthorized:
                return Negotiate.WithStatusCode(HttpStatusCode.Unauthorized)
                    .WithModel(authService.ErrorList)
                    .WithHeader("WWW-Authenticate", "Token realm=\"valorti.com\"");
            }

            var dto = new
            {
                accountId = account.Id,
                loginName = account.Login,
                nationalId = account.Person.NationalId,
                firstName = account.Person.FirstName,
                lastName = account.Person.LastName,
                email = account.Email
            };

            var token = ApiTokenizer.CreateToken(dto, GetDuration(), account.Login, account.Id);
            log.DebugFormat("Leaving Authenticate.");
            return Negotiate.WithHeader("Authorization", token)
                .WithHeader("Access-Control-Expose-Headers", "Authorization")
                .WithStatusCode(HttpStatusCode.OK);
        }

        private static DateTime GetDuration(string tokenDurationFormat = null)
        {
            if (string.IsNullOrWhiteSpace(tokenDurationFormat))
            {
                tokenDurationFormat = ConfigurationManager.AppSettings["token.duration"];
            }
            if (string.IsNullOrWhiteSpace(tokenDurationFormat))
            {
                return DateTime.UtcNow.AddHours(1);
            }

            int duration;
            if (!int.TryParse(Regex.Replace(tokenDurationFormat, @"[^\d]*", "", RegexOptions.Compiled), out duration))
            {
                duration = 1;
            }
            var parsedSuffix = Regex.Replace(tokenDurationFormat, @"[\d]*", "", RegexOptions.Compiled);
            var dt = DateTime.UtcNow;
            switch (parsedSuffix)
            {
                case "y":
                    dt = dt.AddYears(duration);
                    break;
                case "m":
                    dt = dt.AddMinutes(duration);
                    break;
                case "M":
                    dt = dt.AddMonths(duration);
                    break;
                case "h":
                    dt = dt.AddHours(duration);
                    break;
                default:
                    dt = dt.AddDays(duration);
                    break;
            }

            return dt;
        }
    }
}