using LightInject.Interception;
using Slf4Net;

namespace Osiris.Api.Modules {
    /// <summary>
    /// Intercepts the requests to the tenant API to check
    /// if the current logged in user has the required permissions
    /// to execute 
    /// </summary>
    public class TenantApiInterceptor : IInterceptor {
        private readonly ILogger log = LoggerFactory.GetLogger (typeof (TenantApiInterceptor));

        /// <summary>
        /// Intercepts the calls to each verb, checks if the user is
        /// authorized and calls the method, otherwise returns
        /// an unauthorized response.
        /// </summary>
        /// <param name="info">The <see cref="IInvocationInfo" /> implementation.</param>
        /// <returns>The result of executing the method or a unauthorized response
        /// otherwise.</returns>
        public object Invoke (IInvocationInfo info) {
            log.Debug ("Checking permissions to target method '{0}'",
                info.TargetMethod.Name);
            if (info.TargetMethod.IsConstructor) {
                log.Debug ("Intercepted a constructor");
                return info.Proceed ();
            }
            var proxy = info.Proxy;
            if (!proxy is TenantApi) {
                log.Info ("Method '{0}.{1}' is not an instance of TenantApi",
                    info.TargetMethod.Name, info.TargetMethod.DeclaringType.FullName);
                return info.Proceed ();
            }

            var hasAccess = AuthorizationHelper.IsAuthorized (proxy, info.TargetMethod);
            if (hasAccess == null) {
                return info.Proceed ();
            }
            return hasAccess;
        }
    }
}
