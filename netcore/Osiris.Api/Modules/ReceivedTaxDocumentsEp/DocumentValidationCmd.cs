﻿using System;

namespace Osiris.Api.Modules.ReceivedTaxDocumentsEp
{
    public class DocumentValidationCmd
    {
        public DateTime EndDate { get; set; } 

        public DateTime StartDate { get; set; }
    }
}
