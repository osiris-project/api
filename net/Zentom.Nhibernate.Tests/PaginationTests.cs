﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate;
using Zentom.Core.Entities;
using Zentom.Database;

namespace Zentom.Nhibernate.Tests
{
    [TestClass]
    public class PaginationTests
    {
        private static ISession session;

        [TestInitialize]
        public void Init()
        {
            session = NHibernateHelper.SessionFactory.OpenSession();
            session.BeginTransaction();
        }

        [TestCleanup]
        public void Cleanup()
        {
            try
            {
                session.Transaction.Commit();
            }
            catch
            {
                session.Transaction.Rollback();
                throw;
            }
            session.Dispose();
        }

        [TestMethod]
        public void TestAccount()
        {
            
        }

        [TestMethod]
        public void Add100TestServices()
        {
            var employee = session.QueryOver<Employee>()
                .Left.JoinQueryOver(i => i.Person)
                .Where(i => i.NationalId == "188826172")
                .Take(1)
                .SingleOrDefault();

            for (var i = 0; i < 100; i++)
            {
                var service = new Service
                {
                    Tenant = employee.Tenant,
                    IsDiscontinued = false,
                    Name = "Test Service " + i,
                    UnitPrice = 100.0M
                };
                session.Save(service);
            }

        }

        [TestMethod]
        public void Remove100TestServices()
        {
            for (var i = 0; i < 100; i++)
            {
                var i1 = i;
                var service = session.QueryOver<Service>()
                    .Where(j => j.Name == "Test Service " + i1)
                    .Take(1)
                    .SingleOrDefault();
                session.Delete(service);
            }
        }

        [TestMethod]
        public void Add4TestServices()
        {
            var employee = session.QueryOver<Employee>()
                .Left.JoinQueryOver(i => i.Person)
                .Where(i => i.NationalId == "188826172")
                .Take(1)
                .SingleOrDefault();

            for (var i = 0; i < 4; i++)
            {
                var service = new Service
                {
                    Tenant = employee.Tenant,
                    IsDiscontinued = false,
                    Name = "Test Service " + i,
                    UnitPrice = 100.0M * i
                };
                session.Save(service);
            }
        }

        [TestMethod]
        public void Remove4TestServices()
        {
            for (var i = 0; i < 4; i++)
            {
                var i1 = i;
                var service = session.QueryOver<Service>()
                    .Where(j => j.Name == "Test Service " + i1)
                    .Take(1)
                    .SingleOrDefault();
                session.Delete(service);
            }
        }
    }
}
