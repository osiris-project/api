﻿using System;

namespace Osiris.Api.Modules.CommuneEp
{
    public class CommuneDto
    {
        public Guid Id { get; set; }

        public string CommuneName { get; set; }

        public string CityName { get; set; }
    }
}
