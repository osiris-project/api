﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Zentom.Core.Entities;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Core.Services
{
    public interface IReceivedTaxDocumentService
        : IService
    {
        IList<ReceivedTaxDocument> ReceivedTaxDocuments { get; }

        void ReceiveDocument(Stream stream, Tenant t);

        Task ReceiveDocumentAsync(Stream stream, Tenant t);

        Task SaveDocumentAsync(string path);

        void SaveDocument(string path);
    }
}