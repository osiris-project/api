﻿using System;
using Osiris.Api.Modules.EmployeeEp;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.ManagementEp
{
    public class AccountCmd
        : AccountCreateCmd
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalId { get; set; }

        public DateTime? BirthDate { get; set; }

        public Gender Gender { get; set; }
    }
}
