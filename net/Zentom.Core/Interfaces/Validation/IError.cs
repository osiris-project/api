﻿using System;

namespace Zentom.Core.Interfaces.Validation
{
    public interface IError
    {
        string Title { get; }

        string Message { get; }

        Exception Exception { get; }
    }
}
