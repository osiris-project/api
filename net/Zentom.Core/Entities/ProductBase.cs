﻿namespace Zentom.Core.Entities
{
    public abstract class ProductBase
        : TenantEntity
    {
        #region Fields

        private string name;

        private decimal unitPrice;

        private bool isDiscontinued;

        #endregion
        
        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual bool IsDiscontinued
        {
            get { return isDiscontinued; }
            set { isDiscontinued = value; }
        }

        #endregion

        protected ProductBase() { }

        protected ProductBase(Tenant tenant, string name, decimal unitPrice, bool isDiscontinued)
            : base(tenant)
        {
            this.name = name;
            this.unitPrice = unitPrice;
            this.isDiscontinued = isDiscontinued;
        }

        public override string ToString()
        {
            return $"Name: {name}, UnitPrice: {unitPrice}, IsDiscontinued: {isDiscontinued}";
        }
    }
}
