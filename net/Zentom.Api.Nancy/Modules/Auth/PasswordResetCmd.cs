﻿using System;

namespace Zentom.Api.Modules.Auth
{
    public class PasswordResetCmd
    {
        public Guid Id { get; set; } 

        public string Password { get; set; }
    }
}