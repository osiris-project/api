using Osiris.Core.Models;
using Osiris.Core.Entities;
using System;

namespace Osiris.Core.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Validates that the raw password matches the user
        /// specified by the email.
        /// </summary>
        /// <param name="model">The password check model containing the 
        /// user's email and raw password to check.</param>
        /// <returns>True if the operation was successful or false and
        /// a list of errors.</returns>
        IOperationResult<bool, string> CheckPassword(PasswordCheck model);

        /// <summary>
        /// Validates that the password matches the hash.
        /// </summary>
        /// <param name="model"><see cref="PasswordValidation"></param>
        /// <returns>true if the password is valid or false and a list of errors if not.</returns>
        IOperationResult<bool, string> ValidatePassword(PasswordValidation model);

        /// <summary>
        /// Changes the password and updates the account values.
        /// </summary>
        /// <param name="model"><see cref="PasswordChange" />
        /// <returns>true on success, false and a list of errors on failure.</returns>
        IOperationResult<bool, string> ChangePassword(PasswordChange model);

        /// <summary>
        /// Validates the provided account.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IOperationResult<bool, string> ValidateCredentials(AccountValidation model);

        void HashPassword(Account account, string password);

        Account GetAccount(string loginName, string tenantTaxId);

        bool RequestResetLink(string loginName, string baseUrl, string ipv4, string ipv6 = "",
            DateTime? expirationDate = null);

        bool ChangePassword(Guid guid, string newPassword, string ipv4);

        bool IsResetAvailable(Guid id);
    }
}
