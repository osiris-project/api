﻿using System;
using System.Globalization;
using Dobelik.Utils.Identity;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Interfaces.Integration.TributaryDocuments;
using Osiris.Integration.Artikos.TributaryDocuments.Artikos;

namespace Osiris.Integration.Artikos
{
    /// <summary>
    /// Factory to create an instance of a tributary document formatter
    /// for a specific type of entity.
    /// </summary>
    public class ArtikosFactory
    { 
        private readonly INationalIdentity _idTools;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Builds the factory using the es-CL culture info to retrieve
        /// the required INationalIdentity.
        /// </summary>
        public ArtikosFactory() {
            var locator = IdentityLocator.GetInstance();
            _idTools = locator.Get(new CultureInfo("es-CL", false));
        }

        /// <summary>
        /// Builds a factory providing the INationalIdentity instance to
        /// validate the date.
        /// </summary>
        /// <param name="idTools"></param>
        public ArtikosFactory(INationalIdentity idTools)
        {
            _idTools = idTools;
        }

        public ITributaryDocumentFormatter<TaxDocument> Resolve(string doctype)
        {
            if (string.IsNullOrWhiteSpace(doctype))
                throw new ArgumentNullException(nameof(doctype));

            switch (doctype)
            {
                case "33":
                    return new Bill(_idTools);

                case "34":
                    return new ExemptBill(_idTools);

                case "56":
                    return new DebitNote(_idTools);

                case "61":
                    return new CreditNote(_idTools);

                default:
                    throw new NotImplementedException("There is no implementation for the document type code '" + doctype + "' yet.");
            }
        }

        public static string GetRepositoryFolder(string folderName, string documentType, bool isTesting = false)
        {
            var folder = "PRODUCCION";
            if (isTesting)
                folder = "TESTING";
            string subFolder;

            switch (documentType)
            {
                case "LCO":
                    subFolder = "LibroCompra";
                    break;
                case "LVE":
                    subFolder = "LibroVenta";
                    break;
                default:
                    subFolder = "DTE";
                    break;
            }

            return $@"\STCRepositorio\{folder}\{folderName}\{subFolder}\Enviar";
        }

        public static string BuildFileName(string taxId, long folio, string doctype)
        {
            return string.Concat(
                taxId.Replace(".", ""), "_",
                doctype, "_",
                folio, "_",
                DateTime.Now.ToString(string.Concat(DATE_FORMAT, "_HH-mm-ss")),
                ".txt");
        }
    }
}
