﻿using System;
using System.Collections.Generic;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.EnterpriseEp
{
    public class EnterpriseCmd
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }

        public string DisplayName { get; set; }

        public string BankAccountNumber { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public EnterpriseRelationship Relationship { get; set; }

        public IEnumerable<EnterpriseAddressCmd> Addresses { get; set; }

        public IEnumerable<Guid> EconomicActivities { get; set; }

        public IEnumerable<string> ComercialActivities { get; set; }
    }

    public class EnterpriseAddressCmd
        : Command
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public Guid CommuneId { get; set; }

        public IEnumerable<Guid> AddressTypes { get; set; }
    }
}