﻿using Nancy;
using NHibernate;
using Zentom.Api.Extensions.Nancy;

namespace Zentom.Api.Modules
{
    public abstract class CRUDLModule<TRead, TCreate, TUpdate>
        : TenantApi
    {
        protected CRUDLModule(string resourceName, ISessionFactory factory)
            : base(resourceName, factory)
        {
            this.Get<TRead>(BasePath + "/{id:guid}", Read);
            this.Get(BasePath, List);

            this.Post<TCreate>(BasePath, Create);
            this.Put<TUpdate>(BasePath + "/{id:guid}", Update);

            this.Delete<Command>(BasePath + "/{id:guid}", Remove);
            
        }

        public virtual object Read(TRead cmd)
        {
            return Negotiate.WithStatusCode(HttpStatusCode.MethodNotAllowed);
        }

        public virtual object List(dynamic args)
        {
            return Negotiate.WithStatusCode(HttpStatusCode.MethodNotAllowed);
        }

        #region Post/Put/Delete

        public virtual object Create(TCreate cmd, dynamic args)
        {
            return Negotiate.WithStatusCode(HttpStatusCode.MethodNotAllowed);
        }

        public virtual object Update(TUpdate cmd)
        {
            return Negotiate.WithStatusCode(HttpStatusCode.MethodNotAllowed);
        }

        public virtual object Remove(Command id)
        {
            return Negotiate.WithStatusCode(HttpStatusCode.MethodNotAllowed);
        }

        #endregion
    }
}