﻿using System;
using System.Linq;
using log4net;
using Zentom.Core.Entities;

namespace Zentom.Api.Security
{
    public static class IdentityHelper
    {
        private static readonly ILog Log =  LogManager.GetLogger(typeof(IdentityHelper));

        public static bool IsInRole(this AuthorizedAccount account, string role)
        {
            Log.DebugFormat("Entering IsInRole.");
            if (account == null)
                return false;
            if (string.IsNullOrWhiteSpace(role))
                return false;
            var result = false;

            try
            {
                result = account.AccountRoles.Any(i => i.Role.Name == role);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to validate role.", ex);
            }
            Log.InfoFormat("Checking if account '{0}' is in role '{1}': {2}", account.Id, role, result);
            Log.DebugFormat("Leaving IsInRole.");
            return result;
        }

        public static bool RoleHasPermission(this AuthorizedAccount account, string role, string permission)
        {
            if (permission == Constants.ANONYMOUS)
                return true;
            if (account.IsAdmin())
                return true;
            Log.DebugFormat("Checking if role '{0}' has permission '{1}'.", role, permission);
            if (string.IsNullOrWhiteSpace(role) || string.IsNullOrWhiteSpace(permission))
            {
                Log.Info("The role or the permission were empty.");
                return false;
            }

            try
            {
                if (!account.IsInRole(role))
                    return false;
                return account.AccountRoles.Where(i => i.Role.Name == role)
                    .Any(i => i.Role.RolePermissions.Any(j => j.Permission.Name == permission));
            }
            catch (Exception ex)
            {
                Log.Error("Failed to validate role permission.", ex);
            }
            return false;
        }

        public static bool HasPermission(this AuthorizedAccount account, string permission)
        {
            Log.Debug("Entering HasPermission.");
            if (permission == Constants.ANONYMOUS)
                return true;
            if (account.IsAdmin())
                return true;
            Log.DebugFormat("Checking if account has permission '{0}'.", permission);
            if (string.IsNullOrWhiteSpace(permission))
            {
                Log.Info("The permission was empty.");
                return false;
            }

            try
            {
                var result = account.AccountRoles.Any(i =>
                    i.Role.RolePermissions.Any(j => j.Permission.Name == permission));
                Log.DebugFormat("Had permission: {0}", result);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to validate permission.", ex);
            }
            return false;
        }

        public static bool IsAdmin(this AuthorizedAccount account)
        {
            return account.IsInRole(Constants.ADMIN_ROLE_NAME);
        }
    }
}