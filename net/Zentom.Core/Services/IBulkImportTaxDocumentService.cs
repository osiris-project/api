﻿using System.Collections.Generic;
using System.IO;

namespace Zentom.Core.Services
{
    public interface IBulkImportTaxDocumentService
        : IService
    {
        IList<IBulkImportRow> Process(string filePath);

        IList<IBulkImportRow> Process(FileInfo fileInfo);

        IList<IBulkImportRow> Process(Stream stream, string contentType);
    }
}