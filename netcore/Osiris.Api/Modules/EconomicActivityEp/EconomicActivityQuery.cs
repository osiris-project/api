﻿namespace Osiris.Api.Modules.EconomicActivityEp
{
    public class EconomicActivityQuery
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public bool EconomicGroupsOnly { get; set; }

        public bool OnlyExemptSectors { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Code: {Code}, EconomicGroupsOnly: {EconomicGroupsOnly}, OnlyExemptSectors: {OnlyExemptSectors}";
        }
    }
}
