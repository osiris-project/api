﻿using System;

namespace Osiris.Core.Entities
{
    public class Payment
        : Entity
    {
        #region Fields

        private decimal _amount;

        private DateTime _expireDate;

        private PaymentType _paymentType;

        private TaxDocument.TaxDocument _taxDocument;

#endregion

        #region Properties

        public virtual decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public virtual DateTime ExpireDate
        {
            get { return _expireDate; }
            set { _expireDate = value; }
        }

        public virtual PaymentType PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        public virtual TaxDocument.TaxDocument TaxDocument
        {
            get { return _taxDocument; }
            set { _taxDocument = value; }
        }

        #endregion

        public Payment()
        { }

        public Payment(decimal amount, DateTime expireDate, PaymentType paymentType, TaxDocument.TaxDocument taxDocument)
        {
            _amount = amount;
            _expireDate = expireDate;
            _paymentType = paymentType;
            _taxDocument = taxDocument;
        }
    }
}
