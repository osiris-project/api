﻿namespace Osiris.Core.Entities
{
    public class EnterpriseProduct
        : Entity
    {
        #region Properties

        public virtual Enterprise Enterprise { get; set; }

        public virtual Product Product { get; set; }

        #endregion

        public EnterpriseProduct() {}

        #region Object

        //public override bool Equals(object obj)
        //{
        //    var t = obj as EnterpriseProduct;

        //    if (ReferenceEquals(null, t))
        //        return false;

        //    if (ReferenceEquals(this, t))
        //        return true;

        //    return (Enterprise.Equals(t.Enterprise) &&
        //        Product.Equals(t.Product));
        //}

        //public override int GetHashCode()
        //{
        //    unchecked
        //    {
        //        int hash = GetType().GetHashCode();
        //        hash = (hash * 31) + (!ReferenceEquals(null, Enterprise) ? Enterprise.Id.GetHashCode() : 0);
        //        hash = (hash * 31) + (!ReferenceEquals(null, Product) ? Product.Id.GetHashCode() : 0);

        //        return hash;
        //    }
        //}

        #endregion
    }
}
