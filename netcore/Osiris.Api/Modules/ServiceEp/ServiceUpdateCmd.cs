﻿namespace Osiris.Api.Modules.ServiceEp
{
    public class ServiceUpdateCmd
        : Command
    {
        public string Name { get; set; }

        public decimal UnitPrice { get; set; }

        public bool IsDiscontinued { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, UnitPrice: {UnitPrice}, IsDiscontinued: {IsDiscontinued}";
        }
    }
}
