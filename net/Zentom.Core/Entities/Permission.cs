﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class Permission
        : Entity
    {
        #region Fields

        private string _name;

        private string _description;

        private ISet<RolePermission> _rolePermissions;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual ISet<RolePermission> RolePermissions
        {
            get { return _rolePermissions; }
            set { _rolePermissions = value; }
        }

        #endregion

        public Permission()
        {
            _rolePermissions = new HashSet<RolePermission>();
        }

        public Permission(string name, string description, ISet<RolePermission> rolePermissions)
            : this()
        {
            _name = name;
            _description = description;
            if (rolePermissions != null) _rolePermissions = rolePermissions;
        }

        #region Role

        public virtual void AddRole(RolePermission role)
        {
            if (RolePermissions.Contains(role))
                return;

            role.Permission = this;
            RolePermissions.Add(role);
        }

        public virtual void RemoveRole(RolePermission role)
        {
            if (!RolePermissions.Contains(role))
                return;

            role.Permission = null;
            role.Role = null;
            RolePermissions.Remove(role);
        }

        #endregion
    }
}