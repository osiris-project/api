﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class AuthorizedAccountMap
        : ClassMap<AuthorizedAccount>
    {
        public AuthorizedAccountMap()
        {
            Schema("system");
            Table("authorized_account");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Account, "account_id")
                .Cascade.SaveUpdate()
                .UniqueKey("authorized_account_un");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("authorized_account_un");

            HasMany(i => i.AccountRoles)
                .KeyColumn("authorized_account_id")
                .Cascade.AllDeleteOrphan()
                .Inverse();
        }
    }
}