﻿namespace Zentom.Api.Modules.ProfileEp
{
    public class ChangePasswordCommand
    {
        public string OldPassword { get; set; } 

        public string NewPassword { get; set; }

        public override string ToString()
        {
            return $"OldPassword: {OldPassword}, NewPassword: {NewPassword}";
        }
    }
}