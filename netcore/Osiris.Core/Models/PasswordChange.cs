namespace Osiris.Core.Models
{
    /// <summary>
    /// Represents the data required to change a user's password.
    /// </summary>
    public class PasswordChange
    {
        /// <summary>
        /// Builds a Password Change model.
        /// </summary>
        /// <param name="login">The user's login.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        public PasswordChange (string login, string oldPassword, string newPassword)
        {
            Login = login;
            OldPassword = oldPassword;
            NewPassword = newPassword;
        }

        public string Login { get; }

        public string OldPassword { get; }

        public string NewPassword { get; }
    }
}
