﻿using System;

namespace Dobelik.Utils.Password.Exceptions
{
    public class InvalidSaltPositionException
        : Exception
    {
        public InvalidSaltPositionException(string message)
            : base(message)
        {
        }
    }
}
