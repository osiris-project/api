/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import org.junit.After
import org.junit.Before
import org.junit.Test

import com.valorti.osiris.core.organization.Tenant

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class RoleTests {
    def permissions = []
    def tenant = new Tenant('764037022', 'Valorti', 'Valorti')

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        for(def i = 0; i < 10; i++) {
            permissions << new Permission("permission_${i}", '')
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        permissions = null
    }

    @Test
    public void test_addPermission() {
        println 'test_addPermission'
        this.baseRole()
        assert role.permissions[0].permission == this.permissions[0]
        assert role.permissions[0].permission.name == 'permission_0'
    }

    @Test
    void test_includePermissions() {
        println 'test_includePermissions'
        def role = this.baseRole()
        def list = this.permissions[0.. 1]
        role.includePermissions(list)
        this.printPermissions(list)
        this.printPermissions(role)
        assert role.permissions.size() == 2
        assert role.permissions[0].permission.name == 'permission_0'
        assert role.permissions[1].permission.name == 'permission_1'

        list = this.permissions[1.. 2]
        role.includePermissions(list)
        this.printPermissions(list)
        this.printPermissions(role)
        assert role.permissions.size() == 2
        assert role.permissions[1].permission.name == 'permission_1'
        assert role.permissions[0].permission.name == 'permission_2'

        list = []
        role.includePermissions(list)
        this.printPermissions(list)
        this.printPermissions(role)
        assert role.permissions.size() == 0
    }

    private Role baseRole() {
        def role = new Role('role1', '', this.tenant)
        assert role != null
        println 'Adding permission 0'
        role.addPermission(this.permissions[0])
        return role
    }

    private void printPermissions(Role role) {
        println "role.permissions.size: ${role.permissions.size()}"
        role.permissions.each { println it.permission }
    }

    private void printPermissions(List<Permission> list) {
        println "permissions.size: ${list.size()}"
        list.each { println it }
    }
}
