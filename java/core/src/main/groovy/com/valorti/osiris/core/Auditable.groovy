/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import java.time.Instant

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 *
 */
interface Auditable<IdType> {
	Entity<IdType> createdBy
	Instant createdAt
	Entity<IdType> lastModifiedBy
	Instant lastModifiedAt
}
