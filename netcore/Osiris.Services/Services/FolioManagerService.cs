﻿using System;
using Slf4Net;
using NHibernate;
using Osiris.Core.Entities;
using Osiris.Core.Services;

namespace Osiris.Services
{
    public class FolioManagerService
        : Service, IFolioManagerService
    {
        private readonly ILogger log = LoggerFactory.GetLogger(typeof(FolioManagerService));

        public FolioManagerService(ISessionFactory factory)
            : base(factory)
        {
        }

        public long GetNextFolioNumber(string documentType, Tenant tenant)
        {
            // Check if there is a folio range defined
            var entity = session.QueryOver<FolioRange>()
                .Lock().Upgrade
                .Where(
                    i =>
                        i.Tenant.Id == tenant.Id && i.DocumentType == documentType && i.IsCurrentRange &&
                        i.Status == RangeStatus.Enabled)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return 0;
            }

            // Retrieve an unused folio, if there are any
            var folio = GetLastUnusedFolio(entity);
            if (folio > 0)
            {
                return folio;
            }

            FolioRange range;

            // If the folio range has no folios left,
            // try to change to the newest folio range
            if (entity.IsUsed)
            {
                log.Info(
                    "The folio range '{0}' has been consumed. Trying to switch to the next one (if there is one).",
                    entity.Id);
                // Update the old folio range to disable it
                entity.IsCurrentRange = false;
                entity.Status = RangeStatus.Disabled;
                session.Update(entity);
                range = session.QueryOver<FolioRange>()
                    .Lock().Upgrade
                    .Where(
                        i =>
                            i.Tenant.Id == tenant.Id && i.DocumentType == documentType &&
                            i.CreatedDate > entity.CreatedDate)
                    .Take(1)
                    .SingleOrDefault();

                // If there is no newest folio range, return
                if (range == null)
                {
                    return 0;
                }

                log.Info("Successfully switched to folio range '{0}'.", entity.Id);
                // If there was a newest folio range, update it.
                range.IsCurrentRange = true;
                range.Status = RangeStatus.Enabled;
            }
            else
            {
                log.Info("Using folio range '{0}'; Current folio: {1}", entity.Id, entity.CurrentFolio);
                range = entity;
            }

            // Set the current folio as result and increment the current folio.
            folio = range.CurrentFolio;
            range.CurrentFolio++;
            return folio;
        }

        private static long GetLastUnusedFolio(FolioRange range)
        {
            if (string.IsNullOrWhiteSpace(range.UnusedFolios))
                return -1;

            var unusedFolios = range.UnusedFolios;
            var split = unusedFolios.Trim().Split(";".ToCharArray());

            if (split.Length <= 0)
            {
                return -1;
            }

            if (split.Length == 1)
            {
                range.UnusedFolios = "";
                return long.Parse(split[0]);
            }

            var longs = Array.ConvertAll(split, long.Parse);
            Array.Sort(longs);
            var result = longs[0];
            range.UnusedFolios = unusedFolios.Replace(result.ToString(), "");
            return result;
        }
    }
}
