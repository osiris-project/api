﻿namespace Osiris.Api.Modules.RoleEp
{
    public class RoleQuery
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }
    }
}
