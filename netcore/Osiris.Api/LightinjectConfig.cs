﻿using Slf4Net;
using LightInject;
using LightInject.Interception;
using Nancy;
using Nancy.ModelBinding;

namespace Osiris.Api
{
    public static class LightinjectConfig
    {
        public static void Configure(IServiceContainer container)
        {
            var log = LoggerFactory.GetLogger(typeof(LightinjectConfig));
            log.Debug("Configuring lightinject.");
            var serviceContainer = (ServiceContainer)container;
            serviceContainer.RegisterFrom<LightInjectCompositor>();
            serviceContainer.Register<ISerializer, SecureJsonSerializer>();
            serviceContainer.Register<IBodyDeserializer, JsonDeserializer>();
            log.Debug("Done.");
        }

        public static void RegisterInterceptors(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Intercept(sr => sr.ServiceType == typeof(NancyModule),
                (sf, proxyDef) => ProxyTypeDefinition(proxyDef));
        }

        private static void ProxyTypeDefinition(ProxyDefinition proxyDef)
        {
            proxyDef.Implement(() => new LoggingInterceptor());
        }
    }
}
