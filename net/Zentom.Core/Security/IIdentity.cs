﻿using System.Collections.Generic;

namespace Zentom.Core.Security
{
    public interface IIdentity
    {
        IDictionary<string, IList<string>> Credentials { get; }

        string LoginName { get; }

        IList<string> Roles { get; }

        bool IsInRole(string role);

        bool HasPermission(string role, string permission);

        IEnumerable<string> GetPermissionsInRole(string role);
    }
}
