﻿using System.Collections.Generic;
using System.IO;
using log4net;
using Nancy;
using Nancy.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Zentom.Api.Extensions.Nancy;

namespace Zentom.Api
{
    public sealed class SecureJsonSerializer
        : ISerializer
    {
        private readonly JsonSerializer serializer;

        private readonly ILog log;

        public SecureJsonSerializer()
        {
            log = LogManager.GetLogger(typeof(SecureJsonSerializer));
            serializer = JsonSerializer.CreateDefault();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializer.Formatting = Formatting.Indented;
        }

        #region Implementation of ISerializer

        public bool CanSerialize(string contentType)
        {
            var result = NancyContextHelper.IsJsonType(contentType);
            return result;
        }

        public void Serialize<TModel>(string contentType, TModel model, Stream outputStream)
        {
            log.Debug("Entering Serialize<TModel>.");
            log.DebugFormat("TModel type: {0}", typeof(TModel));
            using (var streamWr = new StreamWriter(new UnclosableStreamWrapper(outputStream)))
            {
                streamWr.Write(")]}',\n");
                using (var writer = new JsonTextWriter(streamWr))
                {
                    serializer.Serialize(writer, model);
                }
            }

        }

        public IEnumerable<string> Extensions
        {
            get { yield return "json"; }
        }

        #endregion
    }
}