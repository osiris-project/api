﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Enterprise
{
    public class EnterpriseEconomicActivityMap
        : ClassMap<EnterpriseEconomicActivity>
    {
        public EnterpriseEconomicActivityMap()
        {
            Schema("taxation");
            Table("enterprise_economic_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Enterprise, "enterprise_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("enterprise_economic_activity_un");

            References(i => i.EconomicActivity, "economic_activity_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("enterprise_economic_activity_un");
        }
    }
}
