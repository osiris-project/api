﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class ComercialActivityMap
        : ClassMap<ComercialActivity>
    {
        public ComercialActivityMap()
        {
            Schema("taxation");
            Table("comercial_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Name).UniqueKey("comercial_activity_un");
        }
    }
}