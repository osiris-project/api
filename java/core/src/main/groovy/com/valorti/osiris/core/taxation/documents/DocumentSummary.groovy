/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.MapsId
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class DocumentSummary
    extends BigIntegerEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 15)
    String currency

    @Column(precision = 13, scale = 4, nullable = false)
    Double netAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double exemptAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double baseMeatAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double baseCommercializationMargin

    @Column(precision = 4, scale = 3, nullable = false)
    Double taxRate

    /**
     * netAmount * taxRate
     */
    @Column(precision = 13, scale = 4, nullable = false)
    Double taxAmount

    /**
     * If set, then the thirdPartyTaxAmount must be set too.
     */
    @Column(precision = 13, scale = 4, nullable = false)
    Double ownedTaxAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double thirdPartyTaxAmount

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = 'document_summary')
    def additionalTaxes = new HashSet<AdditionalTax>()

    @Column(precision = 13, scale = 4, nullable = false)
    Double nonRetainedTaxAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double specialCredit

    @Column(precision = 13, scale = 4, nullable = false)
    Double depositGuarantee

    @Embedded
    Commission commission

    @Column(precision = 13, scale = 4, nullable = false)
    Double total

    @Column(precision = 13, scale = 4, nullable = false)
    Double nonBillableAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double periodAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double previousAmount

    @Column(precision = 13, scale = 4, nullable = false)
    Double totalToPay

    @MapsId
    @OneToOne(mappedBy = 'document', fetch = FetchType.EAGER)
    @JoinColumn(name = 'id')
    Document document

    /**
     * Default constructor.
     */
    protected DocumentSummary() {
        this.currency = ''
        this.netAmount = 0
        this.exemptAmount = 0
        this.baseMeatAmount = 0
        this.baseCommercializationMargin = 0
        this.taxRate = 0
        this.taxAmount = 0
        this.ownedTaxAmount = 0
        this.thirdPartyTaxAmount = 0
        this.nonRetainedTaxAmount = 0
        this.specialCredit = 0
        this.depositGuarantee = 0
        this.total = 0
        this.commission = null
        this.nonBillableAmount = 0
        this.periodAmount = 0
        this.previousAmount = 0
        this.totalToPay = 0
    }
    /**
     * @param currency
     * @param netAmount
     * @param exemptAmount
     * @param baseMeatAmount
     * @param baseCommercializationMargin
     * @param taxRate
     * @param taxAmount
     * @param ownedTaxAmount
     * @param thirdPartyTaxAmount
     * @param additionalTaxes
     * @param nonRetainedTaxAmount
     * @param specialCredit
     * @param depositGuarantee
     * @param commission
     * @param total
     * @param nonBillableAmount
     * @param periodAmount
     * @param previousAmount
     * @param totalToPay
     * @throws IllegalArgumentException if the document header is not set.
     * @since 0.0.1
     */
    DocumentSummary(String currency, Double netAmount, Double exemptAmount, Double baseMeatAmount,
                    Double baseCommercializationMargin, Double taxRate, Double taxAmount, Double ownedTaxAmount, Double thirdPartyTaxAmount,
                    Set<AdditionalTax> additionalTaxes, Double nonRetainedTaxAmount, Double specialCredit, Double depositGuarantee,
                    Commission commission, Double total, Double nonBillableAmount, Double periodAmount, Double previousAmount, Double totalToPay,
                    Document document) {
        if (!document) {
            throw new IllegalArgumentException('No document header provided!')
        }
        this.currency = currency
        this.netAmount = netAmount
        this.exemptAmount = exemptAmount
        this.baseMeatAmount = baseMeatAmount
        this.baseCommercializationMargin = baseCommercializationMargin
        this.taxRate = taxRate
        this.taxAmount = taxAmount
        this.ownedTaxAmount = ownedTaxAmount
        this.thirdPartyTaxAmount = thirdPartyTaxAmount
        this.additionalTaxes = additionalTaxes
        this.nonRetainedTaxAmount = nonRetainedTaxAmount
        this.specialCredit = specialCredit
        this.depositGuarantee = depositGuarantee
        this.commission = commission
        this.total = total
        this.nonBillableAmount = nonBillableAmount
        this.periodAmount = periodAmount
        this.previousAmount = previousAmount
        this.totalToPay = totalToPay
        this.documentHeader = document
    }
}
