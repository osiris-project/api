﻿using FluentValidation;

namespace Osiris.Api.Modules.AccountingEp.Validators
{
    public class AccountingVoucherLineValidator
        : AbstractValidator<VoucherDetailCmd>
    {
        public AccountingVoucherLineValidator()
        {
            RuleFor(i => i.ExpiryDate).NotNull();
            RuleFor(i => i.FolioNumber).GreaterThan(0);
        }
    }
}
