/**
 * Copyright (c) Mar 10, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.jpa

import org.hibernate.boot.model.naming.Identifier
import org.hibernate.boot.model.naming.PhysicalNamingStrategy
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class SnakeCaseStrategy
implements PhysicalNamingStrategy {
    private final Logger log = LoggerFactory.getLogger(SnakeCaseStrategy.class)
    /**
     * Default constructor.
     */
    protected SnakeCaseStrategy() {
    }

    /* (non-Javadoc)
     * @see org.hibernate.boot.model.naming.PhysicalNamingStrategy#toPhysicalCatalogName(org.hibernate.boot.model.naming.Identifier, org.hibernate.engine.jdbc.env.spi.JdbcEnvironment)
     */
    @Override
    Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return this.convert(name, jdbcEnvironment)
    }

    /* (non-Javadoc)
     * @see org.hibernate.boot.model.naming.PhysicalNamingStrategy#toPhysicalSchemaName(org.hibernate.boot.model.naming.Identifier, org.hibernate.engine.jdbc.env.spi.JdbcEnvironment)
     */
    @Override
    Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return this.convert(name, jdbcEnvironment)
    }

    /* (non-Javadoc)
     * @see org.hibernate.boot.model.naming.PhysicalNamingStrategy#toPhysicalTableName(org.hibernate.boot.model.naming.Identifier, org.hibernate.engine.jdbc.env.spi.JdbcEnvironment)
     */
    @Override
    Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return this.convert(name, jdbcEnvironment)
    }

    /* (non-Javadoc)
     * @see org.hibernate.boot.model.naming.PhysicalNamingStrategy#toPhysicalSequenceName(org.hibernate.boot.model.naming.Identifier, org.hibernate.engine.jdbc.env.spi.JdbcEnvironment)
     */
    @Override
    Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return this.convert(name, jdbcEnvironment)
    }

    /* (non-Javadoc)
     * @see org.hibernate.boot.model.naming.PhysicalNamingStrategy#toPhysicalColumnName(org.hibernate.boot.model.naming.Identifier, org.hibernate.engine.jdbc.env.spi.JdbcEnvironment)
     */
    @Override
    Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return this.convert(name, jdbcEnvironment)
    }

    private String convert(Identifier i, JdbcEnvironment j) {
        def t = StringUtils.toSnakeCase(name.getText())
        log.debug('Converting to snake case: [original={}, new={}]', i.getText(), t)
        return je.getIdentifierHelper().toIdentifier(t, i.isQuoted)
    }
}
