﻿using System.Collections.Generic;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.EnterpriseEp
{
    public class EnterpriseUpdateCmd
        : Command
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }

        public string DisplayName { get; set; }

        public string BankAccountNumber { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public EnterpriseRelationship Relationship { get; set; }

        public IEnumerable<EnterpriseAddressCmd> Addresses { get; set; }

        public IEnumerable<string> ComercialActivities { get; set; }
    }


}
