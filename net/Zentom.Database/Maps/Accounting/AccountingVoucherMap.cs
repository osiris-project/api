﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities.Accounting;

namespace Zentom.Database.Maps.Accounting
{
    public class AccountingVoucherMap
        : ClassMap<AccountingVoucher>
    {
        public AccountingVoucherMap()
        {
            Schema("accounting");
            Table("accounting_voucher");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Date).UniqueKey("accounting_voucher_un");
            Map(i => i.Description).Length(60);
            HasMany(i => i.Lines)
                .KeyColumn("accounting_voucher_id")
                .Cascade.AllDeleteOrphan()
                .Inverse();
            Map(i => i.Number).Generated.Always().UniqueKey("accounting_voucher_un");
            Map(i => i.Status).CustomType(typeof(AccountingVoucherStatus));
            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("accounting_voucher_un");
        }
    }
}