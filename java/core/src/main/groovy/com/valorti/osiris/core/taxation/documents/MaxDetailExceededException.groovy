/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

/**
 * Thrown when the max detail count has been reached but
 * the user tries to add another line.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class MaxDetailExceededException extends Exception {
    /**
     * Default constructor.
     */
    MaxDetailExceededException(int max) {
        super("Max detail lines has been reached: ${max}.")
    }
}
