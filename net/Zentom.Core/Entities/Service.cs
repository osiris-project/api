﻿namespace Zentom.Core.Entities
{
    public class Service
        : ProductBase
    {
        public Service() { }

        public Service(Tenant tenant, string name, decimal unitPrice, bool isDiscontinued)
            : base(tenant, name, unitPrice, isDiscontinued)
        {
        }
    }
}
