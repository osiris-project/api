/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Contains all the configuration defined in application.yml.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@ConfigurationProperties('app')
class AppConfig {
    final Logging logging = new Logging()

    /**
     * Contains all logging configuration properties.
     */
    static class Logging {
        final Markers markers = new Markers()
        /**
         * Represents custom defined logging markers.
         */
        static class Markers {
            String admin
        }
    }
}
