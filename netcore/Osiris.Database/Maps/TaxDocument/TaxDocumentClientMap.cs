﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class TaxDocumentClientMap
        : ClassMap<TaxDocumentClient>
    {
        public TaxDocumentClientMap()
        {
            Schema("taxation");
            Table("taxdoc_client");

            Id(i => i.Id).GeneratedBy.Foreign("TaxDocument").Not.Nullable();
            Version(i => i.RowVersion);

            HasMany(i => i.Addresses)
                .Cascade.All()
                .Inverse()
                .KeyColumn("taxdoc_client_id");

            Map(i => i.BankAccountNumber).Length(15).Nullable();
            Map(i => i.ComercialActivity, "comercial_activity").Length(60);
            Map(i => i.LegalName).Length(100);
            Map(i => i.Phone).Length(50);
            
            HasOne(i => i.TaxDocument)
                .Not.LazyLoad()
                .Fetch.Join()
                .Cascade.None()
                .Constrained();

            Map(i => i.TaxId).Length(20);
        }
    }
}
