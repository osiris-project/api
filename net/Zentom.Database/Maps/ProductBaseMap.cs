﻿using FluentNHibernate.Mapping;
using System;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class ProductBaseMap
        : ClassMap<AbstractProductBase>
    {
        public ProductBaseMap()
        {
            UseUnionSubclassForInheritanceMapping();

            Id(k => k.Id).Column("id").GeneratedBy.Identity();
            Version(i => i.RowVersion).Column("row_version").Generated.Always();
            Map(p => p.RowGuid).Column("row_guid").Unique();
            Map(p => p.Name).Column("name").Length(100).Unique().Not.Nullable();
            Map(p => p.BasePrice).Column("base_price").Precision(13).Scale(4).Not.Nullable();
            Map(p => p.IsSubjectToTax).Column("is_subject_to_tax").Nullable();
        }
    }
}
