﻿namespace Zentom.Core.Entities
{
    public class Employee
        : TenantEntity
    {
        #region Fields
        
        private Person person;

        #endregion

        #region Properties

        public virtual Person Person
        {
            get { return person; }
            set { person = value; }
        }

        #endregion

        #region Ctor.

        public Employee() { }

        public Employee(Tenant tenant, Person person)
            : base(tenant)
        {
            this.person = person;
        }

        #endregion
    }
}
