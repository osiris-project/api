﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Dobelik.Utils.Identity;
using Slf4Net;
using NHibernate;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Enums;
using Osiris.Core.Services;
using Osiris.I18n;

namespace Osiris.Services
{
    public class ReceivedTaxDocumentService
        : Service, IReceivedTaxDocumentService
    {
        #region Fields

        private readonly ILogger log = LoggerFactory.GetLogger(typeof(ReceivedTaxDocumentService));

        private readonly INationalIdentity identity;

        private int index;

        private string documentId;

        private Tenant tenant;

        private static readonly string[] requiresReferences = { "61", "56" };

        private XmlDocument xmlDocument;

        #endregion

        #region Properties

        public IList<ReceivedTaxDocument> ReceivedTaxDocuments { get; private set; }

        #endregion

        public ReceivedTaxDocumentService(ISessionFactory factory, INationalIdentity identity)
            : base(factory)
        {
            this.identity = identity;
            ReceivedTaxDocuments = new List<ReceivedTaxDocument>();
        }

        #region IReceivedTaxDocumentService Impl.

        /// <summary>
        /// Reads and parses the received XML stream <b>without blocking</b> the thread.
        /// </summary>
        /// <param name="stream">An XML stream</param>
        /// <param name="t">The current application tenant</param>
        /// <returns>Empty Task.</returns>
        public Task ReceiveDocumentAsync(Stream stream, Tenant t)
        {
            PrepareDocumentReception(t);
            return Task.Run(() => ParseDocument(stream));
        }

        /// <summary>
        /// Reads and parses the received XML stream <b>blocking</b> the thread
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="t"></param>
        public void ReceiveDocument(Stream stream, Tenant t)
        {
            PrepareDocumentReception(t);
            ParseDocument(stream);
        }

        public Task SaveDocumentAsync(string path)
        {
            return Task.Run(() => SaveDocument(path));
        }

        public void SaveDocument(string path)
        {
            //var fname = string.Concat(DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss"), ".xml");
            try
            {
                xmlDocument.Save(path);
            }
            catch (Exception ex)
            {
                log.Error("Failed to save XML Document - ", ex);
                ErrorList.Add(new ErrorModel("", Error.XMLDocumentWriteFailed));
            }
        }

        #endregion

        #region Setup

        private void PrepareDocumentReception(Tenant t)
        {
            ErrorList.Clear();
            tenant = t;
            index = -1;
            documentId = null;
            ReceivedTaxDocuments.Clear();
            xmlDocument = null;
        }

        #endregion

        #region Reception Upload

        private void ParseDocument(Stream stream)
        {
            var xdoc = new XmlDocument();
            xdoc.Load(stream);
            var documents = xdoc.SelectNodes("/SetDTE/DTE/Documento");

            if (documents == null)
            {
                AddError("malformedDocument", Error.MalformedReceivedDocument);
                return;
            }

            for (var i = 0; i < documents.Count; i++)
            {
                index++;
                log.Debug("Parsing element at index '{0}'.", index);
                var document = documents.Item(i);

                if (document == null) continue;

                documentId = document.Attributes != null ? document.Attributes.GetNamedItem("ID").Value : index.ToString();
                var header = document.SelectSingleNode("Encabezado");

                if (header == null)
                {
                    AddError(GetMissingMessage("Encabezado"));
                    continue;
                }

                var taxdoc = new ReceivedTaxDocument
                {
                    Tenant = tenant
                };

                if (!ParseReceptor(taxdoc, header.SelectSingleNode("Receptor"))) continue;
                if (!ParseDocumentData(taxdoc, header.SelectSingleNode("IdDoc"))) continue;
                if (!ParseIssuer(taxdoc, header.SelectSingleNode("Emisor"))) continue;

                var existingDocument = session.QueryOver<ReceivedTaxDocument>()
                    .Where(
                        storedDoc =>
                            storedDoc.FolioNumber == taxdoc.FolioNumber && storedDoc.DocumentType == taxdoc.DocumentType &&
                            storedDoc.Tenant == tenant && storedDoc.ProviderTaxId == taxdoc.ProviderTaxId && storedDoc.ReceptionStatus != ReceptionStatus.Pending)
                    .Take(1)
                    .SingleOrDefault();

                if (existingDocument != null)
                {
                    AddError(string.Format(Error.DuplicateReceivedTaxDocument, taxdoc.DocumentType, taxdoc.FolioNumber, 
                        identity.Normalize(taxdoc.ProviderTaxId), documentId));
                    continue;
                }
                if (!ParseTotals(taxdoc, header.SelectSingleNode("Totales"))) continue;
                if (!ParseReferences(taxdoc, document.SelectNodes("Referencia"))) continue;

                var details = document.SelectNodes("Detalle");
                if (!ParseDetails(taxdoc, details)) continue;

                taxdoc.Comments = "";
                taxdoc.FilePath = "";
                taxdoc.ReceptionTime = DateTime.UtcNow;
                ReceivedTaxDocuments.Add(taxdoc);
                // TODO: Save each document separately.
            }

            xmlDocument = xdoc;
        }

        private bool ParseReceptor(ReceivedTaxDocument taxdoc, XmlNode receptor)
        {
            log.Debug("Entering ParseReceptor");
            const string sectionName = "Receptor";
            if (receptor == null)
            {
                AddError(GetMissingMessage(sectionName));
                return false;
            }

            var field = "RUTRecep";
            var str = receptor.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                AddError(GetMissingField(field, sectionName));
                return false;
            }

            str = identity.CleanId(str);

            if (str != tenant.TaxId)
            {
                AddError(string.Format(Error.WrongTenant, identity.Normalize(str), identity.Normalize(tenant.TaxId)));
                return false;
            }

            taxdoc.ReceptorTaxId = str;
            field = "RznSocRecep";
            str = receptor.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                str = tenant.LegalName;
            }

            taxdoc.ReceptorLegalName = str;
            field = "GiroRecep";
            str = receptor.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                log.Warn("No Sector Name was present in the section '{0}' id '{1}'.", sectionName, documentId);
                str = "";
            }

            taxdoc.ReceptorComercialActivity = str;
            field = "DirRecep";
            str = receptor.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                str = "";
            }

            taxdoc.ReceptorAddress = str;
            field = "CmnaRecep";
            str = receptor.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                str = "";
            }

            taxdoc.ReceptorCommune = str;
            return true;
        }

        private bool ParseDocumentData(ReceivedTaxDocument taxdoc, XmlNode node)
        {
            log.Debug("Entering ParseDocumentData");
            const string sectionName = "IdDoc";

            if (node == null)
            {
                AddError(GetMissingMessage(sectionName));
                return false;
            }

            var field = "TipoDTE";
            var str = node.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                AddError(GetMissingField(field, sectionName));
                return false;
            }

            taxdoc.DocumentType = str;
            field = "Folio";
            str = node.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                AddError(GetMissingField(field, sectionName));
                return false;
            }

            taxdoc.FolioNumber = long.Parse(str);
            field = "FchEmis";
            str = node.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                AddError(GetMissingField(field, sectionName));
                return false;
            }

            DateTime dt;

            if (!DateTime.TryParse(str, out dt))
            {
                dt = DateTime.UtcNow;
            }

            taxdoc.AccountingDate = dt;
            field = "FchVenc";
            str = node.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                taxdoc.ExpiryDate = null;
            }
            else
            {
                if (!DateTime.TryParse(str, out dt))
                {
                    taxdoc.ExpiryDate = null;
                }

                taxdoc.ExpiryDate = dt;
            }


            return true;
        }

        private bool ParseIssuer(ReceivedTaxDocument taxdoc, XmlNode issuer)
        {
            log.Debug("Entering ParseIssuer");
            const string section = "Emisor";

            if (issuer == null)
            {
                AddError(GetMissingMessage(section));
                return false;
            }

            // RutEmisor = This tenant's provider.
            var field = "RUTEmisor";
            var providerId = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(providerId))
            {
                AddError(GetMissingField(field, section));
                return false;
            }

            if (!identity.Validate(providerId))
            {
                AddBadField(field, section);
                return false;
            }
            providerId = identity.CleanId(providerId);
            taxdoc.ProviderTaxId = providerId;
            field = "RznSoc";
            var legalName = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(legalName))
            {
                AddError(GetMissingField(field, section));
                return false;
            }

            taxdoc.ProviderLegalName = legalName;
            field = "GiroEmis";
            var comercialActivity = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(comercialActivity))
            {
                AddError(GetMissingField(field, section));
                return false;
            }

            comercialActivity = comercialActivity.Trim();
            taxdoc.ProviderComercialActivity = comercialActivity;
            field = "Acteco";
            var economicActivityCode = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(economicActivityCode))
            {
                AddError(GetMissingField(field, section));
                return false;
            }

            economicActivityCode = economicActivityCode.Trim();
            taxdoc.ProviderEconomicActivity = economicActivityCode;
            field = "DirOrigen";
            var address = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(address))
            {
                address = "";
            }

            address = address.Trim();
            field = "CmnaOrigen";
            var communeStr = issuer.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(communeStr))
            {
                communeStr = "";
            }
            
            var ea = new EnterpriseAddress();
            SetAddress(ea, address);

            var commune = session.QueryOver<Commune>()
                    .Where(i => i.Name == communeStr)
                    .Take(1)
                    .SingleOrDefault();
            if (commune != null)
            {
                ea.Commune = commune;
            }
            else
            {
                ea.Commune = new Commune
                {
                    Name = communeStr
                };
            }

            var enterprise = session.QueryOver<Enterprise>()
                .Where(i => i.TaxId == providerId && i.Tenant == tenant)
                .Take(1)
                .SingleOrDefault();

            if (enterprise == null)
            {
                
                enterprise = new Enterprise
                {
                    TaxId = providerId,
                    LegalName = legalName,
                    DisplayName = legalName,
                    Email = "",
                    BankAccountNumber = "",
                    Relationship = EnterpriseRelationship.Provider,
                    Phone = "",
                    Tenant = tenant
                };

                var caQuery = session.QueryOver<ComercialActivity>()
                    .Where(i => i.Name == comercialActivity)
                    .Take(1)
                    .SingleOrDefault();

                if (caQuery != null)
                {
                    var eca = new EnterpriseComercialActivity(enterprise, caQuery);
                    if (enterprise.EnterpriseComercialActivities.All(i => i != eca))
                    {
                        enterprise.EnterpriseComercialActivities.Add(eca);
                    }
                }
                
                var sector = session.QueryOver<EconomicActivity>()
                    .Where(i => i.Code == economicActivityCode)
                    .Take(1)
                    .SingleOrDefault();

                if (sector != null)
                {
                    var list = new List<EconomicActivity> {sector};
                    enterprise.SetEconomicActivities(list);
                }

                var addrTypes = session.QueryOver<AddressType>()
                    .List();
                foreach (var eat in addrTypes.Select(i => new EnterpriseAddressType(ea, i)))
                {
                    ea.EnterpriseAddressTypes.Add(eat);
                }
                ea.Enterprise = enterprise;
                enterprise.EnterpriseAddresses.Add(ea);
            }

            taxdoc.ProviderAddressCommune = ea.Commune.Name;
            taxdoc.ProviderAddresLine1 = ea.Line1;
            taxdoc.ProviderAddressLine2 = ea.Line2;
            taxdoc.Enterprise = enterprise;
            taxdoc.ProviderAddressCity = "";
            taxdoc.ProviderBankAccountNumber = "";
            taxdoc.ProviderPhone = "";
            return true;
        }

        private bool ParseTotals(ReceivedTaxDocument taxdoc, XmlNode totals)
        {
            log.Debug("Entering ParseTotals");
            const string sectionName = "Totales";

            if (totals == null)
            {
                AddError(GetMissingMessage(sectionName));
                return false;
            }

            var field = "TasaIVA";
            var str = totals.SelectSingleNode(field)?.InnerText;

            if (string.IsNullOrWhiteSpace(str))
            {
                AddError(GetMissingField(field, sectionName));
                return false;
            }

            taxdoc.TaxRate = decimal.Parse(str) / 100;

            field = "MntNeto";
            str = totals.SelectSingleNode(field)?.InnerText;

            if (!string.IsNullOrWhiteSpace(str))
            {
                taxdoc.NetAmount = decimal.Parse(str);
            }

            field = "MntExento";
            str = totals.SelectSingleNode(field)?.InnerText;
            if (!string.IsNullOrWhiteSpace(str))
            {
                taxdoc.ExemptAmount = decimal.Parse(str);
            }

            field = "IVA";
            str = totals.SelectSingleNode(field)?.InnerText;
            if (!string.IsNullOrWhiteSpace(str))
            {
                taxdoc.TaxAmount = decimal.Parse(str);
            }

            field = "MntTotal";
            str = totals.SelectSingleNode(field)?.InnerText;
            if (!string.IsNullOrWhiteSpace(str))
            {
                taxdoc.Total = decimal.Parse(str);
            }
            return true;
        }

        private bool ParseReferences(ReceivedTaxDocument taxdoc, XmlNodeList nodeList)
        {
            log.Debug("Entering ParseReferences");
            const string sectionNanme = "Referencia";

            if (nodeList == null)
            {
                if (requiresReferences.All(i => i != taxdoc.DocumentType)) return true;
                AddError(GetMissingMessage(sectionNanme));
                return false;
            }

            var parsed = false;

            for (var i = 0; i < nodeList.Count; i++)
            {
                var node = nodeList.Item(i);
                if (node == null) continue;

                var field = "TpoDocRef";
                var documentType = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(documentType))
                {
                    AddError(GetMissingField(field, sectionNanme));
                    continue;
                }

                field = "FolioRef";
                var folio = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(folio))
                {
                    AddError(GetMissingField(field, sectionNanme));
                    continue;
                }

                var referencedFolio = long.Parse(folio);
                field = "FchRef";
                DateTime dt;
                var refDate = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(refDate))
                {
                    dt = DateTime.UtcNow;
                }
                else
                {
                    if (!DateTime.TryParse(refDate, out dt))
                        dt = DateTime.UtcNow;
                }

                field = "CodRef";
                var correctionCode = CorrectionCode.None;
                var str = node.SelectSingleNode(field)?.InnerText;

                if (!string.IsNullOrWhiteSpace(str))
                {
                    Enum.TryParse(str, out correctionCode);
                }

                field = "RazonRef";
                str = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(str))
                {
                    str = "";
                }

                var found = session.QueryOver<ReceivedTaxDocument>()
                    .Where(
                        doc =>
                            doc.DocumentType == documentType && doc.FolioNumber == referencedFolio &&
                            doc.Tenant.Id == tenant.Id)
                    .Take(1)
                    .SingleOrDefault();

                if (found == null)
                {
                    var msg = string.Format(Error.ReceivedReferencedDocumentNotFound, taxdoc.FolioNumber,
                        taxdoc.DocumentType, taxdoc.ProviderTaxId);
                    AddError("referenceNotFound", msg);
                    continue;
                }

                taxdoc.AddReference(found, correctionCode, str, dt);
                parsed = true;
            }

            if (!parsed)
            {
                parsed = requiresReferences.All(i => i != taxdoc.DocumentType);
            }
            return parsed;
        }

        private bool ParseDetails(ReceivedTaxDocument taxdoc, XmlNodeList nodeList)
        {
            log.Debug("Entering ParseDetails");
            const string sectionName = "Detalle";
            if (nodeList == null)
            {
                AddError(GetMissingMessage(sectionName));
                return false;
            }

            if (nodeList.Count <= 0)
            {
                AddError(GetMissingMessage(sectionName));
                return false;
            }

            var parsed = false;

            for (var i = 0; i < nodeList.Count; i++)
            {
                var node = nodeList.Item(i);

                if (node == null) continue;
                var field = "NroLinDet";
                var lineNo = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(lineNo))
                {
                    AddError(GetMissingField(field, sectionName));
                    continue;
                }

                field = "NmbItem";
                var name = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(name))
                {
                    AddError(GetMissingField(field, sectionName));
                    return false;
                }

                field = "DscItem";
                var desc = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(desc))
                {
                    desc = "";
                }

                field = "QtyItem";
                var quantity = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(quantity))
                {
                    quantity = "1";
                }

                field = "PrcItem";
                var unitPrice = node.SelectSingleNode(field)?.InnerText;

                if (string.IsNullOrWhiteSpace(unitPrice))
                {
                    unitPrice = "0";
                }

                var line = new ReceivedTaxDocumentLine(int.Parse(lineNo), name, taxdoc, decimal.Parse(quantity), desc, decimal.Parse(unitPrice));
                taxdoc.TaxDocumentLines.Add(line);
                parsed = true;
            }

            return parsed;
        }
        
        #endregion

        #region Utility Methods

        private string GetMissingMessage(string sectionName)
        {
            if (documentId == null)
                documentId = "";
            return string.Format(Error.MissingSection, sectionName, documentId);
        }

        private string GetMissingField(string fieldName, string sectionName)
        {
            return string.Format(Error.MissingField, fieldName, sectionName, documentId);
        }

        private void AddBadField(string fieldName, string sectionName)
        {
            AddError(string.Format(Error.BadField, fieldName, sectionName, documentId));
        }

        private void AddError(string fieldName, string message)
        {
            ErrorList.Add(new ErrorModel(fieldName, message));
        }

        private void AddError(string message)
        {
            AddError("receivedDocumentError", message);
        }

        private void SetAddress(Address entity, string line)
        {
            if (line.Length > 70)
            {
                var indexOf = line.LastIndexOf(' ', 69);
                entity.Line1 = line.Substring(0, indexOf + 1);
                entity.Line2 = line.Substring(indexOf + 1);
            }
            else
            {
                entity.Line1 = line;
            }
        }

        #endregion
    }
}
