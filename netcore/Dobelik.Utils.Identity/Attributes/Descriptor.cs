﻿using System;
using System.Globalization;

namespace Dobelik.Utils.Identity.Attributes
{
    internal class Descriptor
        : Attribute
    {
        private CultureInfo culture;

        public CultureInfo Culture { get { return culture; } }

        public Descriptor(string twoLetterIsoCode, string twoLetterCountryName)
        {
            culture = CultureInfo.CreateSpecificCulture(string.Concat(twoLetterIsoCode, "-", twoLetterCountryName));
        }

        public Descriptor(CultureInfo cultureInfo)
        {
            culture = cultureInfo;
        }
    }
}
