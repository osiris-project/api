﻿using System;
using System.Globalization;
using Dobelik.Utils.Identity;
using Dobelik.Utils.Password;
using Dobelik.Utils.Password.Enums;
using Dobelik.Utils.Password.Interfaces;
using LightInject;
using LightInject.Interception;
using Zentom.Core.Interception;
using Zentom.Core.Services;
using Zentom.Database;
using Zentom.Dependency;
using Zentom.Integration.Artikos;
using Zentom.Services;

[assembly: CompositionRootType(typeof(Composition))]

namespace Zentom.Dependency
{
    public class Composition
        : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            RegisterServices(serviceRegistry);
            RegisterInterceptors(serviceRegistry);
        }

        private static void RegisterServices(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<Random>();
            serviceRegistry.Register(f => NHibernateHelper.SessionFactory, new PerContainerLifetime());
            serviceRegistry.Register(f => IdentityLocator.GetInstance());
            serviceRegistry.Register(f => f.GetInstance<IdentityLocator>().Get(CultureInfo.CreateSpecificCulture("es-CL")));
            serviceRegistry.Register(f => new ArtikosFactory(f.GetInstance<INationalIdentity>()), "artikos_cl");
            serviceRegistry.Register<IHashInfo>(f =>
                new HashInfo(64, 64, 50, SaltPosition.Append, "|"));
            serviceRegistry.Register<IPasswordHasher>(f =>
            new PasswordHashing(f.GetInstance<IHashInfo>()), new PerContainerLifetime());

            // Service layer?
            serviceRegistry.Register<IEnterpriseService, EnterpriseService>();
            serviceRegistry.Register<IAuthenticationService, AuthenticationService>();
            serviceRegistry.Register<IReceivedTaxDocumentService, ReceivedTaxDocumentService>();
            serviceRegistry.Register<IReceivedDocumentValidationService, ReceivedDocumentValidationService>();
            serviceRegistry.Register<IBulkImportTaxDocumentService, BulkImportTaxDocumentService>();
            serviceRegistry.Register<IFolioManagerService, FolioManagerService>();
        }

        private static void RegisterInterceptors(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Intercept(sr => sr.ServiceType == typeof(IInterceptable),
                (sf, proxyDef) => ProxyTypeDefinition(proxyDef));
            serviceRegistry.Intercept(sr => sr.ServiceType == typeof(IPasswordHasher),
                (sf, proxyDef) => ProxyTypeDefinition(proxyDef));
        }

        private static void ProxyTypeDefinition(ProxyDefinition proxyDef)
        {
            proxyDef.Implement(() => new LoggingInterceptor());
        }
    }
}
