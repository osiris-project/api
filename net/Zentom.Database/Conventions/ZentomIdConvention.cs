﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Zentom.Database.Conventions
{
    public class ZentomIdConvention
        : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            instance.Column("id");
            instance.GeneratedBy.GuidComb();
            instance.Not.Nullable();
        }
    }
}
