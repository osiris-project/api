﻿namespace Osiris.Api.Modules.ServiceEp
{
    public class ServiceQuery
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
