﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IRolePermission
        : IVersioned
    {
        IRole Role { get; set; }

        IPermission Permission { get; set; }
    }

    public interface IRolePermissionId
    {
        IRole Role { get; set; }

        IPermission Permission { get; set; }
    }
}
