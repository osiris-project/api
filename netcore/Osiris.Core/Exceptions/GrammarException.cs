﻿using System;

namespace Osiris.Core.Exceptions
{
    [Serializable]
    public class GrammarException
        : Exception
    {
        public GrammarException() { }

        public GrammarException(string message)
            : base(message) {}

        public GrammarException(string message, Exception innerException)
            : base(message, innerException) { }

        public GrammarException(Exception exception)
            : base(exception.Message, exception)
        { }
    }
}
