﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Zentom.Database.Conventions
{
    public class ZentomClassConvention
        : IClassConvention
    {
        #region Implementation of IConvention<IClassInspector,IClassInstance>

        public void Apply(IClassInstance instance)
        {

            instance.OptimisticLock.Version();
        }

        #endregion
    }
}