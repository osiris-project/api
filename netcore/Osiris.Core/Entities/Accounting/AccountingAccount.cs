﻿namespace Osiris.Core.Entities.Accounting
{
    public class AccountingAccount
        : TenantEntity
    {
        #region Fields

        private string code;

        private string description;

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion

        #region Ctors.

        public AccountingAccount()
        {
        }

        public AccountingAccount(Tenant tenant, string code, string description)
            : base(tenant)
        {
            this.code = code;
            this.description = description;
        }

        #endregion
    }
}
