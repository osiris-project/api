﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using Excel;
using log4net;
using Zentom.Core.Interfaces.Validation;
using Zentom.Core.Services;

namespace Zentom.Services
{
    // TODO: change Process(stream, contentType) to use an enum instead.

    public class BulkImportTaxDocumentService
        : IBulkImportTaxDocumentService, IDisposable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(BulkImportTaxDocumentService));

        private Stream stream;

        private readonly IList<IBulkImportRow> rows;

        #region Implementation of IService

        public bool IsValid => ErrorList == null || ErrorList.Count <= 0;

        public IList<IModelValidation> ErrorList { get; }

        #endregion

        public BulkImportTaxDocumentService()
        {
            ErrorList = new List<IModelValidation>();
            rows = new List<IBulkImportRow>();
        }

        #region Implementation of IBulkImportTaxDocumentService

        public IList<IBulkImportRow> Process(FileInfo fileInfo)
        {
            ParseDocument(fileInfo);
            return rows;
        }

        public IList<IBulkImportRow> Process(string filePath)
        {
            ParseDocument(new FileInfo(filePath));
            return rows;
        }

        public IList<IBulkImportRow> Process(Stream s, string contentType)
        {
            ParseDocument(s, contentType);
            return rows;
        } 

        #endregion

        private void ParseDocument(FileSystemInfo fileInfo)
        {
            using (var excel = ReadFile(fileInfo))
            {
                ParseDocument(excel);
            }
        }

        private void ParseDocument(Stream s, string contentType)
        {
            using (var excel = ReadFile(s, contentType))
            {
                ParseDocument(excel);
            }
        }

        private void ParseDocument(IExcelDataReader excel)
        {
            var book = excel.AsDataSet();
            var gloss = book.Tables[0];
            // The first 3 rows are reserved for future use.

            for (var i = 3; i < gloss.Rows.Count; i++)
            {
                var row = IsValidRow(gloss.Rows[i], i);
                if (row == null)
                {
                    continue;
                }
                rows.Add(row);
            }
        }

        private IExcelDataReader ReadFile(FileSystemInfo fileInfo)
        {
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException();
            }
            return ReadFile(File.OpenRead(fileInfo.FullName), fileInfo.Extension);
        }

        private IExcelDataReader ReadFile(Stream s, string contentType)
        {
            stream = s;
            return contentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                ? ExcelReaderFactory.CreateOpenXmlReader(stream)
                : ExcelReaderFactory.CreateBinaryReader(stream);
        }

        private BulkImportRow IsValidRow(DataRow dataRow, int index)
        {
            if (dataRow.IsNull(0))
            {
                ErrorList.Add(new ErrorModel("", string.Format(I18n.Error.MissingGlossColumn, (index + 1))));
                return null;
            }

            if (dataRow.IsNull(1))
            {
                ErrorList.Add(new ErrorModel("", string.Format(I18n.Error.MissingNetAmountColumn, (index + 1))));
                return null;
            }
            
            try
            {
                var doctype = dataRow.Field<string>(2);
                return string.IsNullOrWhiteSpace(doctype)
                    ? new BulkImportRow(dataRow.Field<string>(0), dataRow.Field<double>(1))
                    : new BulkImportRow(dataRow.Field<string>(0), dataRow.Field<double>(1), doctype);
            }
            catch (Exception ex)
            {
                log.Error("Failed to create row.", ex);
            }

            return null;
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (stream != Stream.Null) return;
            stream.Close();
            stream.Dispose();
        }

        #endregion

        private class BulkImportRow
            : IBulkImportRow
        {
            public string Gloss { get; }

            public double NetAmount { get; }

            public string DocumentType { get; set; }

            public BulkImportRow(string gloss, double netAmount)
            {
                Gloss = gloss;
                NetAmount = netAmount;
            }

            public BulkImportRow(string gloss, double netAmount, string documentType)
            {
                Gloss = gloss;
                NetAmount = netAmount;
                DocumentType = documentType;
            }
        }
    }
}