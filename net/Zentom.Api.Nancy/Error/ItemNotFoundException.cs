﻿using System;

namespace Zentom.Api.Error
{
    public class ItemNotFoundException
        : Exception
    {
        public ItemNotFoundException(string entityName)
            : base(string.Format(I18n.Error.ItemNotFound, entityName))
        {
        }

        public ItemNotFoundException(string entityName, object id)
            : base(string.Format(I18n.Error.ItemWithIdNotFound, entityName, id))
        {
        }
    }
}