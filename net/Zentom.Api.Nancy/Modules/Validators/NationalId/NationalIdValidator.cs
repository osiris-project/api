﻿using System;
using Dobelik.Utils.Identity;
using FluentValidation.Validators;
using log4net;

namespace Zentom.Api.Modules.Validators.NationalId
{
    public class NationalIdValidator
        : PropertyValidator
    {
        private readonly INationalIdentity idUtils;

        private NationalIdValidator()
            : base("InvalidTaxId", typeof(I18n.Error))
        {
        }

        public NationalIdValidator(INationalIdentity identity)
            : this()
        {
            idUtils = identity;
        }

        #region Overrides of PropertyValidator

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (idUtils == null)
                throw new ArgumentNullException(nameof(idUtils));
            var log = LogManager.GetLogger(typeof (NationalIdValidator));
            log.DebugFormat("IdUtils instance: {0}", idUtils.GetType());
            return idUtils.Validate(context.PropertyValue.ToString());
        }

        #endregion
    }
}