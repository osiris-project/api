﻿using System.Collections.Generic;
using Zentom.Core.Interception;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Core.Services
{
    public interface IService
        : IInterceptable
    {
        bool IsValid { get; }

        IList<IModelValidation> ErrorList { get; }
    }
}