﻿using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.SpecificTaxEp {
    public class SpecificTaxModule : TenantApi {
        public SpecificTaxModule (ISessionFactory factory) : base ("specific-taxes", factory) {
            this.Get (BasePath, List);
            this.Get<Command> (BasePath + "/{id:guid}", Read);
        }

        [Permission (Permission = "tax-document-read")]
        private Negotiator Read (Command cmd) {
            var entity = db.Get<SpecificTax> (cmd.Id);
            return entity == null ?
                Negotiate.WithNotFound (I18n.Text.SpecificTax, cmd.Id) :
                Negotiate.WithOkAndModel (entity);
        }

        [Permission (Permission = "tax-document-list")]
        private Negotiator List (object arg) {
            var bound = this.BindTo (new SpecificTaxQuery ());
            var query = db.QueryOver<SpecificTax> ();
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var page = Settings.PaginationSettings.Page;

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike ("%" + bound.Name + "%");
            }

            if (bound.Code > 0) {
                query.Where (i => i.Code >= bound.Code || i.Code <= bound.Code);
            }

            var items = query.PageResults (page, rpp);
            return Negotiate.WithOkAndList (items, Context.Request.Url);
        }
    }
}
