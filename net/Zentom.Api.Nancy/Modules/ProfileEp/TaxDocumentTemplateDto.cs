﻿using System;

namespace Zentom.Api.Modules.ProfileEp
{
    public class TaxDocumentTemplateDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string DocumentType { get; set; }

        public string FilePath { get; set; }

        public DateTime CreateAt { get; set; }

        public object TaxDocumentData { get; set; }
    }
}