﻿using Dobelik.Logging;
using Dobelik.Logging.Interfaces;
using Dobelik.Utils.Identity;
using Dobelik.Utils.Password.Enums;
using Dobelik.Utils.Password.Exceptions;
using Dobelik.Utils.Password.Interfaces;
using NHibernate;
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using Zentom.Core.Entities;
using Zentom.Core.Interfaces;
using Zentom.Core.Interfaces.Services;
using Zentom.Extensions.NHibernate;
using Zentom.I18n;

namespace Zentom.Services
{
    public class AccountSvc
        : AbstractSvc<Account>, IAccountSvc
    {
        #region Fields

        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(AccountSvc));

        private readonly IPasswordHasher _passwordHasher;

        private readonly Random _random;

        private readonly INationalIdentity _idTools;

        #endregion

        #region Ctor

        public AccountSvc(Lazy<ISession> session,
            IPasswordHasher passwordHasher, Random random, INationalIdentity idTools)
            : base(session)
        {
            _passwordHasher = passwordHasher;
            _random = random;
            _idTools = idTools;
        }

        #endregion

        public Account FindByLogin(string login, Guid enterpriseId)
        {
            try
            {
                return Session.QueryOver<Account>()
                    .Where(i => i.Login == login && i.Enterprise.Id == enterpriseId)
                    .SingleOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error("Failed to retrieve Account '{0}'. {1}", login, ex);
                Errors.Add(new ModelValidation("Exception", Error.Exception));
            }

            return null;
        }

        public IPagedResult<Account> FindByLoginLike(string login, Guid enterpriseId, int page = 0, int size = 15,
            Expression<Func<Account, object>> orderBy = null, bool asc = true)
        {
            try
            {
                var queryOver = Session.QueryOver<Account>()
                    .WhereRestrictionOn(i => i.Login)
                    .IsInsensitiveLike(login)
                    .Where(i => i.Enterprise.Id == enterpriseId);
                SetOrderBy(queryOver, orderBy, asc);

                return queryOver.PageResults(page, size);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to retrieve Accounts that match '{0}'. {1}", login, ex);
                Errors.Add(new ModelValidation("Exception", Error.Exception));
            }

            return null;
        }

        public object Create(Account account, string password)
        {
            HashPassword(account, password);
            return base.Create(account);
        }

        public override bool Exists(Account account)
        {
            try
            {
                return Exists(i => i.Login == account.Login && i.Enterprise.Id == account.Enterprise.Id);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to verify Account uniqueness.", ex);
                Errors.Add(new ModelValidation("Exception", Error.Exception));
            }

            return false;
        }

        public override void ValidateModel(Account entity, bool isForUpdate = false)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.AccountCreator == null)
            {
                Errors.Add(new ModelValidation("AccountCreator", Error.AccountCreator));
            }

            if (entity.AccountOwner == null)
            {
                Errors.Add(new ModelValidation("AccountOwner", Error.AccountOwner));
            }

            if (entity.Login == null || entity.Login != null && entity.Login.Length <= 0)
            {
                Errors.Add(new ModelValidation("Login", Error.LoginNameRequired));
            }
            else
            {
                if (!isForUpdate && Exists(entity))
                {
                    Errors.Add(new ModelValidation("Login", Error.NonUniqueLogin));
                }
            }
        }

        public Account ValidateCredentials(string login, string password, string enterpriseTaxId)
        {
            enterpriseTaxId = _idTools.CleanId(enterpriseTaxId);
            var account = Session.QueryOver<Account>()
                .Where(i => i.Login == login)
                .JoinQueryOver(i => i.Enterprise)
                .Where(i => i.TaxId == enterpriseTaxId)
                .SingleOrDefault();
            
            if (account != null)
            {
                if (account.IsEnabled)
                {
                    if (!CheckPassword(account, password))
                    {
                        account = null;
                    }
                }
                else
                {
                    Errors.Add(new ModelValidation("", Error.AccountDisabled));
                }
            }
            else
            {
                Log.Info("Account not found: [login = {0}, enterprise tax id = {1}].", login, enterpriseTaxId);
                Errors.Add(new ModelValidation("", Error.InvalidCredentials));
            }

            return account;
        }

        public bool ChangePassword(Guid id, string oldPassword, string newPassword)
        {
            var account = Get(id);

            if (CheckPassword(account, oldPassword))
            {
                HashPassword(account, newPassword);
                Update(account);
            }

            return false;
        }

        #region Utils

        private static byte[] SerializeHashInfo(IHashInfo info)
        {
            byte[] retVal;

            using (var memStream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(memStream, info);
                retVal = memStream.GetBuffer();
            }

            return retVal;
        }

        private static IHashInfo DeserializeHashInfo(byte[] array)
        {
            IHashInfo hashInfo;

            using (var memStream = new MemoryStream(array))
            {
                hashInfo = (IHashInfo)new BinaryFormatter().Deserialize(memStream);
            }

            return hashInfo;
        }

        private void RandomizeHashInfo(IHashInfo hashInfo)
        {
            var values = Enum.GetValues(typeof(SaltPosition));
            hashInfo.SetProperties(_random.Next(50, 100),
                (SaltPosition)values.GetValue(_random.Next(1, values.Length)));
        }

        private bool CheckPassword(Account account, string password)
        {
            if (account != null)
            {
                _passwordHasher.HashInformation = DeserializeHashInfo(account.Salt);

                try
                {
                    if (!_passwordHasher.CompareHashes(account.Password, password))
                    {
                        Log.Error("Login failed: either the password or the user are invalid.");
                        Errors.Add(new ModelValidation("InvalidCredentials", Error.InvalidCredentials));
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (ArgumentNullException ex)
                {
                    Log.Error("Failed to validate login.", ex);
                    Errors.Add(new ModelValidation("Exception", Error.Exception));
                }
                catch (InvalidSaltPositionException ex)
                {
                    Log.Error("Failed to validate login.", ex);
                    Errors.Add(new ModelValidation("Exception", Error.Exception));
                }
            }

            return false;
        }

        public void HashPassword(Account account, string password)
        {
            Log.Debug("HashPassword(account, password = {0})", password);
            try
            {
                RandomizeHashInfo(_passwordHasher.HashInformation);
                var tmp = _passwordHasher.GenerateSalt();
                account.Password = _passwordHasher.HashPassword(password, tmp);

                tmp = SerializeHashInfo(_passwordHasher.HashInformation);
                account.Salt = tmp;
                Log.Debug("Account.password.length: {0}", account.Password?.Length);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to hash password.", ex);
                Errors.Add(new ModelValidation("Exception", Error.Exception));
            }
        }

        public bool HasPermission(Guid accountId, string permission)
        {
            Log.Debug("HasPermission(accountId = '{0}', permission = '{1}')", accountId, permission);
            var accountRole = Session.QueryOver<AccountRole>()
                .Where(i => i.Account.Id == accountId)
                .List();

            if (!(accountRole?.Count > 0)) return false;

            try
            {
                return accountRole.Single(i => i.Role.RolePermissions.Single(j => j.Permission.Name == permission) != null) != null;
            }
            catch(InvalidOperationException) { }

            return false;
        }

        #endregion
    }
}
