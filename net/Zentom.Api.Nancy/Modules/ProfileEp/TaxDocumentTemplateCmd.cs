﻿namespace Zentom.Api.Modules.ProfileEp
{
    public class TaxDocumentTemplateCmd
    {
        public string Name { get; set; }

        public string DocumentType { get; set; }

        public object Content { get; set; }
    }
}