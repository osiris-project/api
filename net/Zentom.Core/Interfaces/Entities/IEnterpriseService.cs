﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseService
        : IVersioned
    {
        IEnterprise Enterprise { get; set; }

        IService Service { get; set; }
    }

    public interface IEnterpriseServiceId
    {
        IEnterprise Enterprise { get; set; }

        IService Service { get; set; }
    }
}
