﻿using System;
using System.Collections.Generic;

namespace Osiris.Api.Modules.Auth
{
    public class UserDto
    {
        public Guid AccountId { get; set; }

        public Guid TenantId { get; set; }

        public string LoginName { get; set; }

        public string NationalId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Dictionary<string, IEnumerable<string>> Credentials { get; set; }
        
        public string TenantDisplayName { get; set; }

        public string TenantName { get; set; }

        public string TenantTaxId { get; set; }

        public override string ToString()
        {
            return $"AccountId: {AccountId}, EnterpriseId: {TenantId}, LoginName: {LoginName}, EnterpriseDisplayName: {TenantDisplayName}";
        }
    }
}
