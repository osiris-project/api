﻿using System.Collections.Generic;
using System.Linq;

namespace Zentom.Core.Entities
{
    public class EnterpriseAddress
        : CompanyAddress
    {
        #region Properties

        public virtual Enterprise Enterprise
        {
            get { return (Enterprise) Company; }
            set { Company = value; }
        }

        public virtual ISet<EnterpriseAddressType> EnterpriseAddressTypes
        {
            get
            {
                if (CompanyAddressTypes == null)
                {
                    CompanyAddressTypes = new HashSet<EnterpriseAddressType>();
                }
                return (ISet<EnterpriseAddressType>) CompanyAddressTypes;
            }
            set { CompanyAddressTypes = value; }
        }

        #endregion

        public EnterpriseAddress() { }

        public EnterpriseAddress(string line1, string line2, Commune commune, Company company,
            IEnumerable<CompanyAddressType> companyAddressTypes)
            : base(line1, line2, commune, company, companyAddressTypes)
        {
        }

        public virtual void AddAddressType(AddressType addressType)
        {
            if (EnterpriseAddressTypes.Any(i => i.AddressType.Id == addressType.Id))
                return;

            EnterpriseAddressTypes.Add(new EnterpriseAddressType(this, addressType));
        }

        public virtual void RemoveAddressType(AddressType addressType)
        {
            if (EnterpriseAddressTypes.All(i => i.AddressType.Id != addressType.Id))
                return;
            EnterpriseAddressTypes.Remove(new EnterpriseAddressType(this, addressType));
        }
    }
}