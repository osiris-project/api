package com.valorti.osiris.rest.auth

import com.valorti.osiris.rest.AbstractController

/**
 * Class responsible for authentication requests.
 * @author Nicolas Mancilla
 * @version 1.0
 * @since 0.0.1
 */
class AuthenticationController
    extends AbstractController {



    @Override
    void defineEndpoints() {

    }


}
