﻿using System;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IPayment
        : IUnique<long>, IVersioned, IRowGuid
    {
        decimal Amount { get; set; }

        DateTime ExpireDate { get; set; }
        
        IPaymentType PaymentType { get; set; }
        
        ITaxDocument TaxDocument { get; set; }
    }
}
