﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Zentom.Database.Conventions
{
    class ZentomReferencesConvention
        : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            instance.Not.Nullable();
        }
    }
}
