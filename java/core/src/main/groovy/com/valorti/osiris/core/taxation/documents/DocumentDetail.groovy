/**
 * Copyright (c) Mar 10, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Embedded
import java.time.LocalDate

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class DocumentDetail
    extends BigIntegerEntity {
    /**
     * Specifies the type of the service/product in regards of the
     * taxes to be applied.
     * <ul>
     *     <li>TAX_FREE: the item is exempt of taxes</li>
     *     <li>NON_BILLABLE: the item cannot be charged, either because it's been
     *     already charged, or
     *
     * @author Nicolas Mancilla <nmancilla@valorti.com>
     * @version 0.0.1
     * @since 0.0.1
     */
    enum ItemModifier {
        NONE(0),
        TAX_FREE(1),
        NON_BILLABLE(2),
        DEPOSIT_GUARANTEE(3),
        NO_SALE(4),
        TO_LOWER(5),
        NEGATIVE_NON_BILLABLE(6)

        final int value

        ItemModifier(int value) {
            this.value = value
        }

        static ItemModifier fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }
    }

    @Transient
    private static final long serialVersionUID = 1L

    @Embedded
    final Retainer retainer

    @Column(nullable = true, length = 10)
    final String documentTypeToLiquidate

    @Column(nullable = false)
    final ItemModifier modifier

    @Column(nullable = false, length = 80)
    final String name

    @Column(nullable = false, length = 1000)
    final String description

    @Column(precision = 13, scale = 4, nullable = false)
    final Double referenceQuantity

    @Column(nullable = false, length = 10)
    final String referenceUnit

    @Column(precision = 13, scale = 4, nullable = false)
    final Double referencePrice

    @Column(precision = 13, scale = 4, nullable = false)
    final Double quantity

    @Column(columnDefinition = 'DATE', nullable = true)
    final LocalDate manufacturedDate

    @Column(columnDefinition = 'DATE', nullable = true)
    final LocalDate expiryDate

    @Column(nullable = false, length = 4)
    final String measurementUnit

    @Column(precision = 13, scale = 4, nullable = false)
    final Double unitPrice

    @Column(precision = 3, scale = 2, nullable = false)
    final Double discountPercent

    @Column(precision = 13, scale = 4, nullable = false)
    final Double discountAmount

    @Column(precision = 3, scale = 2, nullable = false)
    final Double surchargePercent

    @Column(precision = 13, scale = 4, nullable = false)
    final Double surchargeAmount

    @Column(precision = 13, scale = 4, nullable = false)
    final Double lineTotal

    @Column(nullable = true, length = 13)
    private final CharSequence additionalTaxCodes

    @ManyToOne
    Document document

    /**
     * Default constructor.
     */
    protected DocumentDetail() {
        this.modifier = ItemModifier.NONE
        this.name = ''
        this.description = ''
        this.referencePrice = 0
        this.referenceQuantity = 0
        this.referenceUnit = ''
        this.documentTypeToLiquidate = ''
        this.surchargeAmount = 0
        this.surchargePercent = 0
        this.discountAmount = 0
        this.discountPercent = 0
        this.unitPrice = 0
        this.measurementUnit = ''
        this.expiryDate = null
        this.manufacturedDate = null
        this.quantity = 1
        this.lineTotal = 0
        this.additionalTaxCodes = ''
    }

    /**
     * @see DocumentDetail
     */
    DocumentDetail(String name, ItemModifier modifier, String description,
                   Double quantity, Double unitPrice) {
        this(null, modifier, name, description, null, 0,
            '', 0, quantity, null, null,
            '', unitPrice, 0, 0, 0, 0, [])
    }

    /**
     *
     * @param documentTypeToLiquidate Only if the document is intended to settle another document.
     * @param modifier See {@link ItemModifier} for an explanation of the values.
     * @param name The name of the product or service that's being billed.
     * @param description Optional description of the product/service.
     * @param retainer Information regarding the retainer of the taxes
     * @param referenceQuantity
     * @param referenceUnit
     * @param referencePrice
     * @param quantity
     * @param manufacturedDate
     * @param expiryDate
     * @param measurementUnit
     * @param unitPrice
     * @param discountPercent
     * @param discountAmount
     * @param surchargePercent
     * @param surchargeAmount
     * @param additionalTaxCodes
     * @throws IllegalArgumentException
     */
    DocumentDetail(String documentTypeToLiquidate, ItemModifier modifier, String name, String description,
                   Retainer retainer, Double referenceQuantity, String referenceUnit, Double referencePrice,
                   Double quantity, LocalDate manufacturedDate, LocalDate expiryDate,
                   String measurementUnit, Double unitPrice, Double discountPercent, Double discountAmount,
                   Double surchargePercent, Double surchargeAmount, List<CharSequence> additionalTaxCodes)
        throws IllegalArgumentException {
        if (!name) {
            throw new IllegalArgumentException('name')
        }
        if (!quantity) {
            throw new IllegalArgumentException('quantity')
        }
        this.documentTypeToLiquidate = documentTypeToLiquidate
        this.modifier = modifier
        this.name = name.trim()
        this.description = description?.trim()
        this.retainer = retainer
        this.referenceQuantity = referenceQuantity
        this.referenceUnit = referenceUnit
        this.referencePrice = referencePrice
        this.quantity = quantity
        this.manufacturedDate = manufacturedDate
        this.expiryDate = expiryDate
        this.measurementUnit = measurementUnit
        this.unitPrice = unitPrice
        this.discountPercent = discountPercent
        this.discountAmount = discountAmount
        this.surchargePercent = surchargePercent
        this.surchargeAmount = surchargeAmount
        this.additionalTaxCodes = additionalTaxCodes ? additionalTaxCodes.join('|') : ''
        this.document = document
        if (modifier == ItemModifier.NO_SALE || modifier == ItemModifier.TO_LOWER) {
            this.lineTotal = 0
        } else {
            this.lineTotal = ((unitPrice * quantity) - discountAmount + surchargeAmount)
        }
    }

    List<String> getAdditionalTaxCodes() {
        return this.additionalTaxCodes.tokenize('|')
    }

    boolean hasCode(String code) {
        return this.getAdditionalTaxCodes().contains(code)
    }
}
