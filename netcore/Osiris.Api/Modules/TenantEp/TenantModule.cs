﻿using System;
using System.Linq;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api.Modules.TenantEp {
    public class TenantModule : TenantApi {
        private static readonly ILogger log = LoggerFactory.GetLogger (typeof (TenantModule));

        public TenantModule (ISessionFactory factory) : base ("tenant-profile", factory) {
            this.Get (BasePath + "/economic-activities/", TenantEconomicActivities);
            this.Get (BasePath + "/comercial-activities/", TenantComercialActivities);

            // Alerts
            this.Get (BasePath + "/folio-capacity-alerts/", FolioRangeAlerts);

            // Folio Ranges
            this.Get (BasePath + "/folio-ranges/", FolioRangeList);
            this.Post<FolioRangeCmd> (BasePath + "/folio-ranges/", CreateFolioRange);
            this.Patch<Command> (BasePath + "/folio-ranges/{id:guid}/enable/", EnableFolioRange);
            this.Patch<Command> (BasePath + "/folio-ranges/{id:guid}/disable/", DisableFolioRange);

            // Enabled Modules
            this.Get (BasePath + "/modules/", ModuleList);
            this.Get<ModuleQuery> (BasePath + "/modules/{module:string}", HasModule);

            // Addresses
            this.Get (BasePath + "/legal-address/", GetTenantLegalAddress);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator TenantEconomicActivities (dynamic args) {
            EconomicActivity economicActivity = null;
            TenantEconomicActivityDto dto = null;
            var items = db.QueryOver<TenantEconomicActivity> ()
                .Left.JoinAlias (i => i.EconomicActivity, () => economicActivity)
                .Where (i => i.Tenant == currentTenant)
                .SelectList (i =>
                    i.Select (() => economicActivity.Id).WithAlias (() => dto.Id)
                    .Select (() => economicActivity.Name).WithAlias (() => dto.Name)
                    .Select (() => economicActivity.Code).WithAlias (() => dto.Code)
                )
                .OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<TenantEconomicActivityDto> ())
                .PaginateResults<TenantEconomicActivity, TenantEconomicActivityDto> ();

            return Negotiate.WithOkAndList (items, Request.Url);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator TenantComercialActivities (dynamic args) {
            ComercialActivity comercialActivity = null;
            TenantEconomicActivityDto dto = null;
            var items = db.QueryOver<TenantComercialActivity> ()
                .Left.JoinAlias (i => i.ComercialActivity, () => comercialActivity)
                .Where (i => i.Tenant == currentTenant)
                .SelectList (i =>
                    i.Select (() => comercialActivity.Id).WithAlias (() => dto.Id)
                    .Select (() => comercialActivity.Name).WithAlias (() => dto.Name)
                )
                .OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<TenantComercialActivityDto> ())
                .PaginateResults<TenantComercialActivity, TenantComercialActivityDto> ();

            return Negotiate.WithOkAndList (items, Request.Url);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator FolioRangeAlerts () {
            try {
                var alertPercent = GetFolioRangePercentAlert ();
                log.Debug ("Alert percent: {0}", alertPercent);
                var list = db.QueryOver<FolioRange> ()
                    .SelectList (i => i
                        .Select (j => j.DocumentType)
                        .Select (j => j.RangeEnd)
                        .Select (j => j.RangeStart)
                        .Select (j => j.CurrentFolio)
                        .Select (j => j.Status))
                    .Where (i => i.Tenant == currentTenant &&
                        i.IsCurrentRange)
                    .List<object[]> ();
                return
                Negotiate.WithOkAndModel (
                    list.Where (
                        i =>
                        GetFolioRangeUsagePercent ((long) i[3], (long) i[1], (long) i[2]) <= (100 - alertPercent))
                    .Select (i => new {
                        documentType = (string) i[0],
                            foliosLeft = ((((long) i[1]) - ((long) i[3])) + 1),
                            status = (short) i[4]
                    }));
            } catch (Exception ex) {
                log.Error ("Failed to retrieve folio capacity alerts.", ex);
            }

            return Negotiate.WithOk ();
        }

        [Permission (Permission = "folio-range-create")]
        public Negotiator CreateFolioRange (FolioRangeCmd cmd) {
            var entity = db.QueryOver<FolioRange> ()
                .Where (
                    i =>
                    i.Tenant == currentTenant && i.RangeStart == cmd.RangeStart &&
                    i.DocumentType == cmd.DocumentType)
                .Take (1).SingleOrDefault ();

            if (entity != null) {
                return Negotiate.WithDuplicateItem (new [] { "rangeStart", "documentType" });
            }

            // If this will be the current range, disable the current one.
            if (cmd.IsCurrentRange) {
                ResetCurrentRange (cmd.DocumentType);
            }
            entity = new FolioRange (currentTenant, cmd.DocumentType, cmd.RangeStart, cmd.RangeEnd, cmd.IsCurrentRange, "", cmd.RangeStart, cmd.RequestDate, RangeStatus.Enabled);
            db.Save (entity);
            return Negotiate.WithPostSuccess (entity.Id, Request.Url);
        }

        [Permission (Permission = "folio-range-read")]
        public Negotiator FolioRangeList () {
            var bound = this.BindTo (new FolioRangeQuery ());
            var query = db.QueryOver<FolioRange> ()
                .Where (i => i.Tenant == currentTenant);

            if (!string.IsNullOrWhiteSpace (bound.DocumentType)) {
                query.Where (i => i.DocumentType == bound.DocumentType);
            }

            FolioRangeDto dto = null;
            var items = query.SelectList (i => i.Select (j => j.DocumentType).WithAlias (() => dto.DocumentType)
                    .Select (j => j.RangeStart).WithAlias (() => dto.RangeStart)
                    .Select (j => j.RangeEnd).WithAlias (() => dto.RangeEnd)
                    .Select (j => j.IsCurrentRange).WithAlias (() => dto.IsCurrentRange)
                    .Select (j => j.CurrentFolio).WithAlias (() => dto.CurrentFolio)
                    .Select (j => j.Status).WithAlias (() => dto.Status))
                .OrderBy (Settings.OrderBy)
                .ProjectInto<FolioRange, FolioRangeDto> ()
                .PaginateResults<FolioRange, FolioRangeDto> (Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);

            return Negotiate.WithOkAndList (items, Request.Url);
        }

        [Permission (Permission = "folio-range-update")]
        public Negotiator DisableFolioRange (Command cmd) {
            var entity = db.QueryOver<FolioRange> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant && i.IsCurrentRange)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithOk ();
            }

            entity.IsCurrentRange = false;
            entity.Status = RangeStatus.Disabled;
            return Negotiate.WithOk ();
        }

        [Permission (Permission = "folio-range-update")]
        public Negotiator EnableFolioRange (Command cmd) {
            var entity = db.QueryOver<FolioRange> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant && i.IsCurrentRange)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithOk ();
            }

            if (entity.IsUsed || entity.Status == RangeStatus.Disabled) {
                return Negotiate.WithConflict (I18n.Error.FolioRangeUsed);
            }

            entity.IsCurrentRange = true;
            return Negotiate.WithOk ();
        }

        [Permission (RequiresLogin = true)]
        public Negotiator ModuleList () {
            log.Info ("Retrieving tenant '{0}' module list.", currentTenant.TaxId);
            TenantModuleDto dto = null;
            Module module = null;
            var list = db.QueryOver<Core.Entities.TenantModule> ()
                .Where (i => i.Tenant == currentTenant)
                .Left.JoinAlias (i => i.Module, () => module)
                .SelectList (i =>
                    i.Select (j => module.Name).WithAlias (() => dto.Name)
                    .Select (j => module.Description).WithAlias (() => dto.Description)
                    .Select (j => module.Id).WithAlias (() => dto.Id)
                    .Select (j => j.Settings).WithAlias (() => dto.Settings))
                .OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<TenantModuleDto> ())
                .PaginateResults<Core.Entities.TenantModule, TenantModuleDto> (Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            log.Debug ("Enabled Module count: {0}", list.Items.Count);
            return Negotiate.WithOkAndList (list, Request.Url);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator GetTenantLegalAddress () {
            TenantAddressType tenantAddressTypes = null;
            AddressType addressType = null;
            Commune commune = null;
            City city = null;
            var entity = db.QueryOver<TenantAddress> ()
                .Inner.JoinAlias (i => i.TenantAddressTypes, () => tenantAddressTypes)
                .Inner.JoinAlias (i => tenantAddressTypes.AddressType, () => addressType)
                .Inner.JoinAlias (i => i.Commune, () => commune)
                .Left.JoinAlias (i => commune.City, () => city)
                .Where (i => addressType.Name == "legal" && i.Tenant == currentTenant)
                .SelectList (i => i.Select (j => j.Id)
                    .Select (j => j.Line1)
                    .Select (j => j.Line2)
                    .Select (j => commune.Name)
                    .Select (j => city.Name))
                .Take (1)
                .SingleOrDefault<object[]> ();

            if (entity == null) {
                return Negotiate.WithNotFound (I18n.Error.NoLegalAddressDefined);
            }

            var dto = new {
                id = (Guid) entity[0],
                line1 = (string) entity[1],
                line2 = (string) entity[2],
                commune = (string) entity[3],
                city = (string) entity[4]
            };
            return Negotiate.WithOkAndModel (dto);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator HasModule (ModuleQuery query) {
            if (string.IsNullOrWhiteSpace (query.Name)) {
                return Negotiate.WithNotFound ();
            }
            var module = HasModule (query.Name);
            return module != null ? Negotiate.WithOk () : Negotiate.WithNotFound ();
        }

        protected Core.Entities.TenantModule HasModule (string moduleName) {
            Module module = null;
            return db.QueryOver<Core.Entities.TenantModule> ()
                .Left.JoinAlias (i => i.Module, () => module)
                .Where (i => i.Tenant == currentTenant && module.Name == moduleName)
                .Take (1)
                .SingleOrDefault ();
        }

        private void ResetCurrentRange (string documentType) {
            if (string.IsNullOrWhiteSpace (documentType))
                throw new ArgumentNullException (nameof (documentType));

            var currentRanges = db.QueryOver<FolioRange> ()
                .Where (i => i.Tenant == currentTenant && i.DocumentType == documentType && i.IsCurrentRange)
                .List ();
            foreach (var range in currentRanges) {
                range.IsCurrentRange = false;
                range.Status = RangeStatus.Disabled;
            }
        }

        private static decimal GetFolioRangeUsagePercent (long currentFolio, long rangeEnd, long rangeStart) {
            var total = (rangeEnd - rangeStart + 1);
            var current = (rangeEnd - currentFolio + 1) * 100M;
            return (current / total);
        }
    }
}
