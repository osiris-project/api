﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Osiris.Api.Error;
using Osiris.Api.Modules;
using Osiris.Core.Exceptions;
using Slf4Net;

namespace Osiris.Api.Extensions.Nancy {
    public static class NancyContextHelper {
        private static readonly ILogger Log = LoggerFactory.GetLogger (typeof (NancyContextHelper));

        public static void ParseSettings (this ApiModule module) {
            module.Before.AddItemToStartOfPipeline (ctx => ParseSettings (ctx, module));
        }

        private static Response ParseSettings (NancyContext ctx, ApiModule module) {
            SetCulture (ctx);
            var settings = new ApiSettings ();
            SetPagination (ctx, settings);
            settings.IsPrettyPrint = ctx.Request.Query.pretty;
            module.Settings = settings;
            return null;
        }

        private static void SetCulture (NancyContext context) {
            Log.Debug ("Entering SetCulture.");
            Log.Debug ("Setting culture to: {0}", context.Culture);
            Thread.CurrentThread.CurrentCulture = context.Culture;
            Log.Debug ("Current Thread Culture: {0}", Thread.CurrentThread.CurrentCulture);
            Log.Debug ("Leaving SetCulture.");
        }

        private static void SetPagination (NancyContext context, ApiSettings apiSettings) {
            Log.Debug ("Entering ParsePagination.");
            int page, rpp;

            if (!int.TryParse (context.Request.Query.page, out page))
                page = 1;

            if (!int.TryParse (context.Request.Query.rpp, out rpp))
                rpp = 15;

            Log.Info ("Setting pagination to: [page = {0}, items per page = {1}]", page, rpp);
            apiSettings.PaginationSettings = new ApiSettings.Pagination (page, rpp);
            apiSettings.OrderBy = context.Request.Query["orderBy"];
            Log.Debug ("Leaving ParsePagination.");
        }

        public static string GetResourceUrl (this NancyContext context, Guid id) {
            return context.GetResourceUrl (id, null);
        }

        public static string GetResourceUrl (this NancyContext context, Guid baseId, IDictionary<Guid, string> subPaths) {
            try {
                var uri = new Uri (context.Request.Url).Append (baseId.ToString ());
                if (subPaths == null) return uri.ToString ();
                foreach (var item in subPaths) {
                    uri.Append (item.Value).Append (item.Key.ToString ());
                }
                return uri.ToString ();
            } catch (UriFormatException ex) {
                Log.Error ("Failed to retrieve Resource URL.", ex);
            }

            return "";
        }

        public static Negotiator HandleException (this NancyContext context, Exception exception, string defaultMessage) {
            return ParseException (context, exception, defaultMessage);
        }

        private static Negotiator ParseException (NancyContext context, Exception exception, string defaultMessage) {
            var negotiator = new Negotiator (context);
            var model = new ExceptionModel ();
            var status = HttpStatusCode.InternalServerError;
            Log.Error ("The request failed with an exception.", exception);

            if (exception is ItemNotFoundException) {
                model.Code = ModelId.ItemNotFound;
                status = HttpStatusCode.NotFound;
                model.Error = exception.Message;
            } else if (exception is MissingIdException) {
                model.Code = ModelId.MissingId;
                status = HttpStatusCode.ExpectationFailed;
                model.Error = exception.Message;
            }
            // ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
            else if (exception is UniqueKeyException) {
                var ex = (UniqueKeyException) exception;
                model.Code = ModelId.ExistingItem;
                model.Error = string.Format (I18n.Error.ExistingValue, ex.Value);
                status = HttpStatusCode.Conflict;
            } else if (exception is TokenExpiredException) {
                model.Code = ModelId.ExpiredToken;
                status = HttpStatusCode.Unauthorized;
                model.Error = I18n.Error.TokenExpired;
                negotiator.WithHeader ("Location", Constants.BASE_PATH + "auth/");
            } else if (exception is InvalidTokenExpiryDateException ||
                exception is EmptyTokenException ||
                exception is InvalidTokenException) {
                model.Code = ModelId.TamperedToken;
                status = HttpStatusCode.Unauthorized;
                model.Error = I18n.Error.InvalidToken;
                negotiator.WithHeader ("Location", Constants.BASE_PATH + "auth/");
            } else if (exception is ModelBindingException) {
                Log.Error ("Exception probably caused by an incompatible format of the data sent to us and the thread culture.", exception);
                model.Code = ModelId.BadFormattedModel;
                status = HttpStatusCode.Conflict;
                model.Error = string.Format (I18n.Error.ModelBindingException, context.Culture);
            } else {
                model.Code = ModelId.Error;
                if (!string.IsNullOrEmpty (defaultMessage))
                    model.Error = defaultMessage;
                else
                    model.Error = "You just found a bug :O!\n" +
                    "We're very sorry for this, but please contact us at support@zentom.valorti.com to report this incident.";
            }

            negotiator.WithReasonPhrase (model.Error);
            negotiator.WithModel (model);
            return negotiator.WithStatusCode (status);
        }

        public static void EnableCors (this IPipelines pipelines) {
            pipelines.AfterRequest.AddItemToEndOfPipeline (ctx => {
                Log.Debug ("Entering EnableCors After Request.");
                if (!ctx.Request.Headers.Keys.Contains ("Origin")) {
                    return;
                }
                var origins = "" + string.Join (" ", ctx.Request.Headers["Origin"]);
                ctx.Response.Headers["Access-Control-Allow-Origin"] = origins;
                if (ctx.Request.Method != "OPTIONS") {
                    return;
                }
                // handle CORS preflight request
                ctx.Response.Headers["Access-Control-Allow-Methods"] =
                    "GET, POST, PUT, DELETE, PATCH, OPTIONS";
                if (!ctx.Request.Headers.Keys.Contains ("Access-Control-Request-Headers")) {
                    Log.Debug ("No 'Access-Control-Request-Headers' key found.");
                    return;
                }
                var allowedHeaders = "" + string.Join (
                    ", ", ctx.Request.Headers["Access-Control-Request-Headers"]);
                ctx.Response.Headers["Access-Control-Allow-Headers"] = allowedHeaders;
                Log.Debug ("Leaving EnableCors After Request.");
            });
        }

        /// <summary>
        /// Attempts to detect if the content type is JSON.
        /// Supports:
        ///   application/json
        ///   text/json
        ///   application/vnd[something]+json
        /// Matches are case insentitive to try and be as "accepting" as possible.
        /// </summary>
        /// <param name="contentType">Request content type</param>
        /// <returns>True if content type is JSON, false otherwise</returns>
        public static bool IsJsonType (string contentType) {
            Log.Debug ("Entering IsJsonType.");

            if (string.IsNullOrEmpty (contentType)) {
                return false;
            }

            Log.Debug ("Content type: {0}", contentType);
            var contentMimeType = contentType.Split (';') [0];

            return contentMimeType.Equals ("application/json", StringComparison.InvariantCultureIgnoreCase) ||
                contentMimeType.Equals ("text/json", StringComparison.InvariantCultureIgnoreCase) ||
                (contentMimeType.StartsWith ("application/vnd", StringComparison.InvariantCultureIgnoreCase) &&
                    contentMimeType.EndsWith ("+json", StringComparison.InvariantCultureIgnoreCase));
        }

        public static bool IsJsonType (MediaRange range) {
            if (range.IsWildcard) return true;
            if (range.Type != "application" && range.Type != "text") return false;
            return range.Subtype == "json" || ((string) range.Subtype).EndsWith ("json");
        }

        public static string RemoteAddress (this NancyContext context) {
            var list = context.Request.Headers.SingleOrDefault (i => i.Key == "Remote Address").Value;

            if (list != null) return list.SingleOrDefault () ?? "";

            Log.Warn ("'Remote Address' headers missing.");
            return "";
        }
    }
}
