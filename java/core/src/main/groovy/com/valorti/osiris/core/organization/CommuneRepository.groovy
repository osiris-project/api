/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.organization

import com.valorti.osiris.core.UUIDRepository

import java.util.stream.Stream

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
interface CommuneRepository
    extends UUIDRepository<Commune> {

    Commune findOneByNameAndCityName(String name, String cityName)

    Commune findOneByNameAndCity(String name, City city)

    Stream<Commune> findAllByName(String name)
}
