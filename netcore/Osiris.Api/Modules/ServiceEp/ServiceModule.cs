﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api.Modules.ServiceEp {
    public class ServiceModule : CRUDLModule<Command, ServiceUpdateCmd, ServiceUpdateCmd> {
        private readonly ILogger log = LoggerFactory.GetLogger (typeof (ServiceModule));

        public ServiceModule (ISessionFactory factory) : base ("services", factory) { }

        [Permission (Permission = "service-create")]
        public override Negotiator Create (ServiceUpdateCmd cmd, dynamic args) {
            log.Debug ("Entering CreateService.");
            log.Debug ("Command: {0}", cmd);
            var found = db.Exists<Service> (i =>
                i.Name == cmd.Name && i.Tenant == currentTenant);
            if (found)
                return Negotiate.RespondWithValidationFailure (string.Format (I18n.Error.ExistingItemName, I18n.Text.Service));
            var service = new Service {
                Name = cmd.Name,
                IsDiscontinued = cmd.IsDiscontinued,
                UnitPrice = cmd.UnitPrice,
                Tenant = currentTenant
            };
            db.Save (service);
            return Negotiate.WithCreated (Context.GetResourceUrl (service.Id), service.Id);
        }

        [Permission (Permission = "service-delete")]
        public override Negotiator Remove (Command cmd) {
            var prod = db.QueryOver<Service> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();
            if (prod == null)
                return Negotiate.RespondWithValidationFailure (string.Format (I18n.Error.ServiceNotFound, cmd.Id));

            db.Delete (prod);
            return Negotiate.WithStatusCode (HttpStatusCode.OK);
        }

        [Permission (Permission = "service-read")]
        public override Negotiator Read (Command cmd) {
            log.Debug ("Trying to locate Service '{0}'.", cmd.Id);
            var prod = db.QueryOver<Service> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .ProjectToObject<Service, ServiceDto> ();

            if (prod == null)
                return Negotiate.RespondWithValidationFailure (
                    string.Format (I18n.Error.ServiceNotFound, cmd.Id));

            return Negotiate.WithModel (prod)
                .WithStatusCode (HttpStatusCode.OK);
        }

        [Permission (Permission = "service-list")]
        public override Negotiator List (dynamic args) {
            log.Debug ("Entering GetServices.");
            var bound = this.BindTo (new ServiceQuery ());
            log.Debug ("Query: {0}", bound);
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var query = db.QueryOver<Service> ()
                .Where (i => i.Tenant == currentTenant);

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike (bound.Name);
            }

            var items = query.OrderBy (Settings.OrderBy)
                .ProjectInto<Service, ServiceDto> ()
                .PaginateResults<Service, ServiceDto> (page, rpp);

            if (items == null)
                return Negotiate.WithNotFound ();

            log.Debug ("Leaving GetServices.");
            return Negotiate.WithOkAndList (items, Request.Url.ToString ());
        }

        [Permission (Permission = "service-update")]
        public override Negotiator Update (ServiceUpdateCmd cmd) {
            log.Debug ("Updating service: {0}", cmd);
            var found = db.QueryOver<Service> ()
                .Where (i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take (1)
                .SingleOrDefault ();
            if (found == null)
                return Negotiate.WithNotFound (cmd.Id, I18n.Error.ServiceNotFound);
            found.Name = cmd.Name;
            found.UnitPrice = cmd.UnitPrice;
            found.IsDiscontinued = cmd.IsDiscontinued;
            db.Update (found);
            return Negotiate.WithStatusCode (HttpStatusCode.OK);
        }
    }
}
