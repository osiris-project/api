/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.taxation.CommercialActivity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization', uniqueConstraints = [
    @UniqueConstraint(columnNames = ['tenant_id', 'comercial_activity_id'])])
class TenantCommercialActivity extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Tenant tenant

    @ManyToOne
    CommercialActivity commercialActivity

    protected TenantCommercialActivity() {
    }

    /**
     * @param tenant
     * @param comercialActivity
     * @throws IllegalArgumentException if the tenant or the commercial
     * activity are null.
     * @since 0.0.1
     */
    TenantCommercialActivity(Tenant tenant, CommercialActivity commercialActivity) {
        if(!tenant) {
            throw new IllegalArgumentException('No tenant provided!')
        }
        if(!commercialActivity) {
            throw new IllegalArgumentException('No commercial activity provided!')
        }
        this.tenant = tenant
        this.commercialActivity = commercialActivity
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((commercialActivity == null) ? 0 : commercialActivity.hashCode())
        result = prime * result + ((tenant == null) ? 0 : tenant.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof TenantCommercialActivity)) {
            return false
        }
        final other = (TenantCommercialActivity) obj
        if(!Objects.equals(this.tenant, other.tenant)) {
            return false
        }
        return Objects.equals(this.commercialActivity, other.commercialActivity)
    }
}
