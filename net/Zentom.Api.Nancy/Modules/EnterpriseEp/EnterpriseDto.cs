﻿namespace Zentom.Api.Modules.EnterpriseEp
{
    public class EnterpriseDto
        : Command
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }

        public short Relationship { get; set; }
    }
}