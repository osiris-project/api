﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    class ContactInfoBaseMap
        : ClassMap<ContactInfo>
    {
        public ContactInfoBaseMap()
        {
            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.ContactType).Default("0");
            Map(p => p.Value).CustomSqlType("NVARCHAR(MAX)");
            UseUnionSubclassForInheritanceMapping();
        }
    }
}
