﻿using System;
using System.Collections.Generic;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentProviderDto
    {
        public Guid SelectedProviderSector { get; set; }

        public IDictionary<string, string> ProviderSectorList { get; set; }
        
        public Guid SelectedClient { get; set; }

        public IDictionary<string, string>  ClientList { get; set; }
    }
}
