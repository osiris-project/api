﻿using System;

namespace Zentom.Core.Entities.Accounting
{
    public class AccountingVoucherLine
        : Entity
    {
        #region Fields

        private int number;

        private string documentType;

        private long folioNumber;

        private string providerTaxId;

        private string description;

        private DateTime emissionDate;

        private DateTime expiryDate;

        private string referencedDocumentType;

        private long referencedDocumentFolio;

        private decimal debit;

        private decimal asset;

        private decimal netAmount;

        private decimal netFixedAsset;

        private decimal totalExemptAmount;

        private decimal totalTaxAmount;

        private decimal totalUnrecoverableAmount;

        private decimal total;

        private string customField1;

        private string customField2;

        private string customField3;

        private AccountingAccount accountingAccount;

        private CostsCenter costsCenter;

        private AccountingVoucher accountingVoucher;

        private Enterprise provider;

        #endregion

        #region Properties

        public virtual int Number
        {
            get { return number; }
            set { number = value; }
        }

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual long FolioNumber
        {
            get { return folioNumber; }
            set { folioNumber = value; }
        }

        public virtual string ProviderTaxId
        {
            get { return providerTaxId; }
            set { providerTaxId = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual DateTime EmissionDate
        {
            get { return emissionDate; }
            set { emissionDate = value; }
        }

        public virtual DateTime ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string ReferencedDocumentType
        {
            get { return referencedDocumentType; }
            set { referencedDocumentType = value; }
        }

        public virtual long ReferencedDocumentFolio
        {
            get { return referencedDocumentFolio; }
            set { referencedDocumentFolio = value; }
        }

        public virtual decimal Debit
        {
            get { return debit; }
            set { debit = value; }
        }

        public virtual decimal Asset
        {
            get { return asset; }
            set { asset = value; }
        }

        public virtual decimal NetAmount
        {
            get { return netAmount; }
            set { netAmount = value; }
        }

        public virtual decimal NetFixedAsset
        {
            get { return netFixedAsset; }
            set { netFixedAsset = value; }
        }

        public virtual decimal TotalExemptAmount
        {
            get { return totalExemptAmount; }
            set { totalExemptAmount = value; }
        }

        public virtual decimal TotalTaxAmount
        {
            get { return totalTaxAmount; }
            set { totalTaxAmount = value; }
        }

        public virtual decimal TotalUnrecoverableAmount
        {
            get { return totalUnrecoverableAmount; }
            set { totalUnrecoverableAmount = value; }
        }

        public virtual decimal Total
        {
            get { return total; }
            set { total = value; }
        }

        public virtual string CustomField1
        {
            get { return customField1; }
            set { customField1 = value; }
        }

        public virtual string CustomField2
        {
            get { return customField2; }
            set { customField2 = value; }
        }

        public virtual string CustomField3
        {
            get { return customField3; }
            set { customField3 = value; }
        }

        public virtual AccountingAccount AccountingAccount
        {
            get { return accountingAccount; }
            set { accountingAccount = value; }
        }

        public virtual CostsCenter CostsCenter
        {
            get { return costsCenter; }
            set { costsCenter = value; }
        }

        public virtual AccountingVoucher AccountingVoucher
        {
            get { return accountingVoucher; }
            set { accountingVoucher = value; }
        }
        
        public virtual Enterprise Provider
        {
            get { return provider; }
            set { provider = value; }
        }

        #endregion

        #region Ctors.

        public AccountingVoucherLine()
        {
        }

        public AccountingVoucherLine(int number, string documentType, long folioNumber, DateTime emissionDate,
            DateTime expiryDate, string referencedDocumentType, long referencedDocumentFolio, decimal debit,
            decimal asset, decimal netAmount, decimal netFixedAsset, decimal totalExemptAmount, decimal totalTaxAmount,
            decimal totalUnrecoverableAmount, decimal total, AccountingAccount accountingAccount, CostsCenter costsCenter,
            AccountingVoucher accountingVoucher, string customField1 = null, string customField2 = null,
            string customField3 = null)
        {
            this.number = number;
            this.documentType = documentType;
            this.folioNumber = folioNumber;
            this.emissionDate = emissionDate;
            this.expiryDate = expiryDate;
            this.referencedDocumentType = referencedDocumentType;
            this.referencedDocumentFolio = referencedDocumentFolio;
            this.debit = debit;
            this.asset = asset;
            this.netAmount = netAmount;
            this.netFixedAsset = netFixedAsset;
            this.totalExemptAmount = totalExemptAmount;
            this.totalTaxAmount = totalTaxAmount;
            this.totalUnrecoverableAmount = totalUnrecoverableAmount;
            this.total = total;
            this.customField1 = customField1;
            this.customField2 = customField2;
            this.customField3 = customField3;
            this.accountingAccount = accountingAccount;
            this.costsCenter = costsCenter;
            this.accountingVoucher = accountingVoucher;
        }

        #endregion
    }
}