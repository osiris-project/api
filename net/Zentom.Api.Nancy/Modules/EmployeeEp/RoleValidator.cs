﻿using System.Linq;
using FluentValidation;
using Zentom.Api.Modules.Validators.Guid;

namespace Zentom.Api.Modules.EmployeeEp
{
    public class RoleValidator
        : AbstractValidator<RoleCommand>
    {
        public RoleValidator()
        {
            RuleFor(i => i.AccountId).SetValidator(new GuidValidator());
            RuleFor(i => i.Roles).Must(i => i.Any());
        }
    }
}