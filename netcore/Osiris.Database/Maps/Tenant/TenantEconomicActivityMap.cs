﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    public class TenantEconomicActivityMap
        : ClassMap<TenantEconomicActivity>
    {
        public TenantEconomicActivityMap()
        {
            Schema("taxation");
            Table("tenant_economic_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("tenant_economic_activity_un");

            References(i => i.EconomicActivity, "economic_activity_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("tenant_economic_activity_un");
        }
    }
}
