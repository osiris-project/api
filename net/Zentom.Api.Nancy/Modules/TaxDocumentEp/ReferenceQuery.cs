﻿namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class ReferenceQuery
    {
        public long Folio { get; set; }

        public string DocumentType { get; set; }
    }
}