﻿namespace Osiris.Core.Entities
{
    public class TenantGroup
        : TenantEntity
    {
        #region Fields

        private Group group;

        #endregion

        #region Properties

        public virtual Group Group
        {
            get { return @group; }
            set { @group = value; }
        }

        #endregion

        public TenantGroup()
        {
        }

        public TenantGroup(Tenant tenant, Group @group)
            : base(tenant)
        {
            this.@group = @group;
        }
    }
}
