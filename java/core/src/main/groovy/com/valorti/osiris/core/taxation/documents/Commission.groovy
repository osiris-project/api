/**
 * Copyright (c) Mar 10, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Embeddable

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Embeddable
class Commission {
    @Column(precision = 13, scale = 4, nullable = false)
    final Double commissionAmount

    @Column(precision = 13, scale = 4, nullable = false)
    final Double commissionExemptAmount

    @Column(precision = 13, scale = 4, nullable = false)
    final Double commissionOtherCharges

    /**
     * Default constructor.
     */
    protected Commission() {
        this.commissionAmount = 0
        this.commissionExemptAmount = 0
        this.commissionOtherCharges = 0
    }
    /**
     * @param commissionAmount
     * @param commissionExemptAmount
     * @param commissionOtherCharges
     * @throws IllegalArgumentException If the amount, exempt amount or other charges are null.
     */
    Commission(Double commissionAmount, Double commissionExemptAmount, Double commissionOtherCharges)
        throws IllegalArgumentException {
        if (!commissionAmount) {
            throw new IllegalArgumentException('commissionAmount')
        }
        if (!commissionExemptAmount) {
            throw new IllegalArgumentException('commissionExemptAmount')
        }
        if (!commissionOtherCharges) {
            throw new IllegalArgumentException('commissionOtherCharges')
        }
        this.commissionAmount = commissionAmount
        this.commissionExemptAmount = commissionExemptAmount
        this.commissionOtherCharges = commissionOtherCharges
    }
}
