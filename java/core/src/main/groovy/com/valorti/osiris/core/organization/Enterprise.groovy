/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.organization

import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.organization.CompanyAddress.AddressType

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.taxation.CommercialActivity
import com.valorti.osiris.core.taxation.EconomicActivity


/**
 * An enterprise can be a client, a provider or both, depending on
 * the received/generated documents.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization')
class Enterprise
    extends Company {

    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Tenant tenant

    @OneToMany(mappedBy = 'enterprise', cascade = CascadeType.ALL, orphanRemoval = true)
    def commercialActivities = new HashSet<EnterpriseCommercialActivity>()

    @OneToMany(mappedBy = 'enterprise', cascade = CascadeType.ALL, orphanRemoval = true)
    def economicActivities = new HashSet<EnterpriseEconomicActivity>()

    @OneToMany(mappedBy = 'enterprise', cascade = CascadeType.ALL, orphanRemoval = true)
    def addresses = new HashSet<EnterpriseAddress>()

    /**
     *
     */
    protected Enterprise() {
    }

    /**
     *
     * @param taxId
     * @param legalName
     * @param displayName
     * @throws ValidationException
     * @throws IllegalArgumentException If the tenant is null.
     * @since 0.0.1
     */
    Enterprise(String taxId, String legalName, String displayName, Tenant tenant)
        throws ValidationException, IllegalArgumentException {
        super(taxId, legalName, displayName)
        if (!tenant) {
            throw new IllegalArgumentException('No tenant provided')
        }
        this.tenant = tenant
    }

    /**
     *
     * @param taxId See
     * @param legalName
     * @param displayName
     * @param email
     * @param phone
     * @param bankAccountNumber
     * @param tenant The tenant this enterprise belongs to. Should
     * be the tenant in the current context.
     * @throws IllegalArgumentException if the tenant is null.
     * @throws ValidationException
     * @since 0.0.1
     */
    Enterprise(String taxId, String legalName, String displayName, String email,
               String phone, String bankAccountNumber, Tenant tenant)
        throws ValidationException, IllegalArgumentException {
        super(taxId, legalName, displayName, email, phone, bankAccountNumber)
        if (!tenant) {
            throw new IllegalArgumentException('''The tenant cannot be null.
                This is an error on our end, so check that the tenant is set and not null''')
        }
        this.tenant = tenant
    }

    /**
     * Adds an economic activity to this enterprise.
     *
     * @param ea The economic activity.
     * @throws IllegalArgumentException if the economic activity is null.
     * @throws ValidationException
     */
    void addEconomicActivity(EconomicActivity ea)
        throws IllegalArgumentException, ValidationException {
        def tmp = new EnterpriseEconomicActivity(this, ea)
        def found = false
        this.economicActivities.each {
            if (it == tmp) {
                found = true
                return
            }
        }
        if (found) {
            return
        }
        this.economicActivities.add(tmp)
    }

    /**
     * Add a commercial activity to the enterprise.
     * @param ca The commercial activity.
     * @throws IllegalArgumentException If the commercial activity is null.
     * @throws ValidationException
     */
    void addCommercialActivity(CommercialActivity ca)
        throws IllegalArgumentException, ValidationException {
        def tmp = new EnterpriseCommercialActivity(this, ca)
        def found = false
        this.commercialActivities.each {
            if (it == tmp) {
                found = true
                return
            }
        }
        if (found) {
            return
        }
        this.commercialActivities.add(tmp)
    }

    /**
     * Adds an address to this enterprise.
     * @param ea
     */
    void addAddress(String line1, String line2, Commune commune, AddressType addressType) {
        def found = false
        this.addresses.each {
            if (it.line1 == line1 && it.commune == commune
                && it.type == addressType) {
                found = true
                return
            }
        }
        if (found) {
            return
        }
        final ea = new EnterpriseAddress(line1, line2, addressType, commune, this)
        this.addresses.add(ea)
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((tenant == null) ? 0 : tenant.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this.is(obj)) {
            return true
        }
        if (!(obj instanceof Enterprise)) {
            return false
        }
        final other = (Enterprise) obj
        if (!Objects.equals(this.tenant, other.tenant)) {
            return false
        }
        return super.equals(obj)
    }
}
