﻿using System;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocumentService
        : ITaxDocumentProductBase
    {
        IService Service { get; set; }
        
        DateTime StartDate { get; set; }
        
        DateTime EndDate { get; set; }
    }

    public interface ITaxDocumentServiceId : ITaxDocumentProductBaseId
    {
        IService Service { get; set; }
    }
}
