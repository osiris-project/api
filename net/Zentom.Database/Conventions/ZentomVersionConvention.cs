﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Zentom.Database.Conventions
{
    public class ZentomVersionConvention
        : IVersionConvention
    {
        public void Apply(IVersionInstance instance)
        {
            instance.Column("row_version");
            instance.Generated.Always();
            instance.Nullable();
        }
    }
}
