﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Database.Maps.TaxDocument
{
    public class TaxDocumentMap
        : ClassMap<Core.Entities.TaxDocument.TaxDocument>
    {
        public TaxDocumentMap()
        {
            Schema("taxation");
            Table("tax_document");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.DocumentNumber).Generated.Always();
            Map(p => p.EmissionPoint).Length(10);
            Map(p => p.FolioNumber).Unique().UniqueKey("tax_document_un");
            Map(p => p.AccountingDate);
            Map(p => p.AccountingTime).CustomType("TimeAsTimeSpan").Nullable();
            Map(i => i.AffectTotal).Precision(13).Scale(4).Default("0.000");
            Map(p => p.ExpireDate).Nullable();
            Map(p => p.PreviousDebtBalance).Precision(13).Scale(4).Default("0.0000");
            Map(p => p.Comments).Length(200).Default("\"\"");
            Map(p => p.DocumentStatus)
                .Length(30)
                .CustomType<GenericEnumMapper<TaxDocumentStatus>>()
                .Default("'Saved'");
            Map(i => i.TaxRate).Precision(4).Scale(3).Default("0.000");
            Map(i => i.ExemptTotal).Precision(13).Scale(4).Default("0.000");
            Map(i => i.ExtraTaxRate).Precision(4).Scale(3).Default("0.000");
            Map(i => i.ExtraTaxCode).Nullable().Default("0");
            Map(i => i.IsRetainedExtraTax).Default("0");
            Map(i => i.DocumentType).Length(10).UniqueKey("tax_document_un");
            Map(i => i.DocumentResponsible).Length(100).Nullable();
            Map(i => i.GenerationDate);
            Map(i => i.TenantAddress).Length(60);
            Map(i => i.TenantCity).Length(20);
            Map(i => i.TenantCommune).Length(20);
            Map(i => i.TenantLegalName).Length(100);
            Map(i => i.TenantComercialActivity, "tenant_comercial_activity_name").Length(60);
            Map(i => i.TenantEconomicActivityCode).Length(6);
            Map(i => i.TenantEconomicActivityName).Length(110);
            Map(i => i.TenantTaxId).Length(20);
            Map(i => i.DocumentTotal, "total").Precision(13).Scale(4).Default("0.0000");

            References(i => i.DefaultPaymentType, "default_payment_type_id")
                .Nullable()
                .Cascade.SaveUpdate();

            References(i => i.DispatchType, "dispatch_type_id")
                .Nullable()
                .Cascade.SaveUpdate();

            References(i => i.Tenant, "tenant_id")
                .UniqueKey("tax_document_un")
                .Cascade.SaveUpdate();

            HasOne(i => i.Client)
                .Fetch.Join()
                .Cascade.All();

            HasMany(i => i.Payments)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tax_document_id");

            HasMany(i => i.ReferencedDocuments)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("parent_id");

            HasMany(i => i.ReferencedBy)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("reference_id");

            HasMany(i => i.TaxDocumentLines)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tax_document_id");

            HasMany(i => i.WeakReferences)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tax_document_id");

            References(i => i.CurrentClient, "enterprise_id")
                .Nullable()
                .Cascade.None();
        }
    }
}
