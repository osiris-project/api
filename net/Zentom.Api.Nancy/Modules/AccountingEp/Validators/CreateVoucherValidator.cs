﻿using FluentValidation;

namespace Zentom.Api.Modules.AccountingEp.Validators
{
    public class CreateVoucherValidator
        : AbstractValidator<CreateVoucherCmd>
    {
        public CreateVoucherValidator()
        {
            RuleFor(i => i.Date).NotNull();
            RuleFor(i => i.Lines).SetCollectionValidator(new AccountingVoucherLineValidator());
        }
    }
}