﻿using System.IO;
using System.Threading.Tasks;

namespace Osiris.Core.Services
{
    public interface ITaxDocumentService
    {
        Task<IFileUploadResult> HandleFileUpload(string fileName, Stream stream);
    }
}
