﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Dobelik.Utils.Password.Enums;
using Dobelik.Utils.Password.Exceptions;
using Dobelik.Utils.Password.Interfaces;
using log4net;
using NHibernate;
using Zentom.Core.Entities;
using Zentom.Core.Services;
using Zentom.I18n;

namespace Zentom.Services
{
    public class AuthenticationService
        : Service, IAuthenticationService
    {
        private readonly IPasswordHasher passwordHasher;

        private readonly Lazy<Random> lazyRandom;

        private Random random => lazyRandom.Value;

        private readonly ILog log;

        public AuthenticationService(ISessionFactory factory, IPasswordHasher passwordHasher,
            Lazy<Random> lazyRandom)
            : base(factory)
        {
            this.passwordHasher = passwordHasher;
            this.lazyRandom = lazyRandom;
            log = LogManager.GetLogger(GetType());
        }

        public Account ValidateCredentials(string login, string password, string tenantTaxId = null)
        {
            log.DebugFormat("Entering ValidateCredentials.");
            var account = session.QueryOver<Account>()
                .Where(i => i.Login == login)
                .Left.JoinQueryOver(i => i.Person)
                .Take(1)
                .SingleOrDefault();
            log.DebugFormat("Account is null? {0}", account == null);
            if (account != null)
            {
                if (CheckPassword(account, password))
                    return account;
                ErrorList.Add(new ErrorModel("password", Error.InvalidCredentials));

            }
            else
            {
                log.InfoFormat("Account not found: [login = {0}, enterprise tax id = {1}].", login, tenantTaxId);
                ErrorList.Add(new ErrorModel("", Error.InvalidCredentials));
            }
            log.DebugFormat("Leaving ValidateCredentials.");
            return null;
        }

        public bool CheckPassword(Account account, string password)
        {
            log.DebugFormat("Entering CheckPassword.");
            if (account == null) return false;
            passwordHasher.HashInformation = DeserializeHashInfo(account.Salt);
            log.DebugFormat("passwordHasher.HashInformation: {0}", passwordHasher.HashInformation);
            try
            {
                if (!passwordHasher.CompareHashes(account.Password, password))
                {
                    log.Error("login failed: either the password or the user are invalid.");
                    ErrorList.Add(new ErrorModel("InvalidCredentials", Error.InvalidCredentials));
                }
                else
                {
                    return true;
                }
            }
            catch (ArgumentNullException ex)
            {
                log.Error("Failed to validate login.", ex);
                ErrorList.Add(new ErrorModel("", Error.Exception));
            }
            catch (InvalidSaltPositionException ex)
            {
                log.Error("Failed to validate login.", ex);
                ErrorList.Add(new ErrorModel("", Error.Exception));
            }

            return false;
        }

        public void HashPassword(Account account, string password)
        {
            log.DebugFormat("Entering HashPassowrd.");
            try
            {
                RandomizeHashInfo(passwordHasher.HashInformation);
                var tmp = passwordHasher.GenerateSalt();
                account.Password = passwordHasher.HashPassword(password, tmp);
                tmp = SerializeHashInfo(passwordHasher.HashInformation);
                account.Salt = tmp;
            }
            catch (Exception ex)
            {
                log.Error("Failed to hash password.", ex);
                ErrorList.Add(new ErrorModel("", Error.Exception));
            }
        }

        public void ChangePassword(Account account, string newPassword)
        {
            log.DebugFormat("Entering ChangePassword.");
            if (account == null)
                throw new ArgumentNullException(nameof(account));
            if (string.IsNullOrWhiteSpace(newPassword))
                throw new ArgumentException(@"Missing password", nameof(newPassword));
            HashPassword(account, newPassword);
        }

        public bool ValidatePassword(byte[] hash, byte[] hashInfo, string password)
        {
            if (hash == null)
                throw new ArgumentNullException(nameof(hash), @"The 'hash' parameter cannot be null.");
            if (hashInfo == null)
                throw new ArgumentNullException(nameof(hashInfo), @"The 'hashInfo' parameter is required to decode the salt information (even if there is not salt!).");
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentNullException(nameof(password), @"The password is required!");
            }
            passwordHasher.HashInformation = DeserializeHashInfo(hashInfo);
            log.DebugFormat("passwordHasher.HashInformation: {0}", passwordHasher.HashInformation);
            if (passwordHasher.CompareHashes(hash, password))
                return true;
            ErrorList.Add(new ErrorModel("PasswordMismatch", Error.InvalidCredentials));
            return false;
        }

        public bool ChangePassword(Account account, string oldPwd, string newPwd)
        {
            var oldPasswordSuccess = ValidatePassword(account.Password, account.Salt, oldPwd);

            if (!oldPasswordSuccess)
                return false;
            HashPassword(account, newPwd);
            return IsValid;
        }

        public Account GetAccount(string loginName, string tenantTaxId = null)
        {
            var entity = session.QueryOver<Account>()
                .Where(i => i.Login == loginName)
                .Take(1)
                .SingleOrDefault();

            return entity;
        }

        public bool RequestResetLink(string loginName, string baseUrl, string ipv4, string ipv6 = "",
            DateTime? expirationDate = null)
        {
            var account = GetAccount(loginName);

            if (string.IsNullOrWhiteSpace(account?.Email))
            {
                log.ErrorFormat("No email is set for the account '{0}'. Can't send email.", account?.Id);
                return false;
            }

            var passwordRequest = new PasswordRequest
            {
                ResetExpireDate = expirationDate ?? DateTime.UtcNow.AddHours(1),
                RequestDate = DateTime.UtcNow,
                Ipv4 = ipv4,
                Ipv6 = ipv6,
                Account = account
            };

            log.InfoFormat("[password_reset_request] For user = {2}, Url = {3}, Request IP = [v4 '{0}', v6 '{1}']", ipv4,
                ipv6, loginName, baseUrl);

            account.PasswordRequests.Add(passwordRequest);
            try
            {
                session.Save(passwordRequest);
            }
            catch (Exception ex)
            {
                log.Error("Failed to save password request to database.", ex);
                return false;
            }

            // Generate reset url:
            var resetUrl = GenerateResetUrl(baseUrl, passwordRequest.Id);
            var sent = new MailService().SendResetLink(account.Email, account.Person.FirstName, resetUrl, ipv4);
            return sent;
        }

        public bool ChangePassword(Guid guid, string newPassword, string ipv4)
        {
            var passwordRequest = session.QueryOver<PasswordRequest>()
                .Where(i => i.Id == guid)
                .Take(1)
                .SingleOrDefault();

            if (passwordRequest == null) return false;
            if (!IsResetAvailable(guid)) return false;

            ChangePassword(passwordRequest.Account, newPassword);
            log.InfoFormat("[password_change] The password for the account '{0}' has been changed from ip '{1}'.",
                passwordRequest.Account.Id, ipv4);
            RemovePasswordRequest(passwordRequest);
            return IsValid;
        }

        public bool IsResetAvailable(Guid id)
        {
            var passwordRequest = session.QueryOver<PasswordRequest>()
                .Where(i => i.Id == id)
                .Take(1)
                .SingleOrDefault();
            if (passwordRequest == null) return false;
            if (DateTime.UtcNow <= passwordRequest.ResetExpireDate) return true;
            RemovePasswordRequest(passwordRequest);
            return false;
        }

        private void RemovePasswordRequest(PasswordRequest passwordRequest)
        {
            passwordRequest.Account.PasswordRequests.Remove(passwordRequest);
            passwordRequest.Account = null;

            try
            {
                log.InfoFormat("Deleting password request {0}", passwordRequest.Id);
                session.Delete(passwordRequest);
            }
            catch (Exception ex)
            {
                log.Error("Failed to remove password request.", ex);
            }
        }

        private static byte[] SerializeHashInfo(IHashInfo info)
        {
            byte[] retVal;

            using (var memStream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(memStream, info);
                retVal = memStream.GetBuffer();
            }

            return retVal;
        }

        private IHashInfo DeserializeHashInfo(byte[] array)
        {
            IHashInfo hashInfo;
            using (var memStream = new MemoryStream(array))
            {
                hashInfo = (IHashInfo)new BinaryFormatter().Deserialize(memStream);
            }
            log.DebugFormat("DeserializeHashInfo - Deserialized: {0}", hashInfo);
            return hashInfo;
        }

        private void RandomizeHashInfo(IHashInfo hashInfo)
        {
            var values = Enum.GetValues(typeof(SaltPosition));
            hashInfo.SetProperties(random.Next(50, 100),
                (SaltPosition)values.GetValue(random.Next(1, values.Length)));
        }


        private static string GenerateResetUrl(string baseUri, Guid guid)
        {
            var result = baseUri.TrimEnd("/".ToCharArray());
            result = string.Concat(result, "/", guid, "/");
            return result;
        }
    }
}