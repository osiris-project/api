﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ICountryLocale
        : IVersioned
    {
        ICountry Country { get; set; }

        ILocale Locale { get; set; }
    }

    public interface ICountryLocaleId
    {
        ICountry Country { get; set; }

        ILocale Locale { get; set; }
    }
}
