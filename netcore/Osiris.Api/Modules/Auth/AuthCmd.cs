﻿namespace Osiris.Api.Modules.Auth
{
    public class AuthCmd
    {
        public string LoginName { get; set; }

        public string Password { get; set; }

        public string EnterpriseTaxId { get; set; }

        public override string ToString()
        {
            return $"LoginName: {LoginName}, Password: {Password}, EnterpriseTaxId: {EnterpriseTaxId}";
        }
    }
}
