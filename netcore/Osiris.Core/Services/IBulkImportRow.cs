﻿namespace Osiris.Core.Services
{
    public interface IBulkImportRow
    {
        string Gloss { get; }

        double NetAmount { get; }

        string DocumentType { get; set; }
    }
}
