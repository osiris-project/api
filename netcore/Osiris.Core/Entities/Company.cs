﻿using System.Collections.Generic;
using System.Linq;

namespace Osiris.Core.Entities
{
    public abstract class Company
        : Entity
    {
        #region Fields

        private string taxId;

        private string displayName;

        private string legalName;

        private string bankAccountNumber;

        private string email;

        private string phone;

        private IEnumerable<CompanyAddress> addresses;

        private IEnumerable<CompanyComercialActivity> comercialActivities;

        private IEnumerable<CompanyEconomicActivity> economicActivities;

        #endregion

        #region Properties

        public virtual string TaxId
        {
            get { return taxId; }
            set { taxId = value; }
        }

        public virtual string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public virtual string LegalName
        {
            get { return legalName; }
            set { legalName = value; }
        }

        public virtual string BankAccountNumber
        {
            get { return bankAccountNumber; }
            set { bankAccountNumber = value; }
        }

        public virtual string Email
        {
            get { return email; }
            set { email = value; }
        }

        public virtual string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        protected virtual IEnumerable<CompanyAddress> Addresses
        {
            get { return addresses; }
            set { addresses = value; }
        }

        protected virtual IEnumerable<CompanyComercialActivity> ComercialActivities
        {
            get { return comercialActivities; }
            set { comercialActivities = value; }
        }

        protected virtual IEnumerable<CompanyEconomicActivity> EconomicActivities
        {
            get { return economicActivities; }
            set { economicActivities = value; }
        }

        #endregion

        protected Company()
        {
        }

        protected Company(string taxId, string displayName, string legalName, string bankAccountNumber, string email,
            string phone, IEnumerable<CompanyAddress> addresses,
            IEnumerable<CompanyComercialActivity> comercialActivities,
            IEnumerable<CompanyEconomicActivity> economicActivities)
        {
            this.taxId = taxId;
            this.displayName = displayName;
            this.legalName = legalName;
            this.bankAccountNumber = bankAccountNumber;
            this.email = email;
            this.phone = phone;
            this.addresses = addresses;
            this.comercialActivities = comercialActivities;
            this.economicActivities = economicActivities;
        }

        protected Company(string taxId, string displayName, string legalName, string bankAccountNumber, string email, string phone,
            IEnumerable<CompanyAddress> addresses)
        {
            this.taxId = taxId;
            this.displayName = displayName;
            this.legalName = legalName;
            this.bankAccountNumber = bankAccountNumber;
            this.email = email;
            this.phone = phone;
            this.addresses = addresses;
        }

        //public virtual void SetSectors(List<Sector> sectors)
        //{
        //    if (sectors != null)
        //    {
        //        var len = EnterpriseSectors.Count - 1;

        //        for (var index = len; index >= 0; index--)
        //        {
        //            var el = EnterpriseSectors.ElementAt(index);
        //            if (sectors.All(i => i.Code != el.Sector.Code)) continue;
        //            EnterpriseSectors.Remove(el);
        //        }

        //        foreach (var sector in sectors)
        //        {
        //            EnterpriseSectors.Add(new EnterpriseSector(this, sector));
        //        }
        //    }
        //    else
        //    {
        //        EnterpriseSectors.Clear();
        //    }
        //}
    }
}
