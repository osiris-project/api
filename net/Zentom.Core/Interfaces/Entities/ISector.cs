﻿using System;
using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ISector
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Code { get; set; }

        byte IsSubjectToTax { get; set; }

        sbyte? TributaryCategory { get; set; }

        ISector ParentSector { get; set; }

        ICollection<IEnterpriseSector> EnterpriseSectors { get; set; }

        ICollection<IProduct> Products { get; set; }

        ICollection<ISector> Sectors { get; set; }

        ICollection<IService> Services { get; set; }
    }
}
