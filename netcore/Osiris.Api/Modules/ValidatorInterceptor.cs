namespace Osiris.Api.Modules
{
    public class ValidatorInterceptor : IInterceptor
    {
        /// <summary>
        /// Intercepts verb method calls and validates the data
        /// before proceeding. It should intercept calls to 
        /// POST, GET, PATCH, DELETE and PUT.
        /// </summary>
        /// <param name="info">Invocation information <see cref="IInvocationInfo" />.</param>
        /// <returns>The result of the method call or a conflict response otherwise.</returns>
        public object Invoke(IInvokationInfo info) {
            if (info.TargetMethod.IsConstructor) {
                return info.Proceed();
            }

            var module = info.Proxy;
            var method = info.TargetMethod;
            // GetArguments[1] is Nancy's action
            // Verb(<>)?(string path, Func<dynamic, T> action, Func<NancyContext, bool> condition = null, string name = null)
            var action = method.GetArguments()[1];
            var modelType = action.GetMethodInfo().GetArguments()[2];

        }
    }
}
