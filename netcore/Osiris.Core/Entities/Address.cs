﻿namespace Osiris.Core.Entities
{
    public abstract class Address
        : Entity
    {
        #region Fields

        private string line1;

        private string line2;

        private Commune commune;

        #endregion

        #region Properties

        public virtual string Line1
        {
            get { return line1; }
            set { line1 = value; }
        }

        public virtual string Line2
        {
            get { return line2 ?? ""; }
            set { line2 = value; }
        }

        public virtual Commune Commune
        {
            get { return commune; }
            set { commune = value; }
        }

        #endregion

        protected Address() { }

        protected Address(string line1, string line2, Commune commune)
        {
            this.line1 = line1;
            this.line2 = line2 ?? "";
            this.commune = commune;
        }
    }
}
