﻿namespace Zentom.Core.Entities.TaxDocument
{
    public class TaxDocumentClientAddressType
        : Entity
    {
        #region Fields

        private TaxDocumentClientAddress address;

        private AddressType addressType;

        #endregion

        #region Properties

        public virtual TaxDocumentClientAddress Address
        {
            get { return address; }
            set { address = value; }
        }

        public virtual AddressType AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        #endregion

        public TaxDocumentClientAddressType()
        {
        }

        public TaxDocumentClientAddressType(TaxDocumentClientAddress address, AddressType addressType)
        {
            this.address = address;
            this.addressType = addressType;
        }
    }
}
