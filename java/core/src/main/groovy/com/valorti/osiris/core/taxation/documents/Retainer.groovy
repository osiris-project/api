/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Embeddable

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Embeddable()
class Retainer {
    @Column(nullable = true, precision = 13, scale = 4)
    final Double meatWorkBaseAmount

    @Column(nullable = true, precision = 13, scale = 4)
    final Double commercializationMargin

    @Column(nullable = true, precision = 13, scale = 4)
    final Double finalConsumerPrice

    /**
     * Default constructor.
     */
    protected Retainer() {
        this.meatWorkBaseAmount = null
        this.commercializationMargin = null
        this.finalConsumerPrice = null
    }

    Retainer(Double meatWorkBaseAmount, Double commercializationMargin, Double finalConsumerPrice) {
        this.meatWorkBaseAmount = meatWorkBaseAmount
        this.commercializationMargin = commercializationMargin
        this.finalConsumerPrice = finalConsumerPrice
    }
}
