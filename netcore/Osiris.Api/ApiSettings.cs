﻿namespace Osiris.Api
{
    public class ApiSettings
    {
        #region Properties

        public Pagination PaginationSettings { get; set; }

        public bool IsPrettyPrint { get; set; }

        public string OrderBy { get; set; }

        /// <summary>
        /// Should the JSON vulnerability protection be included when serializing JSON?
        /// </summary>
        public bool UseJVP { get; set; }

        #endregion

        public ApiSettings()
        {
            PaginationSettings = new Pagination();
            UseJVP = true;
        }

        #region Classes

        public class Pagination
        {
            #region Fields

            #endregion

            #region Properties

            public int Page { get; }

            public int ResultsPerPage { get; }

            #endregion

            public Pagination(int page = 0, int resultsPerPage = 10)
            {
                Page = page;
                ResultsPerPage = resultsPerPage;
            }
        }

        #endregion
    }
}
