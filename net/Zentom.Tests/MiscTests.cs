﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zentom.Services;

namespace Zentom.Tests
{
    [TestClass]
    public class MiscTests
    {
        [TestMethod]
        public void XmlTest()
        {
            var xml = new XmlDocument();
            xml.Load(@"D:\Projects\Zentom\API\Zentom.Tests\dte.xml");
            var setDte = xml.SelectSingleNode("/SetDTE");
            Assert.IsNotNull(setDte);
            //Log("Count: {0}", setDte.InnerXml);

            var dtes = setDte.SelectNodes("//DTE");
            Assert.IsNotNull(dtes);
            Log("DTEs count: {0}", dtes.Count);

            var documents = xml.SelectNodes("/SetDTE/DTE/Documento");
            Assert.IsNotNull(documents);
            Log("Document Count: {0}", documents.Count);

            /*var docs = dte.SelectNodes("/Documento");
            Assert.IsNotNull(docs);
            Log("Docs count: {0}", docs.Count);*/
        }

        [TestMethod]
        public void ListTest()
        {
            var list = new List<KvP>();

            for (var i = 0; i < 10; i++)
            {
                list.Add(new KvP("", "Index: 1"));
            }

            foreach (var item in list)
            {
                Log("'{0}' = '{1}'", item.Key, item.Value);
            }
        }

        [TestMethod]
        public void TestEBillXmlRead()
        {
            var stream = new FileStream(@"D:\761039156_231252_33.xml", FileMode.Open);
            var xml = new XmlDocument();
            xml.Load(stream);

            var documents = xml.SelectNodes("/SetDTE/DTE/Documento");
            Assert.IsNotNull(documents);

            for(var i = 0; i < documents.Count; i++)
            {
                var document = documents.Item(i);
                Assert.IsNotNull(document);

                var header = document.SelectSingleNode("Encabezado");
                Assert.IsNotNull(header);

                var receptor = header.SelectSingleNode("Receptor");
                Assert.IsNotNull(receptor);

                ValidateReceptorSection(receptor);
            }
        }

        [TestMethod]
        public void ChangeLong()
        {
            long t = 1;
            Log("Before change: {0}", t);
            ChangeValue(ref t);
            Log("After change: {0}", t);
        }

        [TestMethod]
        public void ChangeObject()
        {
            var obj = new Obj();
            Log("Before change: {0}", obj.Val);
            ChangeObj(obj);
            Log("After change: {0}", obj.Val);
        }

        [TestMethod]
        public void FileExtensionsTest()
        {
            const string fileName = "something.old";
            Assert.AreEqual(Path.GetExtension(fileName), ".old");
        }

        [TestMethod]
        public void TestResetTokens()
        {
            const int max = 10;
            var byteList = new List<byte[]>();
            var list = new string[max];
            int i;

            for (i = 0; i < max; i++)
            {
                byteList.Add(PasswordUtils.GenerateResetToken());
                list[i] = byteList.ElementAt(i).ToStringToken();
                Log("Token: {0}", list[i]);
            }

            // Not Repeated
            for (i = 0; i < max; i++)
            {
                var item = list[i];

                for (var x = 0; x < max; x++)
                {
                    if(i == x) continue;
                    var compare = list[x];
                    Assert.AreNotEqual(item, compare);
                }
            }

            // Compare converted back to byte:
            for (i = 0; i < max; i++)
            {
                var item = list[i].ToTokenBytes();
                var compare = byteList.ElementAt(i);
                Assert.IsTrue(item.CompareTo(compare));
            }
        }

        [TestMethod]
        public void PathTest()
        {
            var path = Path.Combine(GetRootPath(), "\\STCRepositorio\\TESTING");
            Assert.AreEqual(@"D:\vhosts\uploads\zentom\STCRepositorio\TESTING", path);

            path = Path.Combine(GetRootPath(), "STCRepositorio\\TESTING");
            Assert.AreEqual(@"D:\vhosts\uploads\zentom\STCRepositorio\TESTING", path);
        }

        [TestMethod]
        public void SubstringTest()
        {
            const string init = "No me acuerdo";
            var indexOf = init.LastIndexOf(' ', 4);
            var line1 = init.Substring(0, indexOf + 1);
            var line2 = init.Substring(indexOf + 1);

            Assert.AreEqual("No ", line1);
            Assert.AreEqual("me acuerdo", line2);
            Assert.AreEqual(line1.Length, 3);
            Assert.AreEqual(line2.Length, 10);
            Console.WriteLine($"'{line1}', '{line2}', {line1.Length}, {line2.Length}");
        }

        #region I18n Tests

        [TestMethod]
        public void TestThreadCulture()
        {
            Console.WriteLine("Current Culture: {0}", Thread.CurrentThread.CurrentCulture);
        }

        #endregion

        private string GetRootPath()
        {
            return @"D:\vhosts\uploads\zentom";
        }

        private void ValidateReceptorSection(XmlNode node)
        {
            var subnode = node.SelectSingleNode("RUTRecep");
            Assert.IsNotNull(subnode);
            var str = subnode.InnerText;
            Log("STR: {0}", str);
            Assert.IsTrue(string.IsNullOrWhiteSpace(str));
        }

        private void Log(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        private void ChangeValue(ref long num)
        {
            num += 10;
        }

        private void ChangeObj(Obj obj)
        {
            obj.Val = "Hello!";
        }

        public class KvP
        {
            public string Key { get; }

            public string Value { get; }

            public KvP(string key, string value)
            {
                Key = key;
                Value = value;
            }
        }

        public class Obj
        {
            public string Val { get; set; }

            public Obj()
            {
                Val = "";
            }
        }
    }
}
