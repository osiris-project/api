﻿using System;

namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class UpdateTextCmd
    {
        public Guid ReferencedDocument { get; set; }
    }
}