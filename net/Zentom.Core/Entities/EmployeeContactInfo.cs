﻿namespace Zentom.Core.Entities
{
    public class EmployeeContactInfo
        : ContactInfo
    {
        #region Fields

        private Employee employee;

        #endregion

        #region Properties

        public virtual Employee Employee
        {
            get { return employee; }
            set { employee = value; }
        }

        #endregion

        public EmployeeContactInfo() { }

        public EmployeeContactInfo(int contactType, string value, Employee employee)
            : base(contactType, value)
        {
            this.employee = employee;
        }
    }
}
