﻿using System.Collections.Generic;
using System.IO;
using Nancy;
using Nancy.IO;
using Nancy.Responses.Negotiation;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Osiris.Api.Extensions.Nancy;
using Slf4Net;

namespace Osiris.Api {
    /// <summary>
    /// A class to add extra characters to the JSON output so
    /// it's less vulnerable to XSS.
    /// </summary>
    public sealed class SecureJsonSerializer : ISerializer {
        private readonly JsonSerializer serializer;

        private readonly ILogger log;

        public SecureJsonSerializer () {
            log = LoggerFactory.GetLogger (typeof (SecureJsonSerializer));
            serializer = JsonSerializer.CreateDefault ();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver ();
            serializer.Formatting = Formatting.Indented;
        }

        #region Implementation of ISerializer

        public bool CanSerialize (string contentType) {
            var result = NancyContextHelper.IsJsonType (contentType);
            return result;
        }

        public bool CanSerialize (MediaRange range) {
            return CanSerialize (range);
        }

        public void Serialize<TModel> (MediaRange range, TModel model, Stream stream) {
            this.Serialize (range, model, stream);
        }

        public void Serialize<TModel> (string contentType, TModel model, Stream outputStream) {
            log.Debug ("Entering Serialize<TModel>.");
            log.Debug ("TModel type: {0}", typeof (TModel));
            using (var streamWr = new StreamWriter (new UnclosableStreamWrapper (outputStream))) {
                streamWr.Write (")]}',\n");
                using (var writer = new JsonTextWriter (streamWr)) {
                    serializer.Serialize (writer, model);
                }
            }

        }

        public IEnumerable<string> Extensions {
            get { yield return "json"; }
        }

        #endregion
    }
}
