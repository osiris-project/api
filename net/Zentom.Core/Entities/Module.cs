﻿namespace Zentom.Core.Entities
{
    public class Module
        : Entity
    {
        #region Fields

        private string name;

        private string description;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion

        #region Ctors.

        public Module()
        {
        }

        public Module(string name, string description)
        {
            this.name = name;
            this.description = description;
        }

        #endregion
    }
}
