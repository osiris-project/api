﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zentom.Api.Modules.EnterpriseEp
{
    public class AddressDto
    {
        public Guid Id { get; set; }

        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public Guid CityId { get; set; }

        public string CityName { get; set; }
        
        public Guid CommuneId { get; set; }

        public string CommuneName { get; set; }

        public string AddressType { get; set; }
    }
}