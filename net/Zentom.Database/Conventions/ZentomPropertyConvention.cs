﻿using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Zentom.Database.Conventions
{
    public class ZentomPropertyConvention
        : IPropertyConvention
    {
        private readonly IList<string> skip;

        public ZentomPropertyConvention()
        {
            skip = new List<string>(new[]
            {
                "FullName", "TotalTaxAmount", "NetAmount", "IsTransient"
            });
        }

        public void Apply(IPropertyInstance instance)
        {
            if (skip.Contains(instance.Name)) return;
            instance.Column(ToSnakeCase(instance.Name));
            instance.Not.Nullable();
        }

        private static string ToSnakeCase(string name)
        {
            var result = new StringBuilder(name.Length);

            for (var i = 0; i < name.Length; i++)
            {
                if (i > 0 && char.IsUpper(name[i]))
                    result.Append('_').Append(char.ToLower(name[i]));
                else
                    result.Append(char.ToLower(name[i]));
            }

            return result.ToString();
        }
    }
}
