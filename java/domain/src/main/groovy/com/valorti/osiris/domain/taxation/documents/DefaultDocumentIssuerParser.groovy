/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.dobelik.utils.nationalId.NationalIdentity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.taxation.documents.Address
import com.valorti.osiris.core.taxation.documents.DocumentIssuer
import com.valorti.osiris.core.taxation.documents.EnterpriseData
import groovy.util.slurpersupport.GPathResult

/**
 * Parses the <Emisor> section of a taxation document.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DefaultDocumentIssuerParser
    extends AbstractParser<DocumentIssuer> {

    private final NationalIdentity identity

    protected DefaultDocumentIssuerParser(NationalIdentity identity) {
        this.identity = identity
    }

    DocumentIssuer parse(GPathResult path) {
        def taxId = path.RUTEmisor.toString().trim()
        if (!this.identity.isValid(taxId)) {
            this.errors << ValidationBuilder.build('', 'El rut del emisor no puede estar vacío')
        }
        taxId = this.identity.clean(taxId)
        def name = path.RznSoc.toString().trim()
        if (!name) {
            this.errors << ValidationBuilder.build('', 'La razón social del emisor no es válida')
        }
        def commercialActivity = path.GiroEmis.toString().trim()
        List<String> phones = []
        if (path.Telefono) {
            path.Telefono.each {
                phones << it.toString().trim()
            }
        }
        def email = path.CorreoEmisor.toString().trim()
        def economicActivities = []
        if (path.Acteco) {
            path.Acteco.each { ecoact ->
                economicActivities << ecoact?.text()?.trim()
            }
        }
        def legalAddress = new Address(
            path.DirOrigen?.text()?.trim() as String,
            path.CmnaOrigen?.text()?.trim() as String,
            path.CiudadOrigen?.text()?.trim() as String)
        return new DocumentIssuer(
            new EnterpriseData(taxId, name, commercialActivity, legalAddress),
            phones, email, economicActivities
        )
    }
}
