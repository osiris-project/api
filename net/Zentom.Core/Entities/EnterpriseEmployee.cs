﻿namespace Zentom.Core.Entities
{
    public class EnterpriseEmployee
        : AbstractEntity
    {
        #region Properties

        public virtual bool IsActive { get; set; }

        public virtual bool IsConsultant { get; set; }

        public virtual Enterprise Enterprise { get; set; }

        public virtual Employee Employee { get; set; }

        #endregion

        public EnterpriseEmployee() { }

        public override bool Equals(object obj)
        {
            var t = obj as EnterpriseEmployee;

            if (ReferenceEquals(null, t))
                return false;

            if (ReferenceEquals(this, t))
                return true;

            return (Enterprise.Id == t.Enterprise.Id &&
                Employee.Id == t.Employee.Id);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) + (!ReferenceEquals(null, Enterprise) ? Enterprise.Id.GetHashCode() : 0);
                hash = (hash * 31) + (!ReferenceEquals(null, Employee) ? Employee.Id.GetHashCode() : 0);

                return hash;
            }
        }

        public override string ToString()
        {
            var ret = string.Concat("Enterprise.Id = ", Enterprise?.Id, ", ");
            ret = string.Concat(ret, "Employee.Id = ", Employee?.Id, ", ");
            ret = string.Concat(ret, "IsEmployee = ", IsActive, ", ");
            //ret = string.Concat(ret, "IsAgent = ", IsAgent);
            return ret;
        }
    }
}
