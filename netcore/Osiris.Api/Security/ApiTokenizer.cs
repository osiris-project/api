﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Jose;
using Slf4Net;
using Osiris.Api.Error;

namespace Osiris.Api.Security
{
    public static class ApiTokenizer
    {
        private static readonly ILogger log = LoggerFactory.GetLogger(typeof(ApiTokenizer));
        private static readonly DateTime UtcEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string CreateToken(object dto, DateTime expiryDate, string loginName, object accountId)
        {
            log.Debug("Entering CreateToken.");
            double offsetSeconds;
            if (double.TryParse(ConfigurationManager.AppSettings["token.offsetSeconds"], out offsetSeconds))
            {
                offsetSeconds = 180;
                log.Warn("Failed to parse offset seconds from configuration file.");
            }
            var millis = (expiryDate - UtcEpoch).TotalMilliseconds + offsetSeconds;
            var payload = new Dictionary<string, object>
            {
                {"sub", loginName},
                {"exp", millis},
                {"identity", dto}
            };

            var key = GetKey();
            log.Info("Successfully created a token for account [{0}] at {1}.", accountId, DateTime.UtcNow.ToString("R"));
            log.Debug("Leaving CreateToken.");
            return JWT.Encode(payload, key, JwsAlgorithm.PS256);
        }

        public static Guid DecodeToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new EmptyTokenException();
            log.Debug("Entering DecodeToken.");
            var payload = HasTokenExpired(token);
            var identity = (Dictionary<string, object>)payload["identity"];
            log.Debug("Leaving DecodeToken.");
            return Guid.Parse(identity["accountId"].ToString());
        }

        private static Dictionary<string, object> HasTokenExpired(string token)
        {
            var payload = Decode(token);
            double millis;
            if (!double.TryParse(payload["exp"].ToString(), out millis))
            {
                throw new InvalidTokenExpiryDateException();
            }
            var expires = UtcEpoch + TimeSpan.FromMilliseconds(millis);
            log.Debug("Expires [{0}] - Now [{1}]", expires, DateTime.UtcNow);
            if (expires < DateTime.UtcNow)
            {
                throw new TokenExpiredException();
            }
            return payload;
        }

        private static Dictionary<string, object> Decode(string token)
        {
            try
            {
                return JWT.Decode<Dictionary<string, object>>(token, GetKey());
            }
            catch (Exception ex)
            {
                log.Error("Failed to decode token.", ex);
                throw new InvalidTokenException();
            }
        }

        private static RSACryptoServiceProvider GetKey()
        {
            var cert = new X509Certificate2(ConfigurationManager.AppSettings["token.certFilePath"], "zZmzj3yCRA3QYpw",
                X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet);
            return (RSACryptoServiceProvider)cert.PrivateKey;
        }
    }
}
