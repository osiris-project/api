﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.Accounting;

namespace Osiris.Database.Maps.Accounting
{
    public class AccountingVoucherLineMap
        : ClassMap<AccountingVoucherLine>
    {
        public AccountingVoucherLineMap()
        {
            Schema("accounting");
            Table("accounting_voucher_line");

            Id(i => i.Id);
            Version(i => i.RowVersion);


            References(i => i.AccountingAccount, "accounting_account_id")
                .Cascade.SaveUpdate()
                .Not.Nullable();
            References(i => i.AccountingVoucher, "accounting_voucher_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("accounting_voucher_line_un");
            Map(i => i.Asset).Precision(13).Scale(4).Default("0.0000");
            References(i => i.CostsCenter, "costs_center_id")
                .Cascade.SaveUpdate()
                .Not.Nullable();
            Map(i => i.CustomField1, "custom_field_1").CustomSqlType("nvarchar(max)");
            Map(i => i.CustomField2, "custom_field_2").CustomSqlType("nvarchar(max)");
            Map(i => i.CustomField3, "custom_field_3").CustomSqlType("nvarchar(max)");
            Map(i => i.Debit).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.Description).Length(60);
            Map(i => i.DocumentType).Length(6);
            Map(i => i.EmissionDate);
            Map(i => i.ExpiryDate);
            Map(i => i.FolioNumber);
            Map(i => i.Number).UniqueKey("accounting_voucher_line_un");
            Map(i => i.NetAmount, "net_amount").Precision(13).Scale(4).Default("0.0000");
            Map(i => i.NetFixedAsset).Precision(13).Scale(4).Default("0.0000");
            References(i => i.Provider, "enterprise_id")
                .Nullable()
                .Cascade.SaveUpdate();
            Map(i => i.ProviderTaxId).Length(20).Not.Nullable().Default("\"\"");
            Map(i => i.ReferencedDocumentType).Length(6).Nullable();
            Map(i => i.ReferencedDocumentFolio).Nullable();
            Map(i => i.Total).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.TotalExemptAmount).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.TotalTaxAmount, "total_tax_amount").Precision(13).Scale(4).Default("0.0000");
            Map(i => i.TotalUnrecoverableAmount).Precision(13).Scale(4).Default("0.0000");
        }
    }
}
