﻿using System;
using System.Collections.Generic;

namespace Zentom.Api.Modules.RoleEp
{
    public class PermissionCommand
    {
        public Guid RoleId { get; set; }

        public IEnumerable<Guid> Permissions { get; set; }
    }
}