﻿using System;

namespace Osiris.Core.Exceptions
{
    [Serializable]
    public class UniqueKeyException
        : Exception
    {
        /// <summary>
        /// The value that was beign inserted.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// The violated constraint name.
        /// </summary>
        public string ConstraintName { get; private set; }

        /// <summary>
        /// A list of properties associated to the violated constraint.
        /// </summary>
        public string PropertyList { get; private set; }

        /// <summary>
        /// Should be used only for debugging.
        /// </summary>
        public string Sql { get; private set; }

        public UniqueKeyException() { }

        public UniqueKeyException(string message)
            : base(message) {}

        public UniqueKeyException(string message, Exception innerException)
            : base(message, innerException) { }

        public UniqueKeyException(Exception exception)
            : base(exception.Message, exception)
        { }

        /// <summary>
        /// Constructs a UniqueKeyException.
        /// </summary>
        /// <param name="exception">The exception that was thrown.</param>
        /// <param name="value">The value that caused the problem..</param>
        /// <param name="constraintName">The name of the constraint.</param>
        /// <param name="sql">Optional - A raw sql query.</param>
        public UniqueKeyException(Exception exception, string value, string constraintName, string sql = "")
            : base(exception.Message, exception)
        {
            Value = value;
            Sql = sql;
            ConstraintName = constraintName;
        }

        /// <summary>
        /// <see cref="UniqueKeyException(Exception, string, string, string)"/>
        /// </summary>
        /// <param name="exception"><see cref="UniqueKeyException(Exception, string, string, string)"/></param>
        /// <param name="value"><see cref="UniqueKeyException(Exception, string, string, string)"/></param>
        /// <param name="constraintName"><see cref="UniqueKeyException(Exception, string, string, string)"/></param>
        /// <param name="properties"><see cref="PropertyList"/></param>
        /// <param name="sql"><see cref="UniqueKeyException(Exception, string, string, string)"/></param>
        public UniqueKeyException(Exception exception, string value, string constraintName, string properties,
            string sql = "")
            : this(exception, value, constraintName, sql)
        {
            PropertyList = properties;
        }
    }
}
