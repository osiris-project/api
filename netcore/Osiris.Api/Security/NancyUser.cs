﻿using System.Collections.Generic;
using System.Security.Principal;
using Nancy.Security;
using Osiris.Core.Entities;

namespace Osiris.Api.Security {
    public class NancyUser : IIdentity {

        public NancyUser (string name, string authenticationType = "jwt") {
            if (name != null) {
                Name = name.Trim ();
            }

            if (authenticationType != null) {
                AuthenticationType = authenticationType.Trim ();
            }
        }

        public string AuthenticationType { get; }

        public bool IsAuthenticated {
            get {
                if (Name == null) return false;
                return Name.Length > 0;
            }
        }

        public string Name { get; }
    }
}
