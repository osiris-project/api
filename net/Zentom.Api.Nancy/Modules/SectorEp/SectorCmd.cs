﻿using System;

namespace Zentom.Api.Modules.SectorEp
{
    public class SectorCmd
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public Guid Parent { get; set; }

        public sbyte? TributaryCategory { get; set; }
    }
}