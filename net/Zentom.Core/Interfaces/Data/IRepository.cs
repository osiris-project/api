﻿using System.Collections.Generic;

namespace Zentom.Core.Interfaces.Data
{
    public interface IRepository<TEntity>
    {
        object Create(TEntity entity);

        void Update(TEntity entity);

        TEntity Get(object id);

        void Delete(TEntity entity);

        IEnumerable<TEntity> List();
    }
}
