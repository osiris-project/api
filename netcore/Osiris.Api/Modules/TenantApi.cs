﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Nancy;
using Nancy.Responses;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api.Modules {
    public abstract class TenantApi : ApiModule {
        #region Fields

        private readonly ILogger log = LoggerFactory.GetLogger (typeof (TenantApi));

        private readonly ISessionFactory factory;

        private string currentPath;

        protected Tenant currentTenant;

        private Account currentAccount;

        protected ISession db => factory.GetCurrentSession ();

        protected string[] pathsToSkip;

        #endregion

        #region Properties

        public string ResourceName { get; }

        public string BasePath { get; }

        public Account Account {
            get {
                return this.currentAccount;
            }
        }

        public Tenant Tenant => currentTenant;

        #endregion

        protected TenantApi (string resourceName, ISessionFactory factory) {
            pathsToSkip = null;
            ResourceName = resourceName;
            BasePath = "/" + "resources/" + resourceName;
            this.factory = factory;
            Before += LoadUserAccount;
            Before += TenantCheck;
        }

        private Response LoadUserAccount (NancyContext context) {
            if (context.CurrentUser == null) {
                return new Response {
                    StatusCode = HttpStatusCode.Unauthorized,
                        ReasonPhrase = I18n.Error.InvalidAccount,
                        Contents = stream => {
                            var error = Encoding.UTF8.GetBytes (I18n.Error.InvalidAccount);
                            stream.Write (error, 0, error.Length);
                        }
                };
            }
            currentAccount = db.Load<Account> (Guid.Parse (context.CurrentUser.Identity.Name));
            if (currentAccount == null) {
                return new Response {
                    StatusCode = HttpStatusCode.Unauthorized,
                        ReasonPhrase = I18n.Error.InvalidAccount,
                        Contents = stream => {
                            var error = Encoding.UTF8.GetBytes (I18n.Error.InvalidAccount);
                            stream.Write (error, 0, error.Length);
                        }
                };
            }
            return null;
        }

        private Response TenantCheck (NancyContext context) {
            log.Debug ("Entering TenantCheck.");

            if (pathsToSkip != null) {
                var path = Context.Request.Path;
                if (pathsToSkip.Any (skip => new Regex (skip).IsMatch (path))) {
                    return null;
                }
            }

            var key = context.Request.Headers.Keys.SingleOrDefault (i => i == "x-tenant-id");
            var response = new NegotiatedResponse (new {
                error = I18n.Error.TenantIdMissing
                }) {
                StatusCode = HttpStatusCode.Conflict,
                ContentType = "application/json"
            };

            if (key == null) {
                log.Debug ("No 'x-tenant-id' header.");
                return response;
            }

            var header = context.Request.Headers[key];
            Guid tenantId;

            if (!Guid.TryParse (header.FirstOrDefault (), out tenantId)) {
                log.Debug ("No 'x-tenant-id' header.");
                return response;
            }

            Tenant tenant = null;
            var result = db.QueryOver<AuthorizedAccount> ()
                .Left.JoinAlias (i => i.Tenant, () => tenant)
                .Where (i => tenant.Id == tenantId && i.Account == currentAccount)
                .Take (1)
                .SingleOrDefault ();

            if (result == null) {
                log.Debug ("Account not authorized for tenant.");
                response = new NegotiatedResponse (new {
                    error = I18n.Error.UnauthorizedAccount
                    }) {
                    StatusCode = HttpStatusCode.NotFound,
                    ContentType = "application/json"
                };
                return response;
            }

            currentTenant = result.Tenant;
            log.Debug ("Leaving TenantCheck.");
            return null;
        }

        protected static string GetRootStoragePath () {
            return ConfigurationManager.AppSettings[Constants.ROOT_STORAGE_KEY];
        }

        protected string RootPath () {
            if (string.IsNullOrWhiteSpace (currentPath)) {
                currentPath = Path.Combine (GetRootStoragePath (), currentTenant.TaxId);
            }

            return currentPath;
        }

        protected SigningProvider GetSigningProvider () {
            var setting = db.QueryOver<TenantSetting> ()
                .Where (i => i.Tenant == currentTenant && i.Name == Constants.SIGNING_PROVIDER_SETTING)
                .Take (1)
                .SingleOrDefault ();

            if (string.IsNullOrWhiteSpace (setting?.Value)) {
                return null;
            }

            var split = setting.Value.Split (";".ToCharArray ());

            if (split.Length <= 0) {
                log.Error ("The signing provider is malformed: '{0}'", setting.Value);
                return null;
            }

            var provider = split[0].Trim ();
            var folderName = split[1].Trim ();
            return new SigningProvider (folderName, provider);
        }

        protected string GetSetting (string settingName) {
            if (string.IsNullOrWhiteSpace (settingName)) {
                return null;
            }
            var setting = db.QueryOver<TenantSetting> ()
                .Where (i => i.Tenant == currentTenant && i.Name == settingName)
                .Take (1)
                .SingleOrDefault ();
            return setting?.Value;
        }

        protected string[] GetSigningProviderSettings () {
            var tmp = GetSetting (Constants.SIGNING_PROVIDER_SETTING);

            if (string.IsNullOrWhiteSpace (tmp)) return new [] { "", "" };

            var split = tmp.Split (";".ToCharArray ());

            if (split.Length == 2) {
                return split;
            }

            return new [] {
                split[0],
                    split[0]
            };
        }

        protected string GetRepositoryFolderName () {
            return GetSetting (Constants.FOLDER_NAME_SETTING);
        }

        protected decimal GetFolioRangePercentAlert () {
            decimal percent;
            return decimal.TryParse (GetSetting (Constants.FOLIO_RANGE_ALERT_PERCENT), out percent) ? percent : 0.1M;
        }

        protected static string GetTempFolder () {
            var path = Path.Combine (GetRootStoragePath (), "tmp");

            if (!Directory.Exists (path)) {
                Directory.CreateDirectory (path);
            }

            return path;
        }

        protected static bool IsTestingEnabled () {
            bool isTesting;
            if (!bool.TryParse (ConfigurationManager.AppSettings[Constants.IS_TESTING_ENABLED_KEY], out isTesting)) {
                isTesting = false;
            }
            return isTesting;
        }
    }
}
