﻿using System;
using System.Linq;

namespace Zentom.Api.Extensions
{
    public static class StringHelper
    {
        public static string ToKebabCase(this string input)
        {
            return
                string.IsNullOrWhiteSpace(input) ? "" :
                string.Concat(input.Select((x, i) => i > 0 && char.IsUpper(x) ? "-" + x.ToString() : x.ToString())).ToLower();
        }

        public static string ToPascalCase(this string str)
        {
            // If there are 0 or 1 characters, just return the string.
            if (string.IsNullOrWhiteSpace(str)) return "";
            if (str.Length < 2) return str.ToUpper();

            // Split the string into words.
            var words = str.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            return words.Aggregate("", (current, word) => current + (word.Substring(0, 1).ToUpper() + word.Substring(1)));
        }

        public static long[] ToLongs(this string[] strings)
        {
            var retVal = new long[strings.Count()];
            for (var i = 0; i < strings.Length; i++)
            {
                long x;
                if (!long.TryParse(strings[i], out x))
                    x = -1L;
                retVal[i] = x;
            }

            return retVal;
        }
    }
}