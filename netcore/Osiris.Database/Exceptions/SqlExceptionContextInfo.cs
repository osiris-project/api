﻿using NHibernate.Exceptions;

namespace Osiris.Database.Exceptions
{
    public class SqlExceptionContextInfo
        : AdoExceptionContextInfo
    { }
}
