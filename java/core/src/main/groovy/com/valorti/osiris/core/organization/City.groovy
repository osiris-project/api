/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

import groovy.transform.ToString

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization')
@ToString(includeNames = true)
class City extends UUIDEntity {
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 100)
    String name

    @OneToMany(mappedBy = 'city', orphanRemoval = true, cascade = CascadeType.ALL)
    def communes = new HashSet<Commune>()

    protected City() {
    }

    /**
     * @param name
     * @throws ValidationException
     * @since 0.0.1
     */
    City(String name) {
        def errors = []
        if(!name?.trim()) {
            errors << ValidationBuilder.empty('city.name')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        this.name = name
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof City)) {
            return false
        }
        final other = (City) obj
        return Objects.equals(this.name, other.name)
    }
}
