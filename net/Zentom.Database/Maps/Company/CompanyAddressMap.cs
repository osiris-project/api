﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Company
{
    public class CompanyAddressMap
        : SubclassMap<CompanyAddress>
    {
        public CompanyAddressMap()
        {
            Schema("organization");
        }
    }
}
