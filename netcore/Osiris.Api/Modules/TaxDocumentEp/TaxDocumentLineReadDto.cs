﻿using System;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentLineReadVm
        : TaxDocumentLineVm
    {
        public Guid Id { get; set; }
    }
}
