﻿using System;

namespace Osiris.Api.Modules.EmployeeEp
{
    public class EmployeeCommand
        : Command
    {
        #region Properties

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalId { get; set; }

        public DateTime? BirthDate { get; set; }

        #endregion
    }
}
