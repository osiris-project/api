/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import com.valorti.osiris.core.ValidationException

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@MappedSuperclass
abstract class CompanyAddress extends BasicAddress {
    /**
     * Represents a type of address.
     * <ul>
     *     <li>PRIMARY: The main or legal address of a company</li>
     *     <li>DELIVERY: An address where the products or services are delivered</li>
     * </ul>
     */
    enum AddressType {
        PRIMARY(0),
        DELIVERY(1),
        POSTAL(2)

        final int value

        AddressType(int value) {
            this.value = value
        }

        static AddressType fromInt(int value) {
            values().each {
                if (it.value == value) {
                    return it
                }
            }
            return null
        }

        static AddressType fromString(String str) {
            values().each {
                if (it.name() == str.toUpperCase()) {
                    return it
                }
            }
            return null
        }
    }

    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false)
    AddressType type

    protected CompanyAddress() {
    }

    /**
     * @param line1
     * @param line2
     * @param commune
     */
    protected CompanyAddress(String line1, String line2, AddressType type, Commune commune)
        throws IllegalArgumentException, ValidationException {
        super(line1, line2, commune)
        this.type = type
    }
}
