﻿using Nancy;
using Zentom.Api.Extensions.Nancy;

namespace Zentom.Api.Modules
{
    public abstract class ApiModule
        : NancyModule
    {
        #region Properties
        
        public ApiSettings Settings { protected get; set; }

        #endregion

        protected ApiModule()
            : base(Constants.BASE_PATH)
        {
            Get[""] = Index;
            Get["welcome"] = Index;
            this.ParseSettings();
        }

        /// <summary>
        /// Simple welcome message. Public to anyone.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private dynamic Index(dynamic args)
        {
            const string model = "Welcome to the Zentom API.";
            // TODO: Return an introduction and link to a tutorial.
            return Negotiate.WithModel(model)
                .WithStatusCode(HttpStatusCode.OK);
        }
    }
}