﻿namespace Osiris.Core.Entities
{
    public class Product
        : ProductBase
    {
        public Product() { }

        public Product(Tenant tenant, string name, decimal unitPrice, bool isDiscontinued)
            : base(tenant, name, unitPrice, isDiscontinued)
        {
        }
    }
}
