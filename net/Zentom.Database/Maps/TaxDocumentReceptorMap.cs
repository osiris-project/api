﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class TaxDocumentReceptorMap
        : SubclassMap<TaxDocumentReceptor>
    {
        public TaxDocumentReceptorMap()
        {
            Table("tax_document_receptor");

            Map(i => i.BankAccount).Column("bank_account").Not.Nullable().Length(15);
            Map(i => i.ReceptionAddressStreet).Column("recept_address_street").Length(60).Not.Nullable();
            Map(i => i.ReceptionAddressPart).Column("recept_address_part").Length(150).Not.Nullable();
            Map(i => i.ReceptionAdminDivisionLevel2).Column("recept_admndiv_level2").Length(20).Not.Nullable();
            Map(i => i.ReceptionAdminDivisionLevel3).Column("recept_admndiv_level3").Length(20).Not.Nullable();
            Map(i => i.AddressPart).Column("address_part").Length(150).Not.Nullable();
            Map(i => i.PhoneNumber).Column("phone_number").Length(15).Not.Nullable();
        }
    }
}
