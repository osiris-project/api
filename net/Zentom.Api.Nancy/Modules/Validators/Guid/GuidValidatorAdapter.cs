﻿using System.Collections.Generic;
using FluentValidation.Internal;
using FluentValidation.Validators;
using Nancy.Validation;
using Nancy.Validation.FluentValidation;

namespace Zentom.Api.Modules.Validators.Guid
{
    public class GuidValidatorAdapter
        : AdapterBase
    {
        #region Overrides of AdapterBase

        public override bool CanHandle(IPropertyValidator validator)
        {
            return validator is GuidValidator;
        }

        public override IEnumerable<ModelValidationRule> GetRules(PropertyRule rule, IPropertyValidator validator)
        {
            yield return
            new GuidValidationRule(
                FormatMessage(rule, validator),
                GetMemberNames(rule));
        }

        #endregion
    }
}