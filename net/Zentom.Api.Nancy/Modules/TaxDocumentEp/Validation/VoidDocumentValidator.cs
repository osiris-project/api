﻿using FluentValidation;
using Zentom.Api.Modules.Validators.Guid;

namespace Zentom.Api.Modules.TaxDocumentEp.Validation
{
    public class VoidDocumentValidator
        : AbstractValidator<VoidDocumentCmd>
    {
        public VoidDocumentValidator()
        {
            RuleFor(i => i.ReferencedDocumentId).SetValidator(new GuidValidator());
            RuleFor(i => i.VoidReason).Length(1, 90);
        }
    }
}