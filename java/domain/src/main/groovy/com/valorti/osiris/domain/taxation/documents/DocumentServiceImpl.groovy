/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.domain.taxation.documents

import org.springframework.stereotype.Service

import com.valorti.osiris.core.taxation.documents.DocumentRepository
import com.valorti.osiris.core.taxation.documents.DocumentService

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Service
class DocumentServiceImpl implements DocumentService {
    private final DocumentRepository documentRepo

    /**
     * Default constructor.
     */
    DocumentServiceImpl(DocumentRepository documentRepo) {
        this.documentRepo = documentRepo
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createCreditNote()
     */
    @Override
    public boolean createCreditNote() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createManualCreditNote()
     */
    @Override
    public boolean createManualCreditNote() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createDebitNote()
     */
    @Override
    public boolean createDebitNote() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createManualDebitNote()
     */
    @Override
    public boolean createManualDebitNote() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createEBill()
     */
    @Override
    public boolean createEBill() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createManualBill()
     */
    @Override
    public boolean createManualBill() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createInvoice()
     */
    @Override
    public boolean createInvoice() {
        // TODO Auto-generated method stub
        return false
    }

    /* (non-Javadoc)
     * @see com.valorti.osiris.core.taxation.documents.DocumentService#createManualInvoice()
     */
    @Override
    public boolean createManualInvoice() {
        // TODO Auto-generated method stub
        return false
    }
}
