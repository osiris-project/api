﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    class EconomicActivityMap
        : ClassMap<EconomicActivity>
    {
        public EconomicActivityMap()
        {
            Schema("taxation");
            Table("economic_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(6).UniqueKey("economic_activity_un");
            Map(p => p.IsSubjectToTax).Default("1");
            Map(p => p.Name).Length(110).UniqueKey("economic_activity_un");
            Map(p => p.TributaryCategory).Nullable();

            References(i => i.Parent, "economic_activity_group_id")
                .Nullable()
                .Cascade.SaveUpdate()
                .UniqueKey("economic_activity_un");
        }
    }
}
