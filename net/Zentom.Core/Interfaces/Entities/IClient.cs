﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IClient
        : IVersioned, IRowGuid
    {
        IEnterprise Provider { get; set; }

        IEnterprise Enterprise { get; set; }
    }

    public interface IClientId
    {
        IEnterprise Provider { get; set; }

        IEnterprise Enterprise { get; set; }
    }
}
