﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Dobelik.Utils.Identity;
using Slf4Net;
using NHibernate;
using NHibernate.Criterion;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Services;

namespace Osiris.Services
{
    public class ReceivedDocumentValidationService
        : Service, IReceivedDocumentValidationService
    {
        private static readonly ILogger log = LoggerFactory.GetLogger(typeof(ReceivedDocumentValidationService));

        private readonly INationalIdentity identity;

        private DateTime? lowestEmissionDate;

        public ReceivedDocumentValidationService(ISessionFactory factory, INationalIdentity identity)
            : base(factory)
        {
            this.identity = identity;
        }

        public void UpdateDocuments(Stream stream, Tenant tenant)
        {
            ParseReceptionFile(stream, tenant);
        }

        #region Reception Checking

        private void ParseReceptionFile(Stream stream, Tenant tenant)
        {
            log.Debug("Parsing correction file for received documents.");
            IList<LineData> parsedData = new List<LineData>();
            var lineStartMatcher = new Regex("^[1-9][0-9]*;", RegexOptions.Compiled);
            using (var fileReader = new StreamReader(stream))
            {
                var lineCount = 0;
                while (fileReader.Peek() >= 0)
                {
                    lineCount++;
                    var line = fileReader.ReadLine()?.Trim();
                    log.Debug("Validating line '{0}' = '{1}'", lineCount, line);
                    if (string.IsNullOrWhiteSpace(line)) continue;
                    if (!lineStartMatcher.IsMatch(line))
                    {
                        log.Warn("Line '{0}' does not start with a number, hence is skipped.", lineCount);
                        continue;
                    }
                    var data = ParseReceptionLine(line, lineCount);
                    if (data != null)
                    {
                        parsedData.Add(data);
                    }
                }
            }

            if (!lowestEmissionDate.HasValue)
            {
                lowestEmissionDate = DateTime.Now.Subtract(new TimeSpan(60, 0, 0, 0));
            }
            else
            {
                var diffDate = (DateTime.Now.Subtract(lowestEmissionDate.Value).Days / (365.2425 / 12));
                if (diffDate > 6)
                {
                    lowestEmissionDate = DateTime.Now.Subtract(new TimeSpan(60, 0, 0, 0));
                }
            }

            log.Debug("Lowest Emission Date: {0}", lowestEmissionDate.Value.ToString("yyyy-MM-dd"));
            var documents = session.QueryOver<ReceivedTaxDocument>()
                .Where(
                    i =>
                        i.Tenant == tenant && i.AccountingDate.IsBetween(lowestEmissionDate.Value).And(DateTime.Now))
                .List();
            log.Debug("Checking documents stored in our DB.");
            // Compare documents in the database with the ones in the file
            // If found on the database AND in the file, then it's 'received'
            // If found on the database but NOT in the file, then it's 'not received'
            // If found in the file but NOT on the database, then it's 'missing'
            foreach (var document in documents)
            {
                log.Debug(
                    $"Before check on document [{document.FolioNumber},{document.DocumentType},{document.ProviderTaxId}]: {document.ReceptionStatus}");
                document.ReceptionStatus = parsedData.Any(
                    i =>
                        i.Folio == document.FolioNumber && i.DocumentType == document.DocumentType &&
                        i.TaxId == document.ProviderTaxId)
                    ? ReceptionStatus.Received
                    : ReceptionStatus.NotReceived;
                log.Debug(
                    $"After check on document [{document.FolioNumber},{document.DocumentType},{document.ProviderTaxId}]: {document.ReceptionStatus}");
                session.Update(document);
            }
            log.Debug("Checking documents missing from our DB.");
            // Checks the documents that are in the taxation entity but not with us.
            foreach (var lineData in parsedData)
            {
                log.Debug("Checking if line (folio={0},doctype={1},provider={2}) is in the database.",
                    lineData.Folio, lineData.DocumentType, lineData.TaxId);
                var document = session.QueryOver<ReceivedTaxDocument>()
                    .Where(i =>
                        i.FolioNumber == lineData.Folio && i.DocumentType == lineData.DocumentType &&
                        i.ProviderTaxId == lineData.TaxId)
                    .Take(1)
                    .SingleOrDefault();
                if (document != null)
                {
                    log.Debug("Found!");
                    continue;
                }
                log.Info("Saving missing document: [type={0}, folio={1}]", lineData.DocumentType,
                    lineData.Folio);
                document = new ReceivedTaxDocument
                {
                    DocumentType = lineData.DocumentType,
                    FolioNumber = lineData.Folio,
                    AccountingDate = lineData.EmissionDate,
                    ExpiryDate = null,
                    Comments = "",
                    FilePath = "",
                    TaxRate = 0,
                    ReceptionTime = lineData.ReceptionTime,
                    ReceptionStatus = ReceptionStatus.Missing,
                    ProviderTaxId = lineData.TaxId,
                    ProviderLegalName = lineData.LegalName,
                    ProviderPhone = "",
                    ProviderBankAccountNumber = "",
                    ProviderComercialActivity = "",
                    ProviderEconomicActivity = "",
                    ProviderAddresLine1 = "",
                    ProviderAddressLine2 = "",
                    ProviderAddressCity = "",
                    ProviderAddressCommune = "",
                    ReceptorTaxId = identity.CleanId(tenant.TaxId),
                    ReceptorLegalName = tenant.LegalName,
                    ReceptorComercialActivity = "",
                    ReceptorAddress = "",
                    ReceptorCommune = "",
                    Tenant = tenant
                };
                session.Save(document);
            }
        }

        private LineData ParseReceptionLine(string line, int lineNo)
        {
            log.Debug("Parsing line '{0}'", lineNo);
            char[] splitChar = { ';' };
            var split = line.Split(splitChar);
            if (split.Length <= 0)
            {
                log.Error("The line was malformed and couldn't be split, at line '{1}'. Line: {0}", line, lineNo);
                ErrorList.Add(new ErrorModel("malformed", lineNo.ToString()));
                return null;
            }

            var taxId = split[1];
            log.Debug("Tax id: {0}", split[1]);

            if (!identity.Validate(taxId))
            {
                log.Error("The tax id '{0}' in line '{1}' is not valid.", taxId, lineNo);
                AddError(lineNo, "taxId");
                return null;
            }

            taxId = identity.CleanId(taxId);

            var legalName = split[2]?.Trim();
            log.Debug("Legal Name: {0}", split[2]);

            if (string.IsNullOrWhiteSpace(legalName))
            {
                log.Error("The legal name was empty or missing, at line '{0}'.", lineNo);
                AddError(lineNo, "legalName");
                return null;
            }

            var doctype = split[3]?.Trim();
            log.Debug("Document type: {0}", split[3]);

            if (string.IsNullOrWhiteSpace(doctype))
            {
                log.Error("The document type was null or empty, at line '{0}'.", lineNo);
                AddError(lineNo, "documentType");
                return null;
            }

            long folioNumber;
            log.Debug("Folio number: {0}", split[4]);

            if (!long.TryParse(split[4], out folioNumber))
            {
                log.Error("The folio number couldn't parsed/read, at line '{0}'.", lineNo);
                AddError(lineNo, "folioNumber");
                return null;
            }

            DateTime emissionDate;
            log.Debug("Emission Date: {0}", split[5]);

            if (!DateTime.TryParse(split[5], out emissionDate))
            {
                log.Error("Failed to parse/read the emission date, at line '{0}'.", lineNo);
                AddError(lineNo, "emissionDate");
                return null;
            }

            decimal total;
            log.Debug("Total: {0}", split[6]);

            if (!decimal.TryParse(split[6], out total))
            {
                log.Error("Failed to parse/read document total, at line '{0}'.", lineNo);
                AddError(lineNo, "total");
                return null;
            }

            DateTime receptionTime;
            log.Debug("Reception Date/Time: {0}", split[7]);

            if (!DateTime.TryParse(split[7], out receptionTime))
            {
                log.Error("Failed to parse/read reception date and time, at line '{0}'.", lineNo);
                AddError(lineNo, "receptionTime");
                return null;
            }

            if ((lowestEmissionDate.HasValue && emissionDate < lowestEmissionDate) || !lowestEmissionDate.HasValue)
            {
                lowestEmissionDate = emissionDate;
            }
            var trackId = split[8];
            log.Debug("Track Id: {0}", split[8]);
            if (!string.IsNullOrWhiteSpace(trackId))
                return new LineData
                {
                    DocumentType = doctype,
                    EmissionDate = emissionDate,
                    Folio = folioNumber,
                    LegalName = legalName,
                    ReceptionTime = receptionTime,
                    TaxId = taxId,
                    Total = total,
                    TrackId = trackId
                };
            log.Error("The track id was not present (or empty), at line '{0}'.", lineNo);
            AddError(lineNo, "trackId");
            return null;
        }

        #endregion

        #region Utils

        private string ConvertDocumentType(string doctypeName)
        {
            switch (doctypeName.ToLowerInvariant())
            {
                case "factura electronica":
                    return "33";
                case "factura exenta electronica":
                    return "34";
                case "factura":
                    return "30";
                case "factura exenta":
                    return "32";
                case "boleta electronica":
                    return "39";
                case "nota de debito":
                    return "55";
                case "nota de debito electronica":
                    return "56";
                case "nota de credito":
                    return "60";
                case "nota de credito electronica":
                    return "61";
            }

            return null;
        }

        private void AddError(int lineNo, string field)
        {
            ErrorList.Add(new ErrorModel(field + $"_{lineNo}", $"The '{field}' is not valid, at line {lineNo}."));
        }

        #endregion

        private class LineData
        {
            public string TaxId { get; set; }

            public string LegalName { get; set; }

            public string DocumentType { get; set; }

            public long Folio { get; set; }

            public DateTime EmissionDate { get; set; }

            public decimal Total { get; set; }

            public DateTime ReceptionTime { get; set; }

            public string TrackId { get; set; }
        }
    }
}
