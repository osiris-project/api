﻿using System;

namespace Zentom.Api.Modules.ProfileEp
{
    public class TenantDto
    {
        public Guid Id { get; set; }

        public string LegalName { get; set; }

        public string TaxId { get; set; }

        public int ReceivedDocumentCount { get; set; }
    }
}