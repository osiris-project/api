﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;
using LightInject;
using LightInject.Nancy;
using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Bootstrapper;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Slf4Net;

namespace Osiris.Api {
    public class Bootstrap : LightInjectNancyBootstrapper {
        #region Fields

        private static readonly ILogger Log = LoggerFactory.GetLogger (typeof (Bootstrap));

        #endregion

        #region Overrides of NancyBootstrapperBase<IServiceContainer>

        protected override void ApplicationStartup (IServiceContainer container, IPipelines pipelines) {
            base.ApplicationStartup (container, pipelines);
            ConfigureLog4Net ();
            Log.Debug ("--- Configuring ---");
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
            // Required in NET Core 2.0
            System.Text.Encoding.RegisterProvider (System.Text.CodePagesEncodingProvider.Instance);
            // Before Request processing
            pipelines.BeforeRequest.AddItemToStartOfPipeline (RequestStart);
            pipelines.BeforeRequest += (context => NHibernateSessionHelper.OpenSession (container));
            StatelessAuthentication.Enable (pipelines,
                new StatelessAuthenticationConfiguration (AuthenticationCheck));

            // Exception Handling
            pipelines.OnError += ((context, exception) => {
                NHibernateSessionHelper.Rollback (container);
                var result = ExceptionHandler (context, exception);
                return result;
            });

            // After Request processing
            pipelines.AfterRequest.AddItemToStartOfPipeline (context => NHibernateSessionHelper.Commit (context, container));
            pipelines.EnableCors ();
            pipelines.AfterRequest.AddItemToEndOfPipeline (RequestEnd);

            Log.Info ("--- Application configured ---");
        }

        #endregion

        #region Overrides of LightInjectNancyBootstrapper

        protected override IServiceContainer GetServiceContainer () {
            var container = base.GetServiceContainer ();
            LightinjectConfig.Configure (container);
            return container;
        }

        protected override void RegisterModules (IServiceContainer container, IEnumerable<ModuleRegistration> moduleRegistrationTypes) {
            base.RegisterModules (container, moduleRegistrationTypes);
            LightinjectConfig.RegisterInterceptors (container);
        }

        #endregion

        #region Utils

        #region Log4Net

        private static void ConfigureLog4Net () {
            log4net.Config.XmlConfigurator.Configure (
                LogManager.GetRepository (Assembly.GetCallingAssembly ()));
        }

        #endregion

        private static ClaimsPrincipal AuthenticationCheck (NancyContext context) {
            if (context.Request.Method.ToLower () == "options") return null;
            var isResourcesPath = context.Request.Path.StartsWith (Constants.BASE_PATH + "resources");
            Log.Debug ("Request path '{0}' is Resources Path? {1}", context.Request.Path, isResourcesPath);
            if (!isResourcesPath) return null;
            var requestPath = Regex.Replace (context.Request.Path, @"\..+$", string.Empty, RegexOptions.Compiled);
            if (requestPath == Constants.BASE_PATH) return null;

            var isAuth = Regex.IsMatch (requestPath, Constants.BASE_PATH + @"auth(/(password-reset(/[a-f0-9\-]+)?)?)?$", RegexOptions.Compiled);
            if (isAuth) return null;

            var token = context.Request.Headers.Authorization;
            Log.Debug ("Token: {0}", token);
            var id = ApiTokenizer.DecodeToken (token);
            return new ClaimsPrincipal (new NancyUser (id.ToString ()));
        }

        private static object ExceptionHandler (NancyContext context, Exception ex) {
            Log.Debug ("Entering Bootstrap ExceptionHandler.");
            var negotiator = context.HandleException (ex, null);
            Log.Debug ("Leaving Bootstrap ExceptionHandler.");
            return negotiator;
        }

        private static Response RequestStart (NancyContext context) {
            Log.Info ("--- Beginning request | Thread Id: {0} ---", Thread.CurrentThread.ManagedThreadId);
            Log.Info ("IsoCode: {0}", Thread.CurrentThread.CurrentCulture);
            Log.Info ("Requested URI: {0}", context.Request.Url);
            Log.Info ("Method: {0}", context.Request.Method);

            //ParseSettings(context);
            return null;
        }

        private static void RequestEnd (NancyContext context) {
            Log.Info ("--- Request End | Thread Id: {0} ---", Thread.CurrentThread.ManagedThreadId);
        }

        #endregion
    }
}
