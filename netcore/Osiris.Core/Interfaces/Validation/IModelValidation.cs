﻿namespace Osiris.Core.Interfaces.Validation
{
    public interface IModelValidation
    {
        string MemberName { get; }

        string ErrorMessage { get; }
    }
}
