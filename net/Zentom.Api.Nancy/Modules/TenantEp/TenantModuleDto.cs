﻿using System;
using Newtonsoft.Json;

namespace Zentom.Api.Modules.TenantEp
{
    public class TenantModuleDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Settings { get; set; }
    }
}