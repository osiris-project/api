﻿using Zentom.Core.Entities;

namespace Zentom.Api.Modules.TenantEp
{
    public class FolioRangeDto
    {
        public string DocumentType { get; set; }

        public long RangeStart { get; set; }

        public long RangeEnd { get; set; }

        public long CurrentFolio { get; set; }

        public bool IsCurrentRange { get; set; }

        public RangeStatus Status { get; set; }
    }
}