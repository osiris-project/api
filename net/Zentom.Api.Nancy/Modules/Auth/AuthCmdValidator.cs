﻿using FluentValidation;

namespace Zentom.Api.Modules.Auth
{
    public class AuthCmdValidator
        : AbstractValidator<AuthCmd>
    {
        public AuthCmdValidator()
        {
            RuleFor(i => i.LoginName).Length(3, 50);
            RuleFor(i => i.Password).NotEmpty();
        }
    }
}