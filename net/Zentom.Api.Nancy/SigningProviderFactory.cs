﻿using Zentom.Core.Entities.TaxDocument;
using Zentom.Core.Interfaces.Integration.TributaryDocuments;
using Zentom.Integration.Artikos;

namespace Zentom.Api
{
    public class SigningProviderFactory
    {
        private readonly ArtikosFactory artikosFactory;

        public SigningProviderFactory(ArtikosFactory artikosFactory)
        {
            this.artikosFactory = artikosFactory;
        }

        public ITributaryDocumentFormatter<TaxDocument> LocateProvider(string signingProviderName, string documentType)
        {
            switch (signingProviderName)
            {
                case "artikos_cl":
                    return artikosFactory.Resolve(documentType);
            }

            return null;
        }
    }
}