﻿namespace Zentom.Core.Entities
{
    public abstract class CompanyEntity
        : Entity
    {
        #region Fields

        private Company enterprise;

        #endregion

        #region Properties
        
        protected virtual Company Company
        {
            get { return enterprise; }
            set { enterprise = value; }
        }

        #endregion

        protected CompanyEntity()
        {
        }

        protected CompanyEntity(Company enterprise)
        {
            this.enterprise = enterprise;
        }
    }
}
