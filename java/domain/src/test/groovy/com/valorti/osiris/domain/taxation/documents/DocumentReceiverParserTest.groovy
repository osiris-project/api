/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class DocumentReceiverParserTest
    extends BaseParserTests {

    @Test
    void test_readReceiverSection() {
        println 'test_readReceiverSection'

        def path = this.buildEBill()
        assert path != null

        def receptor = path.DTE.Documento.Encabezado.Receptor
        assert receptor != null

        def parser = new DefaultDocumentReceptorParser(this.identity)
        assert parser != null

        def receiver = parser.parse(receptor)
        assert receiver.data.taxId == this.identity.clean(receptor.RUTRecep.text())
        assert receiver.data.name == receptor.RznSocRecep.text()
        assert receiver.data.commercialActivity == receptor.GiroRecep.text()
        assert receiver.data.legalAddress.line == receptor.DirRecep.text()
        assert receiver.data.legalAddress.commune == receptor.CmnaRecep.text()
        assert receiver.data.legalAddress.city == receptor.CiudadRecep?.text()

        if (receiver.postalAddress) {
            assert receiver.postalAddress.line == receptor.DirPostal?.text()
            assert receiver.postalAddress.commune == receptor.CmnaPostal?.text()
            assert receiver.postalAddress.city == receptor.CiudadPostal?.text()
        }
    }
}
