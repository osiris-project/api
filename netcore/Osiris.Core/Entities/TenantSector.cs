﻿namespace Osiris.Core.Entities
{
    public class TenantSector
        : CompanySector
    {
        #region Properties

        public virtual Tenant Tenant
        {
            get { return (Tenant)Company; }
            set { Company = value; }
        }

        #endregion

        public TenantSector() { }

        public TenantSector(Sector sector, Company tenant)
            : base(tenant, sector)
        {
        }
    }
}
