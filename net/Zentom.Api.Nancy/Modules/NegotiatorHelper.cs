﻿using System;
using System.Collections.Generic;
using log4net;
using Nancy;
using Nancy.Responses.Negotiation;
using Nancy.Validation;
using Zentom.Api.Responses;
using Zentom.Core.Interfaces.Validation;
using Zentom.Core.Pagination;

namespace Zentom.Api.Modules
{
    public static class NegotiatorHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NegotiatorHelper));

        #region 2** Status (OK responses)

        /// <summary>
        /// No content status, for operations that do not return content (like entities updates - usually put/patch/delete).
        /// </summary>
        /// <param name="negotiator"></param>
        /// <returns></returns>
        public static Negotiator WithNoContent(this Negotiator negotiator)
        {
            return negotiator.WithStatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Ok status, for operations that succeeded and don't return content.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <returns></returns>
        /// <remarks>Use this when the operation succeeded but there was no content to return
        /// e.g., no entities collection was populated.</remarks>
        public static Negotiator WithOk(this Negotiator negotiator)
        {
            return negotiator.WithStatusCode(HttpStatusCode.OK);
        }

        /// <summary>
        /// This should be the default status 
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Negotiator WithOkAndModel(this Negotiator negotiator, object model)
        {
            return negotiator.WithOk().WithModel(model);
        }

        public static Negotiator WithOkAndList<T>(this Negotiator negotiator, IPagedResult<T> paginated, string requestUrl, string orderByList = "")
        {
            Log.DebugFormat(
                "Item Count: {0} | Current Page: {1} | Has Next: {2} | Has Previous: {3} | Page Count: {4}",
                paginated.ResultCount, paginated.CurrentPage,
                paginated.HasNext, paginated.HasPrevious, paginated.PageCount);
            return negotiator.WithModel(paginated.Items)
                .WithHeaders(TransformPagination(paginated, requestUrl))
                .WithHeader("x-order-by-list", orderByList)
                .WithHeader("Access-Control-Expose-Headers",
                    "x-count, x-next, x-previous, x-current-page, x-page-count, x-results-per-page, x-order-by-list")
                .WithStatusCode(HttpStatusCode.OK);
        }

        public static Negotiator WithOkAndList<T>(this Negotiator negotiator, IList<T> list, string requestUrl, long resultCount, int currentPage, long pageCount, int rpp, bool hasPrev, bool hasNext)
        {
            return negotiator.WithModel(list)
                .WithHeaders(TransformPagination(requestUrl, resultCount, currentPage, pageCount, rpp, hasPrev, hasNext))
                .WithHeader("Access-Control-Expose-Headers",
                    "x-count, x-next, x-previous, x-current-page, x-page-count, x-results-per-page")
                .WithStatusCode(HttpStatusCode.OK);
        }

        public static Negotiator WithPostSuccess(this Negotiator negotiator, string resourceLocation = "", object model = null)
        {
            if (!string.IsNullOrWhiteSpace(resourceLocation))
                negotiator.WithHeader("Location", resourceLocation);
            if (model != null)
            {
                negotiator.WithModel(model);
            }
            return negotiator.WithStatusCode(HttpStatusCode.Created);
        }

        public static Negotiator WithCreated(this Negotiator negotiator, string resourceLocation, Guid id)
        {
            return negotiator.WithStatusCode(HttpStatusCode.Created)
                .WithHeader("Location", resourceLocation)
                .WithModel(new
                {
                    id
                });
        }

        public static Negotiator WithPostSuccess(this Negotiator negotiator, object id, string resourceLocation = "")
        {
            if (string.IsNullOrWhiteSpace(resourceLocation))
                return negotiator.WithModel(new
                {
                    generatedId = id
                });
            resourceLocation += "/" + id;
            negotiator.WithHeader("Location", resourceLocation);
            return negotiator.WithModel(new
            {
                generatedId = id
            });
        }

        #endregion

        #region 4** Status (Something went wrong)

        public static Negotiator WithPostFailed(this Negotiator negotiator, object reason, string message = "")
        {
            negotiator.WithStatusCode(HttpStatusCode.Conflict)
                .WithModel(new { error = reason });

            if (!string.IsNullOrWhiteSpace(message))
                negotiator.WithReasonPhrase(message);

            return negotiator;
        }

        /// <summary>
        /// Simply not found status with a generic not found message.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Negotiator WithNotFound(this Negotiator negotiator, object model = null)
        {
            if (model == null)
            {
                negotiator.WithModel(new
                {
                    error = I18n.Error.GenericNotFound
                });
            }
            else
            {
                negotiator.WithModel(model);
            }

            return negotiator.WithStatusCode(HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Not found status which includes the entity name and id of the object.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="entityName">The name of the not found entity.</param>
        /// <param name="id">The entity identifier.</param>
        /// <returns></returns>
        public static Negotiator WithNotFound(this Negotiator negotiator, string entityName, object id)
        {
            return negotiator.WithModel(new
            {
                error = new
                {
                    message = string.Format(I18n.Error.ItemWithIdNotFound, entityName, id)
                },
                entity = entityName,
                id
            }).WithStatusCode(HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Not found status with message.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="message">The error message for the Reason Phrase and model.</param>
        /// <returns></returns>
        public static Negotiator WithNotFound(this Negotiator negotiator, string message)
        {
            return negotiator.WithStatusCode(HttpStatusCode.NotFound)
                .WithReasonPhrase(message)
                .WithModel(new
                {
                    error = message
                });
        }

        /// <summary>
        /// Not found status with a string formatted to include an object identifier.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="id">Object identifier</param>
        /// <param name="format">The <see cref="string.Format(string, object[])"/> format</param>
        /// <returns></returns>
        public static Negotiator WithNotFound(this Negotiator negotiator, object id, string format)
        {
            var msg = string.Format(format, id);
            return negotiator.WithNotFound(msg);
        }

        /// <summary>
        /// Not found with a formatted property name.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="propertyName">The faulty property name.</param>
        /// <param name="format">The <see cref="string.Format(string, object[])"/> format</param>
        /// <returns></returns>
        public static Negotiator WithNotFound(this Negotiator negotiator, string propertyName, string format)
        {
            var msg = string.Format(format, propertyName);
            return negotiator.WithNotFound(msg);
        }

        /// <summary>
        /// Conflict status when the validation process fails.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="validationResult">The model validation results</param>
        /// <returns></returns>
        public static Negotiator RespondWithValidationFailure(this Negotiator negotiator,
            ModelValidationResult validationResult)
        {
            var model = new ValidationFailedResponse(validationResult);
            negotiator.WithStatusCode(HttpStatusCode.Conflict)
                .WithModel(new
                {
                    validation = model.Messages
                });
            return negotiator;
        }

        /// <summary>
        /// Conflict status when the validation process fails.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="errors">The series of reasons why the validation failed.</param>
        /// <returns></returns>
        public static Negotiator WithValidationFailure(this Negotiator negotiator, IEnumerable<IModelValidation> errors)
        {
            var model = new ValidationFailedResponse(errors);
            return negotiator.WithStatusCode(HttpStatusCode.Conflict)
                .WithModel(new
                {
                    validation = model.Messages
                });
        }

        public static Negotiator WithValidationFailure(this Negotiator negotiator,
            IEnumerable<FileErrorValidation> errors)
        {
            var model = new ValidationFailedResponse(errors);
            return negotiator.WithModel(new
            {
                validation = model.MessageDictionary
            }).WithStatusCode(HttpStatusCode.Conflict);
        }

        public static Negotiator WithValidationFailure(this Negotiator negotiator, IModelValidation modelValidation)
        {
            return negotiator.WithModel(new
            {
                validation = new[]
                {
                    new
                    {
                        memberName = modelValidation.MemberName,
                        errorMessage = modelValidation.ErrorMessage
                    }
                }
            }).WithStatusCode(HttpStatusCode.Conflict);
        }

        /// <summary>
        /// Conflict status when the validation process fails.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="msg">The reason why the validation failed.</param>
        /// <returns></returns>
        public static Negotiator RespondWithValidationFailure(this Negotiator negotiator,
            string msg)
        {
            var model = new ValidationFailedResponse(msg);
            return negotiator.WithStatusCode(HttpStatusCode.BadRequest)
                .WithModel(new
                {
                    validation = model.Messages
                });
        }

        /// <summary>
        /// Conflict status when an entity with some property already exists.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="entityName">The existing entity name</param>
        /// <param name="faultyProperty">The duplicate property name</param>
        /// <returns></returns>
        public static Negotiator WithExistingItem(this Negotiator negotiator, string entityName, string faultyProperty)
        {
            return negotiator.WithStatusCode(HttpStatusCode.Conflict).WithModel(new
            {
                validation = new[]
                {
                    new
                    {
                        entityName,
                        faultyProperty
                    }
                }
            });
        }

        public static Negotiator WithDuplicateItem(this Negotiator negotiator, string message, string faultyProperty)
        {
            return negotiator.WithStatusCode(HttpStatusCode.Conflict).WithModel(new
            {
                validation = new[]
                {
                    new
                    {
                        faultyProperty,
                        message
                    }
                }
            });
        }

        public static Negotiator WithDuplicateItem(this Negotiator negotiator, string[] faultyProperties)
        {
            return negotiator.WithStatusCode(HttpStatusCode.Conflict).WithModel(new
            {
                validation = new[]
                {
                    new
                    {
                        duplicate = faultyProperties
                    }
                }
            });
        }

        /// <summary>
        /// Conflict status when the current user does not have the required permissions to apply a CRUDL operation
        /// on a resource.
        /// </summary>
        /// <param name="negotiator"></param>
        /// <param name="message">The optional message to explain the permissions required or why can't the resource
        /// be modified/accessed.</param>
        /// <returns></returns>
        public static Negotiator WithUnauthorized(this Negotiator negotiator, string message = null)
        {
            Log.DebugFormat("WithUnauthorized message: {0}", message);

            if (string.IsNullOrWhiteSpace(message))
            {
                message = I18n.Error.Unauthorized;
            }

            return negotiator.WithModel(new
            {
                error = message
            })
            .WithReasonPhrase(message)
            .WithStatusCode(HttpStatusCode.Unauthorized);
        }

        public static Negotiator WithUnauthorized(this Negotiator negotiator, object model, string message = null)
        {
            negotiator.WithModel(model)
                .WithStatusCode(HttpStatusCode.Unauthorized);
            if (!string.IsNullOrWhiteSpace(message))
            {
                negotiator.WithReasonPhrase(message);
            }
            return negotiator;
        }

        public static Negotiator WithConflict(this Negotiator negotiator, string message)
        {
            return negotiator
                .WithReasonPhrase(message)
                .WithStatusCode(HttpStatusCode.Conflict)
                .WithModel(new
                {
                    error = message
                });
        }

        #endregion

        private static Tuple<string, string>[] TransformPagination<T>(IPagedResult<T> paginated, string baseUrl)
        {
            baseUrl = baseUrl.TrimEnd("/".ToCharArray());
            var result = new[]
            {
                CreateHeader("x-count", paginated.ResultCount.ToString()),
                CreateHeader("x-next", GetNextLink(baseUrl, paginated)),
                CreateHeader("x-previous", GetPrevLink(baseUrl, paginated)),
                CreateHeader("x-current-page", paginated.CurrentPage.ToString()),
                CreateHeader("x-page-count", paginated.PageCount.ToString()),
                CreateHeader("x-results-per-page", paginated.ResultsPerPage.ToString())
            };
            return result;
        }

        private static Tuple<string, string>[] TransformPagination(string baseUrl, long resultCount, int currentPage, long pageCount, int rpp, bool hasPrev, bool hasNext)
        {
            baseUrl = baseUrl.TrimEnd("/".ToCharArray());
            var result = new[]
            {
                CreateHeader("x-count", resultCount.ToString()),
                CreateHeader("x-next", GetNextLink(baseUrl, hasNext, currentPage, rpp)),
                CreateHeader("x-previous", GetPrevLink(baseUrl, hasPrev, currentPage, rpp)),
                CreateHeader("x-current-page", currentPage.ToString()),
                CreateHeader("x-page-count", pageCount.ToString()),
                CreateHeader("x-results-per-page", rpp.ToString())
            };
            return result;
        }

        private static Tuple<string, string> CreateHeader(string key, string value)
        {
            return new Tuple<string, string>(key, value);
        }

        private static string GetNextLink(string baseUrl, bool hasNext, int currentPage, int rpp)
        {
            return !hasNext ? "" :
                string.Concat(baseUrl, "?page=", (currentPage + 1), "&rpp=", rpp);
        }

        private static string GetPrevLink(string baseUrl, bool hasPrev, int currentPage, int rpp)
        {
            return !hasPrev ? "" :
                string.Concat(baseUrl, "?page=", (currentPage + 1), "&rpp=", rpp);
        }

        private static string GetNextLink<T>(string baseUrl, IPagedResult<T> pagination)
        {
            return !pagination.HasNext ? "" :
                string.Concat(baseUrl, "?page=", (pagination.CurrentPage + 1), "&rpp=", pagination.ResultsPerPage);
        }

        private static string GetPrevLink<T>(string baseUrl, IPagedResult<T> pagination)
        {
            return !pagination.HasPrevious ? "" :
                string.Concat(baseUrl, "?page=", (pagination.CurrentPage - 1), "&rpp=", pagination.ResultsPerPage);
        }
    }
}