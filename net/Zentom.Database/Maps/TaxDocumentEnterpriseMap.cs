﻿using FluentNHibernate.Mapping;
using System;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class TaxDocumentEnterpriseMap
        : ClassMap<AbstractTaxDocumentEnterprise>
    {
        public TaxDocumentEnterpriseMap()
        {
            Schema("taxation");
            UseUnionSubclassForInheritanceMapping();

            Id(i => i.Id).Column("tax_document_id").Not.Nullable();
            Version(i => i.RowVersion).Column("row_version").Generated.Always();
            Map(i => i.TaxationId).Column("taxation_id").Not.Nullable().Length(20);
            Map(i => i.LegalName).Column("legal_name").Not.Nullable().Length(100);
            Map(i => i.SectorName).Column("sector_name").Not.Nullable().Length(60);
            Map(i => i.AddressStreet).Column("address_street").Not.Nullable().Length(60);
            Map(i => i.AddressAdminDivisionLevel2).Column("admndiv_level2").Not.Nullable().Length(20);
            Map(i => i.AddressAdminDivisionLevel3).Column("admndiv_level3").Not.Nullable().Length(20);

            References<TaxDocument>(i => i.TaxDocument, "tax_document_id")
                .Unique()
                .Fetch.Join()
                .ForeignKey()
                .Cascade.All();
        }
    }
}
