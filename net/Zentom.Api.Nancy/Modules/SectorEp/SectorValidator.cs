﻿using FluentValidation;

namespace Zentom.Api.Modules.SectorEp
{
    public class SectorValidator
        : AbstractValidator<SectorCmd>
    {
        public SectorValidator()
        {
            RuleFor(i => i.Name).Length(1, 110);
            RuleFor(i => i.Code).Length(1, 6);
        }
    }
}