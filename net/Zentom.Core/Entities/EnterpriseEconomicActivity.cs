﻿namespace Zentom.Core.Entities
{
    public class EnterpriseEconomicActivity
        : CompanyEconomicActivity
    {
        #region Properties

        public virtual Enterprise Enterprise
        {
            get { return (Enterprise)Company; }
            set { Company = value; }
        }

        #endregion

        public EnterpriseEconomicActivity()
        {
        }

        public EnterpriseEconomicActivity(Company enterprise, EconomicActivity economicActivity)
            : base(enterprise, economicActivity)
        {
        }
    }
}