﻿namespace Zentom.Core.Entities
{
    public class TenantSetting
        : TenantEntity
    {
        #region Fields

        private string name;

        private string value;

        #endregion

        #region Properties

        public virtual string Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion
        
        public TenantSetting() { }

        public TenantSetting(Tenant tenant, string name, string value) : base(tenant)
        {
            this.name = name;
            this.value = value;
        }
    }
}