﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class Sector
        : Entity
    {
        #region Fields

        private string name;

        private string code;

        private bool isSubjectToTax;

        private sbyte? tributaryCategory;

        private Sector parentSector;

        private ISet<EnterpriseSector> enterpriseSectors;

        //private ISet<Product> _products;

        private ISet<Sector> sectors;

        //private ISet<Service> _services;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual bool IsSubjectToTax
        {
            get { return isSubjectToTax; }
            set { isSubjectToTax = value; }
        }

        public virtual sbyte? TributaryCategory
        {
            get { return tributaryCategory; }
            set { tributaryCategory = value; }
        }

        public virtual Sector ParentSector
        {
            get { return parentSector; }
            set { parentSector = value; }
        }

        public virtual ISet<EnterpriseSector> EnterpriseSectors
        {
            get { return enterpriseSectors; }
            set { enterpriseSectors = value; }
        }

        //public virtual ISet<Product> Products
        //{
        //    get { return _products; }
        //    set { _products = value; }
        //}

        public virtual ISet<Sector> Sectors
        {
            get { return sectors; }
            set { sectors = value; }
        }

        //public virtual ISet<Service> Services
        //{
        //    get { return _services; }
        //    set { _services = value; }
        //}

        #endregion

        public Sector()
        {
            enterpriseSectors = new HashSet<EnterpriseSector>();
            sectors = new HashSet<Sector>();
            //_products = new HashSet<Product>();
            //_services = new HashSet<Service>();
        }

        public Sector(string name, string code, bool isSubjectToTax, sbyte? tributaryCategory,
            Sector parentSector, ISet<EnterpriseSector> enterpriseSectors,
            ISet<Sector> sectors)
        {
            this.name = name;
            this.code = code;
            this.isSubjectToTax = isSubjectToTax;
            this.tributaryCategory = tributaryCategory;
            this.parentSector = parentSector;
            if (enterpriseSectors != null) this.enterpriseSectors = enterpriseSectors;
            if (sectors != null) this.sectors = sectors;
            //if (products != null) _products = products;
            //if (services != null) _services = services;
        }
    }
}
