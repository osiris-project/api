﻿namespace Zentom.Api.Modules.AccountingEp
{
    public class CostsCenterQuery
    {
        public string Code { get; set; } 

        public string Name { get; set; }
    }
}