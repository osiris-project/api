﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseSector
        : IVersioned
    {
        IEnterprise Enterprise { get; set; }

        ISector Sector { get; set; }
    }

    public interface IEnterpriseSectorId
    {
        IEnterprise Enterprise { get; set; }

        ISector Sector { get; set; }
    }
}
