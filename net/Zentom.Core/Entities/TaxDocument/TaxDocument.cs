﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zentom.Core.Enums;

namespace Zentom.Core.Entities.TaxDocument
{
    public enum TaxDocumentStatus
    {
        Saved, // Only stored in the database
        Pending, // Once has been saved and generated
        Rejected, // The document has some problems, hence is rejected by the authority
        Approved // Document had no errors, hence approved
    }

    public class TaxDocument
        : TenantEntity
    {
        #region Fields

        private Enterprise currentClient;

        private string vatOwnerTaxId;

        private TaxDocumentClient client;

        private string documentType;

        private long folioNumber;

        private long documentNumber;

        private string emissionPoint;

        private DateTime accountingDate;

        private TimeSpan? accountingTime;

        private DateTime generationDate;

        private DateTime? expireDate;

        private decimal previousDebtBalance;

        private string comments;

        private TaxDocumentStatus documentStatus;

        private decimal taxRate;

        private decimal extraTaxRate;

        private decimal exemptTotal;

        private decimal affectTotal;

        /// <summary>
        /// Only applies to extra taxes.
        /// </summary>
        private bool isRetainedExtraTax;

        private int extraTaxCode;

        private decimal documentTotal;

        private string documentResponsible;

        private DispatchType dispatchType;

        private PaymentType defaultPaymentType;

        private CostsCenter costsCenter;

        private int serviceType;

        private bool creditNoteDiscount;


        private ISet<Payment> payments = new HashSet<Payment>();

        private ISet<TaxDocumentReference> referencedDocuments = new HashSet<TaxDocumentReference>();

        private ISet<TaxDocumentReference> referencedBy = new HashSet<TaxDocumentReference>();

        private ISet<TaxDocumentWeakReference> weakReferences = new HashSet<TaxDocumentWeakReference>();

        private ISet<TaxDocumentLine> taxDocumentLines = new HashSet<TaxDocumentLine>();

        /// <summary>
        /// The action it's doing. Not to be persisted.
        /// </summary>
        private CorrectionCode correctionCode;

        private string tenantTaxId;

        private string tenantLegalName;

        private string tenantComercialActivity;

        private string tenantEconomicActivityName;

        private string tenantEconomicActivityCode;

        private string tenantAddress;

        private string tenantCommune;

        private string tenantCity;

        #endregion

        #region Properties

        public virtual TaxDocumentClient Client
        {
            get { return client; }
            set { client = value; }
        }

        public virtual long DocumentNumber
        {
            get { return documentNumber; }
            set { documentNumber = value; }
        }

        public virtual string EmissionPoint
        {
            get { return emissionPoint; }
            set { emissionPoint = value; }
        }

        public virtual long FolioNumber
        {
            get { return folioNumber; }
            set { folioNumber = value; }
        }

        public virtual DateTime AccountingDate
        {
            get { return accountingDate; }
            set { accountingDate = value; }
        }

        public virtual TimeSpan? AccountingTime
        {
            get { return accountingTime; }
            set { accountingTime = value; }
        }

        public virtual DateTime GenerationDate
        {
            get { return generationDate; }
            set { generationDate = value; }
        }

        public virtual DateTime? ExpireDate
        {
            get { return expireDate; }
            set { expireDate = value; }
        }

        public virtual decimal PreviousDebtBalance
        {
            get { return previousDebtBalance; }
            set { previousDebtBalance = value; }
        }

        public virtual string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        public virtual TaxDocumentStatus DocumentStatus
        {
            get { return documentStatus; }
            set { documentStatus = value; }
        }

        public virtual decimal TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }

        public virtual decimal ExtraTaxRate
        {
            get { return extraTaxRate; }
            set { extraTaxRate = value; }
        }

        public virtual bool IsRetainedExtraTax
        {
            get { return isRetainedExtraTax; }
            set { isRetainedExtraTax = value; }
        }

        public virtual int ExtraTaxCode
        {
            get { return extraTaxCode; }
            set { extraTaxCode = value; }
        }

        public virtual decimal DocumentTotal
        {
            get { return documentTotal; }
            set { documentTotal = value; }
        }

        public virtual string DocumentResponsible
        {
            get { return documentResponsible; }
            set { documentResponsible = value; }
        }

        public virtual DispatchType DispatchType
        {
            get { return dispatchType; }
            set { dispatchType = value; }
        }

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual PaymentType DefaultPaymentType
        {
            get { return defaultPaymentType; }
            set { defaultPaymentType = value; }
        }

        public virtual int ServiceType
        {
            get { return serviceType; }
            set { serviceType = value; }
        }

        public virtual bool CreditNoteDiscount
        {
            get { return creditNoteDiscount; }
            set { creditNoteDiscount = value; }
        }

        public virtual string TenantTaxId
        {
            get { return tenantTaxId; }
            set { tenantTaxId = value; }
        }

        public virtual string TenantLegalName
        {
            get { return tenantLegalName; }
            set { tenantLegalName = value; }
        }

        public virtual string TenantComercialActivity
        {
            get { return tenantComercialActivity; }
            set { tenantComercialActivity = value; }
        }

        public virtual string TenantEconomicActivityName
        {
            get { return tenantEconomicActivityName; }
            set { tenantEconomicActivityName = value; }
        }

        public virtual string TenantEconomicActivityCode
        {
            get { return tenantEconomicActivityCode; }
            set { tenantEconomicActivityCode = value; }
        }

        public virtual string TenantAddress
        {
            get { return tenantAddress; }
            set { tenantAddress = value; }
        }

        public virtual string TenantCommune
        {
            get { return tenantCommune; }
            set { tenantCommune = value; }
        }

        public virtual string TenantCity
        {
            get { return tenantCity; }
            set { tenantCity = value; }
        }

        public virtual ISet<Payment> Payments
        {
            get { return payments; }
            set { payments = value; }
        }

        public virtual ISet<TaxDocumentReference> ReferencedDocuments
        {
            get { return referencedDocuments; }
            set { referencedDocuments = value; }
        }

        public virtual ISet<TaxDocumentReference> ReferencedBy
        {
            get { return referencedBy; }
            set { referencedBy = value; }
        }

        public virtual ISet<TaxDocumentWeakReference> WeakReferences
        {
            get { return weakReferences; }
            set { weakReferences = value; }
        }

        public virtual ISet<TaxDocumentLine> TaxDocumentLines
        {
            get { return taxDocumentLines; }
            set { taxDocumentLines = value; }
        }

        public virtual CorrectionCode CorrectionCode
        {
            get { return correctionCode; }
            set { correctionCode = value; }
        }

        public virtual CostsCenter CostsCenter
        {
            get { return costsCenter; }
            set { costsCenter = value; }
        }

        public virtual string VatOwnerTaxId
        {
            get { return vatOwnerTaxId; }
            set { vatOwnerTaxId = value; }
        }

        public virtual Enterprise CurrentClient
        {
            get { return currentClient; }
            set { currentClient = value; }
        }

        public virtual decimal ExemptTotal
        {
            get { return exemptTotal; }
            set { exemptTotal = value; }
        }

        public virtual decimal AffectTotal
        {
            get { return affectTotal; }
            set { affectTotal = value; }
        }

        #endregion

        public TaxDocument()
        {
            serviceType = 2;
            creditNoteDiscount = false;
            generationDate = DateTime.UtcNow;
            vatOwnerTaxId = "";
        }

        public TaxDocument(long folio, string documentType, string emissionPoint, DateTime accountingDate, TimeSpan? accountingTime,
            DateTime? expireDate, decimal previousDebtBalance, string comments, decimal taxRate, decimal extraTaxRate,
            bool isRetainedExtraTax, int extraTaxCode, decimal documentTotal, string documentResponsible, DispatchType dispatchType,
            PaymentType defaultPaymentType, CostsCenter costsCenter, int serviceType,
            bool creditNoteDiscount, CorrectionCode correctionCode, string vatOwnerTaxId = "")
        {
            folioNumber = folio;
            this.documentType = documentType;
            this.emissionPoint = emissionPoint;
            this.accountingDate = accountingDate;
            this.accountingTime = accountingTime;
            this.expireDate = expireDate;
            this.previousDebtBalance = previousDebtBalance;
            this.comments = comments;
            this.taxRate = taxRate;
            this.extraTaxRate = extraTaxRate;
            this.isRetainedExtraTax = isRetainedExtraTax;
            this.extraTaxCode = extraTaxCode;
            this.documentTotal = documentTotal;
            this.documentResponsible = documentResponsible;
            this.dispatchType = dispatchType;
            this.defaultPaymentType = defaultPaymentType;
            this.costsCenter = costsCenter;
            this.serviceType = serviceType;
            this.creditNoteDiscount = creditNoteDiscount;
            this.correctionCode = correctionCode;
            this.vatOwnerTaxId = vatOwnerTaxId;
            generationDate = DateTime.UtcNow;
        }

        public virtual void AddTenant(Tenant tenant, string economicActivityCode, string comercialActivity, string economicActivityName)
        {
            if (tenant == null)
            {
                throw new ArgumentNullException(nameof(tenant));
            }

            Tenant = tenant;

            if (string.IsNullOrWhiteSpace(economicActivityCode))
                throw new ArgumentNullException(nameof(economicActivityCode));
            if (string.IsNullOrWhiteSpace(comercialActivity))
                throw new ArgumentNullException(nameof(comercialActivity));

            var address = Tenant.TenantAddresses.Single(i => i.TenantAddressTypes.Any(j => j.AddressType.Name == "legal"));

            if (address == null)
                throw new NullReferenceException("The required address was not present in the tenant.");
            tenantTaxId = Tenant.TaxId;
            tenantAddress = address.Line1;
            tenantCity = address.Commune.City.Name;
            tenantCommune = address.Commune.Name;
            tenantLegalName = Tenant.LegalName;
            tenantEconomicActivityCode = economicActivityCode;
            tenantComercialActivity = comercialActivity;
            tenantEconomicActivityName = economicActivityName;
        }

        #region TaxDocumentLine

        public virtual void AddTaxDocumentLine(int lineNumber, string text,
            decimal quantity = 1, string additionalGloss = "", decimal unitPrice = 0,
            string measureUnit = "", TaxType taxType = TaxType.Subject)
        {
            if (taxDocumentLines == null)
                taxDocumentLines = new HashSet<TaxDocumentLine>();
            if (additionalGloss == null)
                additionalGloss = "";
            var line = new TaxDocumentLine(lineNumber, text, this, quantity,
                additionalGloss, unitPrice, measureUnit, taxType);
            taxDocumentLines.Add(line);
            documentTotal += line.LineTotal;
        }

        #endregion

        #region TaxDocumentReference

        public virtual void AddTaxDocumentReference(TaxDocumentReference item)
        {
            if (referencedDocuments.Any(i => i.ReferencedDocument.Id == item.ReferencedDocument.Id))
                return;
            referencedDocuments.Add(item);
            item.Parent = this;
        }

        public virtual void AddReference(TaxDocument referencedDocument, CorrectionCode correction, string correctionComments)
        {
            if (referencedDocuments.Any(i => i.ReferencedDocument.Id == referencedDocument.Id))
                return;

            var obj = new TaxDocumentReference(correction.ToString(), correctionComments, this, referencedDocument);
            referencedDocuments.Add(obj);
        }

        #endregion

        #region Weak References

        public virtual void AddReference(long folio, string doctype, DateTime date, string reason)
        {
            var reference = new TaxDocumentWeakReference(folio, doctype, date, reason, this);

            if (weakReferences.Any(i => i.Folio == folio && i.DocumentType == doctype))
            {
                return;
            }
            weakReferences.Add(reference);
        }

#endregion

        #region Payments

        public virtual void AddPayment(Payment item)
        {
            if (payments.Contains(item))
                return;

            payments.Add(item);
            item.TaxDocument = this;
        }

        public virtual void RemovePayment(Payment item)
        {
            if (!payments.Contains(item))
                return;

            payments.Remove(item);
            item.TaxDocument = null;
        }

        #endregion

        public virtual void AddClient(Enterprise enterprise, string sectorName)
        {
            if(string.IsNullOrWhiteSpace(sectorName))
                throw new ArgumentNullException(nameof(sectorName));

            currentClient = enterprise;
            client = new TaxDocumentClient
            {
                TaxDocument = this,
                TaxId = enterprise.TaxId,
                LegalName = enterprise.LegalName,
                Phone = enterprise.Phone,
                BankAccountNumber = enterprise.BankAccountNumber,
                ComercialActivity = sectorName
            };
            var addresses = new HashSet<TaxDocumentClientAddress>();

            foreach (var i in enterprise.EnterpriseAddresses)
            {
                var address = new TaxDocumentClientAddress
                {
                    City = i.Commune.City.Name,
                    Commune = i.Commune.Name,
                    Line1 = i.Line1,
                    Line2 = i.Line2,
                    AddressTypes = new HashSet<TaxDocumentClientAddressType>(),
                    TaxDocumentClient = client
                };
                foreach (var j in i.EnterpriseAddressTypes)
                {
                    address.AddressTypes.Add(new TaxDocumentClientAddressType(address, j.AddressType));
                }
                addresses.Add(address);
            }

            client.Addresses = addresses;
        }
    }
}