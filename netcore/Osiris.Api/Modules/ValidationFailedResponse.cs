﻿using System.Collections.Generic;
using System.Linq;
using Nancy.Validation;
using Osiris.Api.Responses;
using Osiris.Core.Interfaces.Validation;

namespace Osiris.Api.Modules
{
    public class ValidationFailedResponse
    {
        public List<string> Messages { get; }

        public Dictionary<string, IList<string>>  MessageDictionary { get; }

        public ValidationFailedResponse(ModelValidationResult validationResult)
        {
            Messages = new List<string>();
            ErrorsToStrings(validationResult);
        }

        public ValidationFailedResponse(IEnumerable<IModelValidation> validations)
        {
            Messages = new List<string>();
            ErrorsToStrings(validations);
        }

        public ValidationFailedResponse(IEnumerable<FileErrorValidation> fileErrors)
        {
            Messages = new List<string>();
            MessageDictionary = new Dictionary<string, IList<string>>();
            ErrorsToStrings(fileErrors);
        }

        public ValidationFailedResponse(string message)
        {
            Messages = new List<string>
            {
                message
            };
        }

        private void ErrorsToStrings(ModelValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors.SelectMany(errorGroup => errorGroup.Value))
            {
                Messages.Add(error.ErrorMessage);
            }
        }

        private void ErrorsToStrings(IEnumerable<IModelValidation> validations)
        {
            foreach (var error in validations)
            {
                Messages.Add(error.ErrorMessage);
            }
        }

        private void ErrorsToStrings(IEnumerable<FileErrorValidation> fileErrors)
        {
            foreach (var fileError in fileErrors)
            {
                ErrorsToStrings(fileError.ErrorList);
                MessageDictionary.Add(fileError.Key, Messages);
            }
        }
    }
}
