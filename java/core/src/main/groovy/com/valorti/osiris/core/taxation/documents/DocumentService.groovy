/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface DocumentService {

    boolean createCreditNote()

    boolean createManualCreditNote()

    boolean createDebitNote()

    boolean createManualDebitNote()

    boolean createEBill()

    boolean createManualBill()

    boolean createInvoice()

    boolean createManualInvoice()
}
