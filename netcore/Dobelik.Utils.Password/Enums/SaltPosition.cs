﻿namespace Dobelik.Utils.Password.Enums
{
    /// <summary>
    /// Represents the salt position on the hash.
    /// None - The salt is not added to the hash.
    /// Append - Added to the end of the hash.
    /// Prepend - Added before the hash.
    /// SplitHalf - The salt is split in half: the first half is added at the beginning and the other to the end.
    /// SplitHalfReverse - The same as SplitHalf, but the first half is added to the end and the second half to the beginning.
    /// </summary>
    public enum SaltPosition
        : int
    {
        None = 0,
        Append,
        Prepend,
        SplitHalf,
        SplitHalfReverse
    }
}
