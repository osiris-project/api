﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Dobelik.Utils.Identity;
using log4net;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate;
using Zentom.Core.Entities;
using Zentom.Core.Entities.TaxDocument;
using Zentom.Core.Enums;
using Zentom.Core.Interfaces.Integration.TributaryDocuments;
using Zentom.Core.Interfaces.Validation;
using Zentom.Database;
using Zentom.Integration.Artikos;

namespace Zentom.Tests
{
    [TestClass]
    public class ArtikosTests
    {
        #region Properties

        const string Bill = "33";
        const string ExemptBill = "34";
        const string CreditNote = "61";
        const string DebitNote = "56";
        const string exemptSector = "602220";
        const string subjectSector = "132010";
        const string clientTaxId = "93083482";
        const string providerTaxId = "96950918";
        const string dateFormat = "yyyy-MM-dd";
        ArtikosFactory factory;
        private static ISession session;
        private static Person responsible;
        private static string filePath;

        #endregion

        #region Init

        [TestInitialize]
        public void TestInitialize()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-CL");
            XmlConfigurator.Configure();
            factory = new ArtikosFactory(IdentityLocator.GetInstance().Get());
            var sfactory = NHibernateHelper.SessionFactory;
            Assert.IsNotNull(sfactory);
            session = sfactory.OpenSession();
            session.BeginTransaction();
            responsible = session.QueryOver<Employee>()
                .Left.JoinQueryOver(i => i.Person)
                .Where(i => i.NationalId == "146408133")
                .Take(1)
                .SingleOrDefault().Person;
        }

        #endregion

        #region Finalize

        [TestCleanup]
        public void Cleanup()
        {
            if (session == null) return;
            if (!session.Transaction.IsActive) return;
            try
            {
                session.Transaction.Commit();
            }
            catch
            {
                session.Transaction.Rollback();
                throw;
            }
        }

        #endregion

        #region Bills

        [TestCategory("Bills")]
        [TestMethod]
        public void BillTest()
        {
            var log = LogManager.GetLogger("ArtikosTests");
            log.Debug("Starting test.");

            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            var taxDoc = CreateBill(provider, client);
            var formatter = factory.Resolve(Bill);

            if (formatter.IsValid)
            {
                log.Debug("Trying to commit...");
                log.Debug("Commit done. Trying to write file...");
                Write(formatter, taxDoc);
                log.Debug("Write done.");
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private TaxDocument CreateBill(Tenant tenant, Enterprise client)
        {
            if (tenant == null)
                throw new ArgumentNullException(nameof(tenant));

            if (client == null)
                throw new ArgumentNullException(nameof(client));

            const long folio = 1L;
            var taxDoc = FindTaxDoc(folio, Bill, tenant);

            if (taxDoc != null) return taxDoc;

            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.None,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = Bill,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = .19M,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));
            var product = tenant.Products.Single(i => i.Name == "Producto Afecto 1");
            taxDoc.AddTaxDocumentLine(1, product.Name, 5, "", product.UnitPrice);

            var service = tenant.Services.Single(i => i.Name == "Servicio Afecto 1");
            taxDoc.AddTaxDocumentLine(2, service.Name, 1, "", service.UnitPrice);

            session.Save(taxDoc);

            return taxDoc;
        }

        [TestCategory("Bills")]
        [TestMethod]
        public void ExemptBillTest()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            var taxDoc = CreateExemptBill(provider, client);
            var formatter = factory.Resolve(ExemptBill);

            if (formatter.IsValid)
            {
                try
                {

                    session.Transaction.Commit();
                    Write(formatter, taxDoc);
                }
                catch
                {
                    try
                    {
                        session.Transaction.Rollback();
                    }
                    catch (ObjectDisposedException ex)
                    {
                        Console.WriteLine(ex);
                    }

                    throw;
                }
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateExemptBill(Tenant tenant, Enterprise client)
        {
            const long folio = 1L;
            var taxDoc = FindTaxDoc(folio, ExemptBill, tenant);

            if (taxDoc != null) return taxDoc;
            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.None,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = ExemptBill,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = 0,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };

            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == exemptSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);

            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == exemptSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));

            var product = tenant.Products.Single(i => i.Name == "Producto Exento 1");
            taxDoc.AddTaxDocumentLine(1, product.Name, 3, "", product.UnitPrice);

            var service = tenant.Services.Single(i => i.Name == "Servicio Exento 1");
            taxDoc.AddTaxDocumentLine(2, service.Name, 1, "", service.UnitPrice);

            session.Save(taxDoc);

            return taxDoc;
        }

        #endregion

        #region Credit Notes

        [TestCategory("Credit Notes")]
        [TestMethod]
        public void CreditNoteToAmendText()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            client.LegalName = "Google Inc.";
            var taxDoc = CreateNoteToAmendText(provider, client);

            var formatter = factory.Resolve(CreditNote);

            if (formatter.IsValid)
            {
                Write(formatter, taxDoc);
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateNoteToAmendText(Tenant tenant, Enterprise client)
        {
            const long folio = 1L;
            var taxDoc = FindTaxDoc(folio, CreditNote, tenant);

            if (taxDoc != null) return taxDoc;
            Console.WriteLine(@"taxdoc is null.");
            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.AmendText,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = CreditNote,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = .19M,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));
            taxDoc.AddTaxDocumentLine(1, "Modifica el nombre del cliente");

            var refdoc = FindTaxDoc(1L, ExemptBill, tenant);
            var taxDocRef = new TaxDocumentReference
            {
                ReferencedDocument = refdoc,
                ReferenceComment = "Modifica el texto del documento",
                ReferenceReason = CorrectionCode.AmendText.ToString()
            };
            taxDoc.AddTaxDocumentReference(taxDocRef);

            session.Save(taxDoc);

            return taxDoc;
        }

        [TestCategory("Credit Notes")]
        [TestMethod]
        public void CreditNoteToAmendValues()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);

            var taxDoc = CreateCreditNoteToAmendValues(provider, client);
            var formatter = factory.Resolve(CreditNote);

            if (formatter.IsValid)
            {
                Write(formatter, taxDoc);
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateCreditNoteToAmendValues(Tenant tenant, Enterprise client)
        {
            const long folio = 2L;
            var taxDoc = FindTaxDoc(folio, CreditNote, tenant);

            if (taxDoc != null) return taxDoc;
            var refDoc = FindTaxDoc(1L, ExemptBill, tenant);

            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.AmendValues,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = CreditNote,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = 0,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));

            var reference = new TaxDocumentReference
            {
                Parent = taxDoc,
                ReferencedDocument = refDoc,
                ReferenceReason = CorrectionCode.AmendValues.ToString(),
                ReferenceComment = "Corrige montos - Elimina el Producto 1"
            };

            var product = tenant.Products.Single(i => i.Name == "Producto Exento 1");
            taxDoc.AddTaxDocumentLine(1, product.Name, 2, "", product.UnitPrice);
            taxDoc.AddTaxDocumentReference(reference);

            session.Save(taxDoc);

            return taxDoc;
        }

        [TestCategory("Credit Notes")]
        [TestMethod]
        public void CreditNoteToVoidDocument()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            var taxDoc = CreateCreditNoteToVoidDocument(provider, client);
            var formatter = factory.Resolve(CreditNote);

            if (formatter.IsValid)
            {
                Write(formatter, taxDoc);
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateCreditNoteToVoidDocument(Tenant tenant, Enterprise client)
        {
            const long folio = 3L;
            var taxDoc = FindTaxDoc(folio, CreditNote, tenant);

            if (taxDoc != null) return taxDoc;

            var refdoc = FindTaxDoc(1L, Bill, tenant);
            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.VoidDocument,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = CreditNote,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = .19M,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));

            var reference = new TaxDocumentReference
            {
                Parent = taxDoc,
                ReferencedDocument = refdoc,
                ReferenceReason = CorrectionCode.VoidDocument.ToString(),
                ReferenceComment = "Anulación"
            };

            foreach (var item in refdoc.TaxDocumentLines)
            {
                taxDoc.AddTaxDocumentLine(item.LineNumber, item.Text, item.Quantity, item.AdditionalGloss, item.UnitPrice, item.MeasureUnit);
            }

            taxDoc.AddTaxDocumentReference(reference);

            session.Save(taxDoc);

            return taxDoc;
        }

        #endregion

        #region Debit Notes

        [TestCategory("Debit Notes")]
        [TestMethod]
        public void DebitNoteToAmendValues()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            var taxDoc = CreateDebitNoteToAmendValues(provider, client);
            var formatter = factory.Resolve(DebitNote);

            if (formatter.IsValid)
            {
                Write(formatter, taxDoc);
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateDebitNoteToAmendValues(Tenant tenant, Enterprise client)
        {
            const long folio = 1L;
            var taxDoc = FindTaxDoc(folio, DebitNote, tenant);

            if (taxDoc != null)
            {
                return taxDoc;
            }

            var refdoc = FindTaxDoc(1L, Bill, tenant);

            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.AmendValues,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = DebitNote,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = .19M,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));
            var product = tenant.Products.Single(i => i.Name == "Producto Afecto 1");
            taxDoc.AddTaxDocumentLine(1, product.Name, 7, "", product.UnitPrice);
            var reference = new TaxDocumentReference
            {
                Parent = taxDoc,
                ReferencedDocument = refdoc,
                ReferenceReason = CorrectionCode.AmendValues.ToString(),
                ReferenceComment = "Modifica la cantidad de Producto Afecto 1"
            };
            taxDoc.AddTaxDocumentReference(reference);

            session.Save(taxDoc);
            return taxDoc;
        }

        [TestCategory("Debit Notes")]
        [TestMethod]
        public void DebitNoteToVoidDocument()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);
            var taxDoc = CreateDebitNoteToVoidDocument(provider, client);
            var formatter = factory.Resolve(DebitNote);

            if (formatter.IsValid)
            {
                Write(formatter, taxDoc);
            }
            else
            {
                LogErrors(formatter.Errors);
            }
        }

        private static TaxDocument CreateDebitNoteToVoidDocument(Tenant tenant, Enterprise client)
        {
            const long folio = 2L;
            var taxDoc = FindTaxDoc(folio, DebitNote, tenant);

            if (taxDoc != null) return taxDoc;

            var reftaxdoc = FindTaxDoc(3L, CreditNote, tenant);
            taxDoc = new TaxDocument
            {
                AccountingDate = new DateTime(2015, 6, 1),
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.VoidDocument,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = DebitNote,
                DocumentResponsible = responsible.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = .19M,
                ExtraTaxRate = 0,
                IsRetainedExtraTax = false,
                EmissionPoint = "localhost"
            };
            var comAct = tenant.TenantComercialActivities.FirstOrDefault();
            var sector = tenant.TenantEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddTenant(tenant, sector.Code, comAct?.ComercialActivity.Name, sector.Name);
            sector = client.EnterpriseEconomicActivities.Single(i => i.EconomicActivity.Code == subjectSector).EconomicActivity;
            taxDoc.AddClient(client, sector.Name);
            taxDoc.VatOwnerTaxId = tenant.TaxId;
            filePath = string.Concat(@"D:\vhosts\uploads\", GetFileName(taxDoc));

            foreach (var item in reftaxdoc.TaxDocumentLines)
            {
                taxDoc.AddTaxDocumentLine(item.LineNumber, item.Text, item.Quantity, item.AdditionalGloss, item.UnitPrice, item.MeasureUnit);
            }

            var reference = new TaxDocumentReference
            {
                Parent = taxDoc,
                ReferencedDocument = reftaxdoc,
                ReferenceReason = CorrectionCode.VoidDocument.ToString(),
                ReferenceComment = "Anula nota de crédito nº 3."
            };
            taxDoc.AddTaxDocumentReference(reference);
            session.Save(taxDoc);

            return taxDoc;
        }

        #endregion

        [TestMethod]
        public void DeleteAll()
        {
            var tenant = GetTenant(providerTaxId);
            var taxDoc = FindTaxDoc(1, Bill, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(1, ExemptBill, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(1, CreditNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(2, CreditNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(3, CreditNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(4, CreditNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(1, DebitNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);

            taxDoc = FindTaxDoc(2, DebitNote, tenant);
            if (taxDoc != null)
                session.Delete(taxDoc);
        }

        [TestMethod]
        public void Test()
        {
            var tenant = session.QueryOver<Tenant>()
                .Where(i => i.TaxId == "96950918")
                .Take(1)
                .SingleOrDefault();

            var list = session.QueryOver<TaxDocument>()
                .Where(i => i.AccountingDate > new DateTime(2015, 1, 1) && i.AccountingDate < DateTime.Now && i.Tenant == tenant)
                .Take(5)
                .List();

            var recibidos = session.QueryOver<ReceivedTaxDocument>()
                .Where(i => i.AccountingDate > new DateTime(2015, 1, 1) && i.AccountingDate < DateTime.Now && i.Tenant == tenant)
                .Take(5)
                .List();
        }

        public enum LedgerType
        {
            Mensual,
            Especial,
            Total
        }

        #region Utilities

        private static void Write(ITributaryDocumentFormatter<TaxDocument> instance, TaxDocument taxDoc)
        {
            using (var stream = new StreamWriter(filePath, false, Encoding.GetEncoding("ISO-8859-1")))
            {
                instance.Format(stream, taxDoc);
            }
        }

        private static void LogErrors(IEnumerable<IModelValidation> errors)
        {
            foreach (var error in errors)
            {
                Console.WriteLine(@"Key = {0}, Message = {1}.", error.MemberName, error.ErrorMessage);
            }
        }

        private static string GetFileName(TaxDocument taxDoc)
        {
            return string.Concat(
                taxDoc.Tenant.TaxId.Replace(".", ""), "_",
                taxDoc.DocumentType, "_",
                taxDoc.FolioNumber, "_",
                DateTime.Now.ToString(string.Concat(dateFormat, "_HH-mm-ss")),
                ".txt");
        }

        private static Enterprise GetEnterprise(string taxId)
        {
            return session.QueryOver<Enterprise>().Where(i => i.TaxId == taxId).Take(1)
.SingleOrDefault();
        }

        private static Tenant GetTenant(string taxid)
        {
            return session.QueryOver<Tenant>().Where(i => i.TaxId == taxid).Take(1)
.SingleOrDefault();
        }

        private static TaxDocument FindTaxDoc(long folio, string doctypeCode, Tenant enterprise)
        {
            return
                session.QueryOver<TaxDocument>()
                    .Where(i => i.FolioNumber == folio && i.DocumentType == doctypeCode && i.Tenant == enterprise)
                    .Take(1)
.SingleOrDefault();
        }

        //private static TaxDocument FindTaxDoc(long folio, string doctype, string tenantTaxId)
        //{
        //    return session.QueryOver<TaxDocument>()
        //        .Where(i => i.FolioNumber == folio && i.DocumentType == doctype && i.Tenant.TaxId == tenantTaxId)
        //        .Take(1).SingleOrDefault();
        //}

        #endregion
    }
}