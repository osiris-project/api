﻿namespace Zentom.Core.Entities.Ledger
{
    public abstract class LedgerSummary
    {
        private int docType;

        private int docCount;

        private int nullDocs;

        private int exemptDocs;

        private decimal exemptAmount;

        private decimal netAmount;

        private decimal vatAmount;

        private decimal total;

        /// <summary>
        /// The type of documents included in this summary.
        /// </summary>
        public virtual int DocType
        {
            get { return docType; }
            set { docType = value; }
        }

        /// <summary>
        /// Number of documents of the specified <see cref="DocType"/>
        /// </summary>
        public virtual int DocCount
        {
            get { return docCount; }
            set { docCount = value; }
        }

        /// <summary>
        /// Number of nulled documents that weren't send to the tax collection service.
        /// </summary>
        public virtual int NullDocs
        {
            get { return nullDocs; }
            set { nullDocs = value; }
        }

        /// <summary>
        /// Number of Exempt documents.
        /// </summary>
        public virtual int ExemptDocs
        {
            get { return exemptDocs; }
            set { exemptDocs = value; }
        }

        /// <summary>
        /// Total exempt amount.
        /// </summary>
        public virtual decimal ExemptAmount
        {
            get { return exemptAmount; }
            set { exemptAmount = value; }
        }

        /// <summary>
        /// Total net amount.
        /// </summary>
        public virtual decimal NetAmount
        {
            get { return netAmount; }
            set { netAmount = value; }
        }

        /// <summary>
        /// Total vat amount.
        /// </summary>
        public virtual decimal VatAmount
        {
            get { return vatAmount; }
            set { vatAmount = value; }
        }

        /// <summary>
        /// Sum of the other totals.
        /// </summary>
        public virtual decimal Total
        {
            get { return total; }
            set { total = value; }
        }

        protected LedgerSummary() { }

        protected LedgerSummary(int docType, int docCount, int nullDocs, int exemptDocs,
            decimal exemptAmount, decimal netAmount, decimal vatAmount, decimal total)
        {
            this.docType = docType;
            this.docCount = docCount;
            this.nullDocs = nullDocs;
            this.exemptDocs = exemptDocs;
            this.exemptAmount = exemptAmount;
            this.netAmount = netAmount;
            this.vatAmount = vatAmount;
            this.total = total;
        }
    }
}
