namespace Osiris.Core.Models {
    /// <summary>
    /// Data required to validate a user credentials.
    /// </summary>
    public class AccountValidation {
        /// <summary>
        /// Builds an account validation model.
        /// </summary>
        /// <param name="login">The user's login.</param>
        /// <param name="password">The password to check.</param>
        public AccountValidation(string login, string password) {
            Login = login;
            Password = password;
        }

        public string Login { get; }

        public string Password { get; }
    }
}
