﻿using FluentValidation;

namespace Zentom.Api.Modules.Auth
{
    public class RecoverPasswordValidator
        : AbstractValidator<RecoverPasswordCmd>
    {
        public RecoverPasswordValidator()
        {
            RuleFor(i => i.Login).NotEmpty();
            RuleFor(i => i.ReturnUri).NotEmpty();
        }
    }
}