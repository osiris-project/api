package com.valorti.osiris.rest

/**
 * The base controller for the application.
 *
 * @author Nicolas Mancilla
 * @version 0.0.1
 * @since 0.0.1
 */
abstract class AbstractController {

    /**
     * Constructor that calls the defineEndpoints function.
     */
    protected AbstractController() {
        defineEndpoints()
    }

    /**
     * Defines the endpoints that the controller uses.
     */
    abstract void defineEndpoints()
}
