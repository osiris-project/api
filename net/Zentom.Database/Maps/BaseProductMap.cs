﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    class BaseProductMap
        : ClassMap<ProductBase>
    {
        public BaseProductMap()
        {
            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(100).Unique();
            Map(p => p.UnitPrice).Precision(13).Scale(4);
            Map(i => i.IsDiscontinued).Default("0");
            UseUnionSubclassForInheritanceMapping();

            References(i => i.Tenant, "tenant_id")
                .Unique()
                .Cascade.SaveUpdate();
        }
    }
}
