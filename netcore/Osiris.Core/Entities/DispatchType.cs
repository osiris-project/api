﻿using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public class DispatchType
        : Entity
    {
        #region Fields

        private string _code;

        private string _description;

        private ISet<TaxDocument.TaxDocument> _taxDocuments;

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual ISet<TaxDocument.TaxDocument> TaxDocuments
        {
            get { return _taxDocuments; }
            set { _taxDocuments = value; }
        }

        #endregion

        public DispatchType()
        {
            _taxDocuments = new HashSet<TaxDocument.TaxDocument>();
        }

        public DispatchType(string code, string description, ISet<TaxDocument.TaxDocument> taxDocuments)
            : this()
        {
            _code = code;
            _description = description;

            if(taxDocuments != null)
                _taxDocuments = taxDocuments;
        }
    }
}
