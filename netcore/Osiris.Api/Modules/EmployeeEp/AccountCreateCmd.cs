﻿using System;

namespace Osiris.Api.Modules.EmployeeEp
{
    public class AccountCreateCmd
        : AccountCmd
    {
        public string Password { get; set; }

        public Guid PersonId { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}, Password: {Password}, PersonId: {PersonId}";
        }
    }
}
