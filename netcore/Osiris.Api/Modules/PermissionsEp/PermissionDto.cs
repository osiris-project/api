﻿namespace Osiris.Api.Modules.PermissionsEp
{
    public class PermissionDto
        : Command
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
