﻿namespace Osiris.Core.Entities
{
    public abstract class CompanyEconomicActivity
        : CompanyEntity
    {
        #region Fields

        private EconomicActivity economicActivity;

        #endregion

        #region Properties

        public virtual EconomicActivity EconomicActivity
        {
            get { return economicActivity; }
            set { economicActivity = value; }
        }

        #endregion

        protected CompanyEconomicActivity()
        {
        }

        protected CompanyEconomicActivity(Company enterprise, EconomicActivity economicActivity)
            : base(enterprise)
        {
            this.economicActivity = economicActivity;
        }
    }
}
