﻿namespace Osiris.Core.Entities
{
    public abstract class CompanyAddressType
        : Entity
    {
        #region Fields

        private CompanyAddress companyAddress;

        private AddressType addressType;

        #endregion

        #region Properties

        public virtual AddressType AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        protected virtual CompanyAddress CompanyAddress
        {
            get { return companyAddress; }
            set { companyAddress = value; }
        }

        #endregion

        protected CompanyAddressType() { }

        protected CompanyAddressType(CompanyAddress companyAddress, AddressType addressType)
        {
            this.companyAddress = companyAddress;
            this.addressType = addressType;
        }
    }
}
