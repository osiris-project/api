﻿using System;

namespace Zentom.Api.Error
{
    public class InvalidTokenExpiryDateException
        : Exception
    {
        public InvalidTokenExpiryDateException()
            : base("The token expiry date is not valid, null or empty. Someone may have tampered with it.")
        {
        }
    }
}