﻿namespace Zentom.Api.Modules.SpecificTaxEp
{
    public class SpecificTaxQuery
    {
        public string Name { get; set; }

        public int Code { get; set; }
    }
}