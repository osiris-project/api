﻿using FluentValidation;
using Osiris.Api.Modules.Validators.Guid;

namespace Osiris.Api.Modules.TaxDocumentEp.Validation
{
    public class VoidDocumentValidator
        : AbstractValidator<VoidDocumentCmd>
    {
        public VoidDocumentValidator()
        {
            RuleFor(i => i.ReferencedDocumentId).SetValidator(new GuidValidator());
            RuleFor(i => i.VoidReason).Length(1, 90);
        }
    }
}
