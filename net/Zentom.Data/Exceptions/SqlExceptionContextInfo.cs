﻿using NHibernate.Exceptions;

namespace Zentom.Data.Exceptions
{
    public class SqlExceptionContextInfo
        : AdoExceptionContextInfo
    { }
}
