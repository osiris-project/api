﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    public class TenantGroupMap
        : ClassMap<TenantGroup>
    {
        public TenantGroupMap()
        {
            Schema("organization");
            Table("tenant_group");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_group_un");

            References(i => i.Group)
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_group_un");
        }
    }
}
