﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Osiris.Database.Conventions
{
    class OsirisReferencesConvention
        : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            instance.Not.Nullable();
        }
    }
}
