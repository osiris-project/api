﻿using System;

namespace Osiris.Api.Modules
{
    public class DateRange
    {
        public DateTime StartDate { get; set; } 

        public DateTime? EndDate { get; set; }
    }
}
