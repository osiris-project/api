﻿using Zentom.Core.Interfaces.Validation;

namespace Zentom.Api.Modules
{
    public class ModelError
        : IModelValidation
    {
        private string memberName;

        private string errorMessage;

        #region Implementation of IModelValidation

        public string MemberName
        {
            get { return memberName; }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
        }

        #endregion

        public ModelError(string memberName, string errorMessage)
        {
            this.memberName = memberName;
            this.errorMessage = errorMessage;
        }
    }
}