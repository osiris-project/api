﻿using FluentValidation;

namespace Zentom.Api.Modules.EconomicActivityEp
{
    public class EconomicActivityCmdValidator
        : AbstractValidator<EconomicActivityCmd>
    {
        public EconomicActivityCmdValidator()
        {
            RuleFor(i => i.Name).Length(1, 110);
            RuleFor(i => i.Code).Length(1, 6);
        }
    }
}