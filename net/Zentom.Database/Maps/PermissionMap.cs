﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class PermissionMap
        : ClassMap<Permission>
    {
        public PermissionMap()
        {
            Schema("system");
            Table("permission");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(100).Unique();
            Map(p => p.Description).Length(300).Default("").Nullable();

            HasMany(i => i.RolePermissions)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("permission_id");
        }
    }
}
