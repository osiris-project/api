﻿using System;

namespace Zentom.Api.Modules.AccountingEp
{
    public class CostsCenterDto
    {
        public Guid Id { get; set; } 

        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}