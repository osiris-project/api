﻿using log4net;
using Nancy;
using Nancy.ModelBinding;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.ServiceEp
{
    public class ServiceModule
        : CRUDLModule<Command, ServiceUpdateCmd, ServiceUpdateCmd>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ServiceModule));

        public ServiceModule(ISessionFactory factory)
            : base("services", factory)
        {
        }

        [Permission(Permission = "service-create")]
        public override object Create(ServiceUpdateCmd cmd, dynamic args)
        {
            log.Debug("Entering CreateService.");
            log.DebugFormat("Command: {0}", cmd);
            var found = db.Exists<Service>(i =>
                i.Name == cmd.Name && i.Tenant == currentTenant);
            if (found)
                return Negotiate.RespondWithValidationFailure(string.Format(I18n.Error.ExistingItemName, I18n.Text.Service));
            var service = new Service
            {
                Name = cmd.Name,
                IsDiscontinued = cmd.IsDiscontinued,
                UnitPrice = cmd.UnitPrice,
                Tenant = currentTenant
            };
            db.Save(service);
            return Negotiate.WithCreated(Context.GetResourceUrl(service.Id), service.Id);
        }

        [Permission(Permission = "service-delete")]
        public override object Remove(Command cmd)
        {
            var prod = db.QueryOver<Service>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            if (prod == null)
                return Negotiate.RespondWithValidationFailure(string.Format(I18n.Error.ServiceNotFound, cmd.Id));

            db.Delete(prod);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        [Permission(Permission = "service-read")]
        public override object Read(Command cmd)
        {
            log.DebugFormat("Trying to locate Service '{0}'.", cmd.Id);
            var prod = db.QueryOver<Service>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .ProjectToObject<Service, ServiceDto>();

            if (prod == null)
                return Negotiate.RespondWithValidationFailure(
                    string.Format(I18n.Error.ServiceNotFound, cmd.Id));

            return Negotiate.WithModel(prod)
                .WithStatusCode(HttpStatusCode.OK);
        }

        [Permission(Permission = "service-list")]
        public override object List(dynamic args)
        {
            log.DebugFormat("Entering GetServices.");
            var bound = this.BindTo(new ServiceQuery());
            log.DebugFormat("Query: {0}", bound);
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var query = db.QueryOver<Service>()
                .Where(i => i.Tenant == currentTenant);

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike(bound.Name);
            }

            var items = query.OrderBy(Settings.OrderBy)
                .ProjectInto<Service, ServiceDto>()
                .PaginateResults<Service, ServiceDto>(page, rpp);
            
            if (items == null)
                return Negotiate.WithNotFound();

            log.DebugFormat("Leaving GetServices.");
            return Negotiate.WithOkAndList(items, Request.Url.ToString());
        }

        [Permission(Permission = "service-update")]
        public override object Update(ServiceUpdateCmd cmd)
        {
            log.DebugFormat("Updating service: {0}", cmd);
            var found = db.QueryOver<Service>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            if (found == null)
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.ServiceNotFound);
            found.Name = cmd.Name;
            found.UnitPrice = cmd.UnitPrice;
            found.IsDiscontinued = cmd.IsDiscontinued;
            db.Update(found);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }
    }
}