﻿namespace Osiris.Api.Modules.EmployeeEp
{
    public class AccountCmd
        : Command
    {
        #region Properties

        public string Login { get; set; }

        public string Email { get; set; }


        #endregion

        public override string ToString()
        {
            return $"Id: {Id}, Login: {Login}, Email: {Email}";
        }
    }
}
