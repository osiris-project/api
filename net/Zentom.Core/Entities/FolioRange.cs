﻿using System;

namespace Zentom.Core.Entities
{
    public enum RangeStatus
        : short
    {
        Disabled,
        Enabled
    }


    public class FolioRange
        : TenantEntity
    {
        #region Fields

        private string documentType;

        private long rangeStart;

        private long rangeEnd;

        private bool isCurrentRange;

        private long currentFolio;

        private string unusedFolios;

        private DateTime createdDate;

        private DateTime requestDate;

        private RangeStatus status;

        #endregion

        #region Properties

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual long RangeStart
        {
            get { return rangeStart; }
            set { rangeStart = value; }
        }

        public virtual long RangeEnd
        {
            get { return rangeEnd; }
            set { rangeEnd = value; }
        }

        public virtual long CurrentFolio
        {
            get { return currentFolio; }
            set { currentFolio = value; }
        }

        public virtual string UnusedFolios
        {
            get { return unusedFolios; }
            set { unusedFolios = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public virtual DateTime RequestDate
        {
            get { return requestDate; }
            set { requestDate = value; }
        }

        public virtual RangeStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public virtual bool IsCurrentRange
        {
            get { return isCurrentRange; }
            set { isCurrentRange = value; }
        }

        public virtual bool IsUsed => CurrentFolio > RangeEnd && string.IsNullOrEmpty(UnusedFolios);

        public virtual decimal UsagePercent => (CurrentFolio*100M/(RangeEnd - RangeStart + 1));

        #endregion

        #region CTORs.

        public FolioRange()
        {
        }

        public FolioRange(Tenant tenant, string documentType, long rangeStart, long rangeEnd, bool isCurrentRange,
            string unusedFolios, long currentFolio, DateTime requestDate, RangeStatus status = RangeStatus.Disabled) : base(tenant)
        {
            this.documentType = documentType;
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
            this.isCurrentRange = isCurrentRange;
            this.unusedFolios = unusedFolios;
            this.currentFolio = currentFolio;
            createdDate = DateTime.UtcNow;
            this.requestDate = requestDate;
            this.status = status;
        }

        #endregion
    }
}