﻿namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class ReferenceDto
    {
        public string Comment { get; set; } 

        public long ReferencedFolio { get; set; }

        public string ReferencedDocumentType { get; set; }

        public string ReferenceReason { get; set; }
    }
}