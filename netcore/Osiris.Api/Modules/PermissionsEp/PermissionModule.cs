﻿using Slf4Net;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.PermissionsEp
{
    public class PermissionModule
        : TenantApi
    {
        private static readonly ILogger log = LoggerFactory.GetLogger(typeof (PermissionModule));
        public PermissionModule(ISessionFactory factory)
            : base("permissions", factory)
        {
            this.Get<Command>(BasePath, ReadPermission);
            this.Get(BasePath, ListPermissions);
        }

        [Permission(Permission = "permission-list")]
        public Negotiator ListPermissions(dynamic arg)
        {
            log.Debug("Retrieving permission list.");
            var bound = this.BindTo(new PermissionQuery());
            var query = db.QueryOver<Permission>();
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var page = Settings.PaginationSettings.Page;
            log.Debug("Starting at page {0}; Results Per page: {1}", page, rpp);

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike(bound.Name);
            }

            PermissionDto dto = null;
            var list = query.SelectList(i =>
                i.Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.Name).WithAlias(() => dto.Name)
                    .Select(j => j.Description).WithAlias(() => dto.Description))
                    .TransformUsing(Transformers.AliasToBean<PermissionDto>())
                .PaginateResults<Permission, PermissionDto>(page, rpp);

            log.Debug("Retrieved permission count: {0}", list?.Items.Count);
            return list == null ? Negotiate.WithNotFound() : Negotiate.WithOkAndList(list, Context.Request.Url);
        }

        [Permission(Permission = "permission-read")]
        public Negotiator ReadPermission(Command arg)
        {
            var entity = db.Get<Permission>(arg.Id);

            if (entity == null)
            {
                return Negotiate.WithNotFound(arg.Id, I18n.Error.PermissionNotFound);
            }

            return Negotiate.WithOkAndModel(new
            {
                id = entity.Id,
                name = entity.Name,
                description = entity.Description
            });
        }
    }
}
