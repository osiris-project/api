﻿namespace Zentom.Core.Entities
{
    public class ComercialActivity
        : Entity
    {
        #region Fields

        private string name;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion

        public ComercialActivity()
        {
        }

        public ComercialActivity(string name)
        {
            this.name = name;
        }
    }
}
