﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseStaff
        : IVersioned
    {
        bool IsEmployee { get; set; }

        bool IsAgent { get; set; }

        IEnterprise Enterprise { get; set; }

        IEmployee Employee { get; set; }
    }

    public interface IEnterpriseStaffId
    {
        IEnterprise Enterprise { get; set; }

        IEmployee Employee { get; set; }
    }
}
