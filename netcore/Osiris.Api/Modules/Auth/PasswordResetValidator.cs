﻿using FluentValidation;
using Osiris.Api.Modules.Validators.Guid;

namespace Osiris.Api.Modules.Auth
{
    public class PasswordResetValidator
        : AbstractValidator<PasswordResetCmd>
    {
        public PasswordResetValidator()
        {
            RuleFor(i => i.Id).SetValidator(new GuidValidator());
            RuleFor(i => i.Password).Length(6, 1000);
        }
    }
}
