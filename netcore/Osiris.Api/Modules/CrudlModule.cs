﻿using Nancy;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;

namespace Osiris.Api.Modules {
    public abstract class CRUDLModule<TRead, TCreate, TUpdate> : TenantApi {
        protected CRUDLModule (string resourceName, ISessionFactory factory) : base (resourceName, factory) {
            this.Get<TRead> (BasePath + "/{id:guid}", Read);
            this.Get (BasePath, List);

            this.Post<TCreate> (BasePath, Create);
            this.Put<TUpdate> (BasePath + "/{id:guid}", Update);

            this.Delete<Command> (BasePath + "/{id:guid}", Remove);

        }

        public virtual Negotiator Read (TRead cmd) {
            return Negotiate.WithStatusCode (HttpStatusCode.MethodNotAllowed);
        }

        public virtual Negotiator List (dynamic args) {
            return Negotiate.WithStatusCode (HttpStatusCode.MethodNotAllowed);
        }

        #region Post/Put/Delete

        public virtual Negotiator Create (TCreate cmd, dynamic args) {
            return Negotiate.WithStatusCode (HttpStatusCode.MethodNotAllowed);
        }

        public virtual Negotiator Update (TUpdate cmd) {
            return Negotiate.WithStatusCode (HttpStatusCode.MethodNotAllowed);
        }

        public virtual Negotiator Remove (Command id) {
            return Negotiate.WithStatusCode (HttpStatusCode.MethodNotAllowed);
        }

        #endregion
    }
}
