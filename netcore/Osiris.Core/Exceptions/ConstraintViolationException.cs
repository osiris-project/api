﻿using System;

namespace Osiris.Core.Exceptions
{
    [Serializable]
    public class ConstraintViolationException
        : Exception
    {
        public ConstraintViolationException() { }

        public ConstraintViolationException(string message)
            : base(message) {}

        public ConstraintViolationException(string message, Exception innerException)
            : base(message, innerException) { }

        public ConstraintViolationException(Exception exception)
            : base(exception.Message, exception)
        { }
    }
}
