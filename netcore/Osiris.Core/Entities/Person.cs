﻿using System;
using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public enum Gender
        : short
    {
        None,
        Male,
        Female
    }

    public class Person
        : Entity
    {
        #region Fields

        private string nationalId;

        private string firstName;

        private string lastName;

        private string fullName;

        private DateTime? birthDate;
        
        private Gender gender;

        // TODO: Add address support
        // private ISet<Address> addresses = new HashSet<Address>();

        private Account account;

        private ISet<Employee> employers = new HashSet<Employee>();

        #endregion

        #region Properties

        public virtual string NationalId
        {
            get { return nationalId; }
            set { nationalId = value; }
        }

        public virtual string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public virtual string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public virtual string FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        public virtual DateTime? BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        public virtual Gender Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public virtual Account Account
        {
            get { return account; }
            set { account = value; }
        }

        public virtual ISet<Employee> Employers
        {
            get { return employers; }
            set { employers = value; }
        }

        #endregion

        public Person()
        {
        }

        public Person(string firstName, string lastName, DateTime? birthDate, Gender gender, string nationalId,
            Account account, ISet<Employee> employers)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            this.gender = gender;
            this.nationalId = nationalId;
            this.account = account;
            this.employers = employers;
        }
    }
}
