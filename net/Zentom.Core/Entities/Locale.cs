﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class Locale
        : Entity
    {
        #region Fields

        private string _code;

        private string _fullName;

        private ISet<CountryLocale> _countryLocales;

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        public virtual ISet<CountryLocale> CountryLocales
        {
            get { return _countryLocales; }
            set { _countryLocales = value; }
        }

        #endregion

        public Locale()
        {
            _countryLocales = new HashSet<CountryLocale>();
        }

        public Locale(string code, string fullName, ISet<CountryLocale> countryLocales)
            : this()
        {
            _code = code;
            _fullName = fullName;
            if (countryLocales != null) _countryLocales = countryLocales;
        }
    }
}