﻿namespace Dobelik.Utils.Identity.Instances {
    /// <summary>
    /// Default INationalIdentity implementation. Basically
    /// a mock object.
    /// </summary>
    internal class Default : INationalIdentity {
        public object CalculateVerifierPart (string partialId) {
            return "";
        }

        public string CleanId (string id) {
            return id;
        }

        public string Normalize (string rawId) {
            return rawId;
        }

        public bool Validate (string id) {
            return false;
        }
    }
}
