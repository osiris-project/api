﻿using System;
using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IBaseProduct
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        decimal BasePrice { get; set; }

        bool? IsSubjectToTax { get; set; }

        ISector Sector { get; set; }

        IEmployee Creator { get; set; }

        ISpecificTax SpecificTax { get; set; }
    }
}
