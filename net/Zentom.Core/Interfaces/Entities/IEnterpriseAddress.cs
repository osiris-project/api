﻿using System;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseAddress
        : IAddress
    {
        int? Office { get; set; }

        IEnterprise Enterprise { get; set; }
    }
}