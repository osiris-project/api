/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core

import groovy.util.slurpersupport.GPathResult
import org.junit.Before
import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class XmlTests {

    static String data

    @Before
    void setUp()
        throws Exception {
        data = '''
<SetDTE>
    <DTE version="1.0" >
        <Documento ID="F2262T61">
            <Encabezado>
                <IdDoc>
                    <TipoDTE>61</TipoDTE>
                    <Folio>2262</Folio>
                    <FchEmis>2016-01-08</FchEmis>
                    <FchVenc>2016-01-18</FchVenc>
                </IdDoc>
                <Emisor>
                    <RUTEmisor>20045678-5</RUTEmisor>
                    <RznSoc>P2 SA</RznSoc>
                    <GiroEmis>EMPRESA DE SERVICIOS INTEGRALES DE INFORMATICA</GiroEmis>
                    <Acteco>1</Acteco>
                    <DirOrigen>Dirección falsa 1332, Of. 23</DirOrigen>
                    <CmnaOrigen>SANTIAGO</CmnaOrigen>
                </Emisor>
                <Receptor>
                    <RUTRecep>9695091-8</RUTRecep>
                    <RznSocRecep>Proveedor 1</RznSocRecep>
                    <GiroRecep>Extracción De Oro Y Plata</GiroRecep>
                    <DirRecep>Some fake street 200</DirRecep>
                    <CmnaRecep>SANTIAGO</CmnaRecep>
                </Receptor>
                <Totales>
                    <MntNeto>38744</MntNeto>
                    <TasaIVA>19</TasaIVA>
                    <IVA>7361</IVA>
                    <MntTotal>46105</MntTotal>
                </Totales>
            </Encabezado>
            <Detalle>
                <NroLinDet>1</NroLinDet>
                <NmbItem>SCONT20</NmbItem>
                <DscItem>Contabilidad Plan Inicio, correspondiente a Enero</DscItem>
                <QtyItem>1</QtyItem>
                <PrcItem>38744</PrcItem>
                <MontoItem>38744</MontoItem>
            </Detalle>
            <Detalle>
                <NroLinDet>2</NroLinDet>
                <NmbItem>SCONT20</NmbItem>
                <DscItem>Linea 2</DscItem>
                <QtyItem>1</QtyItem>
                <PrcItem>38744</PrcItem>
                <MontoItem>38744</MontoItem>
            </Detalle>
            <Referencia>
                <NroLinRef>1</NroLinRef>
                <TpoDocRef>33</TpoDocRef>
                <FolioRef>231252</FolioRef>
                <FchRef>2016-01-08</FchRef>
                <CodRef>1</CodRef>
                <RazonRef>ANULA DOCUMENTO</RazonRef>
            </Referencia>
            <TED version="1.0">
                <DD>
                    <RE>76103915-6</RE>
                    <TD>61</TD>
                    <F>2262</F>
                    <FE>2016-01-08</FE>
                    <RR>9695091-8</RR>
                    <RSR>Proveedor 1</RSR>
                    <MNT>46105</MNT>
                    <IT1>SCONT20</IT1>
                    <CAF version="1.0">
                        <DA>
                            <RE>76103915-6</RE>
                            <RS>NUBOX SPA</RS>
                            <TD>61</TD>
                            <RNG>
                                <D>2001</D>
                                <H>2430</H>
                            </RNG>
                            <FA>2015-08-12</FA>
                            <RSAPK>
                                <M>09YYWw40+PGsyeFWyPAmWLYGq/21CbO3jEch0xtadVOBHsC0bVF7vEe/GKYUA6mjBysRpOnzakeAiejkg7O6dw==</M>
                                <E>Aw==</E>
                            </RSAPK>
                            <IDK>300</IDK>
                        </DA>
                        <FRMA algoritmo="SHA1withRSA">Cr385jhpYaLNE2rHzBBSyQSWrD2GAc9FsmjVQvVGUhYZ/CuAtPOqEGB4lNCB12Jy2OsZV19H6sQ0/SlUQ2yHyw==</FRMA>
                    </CAF>
                    <TSTED>2016-01-28T09:15:09</TSTED>
                </DD>
                <FRMT algoritmo="SHA1withRSA">
                    YkAA2pIyNHHgnzJ43HS1gOkL48DWLBTnkcrRx4uaD2jj64wDJsnZ9LzmDa0JQCVJWcPiaVC6EpdBBB4vJd7Icg==
                </FRMT>
            </TED>
            <TmstFirma>2016-01-28T09:15:09</TmstFirma>
        </Documento>
        <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
            <SignedInfo>
                <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
                <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />
                <Reference URI="#F2262T61">
                    <Transforms>
                        <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />
                    </Transforms>
                    <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />
                    <DigestValue>D2X65Y+T+MnldX6NWlHwtovco7Q=</DigestValue>
                </Reference>
            </SignedInfo>
            <SignatureValue>xGHD/b1uOzV9M5q6bCSkvknUuDwNdpJVR0+/AjLVw369MvK08SsbOlgPY3iRuNgWUqx+9br5TQFTJ/e6UUa6Z8R0Q7oTFCgFpLhsXRGEq+GAZbF3ev3dSTA27EfM1PZ1Z58facqnZujytQyUUahJ0361gU5GrzrntKfvAnfXBvSbup3Rt3W+yPA5kEsAgA3UJWRnpeh8Weji+ll47Nx0tCdCz0w2f0Q+/D2l2NLnyhtVXHoZB6fbl0US7hzI/5ca3dAvSb5rpsDb0WrQkhM59+0MEp7Eu3Li3v5HBvijduw/WRh9fIPAFTQ7UUHzsFm/dwFSVLy99PwO9b00B4JmWA==</SignatureValue>
            <KeyInfo>
                <KeyValue>
                    <RSAKeyValue>
                        <Modulus>
                            ymr+9v8FFUBF3eue5yxEiMVu5Nhk5twjw9pv7IXw/1HQNYGqFVO/HqVicbiDleoV
                            g0zyDg0t+rJkyWJQQDimmwaxN5eUy5h0pSO8Yj4YD5i+cs4eZ46MMOakpztBnoe8
                            CjMgrUVOJccpXs+LEeAIKaIHACoYaMbgFncv8pV3uEXId/NzjrXDMKjwPV1RfQQn
                            rmpZ1Dz3iR/yvvLBNdvi9myvOMJLUZ5j9YDgitfNUHeTW/wIB3YBktjsSi1Y77el
                            HccPIg0kRk8qG/qluMqvGN1m4WRY/AQRuGg4bZeUoLog7JGX57WNH53jZ7NtMRFs
                            ROKd5IzGGK5j8kQ0AIsUxQ==
                        </Modulus>
                        <Exponent>AQAB</Exponent>
                    </RSAKeyValue>
                </KeyValue>
                <X509Data>
                    <X509Certificate>
                        MIIGTzCCBTegAwIBAgIQNbuqLFOQOJgEkLPFl/+QOTANBgkqhkiG9w0BAQUFADCB
                        qDELMAkGA1UEBhMCQ0wxFDASBgNVBAoTC0UtU2lnbiBTLkEuMR8wHQYDVQQLExZT
                        eW1hbnRlYyBUcnVzdCBOZXR3b3JrMUEwPwYDVQQDEzhFLVNpZ24gU0MgQ2xhc3Mg
                        MiBDb25zdW1lciBJbmRpdmlkdWFsIFN1YnNjcmliZXIgQ0EgLSBHMjEfMB0GCSqG
                        SIb3DQEJARYQZS1zaWduQGUtc2lnbi5jbDAeFw0xNTA3MjQwMDAwMDBaFw0xODA3
                        MjMyMzU5NTlaMIIBGDEUMBIGA1UEChMLRS1TaWduIFMuQS4xLTArBgNVBAsTJFRl
                        cm1pbm9zIGRlIHVzbyBlbiB3d3cuZS1zaWduLmNsL3JwYTElMCMGA1UECxMcQXV0
                        aGVudGljYXRlZCBieSBFLVNpZ24gUy5BLjEnMCUGA1UECxMeTWVtYmVyLCBTeW1h
                        bnRlYyBUcnVzdCBOZXR3b3JrMRswGQYDVQQLExJEaWdpdGFsIElEIENsYXNzIDIx
                        GDAWBgNVBAsUD1JVVCAtIDg3OTE0MTMtNTEkMCIGA1UEAwwbU1VTQU5BIEJFQVRS
                        SVogTFVQTyBWRUxBU0NPMSQwIgYJKoZIhvcNAQkBFhVzdXNhbmEubHVwb0BudWJv
                        eC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKav72/wUVQEXd
                        657nLESIxW7k2GTm3CPD2m/shfD/UdA1gaoVU78epWJxuIOV6hWDTPIODS36smTJ
                        YlBAOKabBrE3l5TLmHSlI7xiPhgPmL5yzh5njoww5qSnO0Geh7wKMyCtRU4lxyle
                        z4sR4AgpogcAKhhoxuAWdy/ylXe4Rch383OOtcMwqPA9XVF9BCeualnUPPeJH/K+
                        8sE12+L2bK84wktRnmP1gOCK181Qd5Nb/AgHdgGS2OxKLVjvt6Udxw8iDSRGTyob
                        +qW4yq8Y3WbhZFj8BBG4aDhtl5SguiDskZfntY0fneNns20xEWxE4p3kjMYYrmPy
                        RDQAixTFAgMBAAGjggIAMIIB/DAiBgNVHREEGzAZoBcGCCsGAQQBwQEBoAsWCTg3
                        OTE0MTMtNTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIFoDBPBgNVHR8ESDBGMESgQqBA
                        hj5odHRwOi8vb25zaXRlY3JsLnZlcmlzaWduLmNvbS9FU2lnblNBQ1NDQ2xhc3My
                        RzIvTGF0ZXN0Q1JMLmNybDAfBgNVHSMEGDAWgBTbz3diWAs4NiUtGtJqxFQBsm5M
                        DjAdBgNVHQ4EFgQUDgwyl5xI5iO4zxmVF+vSZtS5F44wOwYIKwYBBQUHAQEELzAt
                        MCsGCCsGAQUFBzABhh9odHRwOi8vb25zaXRlLW9jc3AudmVyaXNpZ24uY29tMIGY
                        BgNVHSAEgZAwgY0wgYoGC2CGSAGG+EUBBxcCMHswMQYIKwYBBQUHAgEWJWh0dHBz
                        Oi8vd3d3LmUtc2lnbi5jbC9yZXBvc2l0b3Jpby5odG0wRgYIKwYBBQUHAgIwOho4
                        Q2VydGlmaWNhZG8gcGFyYSB1c28gVHJpYnV0YXJpbywgQ29tZXJjaW8sIFBhZ29z
                        IHkgT3Ryb3MwEQYJYIZIAYb4QgEBBAQDAgeAMB0GA1UdJQQWMBQGCCsGAQUFBwMC
                        BggrBgEFBQcDBDAjBgNVHRIEHDAaoBgGCCsGAQQBwQECoAwWCjk5NTUxNzQwLUsw
                        DQYJKoZIhvcNAQEFBQADggEBAO3E0aoqCIEg7iBNNqA0kfSLyTW2lC9gAPz9sovN
                        83FFuUgAhEOtji0JF2ytYskFMdpsMG/17k23yyxAziGtEuLFDN1UJruDbB/k540p
                        w55dz4GdVvWu0UP2GeB/lfbHKP55ttuAtYVbVV1NZtIJUXqy6bZmUkLCOBfyLN/Q
                        WU+m/uxn/yERb4hf64ChMiL4+09kHRaOCWY1C6OmDoshKIlbNrK1ggyVMj51wVa/
                        nPeUZtVh5+n1eooyVBScL2IZFMZ8SSjvIkR4FeFUf1J59M0VnUPs4ryVqGO39WP6
                        4cmYmzQay3lpt+G7egYpLnl9/eXJfBsxqsJGs1Pef1EYT70=
                    </X509Certificate>
                </X509Data>
            </KeyInfo>
        </Signature>
    </DTE>
</SetDTE>
'''
    }

    @Test
    void test_readDetailLines() {
        println 'test_readDetailLines'
        def xml = new XmlSlurper().parseText(data)
        assert xml.DTE.Documento.Detalle[0].DscItem == 'Contabilidad Plan Inicio, correspondiente a Enero'
    }

    @Test
    void test_iterateDetailLines() {
        println 'test_iterateDetailLines'
        def xml = new XmlSlurper().parseText(data)
        def count = 0
        xml.DTE.Documento.Detalle.each {
            assert it.NroLinDet == (count + 1)
            count++
        }
    }

    @Test
    void test_fullPath() {
        println 'test_fullPath'
        def xml = new XmlSlurper().parseText(data)
        def emisor = xml.DTE.Documento.Encabezado.Emisor
        def fullPath = ""
        GPathResult curr = emisor.parent()
        def count = 0
        while (curr != (curr = curr.pop())) {
            //curr = curr.pop()
            println curr.name()
            fullPath = "/${curr.name()}${fullPath}"
            count++
            if (count >= 1000) {
                break
            }
        }
        println fullPath
        assert fullPath == '/DTE/Documento/Encabezado/Emisor'
    }
}
