/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.taxation.CommercialActivity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization', uniqueConstraints = [
    @UniqueConstraint(columnNames = [ 'enterprise_id', 'comercial_activity_id' ])])
class EnterpriseCommercialActivity extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Enterprise enterprise

    @ManyToOne
    CommercialActivity commercialActivity

    /**
     *
     */
    protected EnterpriseCommercialActivity() {
    }

    /**
     * @param enterprise
     * @param comercialActivity
     */
    EnterpriseCommercialActivity(Enterprise enterprise, CommercialActivity commercialActivity) {
        if(!enterprise) {
            throw new IllegalArgumentException('No enterprise provided!')
        }
        if(!commercialActivity) {
            throw new IllegalArgumentException('No commercial activity provided!')
        }
        this.enterprise = enterprise
        this.commercialActivity = commercialActivity
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((commercialActivity == null) ? 0 : commercialActivity.hashCode())
        result = prime * result + ((enterprise == null) ? 0 : enterprise.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof EnterpriseCommercialActivity)) {
            return false
        }
        final other = (EnterpriseCommercialActivity) obj
        if(!Objects.equals(this.enterprise, other.enterprise)) {
            return false
        }
        return Objects.equals(this.commercialActivity, other.commercialActivity)
    }
}
