/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.organization.Tenant

/**
 * Converts an XML document into a {@link Document} object instance.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface DocumentParser {
    /**
     * Retrieves the current detail (<Detalle>) parser.
     * @return An element parser instance.
     */
    SectionParser<DocumentDetail> getDetailParser()

    /**
     * Retrieves the current reference parser.
     * @return The reference parser.
     */
    SectionParser<DocumentReference> getReferenceParser()

    /**
     * Retrieves the current receptor parser.
     * @return The receptor/receiver parser
     */
    SectionParser<DocumentReceiver> getReceptorParser()

    /**
     * Retrieves the current issuer parser.
     * @return The issuer parser.
     */
    SectionParser<DocumentIssuer> getIssuerParser()

    /**
     * Sets the detail parser.
     * @param detailParser
     */
    void setDetailParser(SectionParser<DocumentDetail> detailParser)

    /**
     * Sets the reference parser.
     * @param referenceParser
     */
    void setReferenceParser(SectionParser<DocumentReference> referenceParser)

    /**
     * Sets the receptor parser.
     * @param receptorParser
     */
    void setReceptorParser(SectionParser<DocumentReceiver> receptorParser)

    /**
     * Sets the issuer parser.
     * @param issuerParser
     */
    void setIssuerParser(SectionParser<DocumentIssuer> issuerParser)

    /**
     * Converts an xml document contents into a {@link Document} object.
     * @param filePath
     * @return The parsed document.
     * @throws ValidationException If there are any validation errors.
     * @throws UnexpectedReceiverException If the Receptor of this document is not
     * the current tenant.
     */
    Document parse(String filePath)
        throws ValidationException, UnexpectedReceiverException

    /**
     * Converts an XML document into a {@link Document} object.
     * @param file The file to be converted.
     * @return The parsed document.
     * @throws ValidationException If there are any validation errors.
     * @throws UnexpectedReceiverException If the Receptor of this document is not
     * the current tenant.
     */
    Document parse(File file)
        throws ValidationException, UnexpectedReceiverException

    /**
     * Retrieves the current tenant.
     * @return A tenant instance.
     */
    Tenant getTenant()
}
