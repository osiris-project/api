﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class ServiceMap
        : SubclassMap<Service>
    {
        public ServiceMap()
        {
            Schema("organization");
            Table("service");
            Abstract();
        }
    }
}
