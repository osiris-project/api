/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import com.valorti.osiris.core.ValidationException

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization')
class EnterpriseAddress extends CompanyAddress {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Enterprise enterprise

    /**
     *
     */
    protected EnterpriseAddress() {
    }

    /**
     * @param line1
     * @param line2
     * @param type
     * @param commune
     * @throws IllegalArgumentException If the commune or the enterprise is null.
     * @throws ValidationException if the line1 is empty or null.
     */
    EnterpriseAddress(String line1, String line2, AddressType type, Commune commune, Enterprise enterprise)
        throws IllegalArgumentException, ValidationException {
        super(line1, line2, type, commune)
        if (!enterprise) {
            throw new IllegalArgumentException('No enterprise provided')
        }
        this.enterprise = enterprise
    }
}
