﻿using System;
using System.Globalization;
using System.Linq;
using Dobelik.Utils.Identity;
using log4net;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Enums;
using Osiris.Core.Extensions.String;

namespace Osiris.Integration.Artikos.TributaryDocuments.Artikos
{
    internal abstract class AbstractArtikosFormatter
        : AbstractTributaryDocumentFormatter<TaxDocument>
    {
        #region Fields

        private static readonly ILog log = LogManager.GetLogger(typeof(AbstractArtikosFormatter));

        protected bool isExempt = false;

        /// <summary>
        /// The file type of document.
        /// </summary>
        protected readonly string fileType;

        /// <summary>
        /// Counts the numbers of lines on the document.
        /// </summary>
        protected int registryCount;

        /// <summary>
        /// The owner of the taxes.
        /// </summary>
        protected string vatOwnerTaxId;

        /// <summary>
        /// The service type code.
        /// </summary>
        protected int serviceType;

        /// <summary>
        /// This taxes are not to be included in the document details.
        /// </summary>
        protected readonly int[] excludedExtraTaxes = { 14, 17, 28, 35 };

        /// <summary>
        /// The amount of retained taxes.
        /// </summary>
        protected decimal retainedTaxes;

        protected readonly int maxGlossLength = 200;

        protected decimal nonBillableAmount;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates an instance of class that validates the data,
        /// so the user can call IsValid right after construction.
        /// </summary>
        /// <param name="idUtils">Utility class to manipulate identification numbers.</param>
        protected AbstractArtikosFormatter(INationalIdentity idUtils)
            : base(idUtils)
        {
            registryCount = 0;
            IdUtils = idUtils;
            DateFormat = "yyyy-MM-dd";
            fileType = "DTEELE";
        }

        #endregion

        #region AbstractTributaryDocumentFormatter

        protected override void Setup()
        {
            log.DebugFormat("AbstractArtikosFormatter.Setup.");
            serviceType = entity.ServiceType;
            vatOwnerTaxId = FormatTaxationId(vatOwnerTaxId ?? entity.VatOwnerTaxId);
        }

        /// <summary>
        /// Calls the methods to format the document.
        /// </summary>
        protected override void Format()
        {
            if (IsValid)
            {
                WriteHeader();
                WriteDocumentInfo();
                WriteIssuer();
                WriteReceiver();
                log.DebugFormat("- Details -");
                WriteDetails();
                log.DebugFormat("- END -");
                WriteRetainedTaxes();
                WriteDocumentReferences();
                WriteWeakReferences();
                WriteTotals();
                WriteFinish();
            }
            else
            {
                log.Error("The required fields are not present in the entity object.");
            }
        }

        protected override abstract void Validate();

        #endregion

        #region Format Utilities

        /// <summary>
        /// Writes the document info to the file.
        /// Advances the <see cref="registryCount"/> by one.
        /// </summary>
        protected virtual void WriteDocumentInfo()
        {
            log.DebugFormat("- Document Info. -");
            var tmp = "";
            StreamWriter.Write("0100");

            // Punto de emisión
            StreamWriter.Write(CorrectString(entity.EmissionPoint, 10));
            log.DebugFormat("User: {0}", entity.EmissionPoint);

            // Bill type Code
            StreamWriter.Write(CorrectString(entity.DocumentType, 3));
            log.DebugFormat("Bill type Code:  {0}", entity.DocumentType);

            // Número de folio
            StreamWriter.Write(CorrectNumber(entity.FolioNumber.ToString(), 10));
            log.DebugFormat("Folio number:  {0}", entity.FolioNumber);

            // Número de documento interno
            StreamWriter.Write(CorrectString(entity.DocumentNumber.ToString(), 15));
            log.DebugFormat("Document number:  {0}", entity.DocumentNumber);

            // Fecha de emisión contable
            StreamWriter.Write(entity.AccountingDate.ToString(DateFormat));
            log.DebugFormat("Date issued:  {0}", entity.AccountingDate.ToString(DateFormat));

            // Tipo de servicio
            StreamWriter.Write(serviceType);
            log.DebugFormat("Service type Code:  {0}", serviceType);

            // Indicador de monto neto (sólo para boletas)
            if (entity.DocumentType == "39")
            {
                StreamWriter.Write("1");
                log.DebugFormat("Net amount: 1");
            }
            else if (entity.DocumentType == "41")
            {
                StreamWriter.Write("0");
                log.DebugFormat("Net amount: 0");
            }
            else
            {
                StreamWriter.Write(" ");
                log.DebugFormat("Net amount: empty");
            }

            // Fecha de vencimiento del pago
            if (entity.ExpireDate != null)
            {
                StreamWriter.Write(entity.ExpireDate.Value.ToString(DateFormat));
                log.DebugFormat("Payment expiration date:  {0}", entity.ExpireDate.Value.ToString(DateFormat));
            }
            else
            {
                StreamWriter.Write(GetSpaces(10));
            }

            // Tipo/forma de pago
            if (entity.DefaultPaymentType != null)
            {
                StreamWriter.Write(CorrectString(entity.DefaultPaymentType.Code, 3));
                log.DebugFormat("Payment type:  {0}", tmp);
            }
            else
            {
                StreamWriter.Write(GetSpaces(3));
                log.DebugFormat("Payment type: empty");
            }

            // Formato de impresión
            StreamWriter.Write(CorrectNumber("", 5));

            // Cantidad de líneas de detalle (opcional - omitido).
            StreamWriter.Write(GetZeroes(3));

            // Clase de documento
            StreamWriter.Write(GetSpaces(3));

            // Código de envío (forma en que se despachan los documentos)
            if (entity.DispatchType != null)
            {
                StreamWriter.Write(CorrectString(entity.DispatchType.Code, 4));
                log.DebugFormat("Dispatch Code:  {0}", entity.DispatchType.Code);
            }
            else
            {
                StreamWriter.Write(GetSpaces(4));
                log.DebugFormat("Dispatch Code:  {0}", "none");
            }

            // TODO: implement dispatch guides
            // Indicador de traslado - sólo para guías de despacho.
            // V. 1.0: not supported.
            StreamWriter.Write("0");

            StreamWriter.Write("0");
            log.DebugFormat("Discount indicator:  0");

            // END
            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        /// <summary>
        /// Writes the Header information of the document to the file.
        /// Advances the <see cref="registryCount"/> by one.
        /// </summary>
        protected virtual void WriteHeader()
        {
            StreamWriter.Write("0000");
            log.DebugFormat("- Header -");

            StreamWriter.Write(CorrectString(fileType, 10));
            log.DebugFormat("File type:  {0}", fileType);

            var dt = DateTime.Now;
            // Fecha de emisión del archivo
            StreamWriter.Write(dt.Date.ToString(DateFormat));
            log.DebugFormat("File Date:  {0}", dt.Date.ToString(DateFormat));

            // Hora de emisión (HH:mm:ss)
            StreamWriter.Write(dt.ToString("HH:mm:ss"));
            log.DebugFormat("File time:  {0}", dt.Date.ToString("HH:mm:ss"));

            // Código Sistema Emisión
            StreamWriter.Write("zentom");
            StreamWriter.Write(GetSpaces(4));
            log.DebugFormat("Sistem Code: zentom");

            // Sistema de emisión archivo
            StreamWriter.Write("vti_zentom");
            StreamWriter.Write(GetSpaces(10));
            log.DebugFormat("Second system Code: vti_zentom");

            // END
            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        /// <summary>
        /// Writes the Issuer information of the document to the file.
        /// Advances the <see cref="registryCount"/> by one.
        /// </summary>
        protected virtual void WriteIssuer()
        {
            log.DebugFormat("- Submitter -");
            StreamWriter.Write("0200");

            // Emisor del documento
            StreamWriter.Write(CorrectString(FormatTaxationId(entity.Tenant.TaxId), 12));
            log.DebugFormat("Id:  {0}", entity.Tenant.TaxId);

            // Razón social
            StreamWriter.Write(CorrectString(entity.Tenant.LegalName, 100));
            log.DebugFormat("Name:  {0}", entity.Tenant.LegalName);

            // Giro comercial relevante para el documento actual.
            StreamWriter.Write(CorrectString(entity.TenantComercialActivity, 60));
            log.DebugFormat("Relevant Sector:  {0}", entity.TenantComercialActivity);

            // Código de sucursal SII
            StreamWriter.Write(GetZeroes(9));

            // Dirección de origen
            log.DebugFormat("Src address:  {0}", entity.TenantAddress);
            StreamWriter.Write(CorrectString(entity.TenantAddress, 60));

            // Comuna de origen
            StreamWriter.Write(CorrectString(entity.TenantCommune, 20));
            log.DebugFormat("Src Commune:  {0}", entity.TenantCommune);

            // Ciudad de origen
            StreamWriter.Write(CorrectString(entity.TenantCity, 20));
            log.DebugFormat("Src City:  {0}", entity.TenantCity);

            // Código de actividad económica
            StreamWriter.Write(CorrectString(entity.TenantEconomicActivityCode, 6));
            log.DebugFormat("Sector Code:  {0}", entity.TenantEconomicActivityCode);

            // Responsable del documento
            StreamWriter.Write(CorrectString(entity.DocumentResponsible, 100));
            log.DebugFormat("Document responsible:  {0}", entity.DocumentResponsible);

            // Rut responsable del IVA
            if (!string.IsNullOrEmpty(vatOwnerTaxId))
            {
                StreamWriter.Write(CorrectString(vatOwnerTaxId.Replace(".", ""), 10));
                log.DebugFormat("Tax owner id:  {0}", vatOwnerTaxId);
            }
            else
            {
                StreamWriter.Write(GetSpaces(10));
            }

            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        /// <summary>
        /// Writes the Receiver information of the document to the file.
        /// Advances the <see cref="registryCount"/> by one.
        /// </summary>
        protected virtual void WriteReceiver()
        {
            log.DebugFormat("- Receiver -");

            StreamWriter.Write("0300");

            // Rut del receptor
            var tmp = FormatTaxationId(entity.Client.TaxId);
            StreamWriter.Write(CorrectString(tmp, 12));

            log.DebugFormat("Id:  {0}", entity.Client.TaxId);

            // Razón social
            tmp = CorrectString(entity.Client.LegalName, 100);
            StreamWriter.Write(tmp);
            log.DebugFormat("Legal Name:  {0}", tmp);

            // Número de cuenta corriente
            StreamWriter.Write(CorrectNumber(entity.Client.BankAccountNumber, 15));
            log.DebugFormat("Bank account:  {0}", entity.Client.BankAccountNumber);

            #region Service/Product reception address

            var receptionAddress =
                entity.Client.Addresses.First(
                    i => i.AddressTypes.Any(j => j.AddressType.Name == "service_reception"));

            if (receptionAddress == null)
                throw new Exception("Falta la dirección recepción de los productos/servicios del cliente.");

            tmp = receptionAddress.Line1;
            StreamWriter.Write(CorrectString(tmp, 70));
            log.DebugFormat("Address:  {0}", tmp);

            // Resto de la dirección xD
            tmp = receptionAddress.Line2;
            StreamWriter.Write(CorrectString(tmp, 150));
            log.DebugFormat("Address part:  {0}", tmp);

            // Comuna
            tmp = CorrectString(receptionAddress.Commune, 20);
            StreamWriter.Write(tmp);
            log.DebugFormat("Commune:  {0}", tmp);

            // Ciudad
            tmp = CorrectString(receptionAddress.City, 15);
            StreamWriter.Write(tmp);
            log.DebugFormat("City:  {0}", tmp);

            #endregion

            #region Legal Address

            var mainAddress = entity.Client.Addresses.First(i => i.AddressTypes.Any(j => j.AddressType.Name == "legal"));

            if (mainAddress == null)
                throw new Exception("Falta la dirección legal del cliente.");

            // Dirección postal
            tmp = CorrectString(mainAddress.Line1, 70);
            StreamWriter.Write(tmp);
            log.DebugFormat("Postal Address:  {0}", tmp);

            // Resto de la dirección postal
            tmp = CorrectString(mainAddress.Line2, 150);// string.Concat("Oficina ", address.Office, ", Piso ", address.Floor).Trim();
            StreamWriter.Write(tmp);
            log.DebugFormat("Postal Address part:  {0}", tmp);

            // Comuna postal
            tmp = CorrectString(mainAddress.Commune, 20);
            StreamWriter.Write(tmp);
            log.DebugFormat("Postal Commune:  {0}", tmp);

            // Ciudad
            tmp = CorrectString(mainAddress.City, 15);
            StreamWriter.Write(tmp);
            log.DebugFormat("Postal City:  {0}", tmp);

            #endregion

            // Giro comercial
            tmp = CorrectString(entity.Client.ComercialActivity, 40);
            StreamWriter.Write(tmp);
            log.DebugFormat("Sector Name:  {0}", entity.Client.ComercialActivity);

            // Teléfono
            tmp = CorrectString(entity.Client.Phone, 15);
            StreamWriter.Write(tmp);
            log.DebugFormat("Phone:  {0}", entity.Client.Phone);

            #region Supply Address
            // Falta la dirección de suministro del cliente.

            // Dirección de suministro
            StreamWriter.Write(GetSpaces(70));

            // Dirección resto suministro
            StreamWriter.Write(GetSpaces(150));

            // Comuna suministro
            StreamWriter.Write(GetSpaces(20));

            // Ciudad suministro
            StreamWriter.Write(GetSpaces(15));

            // Número interlocutor comercial
            StreamWriter.Write(GetSpaces(15));

            // Número de instalación
            StreamWriter.Write(GetSpaces(15));

            #endregion

            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        protected virtual void WriteTotals()
        {
            log.DebugFormat("- Totals -");

            var total = NetAmount + VatAmount;// + exemptAmount;

            // Start
            StreamWriter.Write("0301");

            // Monto neto
            StreamWriter.Write(CorrectNumber(Convert.ToInt32(NetAmount).ToString(), 18));
            log.DebugFormat("Net Amount: {0}", NetAmount);

            // Monto exento (boleta electrónica exenta)
            StreamWriter.Write(CorrectNumber(Convert.ToInt32(ExemptAmount).ToString(), 18));
            log.DebugFormat("Exempt Amount: {0}", ExemptAmount);

            // Tasa de IVA
            if (entity.DocumentType == "39")
            {
                StreamWriter.Write(CorrectNumber((entity.TaxRate * 100).ToString(CultureInfo.CurrentCulture), 6));
                log.DebugFormat("Tax Rate: {0}", (entity.TaxRate * 100));
            }
            else
            {
                StreamWriter.Write(GetZeroes(6));
            }

            // Monto de IVA
            StreamWriter.Write(CorrectNumber(Convert.ToInt32(VatAmount).ToString(), 18));
            log.DebugFormat("Tax amount: {0}", VatAmount);

            // Monto total (+ IVA)
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));
            log.DebugFormat("Total + Tax: {0}", total);

            // Monto no facturable
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(nonBillableAmount).ToString(), 18));

            // TODO - Total Periodo
            // TODO - Saldo anterior electrónico
            // TODO - Saldo anterior otras
            StreamWriter.Write(GetZeroes(54));

            // Valor a pagar total del documento
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));

            // TODO - Saldo anterior total
            // TODO - Valor comisión neto
            // TODO - " exento
            // TODO - " IVA
            // TODO - Total comisión
            // TODO - Total venta a tercero
            // TODO - Iva propio o de terceros
            // TODO - Total venta terceros
            // TODO - Iva no retornable (IVANoRet)
            // TODO - CredEC
            // TODO - GrntDep
            StreamWriter.Write(GetZeroes(198));

            // END
            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        protected void WriteDetails()
        {
            var ordered = entity.TaxDocumentLines.OrderBy(i => i.LineNumber);

            foreach (var line in ordered)
            {
                // Start
                StreamWriter.Write("0500");

                // Número de línea
                StreamWriter.Write(CorrectNumber(line.LineNumber.ToString(), 4));

                WriteDetails(line);

                // End
                StreamWriter.WriteLine();
                registryCount++;

                if (!string.IsNullOrEmpty(line.AdditionalGloss))
                    AddGloss(line.LineNumber, line.AdditionalGloss);
            }
        }

        protected virtual void WriteDetails(TaxDocumentLine line)
        {
            // Tipo de código
            StreamWriter.Write(GetSpaces(10));

            // Codificación del item
            StreamWriter.Write(GetSpaces(35));

            // Indicador de exención
            StreamWriter.Write((short)line.TaxType);

            // Nombre del servicio
            StreamWriter.Write(CorrectString(line.Text, 80));
            log.DebugFormat("Service Name: {0}", line.Text);

            // Cantidad
            StreamWriter.Write(FormatDecimal(line.Quantity));
            log.DebugFormat("Quantity: {0}", line.Quantity);

            // Unidad de medida
            StreamWriter.Write(CorrectString(line.MeasureUnit, 4));

            // Precio unitario
            StreamWriter.Write(FormatDecimal(line.UnitPrice));
            log.DebugFormat("Unit price: {0}", line.UnitPrice);

            var amount = line.NetAmount;
            var tmp = CorrectNumber(Convert.ToInt32(amount).ToString(), 18);
            NetAmount += amount;
            VatAmount += line.TaxAmount;

            // Monto item - Neto
            StreamWriter.Write(tmp);
            log.DebugFormat("Net amount: {0}", tmp);

            switch (line.TaxType)
            {
                case TaxType.Exempt:
                    ExemptAmount += amount;
                    tmp = FormatDecimal(amount);
                    log.DebugFormat("Total price (exempt): {0}", amount);
                    break;
                case TaxType.NegativeNonBillable:
                case TaxType.PositiveNonBillable:
                    var nonBillable = amount + line.TaxAmount;
                    tmp = CorrectNumber(Convert.ToInt32(nonBillable).ToString(), 18);
                    nonBillableAmount += nonBillable;
                    log.DebugFormat("Non Billable amount: {0}", nonBillable);
                    break;
                default:
                    var grossAmount = amount + line.TaxAmount;
                    tmp = CorrectNumber(Convert.ToInt32(grossAmount).ToString(), 18);
                    log.DebugFormat("Gross amount: {0}", grossAmount);
                    break;
            }

            // Monto con IVA
            StreamWriter.Write(tmp);
            // Monto de Impresión?
            StreamWriter.Write(tmp);
            log.DebugFormat("Printing value: {0}", amount);

            log.DebugFormat("Discount or Surcharge? ");
            // Descuento o recargo
            if (amount >= 0)
            {
                StreamWriter.Write("R");
                log.DebugFormat("Surcharge");
            }
            else
            {
                StreamWriter.Write("D");
                log.DebugFormat("Discount");
            }

            // TODO -Indicador de mensaje?
            StreamWriter.Write(GetSpaces(5));

            // TODO - DocReferencia? These names...
            StreamWriter.Write(GetSpaces(15));

            // CodImpAdicional, Código de impuesto adicional o retención
            if (line.HasExtraTaxes)
            {
                var taxCode = entity.ExtraTaxCode;

                log.DebugFormat("Aditional tax Code: {0}", taxCode);
                StreamWriter.Write(CorrectNumber(taxCode.ToString(), 3));

                if (!excludedExtraTaxes.Any(i => i == taxCode))
                {
                    var sum = line.ExtraTaxAmount;

                    if (entity.IsRetainedExtraTax)
                        retainedTaxes += sum;
                    else
                        VatAmount += sum;
                }
                else
                {
                    log.InfoFormat("The extra tax code '{0}' has been excluded.", taxCode);
                }
            }
            else
            {
                StreamWriter.Write(GetZeroes(3));
                log.DebugFormat("No aditional taxes.");
            }

            // TpoDocLiq, Tipo de documento que se liquida.
            StreamWriter.Write(GetZeroes(3));

            // Sección, Sección a la que pertenece el item - We don't care about this ¬¬.
            StreamWriter.Write(GetZeroes(2));

            // TpoCodigo, Tipo de código
            StreamWriter.Write(GetSpaces(10));

            // VlrCodigo
            StreamWriter.Write(GetSpaces(35));
        }

        protected virtual void WriteRetainedTaxes()
        {
            if (entity.DocumentType == "46")
            {
                // Start
                StreamWriter.Write("0302");
                // TipoImp, tipo de impuesto
                StreamWriter.Write(CorrectNumber(entity.ExtraTaxCode.ToString(), 3));
                log.DebugFormat("Tax type: {0}", entity.ExtraTaxCode);

                // TasaImp, Tasa de impuestos.
                var taxPercent = Convert.ToInt32(entity.ExtraTaxRate * 100);
                StreamWriter.Write(CorrectNumber(taxPercent.ToString(), 5));
                log.DebugFormat("Tax rate (%): {0}", taxPercent);

                // MontoImp, Monto de impuestos
                var total = entity.TaxRate * retainedTaxes;
                StreamWriter.Write(CorrectNumber(Convert.ToInt32(total).ToString(), 18));
                log.DebugFormat("Retained taxes amount: {0}", total);
                // End
                StreamWriter.WriteLine();
                registryCount++;
            }

            log.DebugFormat("- END -");
        }

        protected virtual void WriteDocumentReferences()
        {
            log.DebugFormat("- Document References -");
            log.DebugFormat("Documente references: [null? {0}, count = {1}]", entity.ReferencedDocuments == null, entity.ReferencedDocuments?.Count);

            if (entity.ReferencedDocuments != null && entity.ReferencedDocuments.Count > 0)
            {
                var lineCount = 0;

                for (var i = 0; i < entity.ReferencedDocuments.Count; i++)
                {
                    lineCount++;
                    var reference = entity.ReferencedDocuments.ElementAt(i);

                    // Start
                    StreamWriter.Write("0400");

                    // Line count
                    StreamWriter.Write(CorrectNumber((i + 1).ToString(), 2));
                    log.DebugFormat("Line number: {0}", i.ToString());

                    // Tipo de documento referenciado
                    StreamWriter.Write(CorrectNumber(reference.ReferencedDocument.DocumentType, 18));
                    log.DebugFormat("Ref. Document type: {0}", reference.ReferencedDocument.DocumentType);

                    // Folio de referencia
                    StreamWriter.Write(CorrectNumber(reference.ReferencedDocument.FolioNumber.ToString(), 18));
                    log.DebugFormat("Ref. Document folio number: {0}", reference.ReferencedDocument.FolioNumber);

                    // Fecha de referencia
                    StreamWriter.Write(reference.ReferencedDocument.AccountingDate.ToString(DateFormat));
                    log.DebugFormat("Ref. Issued Date: {0}", reference.ReferencedDocument.AccountingDate.ToString(DateFormat));

                    // Código de referencia (Acción que realiza)
                    if (reference.ReferenceReason != null && reference.ReferenceReason.Trim().Length > 0)
                    {
                        var reason = (CorrectionCode)Enum.Parse(typeof(CorrectionCode), reference.ReferenceReason, true);
                        StreamWriter.Write(CorrectNumber(((int)reason).ToString(), 2));
                        log.DebugFormat("Reference reason: {0}", ((int)reason));
                    }
                    else
                    {
                        StreamWriter.Write(GetSpaces(2));
                        log.DebugFormat("Reference reason: empty");
                    }

                    // Razón de la referencia
                    if (reference.ReferenceComment != null && reference.ReferenceComment.Trim().Length > 0)
                    {
                        StreamWriter.Write(CorrectString(reference.ReferenceComment, 90));
                        log.DebugFormat("Reference comment: {0}", reference.ReferenceComment);
                    }
                    else
                    {
                        StreamWriter.Write(GetSpaces(90));
                        log.DebugFormat("Correction Comment: empty");
                    }

                    // End
                    StreamWriter.WriteLine();
                }

                registryCount += lineCount;
                log.DebugFormat("- END -");
            }

            log.DebugFormat("- END -");
        }

        protected virtual void WriteWeakReferences()
        {
            if (!entity.WeakReferences.Any()) return;
            var lineCount = 0;

            for (var i = 0; i < entity.WeakReferences.Count; i++)
            {
                lineCount++;
                var reference = entity.WeakReferences.ElementAt(i);

                // Start
                StreamWriter.Write("0400");

                // Line count
                StreamWriter.Write(CorrectNumber((i + 1).ToString(), 2));
                log.DebugFormat("Line number: {0}", i.ToString());

                // Tipo de documento referenciado
                StreamWriter.Write(CorrectNumber(reference.DocumentType, 18));
                log.DebugFormat("Ref. Document type: {0}", reference.DocumentType);

                // Folio de referencia
                StreamWriter.Write(CorrectNumber(reference.Folio.ToString(), 18));
                log.DebugFormat("Ref. Document folio number: {0}", reference.Folio);

                // Fecha de referencia
                StreamWriter.Write(reference.Date.ToString(DateFormat));
                log.DebugFormat("Ref. Issued Date: {0}",
                    reference.Date.ToString(DateFormat));

                // Código de referencia (Acción que realiza)
                StreamWriter.Write(GetSpaces(2));

                // Razón de la referencia
                if (reference.Reason != null && reference.Reason.Trim().Length > 0)
                {
                    StreamWriter.Write(CorrectString(reference.Reason, 90));
                    log.DebugFormat("Reference comment: {0}", reference.Reason);
                }
                else
                {
                    StreamWriter.Write(GetSpaces(90));
                    log.DebugFormat("Correction Comment: empty");
                }

                // End
                StreamWriter.WriteLine();
            }

            registryCount += lineCount;
            log.DebugFormat("- END -");
        }

        /// <summary>
        /// Writes the End of the document to the file.
        /// Advances the <see cref="registryCount"/> by one.
        /// </summary>
        protected virtual void WriteFinish()
        {
            log.DebugFormat("- Document end -");
            // Start
            StreamWriter.Write("9901");

            // Num registros
            var tmp = registryCount.ToString();
            var count = 10 - tmp.Length;
            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);
            log.DebugFormat("Registry count:  {0}", tmp);

            // num documentos?
            tmp = "1";
            count = 10 - tmp.Length;
            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);
            log.DebugFormat("Num Documentos:  {0}", tmp);

            // CantidadCE
            StreamWriter.Write(GetZeroes(9));
            StreamWriter.Write("1");
            log.DebugFormat("CantidadCE: 1");

            // [Min|Max]NumDoctoCE
            tmp = entity.FolioNumber.ToString();
            count = 10 - tmp.Length;
            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);
            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);
            log.DebugFormat("Min|Max Document folio:  {0}", tmp);

            StreamWriter.Write(GetZeroes(9));
            StreamWriter.Write("1");

            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);
            StreamWriter.Write(GetZeroes(count));
            StreamWriter.Write(tmp);

            tmp = "DCCEDCNODCOTOCCA";
            count = 40 - tmp.Length;
            StreamWriter.Write(tmp);
            StreamWriter.Write(GetSpaces(count));
            log.DebugFormat("Code:  {0}", tmp);

            // End
            StreamWriter.WriteLine();
            log.DebugFormat("- END -");
            registryCount++;
        }

        protected virtual void AddGloss(int detailLineNumber, string gloss)
        {
            var i = 0;

            while (gloss.Length > 0)
            {
                StreamWriter.Write("0501");
                StreamWriter.Write(CorrectNumber(detailLineNumber.ToString(), 4));
                StreamWriter.Write(CorrectNumber((i + 1).ToString(), 4));
                string part;

                if (gloss.Length > maxGlossLength)
                {
                    part = gloss.Substring(0, maxGlossLength);
                    gloss = gloss.Substring(maxGlossLength);
                }
                else
                {
                    part = gloss;
                    gloss = "";
                }

                StreamWriter.Write(CorrectString(part, 200));
                StreamWriter.WriteLine();
                registryCount++;
                i++;
            }
        }

        #endregion

        #region Utilities

        protected static string GetSpaces(int count)
        {
            return count > 0 ? new string(' ', count) : "";
        }

        protected static string GetZeroes(int count)
        {
            return count > 0 ? new string('0', count) : "";
        }

        protected virtual string FormatDecimal(decimal d)
        {
            log.DebugFormat("FormatDecimal(d = {0})", d);
            var tmp = d.ToString(CultureInfo.CurrentCulture);
            var split = tmp.Split(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray());

            if (split.Length < 2)
                return string.Concat(CorrectNumber(tmp, 12), GetZeroes(6));

            log.DebugFormat("Split.length: {0}", split.Length);

            if (split[0].Length > 12)
            {
                split[0] = split[0].Substring(0, 12);
            }
            else
            {
                if (split[0].Length < 12)
                {
                    split[0] = string.Concat(GetZeroes(12 - split[0].Length), split[0]);
                }
            }

            if (split[1].Length > 6)
            {
                split[1] = split[1].Substring(0, 6);
            }
            else
            {
                if (split[1].Length < 6)
                    split[1] = string.Concat(split[1], GetZeroes(6 - split[1].Length));
            }

            return string.Join("", split[0], split[1]);
        }

        protected virtual string CorrectNumber(string number, int maxLength)
        {
            number = Correct(number, maxLength);
            var count = maxLength - number.Length;
            number = string.Concat(GetZeroes(count), number);
            log.DebugFormat("CorrectNumber - in = {0}, toAscii= {1}.", number, number.ToAscii());
            return number.ToIso88591();
        }

        protected string CorrectString(string s, int maxLength)
        {
            s = Correct(s, maxLength);
            var count = maxLength - s.Length;
            s = string.Concat(s, GetSpaces(count));
            log.DebugFormat("CorrectString - in = {0}, toAscii= {1}.", s, s.ToAscii());
            return s.ToIso88591();
        }

        private static string Correct(string s, int maxLength)
        {
            s = s.Trim();
            if (s.Length <= maxLength) return s;
            s = s.Substring(0, maxLength);
            s = RemoveSpecialChars(s);
            return s;
        }

        private static string RemoveSpecialChars(string s)
        {
            char[] latinChars =
            {
                '&', 'á', 'Á', 'é', 'É', 'í', 'Í', 'ó', 'Ó', 'ú', 'Ú', 'ñ', 'Ñ'
            };
            char[] replaces =
            {
                'y', 'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'n', 'N'
            };

            for (var i = 0; i < latinChars.Length; i++)
            {
                s = s.Replace(latinChars[i], replaces[i]);
            }
            return s;
        }

        protected virtual string FormatTaxationId(string taxid)
        {
            taxid = IdUtils.Normalize(taxid).Replace(".", "").Trim();
            return taxid.ToUpper();
        }

        #endregion
    }
}
