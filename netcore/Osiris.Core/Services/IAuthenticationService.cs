﻿using System;
using Osiris.Core.Entities;

namespace Osiris.Core.Services
{
    public interface IAuthenticationService
        : IService
    {
        bool CheckPassword(Account account, string password);

        /// <summary>
        /// Validates that the password matches the hash.
        /// </summary>
        /// <param name="passwordHash">The original hashed password.</param>
        /// <param name="hashMeta">The hash information required to decode the salt.</param>
        /// <param name="password">The password to validate against the hash.</param>
        /// <returns>true if valid, false otherwise.</returns>
        bool ValidatePassword(byte[] passwordHash, byte[] hashMeta, string password);

        /// <summary>
        /// Changes the password and updates the account values.
        /// </summary>
        /// <param name="account">The account whose values will be updated.</param>
        /// <param name="oldPwd">The old password.</param>
        /// <param name="newPwd">The new password</param>
        /// <returns>true on success, false on failure.</returns>
        bool ChangePassword(Account account, string oldPwd, string newPwd);

        Account ValidateCredentials(string login, string password, string tenantTaxId);

        void HashPassword(Account account, string password);

        Account GetAccount(string loginName, string tenantTaxId);

        bool RequestResetLink(string loginName, string baseUrl, string ipv4, string ipv6 = "",
            DateTime? expirationDate = null);

        bool ChangePassword(Guid guid, string newPassword, string ipv4);

        bool IsResetAvailable(Guid id);
    }
}
