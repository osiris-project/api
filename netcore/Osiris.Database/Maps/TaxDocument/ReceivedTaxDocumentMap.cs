﻿using FluentNHibernate.Mapping;
using NHibernate.Tuple;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class ReceivedTaxDocumentMap
        : ClassMap<ReceivedTaxDocument>
    {
        public ReceivedTaxDocumentMap()
        {
            Schema("taxation");
            Table("received_tax_document");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.AcceptedForAccounting).Default("0");
            Map(i => i.AccountingDate);
            Map(i => i.Comments).Length(200).Default("''");
            Map(i => i.DocumentType).Length(3).UniqueKey("received_tax_document_un");
            References(i => i.Enterprise, "enterprise_id")
                .Nullable()
                .Cascade.SaveUpdate()
                .UniqueKey("received_tax_document_un");
            Map(i => i.ExemptAmount).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.ExtraTaxRate).Precision(4).Scale(3).Default("0.000");
            Map(i => i.ExtraTaxCode).Nullable().Default("0");
            Map(i => i.ExpiryDate).Nullable();
            Map(i => i.FilePath).CustomSqlType("NVARCHAR(MAX)");
            Map(i => i.FolioNumber).UniqueKey("received_tax_document_un");
            Map(i => i.NetAmount, "net_amount").Precision(13).Scale(4).Default("0.0000");
            Map(i => i.ReceptionStatus).CustomType(typeof (ReceptionStatus));
            Map(i => i.ProviderAddressCity, "prvdr_address_city").Length(20);
            Map(i => i.ProviderAddressCommune, "prvdr_address_commune").Length(20);
            Map(i => i.ProviderAddresLine1, "prvdr_address_line1").Length(70);
            Map(i => i.ProviderAddressLine2, "prvdr_address_line2").Length(150);
            Map(i => i.ProviderBankAccountNumber, "prvdr_bank_account_number").Length(15);
            Map(i => i.ProviderLegalName, "prvdr_legal_name").Length(100);
            Map(i => i.ProviderPhone, "prvdr_phone").Length(50);
            Map(i => i.ProviderComercialActivity, "prvdr_comercial_activity").Length(6);
            Map(i => i.ProviderEconomicActivity, "prvdr_economic_activity").Length(60);
            Map(i => i.ProviderTaxId, "prvdr_tax_id").Length(20).UniqueKey("received_tax_document_un");
            Map(i => i.ReceptionTime, "reception_date");
            Map(i => i.ReceptorAddress, "rcptr_address").Length(400);
            Map(i => i.ReceptorCommune, "rcptr_commune").Length(50);
            Map(i => i.ReceptorLegalName, "rcptr_legal_name").Length(100);
            Map(i => i.ReceptorComercialActivity, "rcptr_comercial_activity_name").Length(120);
            Map(i => i.ReceptorTaxId, "rcptr_tax_id").Length(20);
            Map(i => i.RetainedTaxAmount).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.SubjectAmount).Precision(13).Scale(4).Default("0.0000");

            HasMany(i => i.TaxDocumentLines)
                .KeyColumn("received_tax_document_id")
                .Inverse()
                .Cascade.AllDeleteOrphan();

            Map(i => i.TaxRate).Precision(4).Scale(3).Default("0.000");
            Map(i => i.TaxAmount).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.Total).Precision(13).Scale(4).Default("0.0000");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("received_tax_document_un");

            HasMany(i => i.References)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("parent_id");

            HasMany(i => i.ReferencedBy)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("reference_id");

            References(i => i.Voucher, "accounting_voucher_id")
                .Cascade.SaveUpdate()
                .Nullable();

            HasMany(i => i.WeakReferences)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("received_document_id");
        }
    }
}
