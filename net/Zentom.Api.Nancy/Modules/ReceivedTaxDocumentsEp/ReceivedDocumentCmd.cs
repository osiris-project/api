﻿using System;
using System.Collections.Generic;
using Zentom.Api.Modules.TaxDocumentEp;
using Zentom.Core.Enums;

namespace Zentom.Api.Modules.ReceivedTaxDocumentsEp
{
    public class ReceivedDocumentCmd
    {
        public long FolioNumber { get; set; }

        public string DocumentType { get; set; }

        public DateTime AccountingDate { get; set; }

        public string VatOwnerTaxId { get; set; }

        public short ExtraTaxCode { get; set; }

        public decimal ExtraTaxRate { get; set; }

        public bool IsRetainedTax { get; set; }

        public decimal TaxRate { get; set; }
        
        public CorrectionCode CorrectionCode { get; set; }

        public ProviderCmd Provider { get; set; }

        public TenantCmd Tenant { get; set; }

        public IEnumerable<TaxDocumentLineCmd> Lines { get; set; }

        public IEnumerable<TaxDocumentReferenceCmd> References { get; set; }
    }

    public class ProviderCmd
        : Command
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string BankAccountNumber { get; set; }
        
        public CommercialActivityCmd CommercialActivity { get; set; }

        public EconomicActivityCmd EconomicActivity { get; set; }

        public AddressCmd Address { get; set; }
    }

    public class AddressCmd
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Commune { get; set; }

        public string City { get; set; }
    }

    public class TenantCmd
    {
        public CommercialActivityCmd CommercialActivity { get; set; }
    }

    public class EconomicActivityCmd
        : Command
    {
        public string Code { get; set; }
    }

    public class CommercialActivityCmd
        : Command
    {
        public string Name { get; set; }
    }
}