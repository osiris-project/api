﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Dobelik.Utils.Identity;
using log4net;
using Nancy;
using Nancy.ModelBinding;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Transform;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;
using Zentom.Core.Services;

namespace Zentom.Api.Modules.ProfileEp
{
    [Permission(RequiresLogin = true)]
    public class ProfileModule
        : TenantApi
    {
        private readonly IAuthenticationService authService;

        private readonly INationalIdentity identity;

        private readonly ILog log;

        private string templateFilePath;

        private bool deleteTemplateFile;

        public ProfileModule(ISessionFactory factory, IAuthenticationService authService, INationalIdentity identity)
            : base("profile", factory)
        {
            pathsToSkip = new[]
            {
                BasePath + "/tenants/"
            };
            After.AddItemToEndOfPipeline(DeleteTemplateFileOnSuccess);
            OnError.AddItemToStartOfPipeline(DeleteTemplateFileOnError);
            this.authService = authService;
            this.identity = identity;
            log = LogManager.GetLogger(typeof(ProfileModule));

            // Profile Changes
            this.Patch<ChangePasswordCommand>(BasePath + "/change-password/", ChangePassword);

            // Roles and Tenants
            this.Get(BasePath + "/tenants/", TenantList);
            this.Get<Command>(BasePath + "/tenants/{id:guid}/roles/", GetCurrentRoles);

            // Tax Document Templates
            var subPath = BasePath + "/tax-document-templates/";
            this.Post<TaxDocumentTemplateCmd>(BasePath + "/tax-document-templates/", CreateTaxDocumentTemplate);
            this.Get(subPath, TaxDocumentTemplateList);
            this.Get(subPath + "/check-name/", CheckTemplateNameAvailability);
            this.Delete<Command>(subPath + "/{id:guid}/", DeleteTaxDocumentTemplate);

            // Address
            this.Get(BasePath + "/address-types/", GetAddressTypes);

            
        }

        public object ChangePassword(ChangePasswordCommand cmd, dynamic args)
        {
            log.DebugFormat("Entering ChangePassword.");
            log.DebugFormat("Change password command: {0}", cmd);
            var toUpdate = currentAccount;

            if (!authService.ChangePassword(toUpdate, cmd.OldPassword, cmd.NewPassword))
            {
                return Negotiate.WithValidationFailure(authService.ErrorList);
            }

            // TODO: Invalidate token.
            log.DebugFormat("Leaving ChangePassword.");
            return Negotiate.WithStatusCode(HttpStatusCode.OK)
                .WithHeader("Location", Constants.BASE_PATH + "auth");
        }

        public object CreateTaxDocumentTemplate(TaxDocumentTemplateCmd cmd)
        {
            var entity = db.QueryOver<TaxDocumentTemplate>()
                .Where(i => i.Account == Account && i.Name == cmd.Name)
                .Take(1)
                .SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithExistingItem(I18n.Text.TaxDocumentTemplate, "name");
            }

            entity = new TaxDocumentTemplate
            {
                Name = cmd.Name,
                DocumentType = cmd.DocumentType,
                CreatedAt = DateTime.UtcNow,
                Tenant = currentTenant
            };
            entity.FilePath = GetFileName(entity.DocumentType, entity.CreatedAt);
            entity.Account = Account;
            db.Save(entity);
            var serializedContent = JsonConvert.SerializeObject(cmd.Content);
            log.InfoFormat("Saving serialized content to '{0}'.", entity.FilePath);
            entity.Save(serializedContent);
            var url = string.Concat(Context.Request.Url.BasePath.TrimEnd("/".ToCharArray()), "/", entity.Id);
            return Negotiate.WithCreated(url, entity.Id);
        }

        public object TaxDocumentTemplateList(dynamic args)
        {
            var bound = this.BindTo(new TaxDocumentTemplateQuery());
            var query = db.QueryOver<TaxDocumentTemplate>()
                .Where(i => i.Account == Account);

            if (!string.IsNullOrWhiteSpace(bound.DocumentType))
            {
                query.Where(i => i.DocumentType == bound.DocumentType);
            }


            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.Name + "%");
            }

            TaxDocumentTemplateDto dto = null;
            var items = query
                .SelectList(i =>
                    i.Select(j => j.Id).WithAlias(() => dto.Id)
                        .Select(j => j.Name).WithAlias(() => dto.Name)
                        .Select(j => j.DocumentType).WithAlias(() => dto.DocumentType)
                        .Select(j => j.FilePath).WithAlias(() => dto.FilePath)
                        .Select(j => j.CreatedAt).WithAlias(() => dto.CreateAt))
                .OrderBy(Settings.OrderBy)
                .TransformToBean<TaxDocumentTemplate, TaxDocumentTemplateDto>()
                .PaginateResults<TaxDocumentTemplate, TaxDocumentTemplateDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);

            log.DebugFormat("Including Json? {0}", bound.IncludeJson);
            if (!bound.IncludeJson) return Negotiate.WithOkAndList(items, Request.Url);

            var len = items.Items.Count;
            var list = new List<object>();
            for (var index = 0; index < len; index++)
            {
                var item = items.Items[index];
                object taxDocumentData = null;
                if (!string.IsNullOrWhiteSpace(item.FilePath))
                {
                    if (File.Exists(item.FilePath))
                        taxDocumentData = JsonConvert.DeserializeObject(File.ReadAllText(item.FilePath));
                }

                list.Add(new
                {
                    id = item.Id,
                    name = item.Name,
                    documentType = item.DocumentType,
                    createdAt = item.CreateAt,
                    taxDocumentData
                });
            }
            return Negotiate.WithOkAndList(list, Request.Url, items.ResultCount, items.CurrentPage, items.PageCount,
                items.ResultsPerPage, items.HasPrevious, items.HasNext);
        }

        public object DeleteTaxDocumentTemplate(Command cmd)
        {
            log.DebugFormat("Trying to delete tax document template '{0}'", cmd.Id);
            var entity = db.QueryOver<TaxDocumentTemplate>()
                .Where(i => i.Account.Id == Account.Id && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithOk();
            }

            templateFilePath = entity.FilePath;
            deleteTemplateFile = true;
            db.Delete(entity);
            return Negotiate.WithOk();
        }

        public object CheckTemplateNameAvailability(dynamic args)
        {
            deleteTemplateFile = false;
            var bound = this.BindTo(new TaxDocumentTemplateQuery());
            log.DebugFormat("Checking template name: {0}", bound);

            if (string.IsNullOrWhiteSpace(bound.Name))
            {
                return Negotiate.WithOkAndModel(new
                {
                    isAvailable = false
                });
            }

            var entity = db.QueryOver<TaxDocumentTemplate>()
                .Where(i => i.Account == Account && i.Name == bound.Name)
                .Take(1)
                .SingleOrDefault();

            return Negotiate.WithOkAndModel(new
            {
                isAvailable = (entity == null)
            });
        }

        public object TenantList(dynamic args)
        {
            Tenant tenant = null;
            TenantDto dto = null;
            var query = db.QueryOver<AuthorizedAccount>()
                .Where(i => i.Account == currentAccount)
                .Left.JoinAlias(i => i.Tenant, () => tenant)
                .SelectList(i => i
                    .Select(() => tenant.Id).WithAlias(() => dto.Id)
                    .Select(() => tenant.LegalName).WithAlias(() => dto.LegalName)
                    .Select(() => tenant.TaxId).WithAlias(() => dto.TaxId));

            var bound = this.BindTo(new TenantQuery());
            if (!string.IsNullOrWhiteSpace(bound.TaxId))
            {
                bound.TaxId = identity.CleanId(bound.TaxId);
                query.Where(() => tenant.TaxId == bound.TaxId);
            }

            if (!string.IsNullOrWhiteSpace(bound.LegalName))
            {
                query.WhereRestrictionOn(() => tenant.LegalName)
                    .IsInsensitiveLike("%" + bound.LegalName + "%");
            }
            
            var items = query
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<TenantDto>())
                .PaginateResults<AuthorizedAccount, TenantDto>(Settings.PaginationSettings.Page, Settings.PaginationSettings.ResultsPerPage);

            return Negotiate.WithOkAndList(items, Request.Url);
        }

        public object GetCurrentRoles(Command cmd)
        {
            var entity = db.QueryOver<AuthorizedAccount>()
                .Where(i => i.Account == Account && i.Tenant.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Error.NoManagedTenants);
            }
            log.DebugFormat("Retrieving roles for tenant {0}", cmd.Id);



            var accountRoles = db.QueryOver<AccountRole>().List();
            var roles = new Dictionary<string, List<object>>();

            foreach (var role in accountRoles)
            {
                var list = new List<object>();

                foreach (var permission in role.Role.RolePermissions)
                {
                    list.Add(new
                    {
                        id = permission.Permission.Id,
                        name = permission.Permission.Name
                    });
                }

                if (!roles.ContainsKey(role.Role.Name))
                {
                    roles.Add(role.Role.Name, list);
                }
                else
                {
                    roles[role.Role.Name].AddRange(list);
                }
            }
            return Negotiate.WithOkAndModel(roles);
        }

        public object GetAddressTypes()
        {
            var list = db.QueryOver<AddressType>()
                .SelectList(i => i
                    .Select(j => j.Name)
                    .Select(j => j.Id))
                .List<object[]>()
                .Select(i => new
                {
                    name = (string) i[0],
                    id = (Guid) i[1]
                });

            return Negotiate.WithOkAndModel(list);
        }

        private string GetFileName(string documentType, DateTime createdAt)
        {
            var path = Path.Combine(ConfigurationManager.AppSettings["zentom.root_storage"], currentTenant.TaxId, Account.Id.ToString());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fname = string.Concat(documentType, "_", createdAt.ToString("yyyy-MMMM-dd_HH-mm-ss"), ".json");
            templateFilePath = Path.Combine(path, fname);
            log.DebugFormat("Generated file path: {0}", templateFilePath);
            return templateFilePath;
        }

        private Response DeleteTemplateFileOnError(NancyContext context, Exception ex)
        {
            DeleteTemplateFile();
            return null;
        }

        private void DeleteTemplateFileOnSuccess(NancyContext context)
        {
            DeleteTemplateFile();
        }

        private void DeleteTemplateFile()
        {
            if (!deleteTemplateFile) return;
            if (string.IsNullOrWhiteSpace(templateFilePath))
            {
                log.Warn("The template file path was null or empty; nothing to delete.");
                return;
            }

            File.Delete(templateFilePath);

            if (File.Exists(templateFilePath))
            {
                // TODO: create a system alert to delete the file.
                log.ErrorFormat("Failed to delete file '{0}'.", templateFilePath);
            }
        }
    }
}