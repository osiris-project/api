/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import java.time.LocalDate

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'payment')
class Payment extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(columnDefinition = 'DATE', nullable = false)
    LocalDate date

    @Column(precision = 13, scale = 4, nullable = false)
    Double amount

    @Column(nullable = false, length = 200)
    String details

    @ManyToOne
    Document document

    /**
     *
     */
    protected Payment() {
        this.date = null
        this.amount = 0
        this.details = ''
        this.document = null
    }

    /**
     * @param paymentDate
     * @param details
     * @param document
     */
    Payment(LocalDate date, String details, Document document) {
        def e = []
        if (!date) {
            e << ValidationBuilder.empty('payment.date')
        }
        if (!details.trim()) {
            e << ValidationBuilder.empty('payment.details')
        }
        if (!document) {
            throw new IllegalArgumentException('No document provided')
        }
        if (e) {
            throw new ValidationException(e)
        }
        this.date = date
        this.details = details.trim()
        this.document = document
    }
}
