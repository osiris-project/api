﻿using System;
using System.Collections.Generic;
using Nancy.Validation;

namespace Osiris.Api.Modules.Validators.NationalId
{
    public class NationalIdValidationRule
        : ModelValidationRule
    {
        public NationalIdValidationRule(Func<string, string> errorMessageFormatter, IEnumerable<string> memberNames)
            : base("NationalIdValidator", errorMessageFormatter, memberNames)
        {
        }
    }
}
