﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace Zentom.Api.Nancy.Tests
{
    [TestClass]
    public class RoutTests
    {
        private const string API_URI = "/";
        private Browser _browser;

        private Bootstrap _bootstrap;

        private const string enterpriseId = "3B0F1723-F68F-E511-80C6-080027018225";
        private const string creatorId = "D1E9DE36-F68F-E511-80C6-080027018225";
        private const string javieraSpaPassword = "nomeacuerdo";


        [TestInitialize]
        public void Init()
        {
            _bootstrap = new Bootstrap();
            _browser = new Browser(_bootstrap);
        }

        [TestMethod]
        public void ReadConfiguration()
        {
            var val = ConfigurationManager.AppSettings["log4net.Config"];
            Assert.IsFalse(string.IsNullOrEmpty(val), "Failed to read configuration file.");
            Log("Log4net.Config: {0}", val);
            var conn = ConfigurationManager.ConnectionStrings["MssqlZentom"];
            Assert.IsFalse(string.IsNullOrEmpty(conn.ConnectionString));
            Log("Conn. String: {0}", conn.ConnectionString);
        }

        [TestMethod]
        public void RetrieveProduct()
        {
            var result = _browser.Get("/api/product/DEE0B638-F67B-E511-80C4-080027018225", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.Query("enterpriseId", enterpriseId);
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ListProducts()
        {
            var result = _browser.Get("/api/product", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
                with.Query("enterpriseId", enterpriseId);
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }


        [TestMethod]
        public void CreateProduct()
        {
            var result = _browser.Post("/api/product", with =>
            {
                with.HttpRequest();
                with.FormValue("name", "Test Product X");
                with.FormValue("unitPrice", "5000,000");
                with.Query("creatorId", creatorId);
                with.Query("enterpriseId", enterpriseId);
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "es-cl");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void UpdateProduct()
        {
            var result = _browser.Put("/api/product", with =>
            {
                with.HttpRequest();
                with.FormValue("name", "Test Product XX");
                with.FormValue("unitPrice", "13500,000");
                with.FormValue("id", "ec7d019b-a45c-46f3-86b6-a54701751a59");
                with.Query("enterpriseId", enterpriseId);
                with.Query("creatorId", creatorId);
                with.Header("Accept", "application/json");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void DeleteProduct()
        {
            var result = _browser.Delete("/api/product/BF160D69-1CC8-4E3C-BF8B-A54601039E24", with =>
            {
                with.HttpRequest();
                with.Query("enterpriseId", enterpriseId);
                with.Query("creatorId", creatorId);
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "en-US");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void RootTest()
        {
            var result = _browser.Get("/", with =>
            {
                with.HttpRequest();
                with.Header("Accept", "application/json");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void AuthTest()
        {
            var result = _browser.Post(API_URI + "auth", with =>
            {
                with.HttpRequest();
                with.FormValue("loginName", "javiera_spa");
                with.FormValue("enterpriseTaxId", "763766357");
                with.FormValue("password", "nomeacuerdo");
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "es-cl");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ArrayDeserializationTest()
        {
            var result = _browser.Put("/employees/3FD1780A-83B5-E511-80C7-080027018225/account/roles", with =>
            {
                WithDefaultBrowserContext(with);
                with.JsonBody(new
                {
                    employeeId = "3FD1780A-83B5-E511-80C7-080027018225",
                    roles = new List<object>
                    {
                        new
                        {
                            id = "A06769D1-A37F-41AC-8B92-9152CD529674",
                            name = "role1"
                        },
                        new
                        {
                            id = "A06769D1-A37F-41AC-8B92-9152CD529674",
                            name = "role2"
                        }
                    }
                });
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ResetPasswordRequestTest()
        {
            var result = _browser.Post("/auth/password-reset/", with =>
            {
                with.DefaultBrowserContextWithoutAuth();
                with.FormValue("login", "146408133");
                with.FormValue("tenantTaxId", "96950918");
                with.FormValue("returnUri", "https://zentom.valorti.com/reset-password/");
                with.Header("Remote Address", "127.0.0.1");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ResetPasswordTest()
        {
            const string uuid = "f7c97c60-3d4e-4b43-b499-a5b200e4bfe9";

            var result = _browser.Get("/auth/password-reset/" + uuid, with =>
            {
                with.DefaultBrowserContextWithoutAuth();
            });

            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

            result = _browser.Post("/auth/password-reset/" + uuid, with =>
            {
                with.DefaultBrowserContextWithoutAuth();
                with.FormValue("password", "123456");
                with.FormValue("passwordConfirm", "123456");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ExpiredResetToken()
        {
            const string uuid = "850A748E-9EB1-4B78-91C2-A5B2003D2386";

            var result = _browser.Get("/auth/password-reset/" + uuid, with =>
            {
                with.DefaultBrowserContextWithoutAuth();
            });

            Assert.AreEqual(HttpStatusCode.ExpectationFailed, result.StatusCode);
        }

        [TestMethod]
        public void DocumentReferencesTest()
        {
            const string uuid = "4f23851f-4742-4302-90be-a59f00eaaccf";
            var result = _browser.Get("/resources/tax-documents/" + uuid + "/references/", with =>
            {
                with.DefaultBrowserContext(_browser);
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void system_module_retrieval_test()
        {
            var result = _browser.Get("/resources/tenant-profile/modules/", with =>
            {
                with.DefaultBrowserContext(_browser);
            });
            result.Log();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestCleanup]
        public void End()
        {
            _browser = null;
            _bootstrap?.Dispose();
        }

        private string GetToken()
        {
            var result = _browser.Post("/auth", with =>
            {
                with.HttpRequest();
                with.FormValue("loginName", "javiera_spa");
                with.FormValue("enterpriseTaxId", "763766357");
                with.FormValue("password", javieraSpaPassword);
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "es-cl");
            });
            return result.Headers["Authorization"];
        }

        private void WithDefaultBrowserContext(BrowserContext with)
        {
            var token = GetToken();
            with.HttpRequest();
            with.Header("Accept-Language", "es-cl");
            with.Header("Accept", "application/json");
            with.Header("Authorization", token);
        }

        private static void LogResponse(BrowserResponse response)
        {
            Log("Response: {0}", response.Body.AsString());
            Log("Headers:");
            foreach (var item in response.Context.Response.Headers)
            {
                Log("'{0}' => {1}", item.Key, item.Value);
            }
        }

        private static void Log(string msg, params object[] args)
        {
            Console.WriteLine(msg, args);
        }
    }
}
