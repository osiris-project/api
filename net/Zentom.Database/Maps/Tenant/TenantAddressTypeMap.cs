﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Tenant
{
    public class TenantAddressTypeMap
        : ClassMap<TenantAddressType>
    {
        public TenantAddressTypeMap()
        {
            Schema("organization");
            Table("tenant_address_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.TenantAddress, "tenant_address_id")
                .Cascade.All()
                .UniqueKey("tenant_address_type_un");

            References(i => i.AddressType, "address_type_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_address_type_un");
        }
    }
}
