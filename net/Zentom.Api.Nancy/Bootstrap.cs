﻿using System;
using System.Collections.Generic;
using System.Threading;
using log4net;
using log4net.Config;
using LightInject;
using LightInject.Nancy;
using Nancy;
using Nancy.Bootstrapper;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Security;
using Zentom.Core.Entities;

namespace Zentom.Api
{
    public class Bootstrap
        : LightInjectNancyBootstrapper
    {
        #region Fields

        private static readonly ILog Log = LogManager.GetLogger(typeof(Bootstrap));

        #endregion

        #region Overrides of NancyBootstrapperBase<IServiceContainer>

        protected override void ApplicationStartup(IServiceContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            ConfigureLog4Net();
            Log.Debug("--- Configuring ---");
            StaticConfiguration.EnableRequestTracing = true;
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            // Before Request processing
            pipelines.BeforeRequest.AddItemToStartOfPipeline(RequestStart);
            pipelines.BeforeRequest += (context => NHibernateSessionHelper.OpenSession(container));
            pipelines.BeforeRequest += ctx =>
                AuthenticationCheck(ctx, container.GetInstance<ISessionFactory>().GetCurrentSession());
            
            
            // Exception Handling
            pipelines.OnError += ((context, exception) =>
            {
                NHibernateSessionHelper.Rollback(container);
                var result = ExceptionHandler(context, exception);
                return result;
            });

            // After Request processing
            pipelines.AfterRequest.AddItemToStartOfPipeline(context => NHibernateSessionHelper.Commit(context, container));
            pipelines.EnableCors();
            pipelines.AfterRequest.AddItemToEndOfPipeline(RequestEnd);

            Log.Info("--- Application configured ---");
        }

        #endregion

        #region Overrides of LightInjectNancyBootstrapper

        protected override IServiceContainer GetServiceContainer()
        {
            var container = base.GetServiceContainer();
            LightinjectConfig.Configure(container);
            return container;
        }

        protected override void RegisterModules(IServiceContainer container, IEnumerable<ModuleRegistration> moduleRegistrationTypes)
        {
            base.RegisterModules(container, moduleRegistrationTypes);
            LightinjectConfig.RegisterInterceptors(container);
        }

        #endregion

        #region Utils

        #region Log4Net

        private static void ConfigureLog4Net()
        {
            XmlConfigurator.Configure();
        }

        #endregion

        private static Response AuthenticationCheck(NancyContext context, ISession session)
        {
            Log.DebugFormat("Authentication check session id '{0}'.", session.GetSessionImplementation().SessionId);

            if (context.Request.Method.ToLower() == "options") return null;
            var isResPath = context.Request.Path.StartsWith(Constants.BASE_PATH + "resources");
            Log.DebugFormat("Request path '{0}' is Resources Path? {1}", context.Request.Path, isResPath);
            if (!isResPath) return null;
            //var requestPath = Regex.Replace(context.Request.Path, @"\..+$", string.Empty, RegexOptions.Compiled);


            //if (requestPath == Constants.BASE_PATH) return null;

            //var isAuth = Regex.IsMatch(requestPath, Constants.BASE_PATH + @"auth(/(password-reset(/[a-f0-9\-]+)?)?)?$", RegexOptions.Compiled);
            //if (isAuth) return null;

            var headToken = context.Request.Headers.Authorization;
            Log.DebugFormat("Token: {0}", headToken);
            var id = ApiTokenizer.DecodeToken(headToken);
            return context.CheckAccount(session.Get<Account>(id));
        }

        //private static void SetCulture(NancyContext context)
        //{
        //    Log.Debug("Entering SetCulture.");
        //    Log.DebugFormat("Setting culture to: {0}", context.Culture);
        //    Thread.CurrentThread.CurrentCulture = context.Culture;
        //    Log.Debug("Leaving SetCulture.");
        //}

        //private static void SetPagination(NancyContext context, ApiSettings apiSettings)
        //{
        //    Log.Debug("Entering ParsePagination.");
        //    int page, rpp;

        //    if (!int.TryParse(context.Request.Query.page, out page))
        //        page = 1;

        //    if (!int.TryParse(context.Request.Query.rpp, out rpp))
        //        rpp = 15;

        //    Log.InfoFormat("Setting pagination to: [page = {0}, items per page = {1}]", page, rpp);
        //    apiSettings.PaginationSettings = new ApiSettings.Pagination(page, rpp);
        //    apiSettings.OrderBy = context.Request.Query["orderBy"];
        //    Log.Debug("Leaving ParsePagination.");
        //}

        //private static void ParseSettings(NancyContext context)
        //{
        //    SetCulture(context);
        //    var Settings = new ApiSettings();
        //    SetPagination(context, Settings);
        //    Settings.IsPrettyPrint = context.Request.Query.pretty;
        //}

        private static object ExceptionHandler(NancyContext context, Exception ex)
        {
            Log.Debug("Entering Bootstrap ExceptionHandler.");
            var negotiator = context.HandleException(ex, null);
            Log.Debug("Leaving Bootstrap ExceptionHandler.");
            return negotiator;
        }

        private static Response RequestStart(NancyContext context)
        {
            Log.InfoFormat("--- Beginning request | Thread Id: {0} ---", Thread.CurrentThread.ManagedThreadId);
            Log.InfoFormat("IsoCode: {0}", Thread.CurrentThread.CurrentCulture);
            Log.InfoFormat("Requested URI: {0}", context.Request.Url);
            Log.InfoFormat("Method: {0}", context.Request.Method);

            //ParseSettings(context);
            return null;
        }

        private static void RequestEnd(NancyContext context)
        {
            Log.InfoFormat("--- Request End | Thread Id: {0} ---", Thread.CurrentThread.ManagedThreadId);
        }

        #endregion
    }
}