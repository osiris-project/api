﻿namespace Zentom.Core.Enums
{
    public enum ValueTypes
    {
        String,
        Integer,
        Decimal,
        Bool
    }
}
