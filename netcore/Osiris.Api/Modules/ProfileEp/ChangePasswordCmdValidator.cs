﻿using FluentValidation;

namespace Osiris.Api.Modules.ProfileEp
{
    public class ChangePasswordCmdValidator
        : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCmdValidator()
        {
            RuleFor(i => i.NewPassword).Length(6, 100);
            RuleFor(i => i.OldPassword).Length(6, 100);
        }
    }
}
