﻿using Osiris.Core.Entities;

namespace Osiris.Api.Modules.EnterpriseEp
{
    public class EnterpriseQuery
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }

        /// <summary>
        /// The relation between enterprises:
        /// -1: clients only,
        /// 0: both,
        /// 1: providers only,
        /// </summary>
        public EnterpriseRelationship RelationFilter { get; set; }
    }
}
