﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Criterion;
using Osiris.Core.Pagination;

namespace Osiris.Extensions.NHibernate
{
    public static class QueryOverExtensions
    {
        public static IPagedResult<TEntity> PageResults<TEntity, TSubType>(this IQueryOver<TEntity, TSubType> query, int page = 0, int pageSize = 15) where TEntity : class
        {
            var clone = query.Clone();
            clone.ClearOrders();

            var tmp = query.Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Future<TEntity>();

            var count = clone.Select(Projections.RowCount())
                .FutureValue<int>().Value;
            return new PagedResults<TEntity>(tmp, page, pageSize, count);
        }

        public static IQueryOver<TEntity, TSubType> JoinTo<TEntity, TSubType>(this IQueryOver<TEntity, TSubType> query,
            IDictionary<Expression<Func<TSubType, object>>, Expression<Func<object>>> joins)
        {
            if (joins?.Count > 0)
            {
                foreach (var join in joins)
                {
                    if (join.Key != null && join.Value != null)
                    {
                        query.JoinAlias(join.Key, join.Value);
                    }
                }
            }

            return query;
        }
    }
}
