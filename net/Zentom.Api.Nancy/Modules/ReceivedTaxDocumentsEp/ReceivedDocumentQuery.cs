﻿using System;
using System.Collections.Generic;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Api.Modules.ReceivedTaxDocumentsEp
{
    public class ReceivedDocumentQuery
    {
        public long Folio1 { get; set; }

        public long Folio2 { get; set; }

        /// <summary>
        /// The operation to be applied.
        /// <example>
        /// greaterThan (x &gt; y)
        /// lessThan (x &lt; y)
        /// equal (x = y)
        /// between (x &gt;= y && x &lt;= y)
        /// </example>
        /// </summary>
        public string FolioOperator { get; set; }

        public DateTime? AccountingDate1 { get; set; }

        public DateTime? AccountingDate2 { get; set; }

        /// <summary>
        /// Client's tax document id
        /// </summary>
        public string ProviderTaxId { get; set; }

        public string ProviderName { get; set; }

        public string DocumentType { get; set; }

        public IEnumerable<ReceptionStatus> ReceptionStatuses { get; set; }

        public bool NotInVoucher { get; set; }

        public bool? AcceptedForAccounting { get; set; }

        public override string ToString()
        {
            return
                $"Folio1: {Folio1}, Folio2: {Folio2}, FolioOperator: {FolioOperator}, AccountingDate1: {AccountingDate1}, AccountingDate2: {AccountingDate2}, ProviderTaxId: {ProviderTaxId}, ProviderName: {ProviderName}, DocumentType: {DocumentType}";
        }
    }
}