﻿namespace Osiris.Core.Entities
{
    public class SpecificTax
        : Entity
    {
        #region Fields

        private string _name;

        private int _code;

        private bool _isRetained;

        private decimal _taxRate;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual bool IsRetained
        {
            get { return _isRetained; }
            set { _isRetained = value; }
        }

        public virtual decimal TaxRate
        {
            get { return _taxRate; }
            set { _taxRate = value; }
        }

        #endregion

        public SpecificTax() { }

        public SpecificTax(string name, int code, bool isRetained, decimal taxRate)
        {
            _name = name;
            _code = code;
            _isRetained = isRetained;
            _taxRate = taxRate;
        }
    }
}
