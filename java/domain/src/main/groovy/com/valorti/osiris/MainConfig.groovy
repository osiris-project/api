/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris

import com.dobelik.utils.nationalId.IdentityFactory
import com.valorti.osiris.config.AppConfig
import com.valorti.osiris.core.location.LocationService
import com.valorti.osiris.core.taxation.EconomicActivityRepository
import com.valorti.osiris.core.taxation.documents.XmlDocumentParserService
import com.valorti.osiris.domain.taxation.documents.XmlDocumentParserServiceImpl
import org.slf4j.MarkerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Domain main configuration.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Configuration
@EnableConfigurationProperties(AppConfig.class)
class MainConfig {

    @Bean
    IdentityFactory identityFactory() {
        return new IdentityFactory()
    }

    @Bean
    XmlDocumentParserService xmlDocumentParserService(AppConfig appConfig,
                                                      LocationService locationService,
                                                      EconomicActivityRepository economicActivityRepository) {
        return new XmlDocumentParserServiceImpl(economicActivityRepository,
            locationService,
            MarkerFactory.getMarker(appConfig.logging.markers.admin))
    }
}
