﻿using Zentom.Core.Interfaces.Data;
namespace Zentom.Core.Interfaces.Entities
{
    public interface IOptionPermission
        : IVersioned
    {
        IOption Option { get; set; }

        IPermission Permission { get; set; }
    }

    public interface IOptionPermissionId
    {
        IOption Option { get; set; }

        IPermission Permission { get; set; }
    }
}
