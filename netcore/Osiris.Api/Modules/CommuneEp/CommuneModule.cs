﻿using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.CommuneEp
{
    [Permission(RequiresLogin = true)]
    public class CommuneModule
        : TenantApi
    {
        public CommuneModule(ISessionFactory factory)
            : base("communes", factory)
        {
            this.Get(BasePath, List);
        }

        public Negotiator List(dynamic args)
        {
            var bound = this.BindTo(new CommuneQuery());

            Commune commune = null;
            var query = db.QueryOver<City>()
                .Left.JoinAlias(i => i.Communes, () => commune);

            if (!string.IsNullOrWhiteSpace(bound.CityName))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.CityName + "%");
            }

            if (!string.IsNullOrWhiteSpace(bound.CommuneName))
            {
                query.WhereRestrictionOn(() => commune.Name)
                    .IsInsensitiveLike("%" + bound.CommuneName + "%");
            }

            CommuneDto dto = null;
            var items = query.SelectList(i =>
                i.Select(j => j.Name).WithAlias(() => dto.CityName)
                    .Select(() => commune.Id).WithAlias(() => dto.Id)
                    .Select(() => commune.Name).WithAlias(() => dto.CommuneName))
                    .OrderBy(i => i.Name).Asc
                    .TransformToBean<City, CommuneDto>()
                    .PaginateResults<City, CommuneDto>(Settings.PaginationSettings.Page, Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(items, Request.Url);
        }
    }
}
