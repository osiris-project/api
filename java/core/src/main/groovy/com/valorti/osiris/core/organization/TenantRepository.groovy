/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.organization

import com.valorti.osiris.core.UUIDRepository

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
interface TenantRepository
    extends UUIDRepository<Tenant> {

}
