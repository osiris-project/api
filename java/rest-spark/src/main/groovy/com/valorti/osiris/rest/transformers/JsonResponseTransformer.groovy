package com.valorti.osiris.rest.transformers

import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.ResponseTransformer

/**
 * @author Nicolas Mancilla
 * @version 0.0.1
 * @since 0.0.1
 */
final class JsonResponseTransformer implements ResponseTransformer {
    private final Logger log = LoggerFactory.getLogger(JsonResponseTransformer.class)

    /**
     * Defaults to false.
     */
    boolean prettyPrint

    /**
     * Default CTOR.
     */
    JsonResponseTransformer() {
        this(false)
    }

    JsonResponseTransformer(boolean prettyPrint) {
        this.prettyPrint = prettyPrint
    }

    @Override
    String render(Object input) {
        def output = ''
        if (!input) {
            log.warn('The input object is null or empty.')
            return output
        }
        def mapper = new ObjectMapper()
        try {
            ObjectWriter writer = null
            if (prettyPrint) {
                log.debug('No pretty printing')
                writer = mapper.writerWithDefaultPrettyPrinter()
            } else {
                writer = mapper.writer()
            }
            output = writer.writeValueAsString(input)
        } catch (JsonMappingException ex) {
            log.error('Failed to map the response object', ex)
        }
        log.debug('Generated json: {}', output)
        return output
    }
}
