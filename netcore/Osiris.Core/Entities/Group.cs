﻿using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public class Group
        : Entity
    {
        #region Fields

        private string name;

        private Tenant headquarters;

        private ISet<TenantGroup> tenants = new HashSet<TenantGroup>();

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual Tenant Headquarters
        {
            get { return headquarters; }
            set { headquarters = value; }
        }

        public virtual ISet<TenantGroup> Tenants
        {
            get { return tenants; }
            set { tenants = value; }
        }

        #endregion

        public Group()
        {
        }
        
        public Group(Tenant tenant, string name, Tenant headquarters, ISet<TenantGroup> tenants)
        {
            this.name = name;
            this.headquarters = headquarters;
            this.tenants = tenants;
        }
    }
}
