﻿using System;

namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentHeaderDto
    {
        // TODO
        //public decimal PreviousDebtBalance { get; set; }

        // TODO - Still don't know what this is for.
        //public string Comments { get; set; }

        //public Guid SelectedDispatchType { get; set; }
        
        public Guid SelectedPaymentType { get; set; }

        public long FolioNumber { get; set; }

        public DateTime AccountingDate { get; set; }
        
        public DateTime? ExpireDate { get; set; }

        public ExtraTaxDto ExtraTaxes { get; set; }

        public TaxDocumentProviderDto Provider { get; set; }

        public TaxDocumentClientDto Client { get; set; }
    }
}
