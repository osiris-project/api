﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IDocumentType
        : IUnique<int>, IVersioned, IRowGuid
    {
        string Code { get; set; }
        
        string Description { get; set; }
        
        decimal TaxRate { get; set; }
        
        ICollection<ITaxDocument> TaxDocuments { get; set; }
    }
}
