﻿using FluentValidation;
using Zentom.Api.Modules.Validators.Guid;
using Zentom.Core.Enums;

namespace Zentom.Api.Modules.ReceivedTaxDocumentsEp
{
    public class ReceivedDocumentCmdValidator
        : AbstractValidator<ReceivedDocumentCmd>
    {
        public ReceivedDocumentCmdValidator()
        {
            RuleFor(i => i.AccountingDate).NotNull();
            RuleFor(i => i.DocumentType).NotEmpty();
            RuleFor(i => i.Provider.CommercialActivity.Id).SetValidator(new GuidValidator());
            RuleFor(i => i.Lines).NotEmpty();

            When(i => i.DocumentType == "33", () =>
            {
                RuleFor(i => i.TaxRate).GreaterThan(0M);
            });

            When(i => i.DocumentType == "34", () =>
            {
                RuleFor(i => i.TaxRate).LessThanOrEqualTo(0M);
            });

            When(i => i.ExtraTaxCode > 0, () =>
            {
                RuleFor(i => i.ExtraTaxRate).GreaterThan(0M);
            });

            When(
                i =>
                    i.DocumentType == "30" || i.DocumentType == "32" || i.DocumentType == "55" || i.DocumentType == "60",
                () =>
                {
                    RuleFor(i => i.FolioNumber).GreaterThan(0);
                });

            When(i => i.DocumentType == "801", () =>
            {
                RuleFor(i => i.FolioNumber).GreaterThan(0);
            });

            // Credit/Debit note
            When(i => i.DocumentType == "56" || i.DocumentType == "61", () =>
            {
                RuleFor(i => i.References).NotEmpty();
                RuleForEach(i => i.References).NotEmpty()
                    .Must(i => (short)i.ReferenceReason != (short)CorrectionCode.None);
            });
        }
    }
}