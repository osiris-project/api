﻿using System;
using System.Configuration;

namespace Zentom.Api.Modules
{
    public class SigningProvider
    {
        public string FolderName { get; }

        public string Provider { get; }

        public string IsoCode { get; }

        public SigningProvider(string folderName, string provider)
        {
            var pair = folderName.Split("=".ToCharArray(), 2);
            FolderName = pair[1];
            pair = provider.Split("=".ToCharArray(), 2);
            var split = pair[1].Split("_".ToCharArray());
            if (split.Length == 2)
            {
                IsoCode = split[1];
            }
            Provider = split[0];
        }

        public string GetFolderPath()
        {
            var isTestingEnabled = bool.Parse(ConfigurationManager.AppSettings[Constants.IS_TESTING_ENABLED_KEY]);

            switch (Provider)
            {
                case "artikos":
                    return isTestingEnabled ? @"STCRepositorio\TESTING" : @"STCRepositorio\PRODUCCION";
            }

            return null;
        }
    }
}