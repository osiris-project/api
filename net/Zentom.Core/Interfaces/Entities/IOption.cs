﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IOption
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Value { get; set; }

        string ValueType { get; set; }

        string Description { get; set; }

        ICollection<IOptionPermission> OptionPermissions { get; set; }
    }
}
