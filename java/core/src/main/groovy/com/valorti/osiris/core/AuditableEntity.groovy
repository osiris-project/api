/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import java.time.Instant

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

@MappedSuperclass
abstract class AuditableEntity<IdType>
implements Auditable<IdType> {
    @Transient
    private static final long serialVersionUID = 1L

    @Column
    Entity<IdType> createdBy

    @Column(columnDefinition = 'DATETIME')
    Instant createdAt

    @Column
    Entity<IdType> lastModifiedBy

    @Column(columnDefinition = 'DATETIME')
    Instant lastModifiedAt
}
