﻿namespace Zentom.Core.Entities
{
    public class EnterpriseComercialActivity
        : CompanyComercialActivity
    {
        #region Properties

        public virtual Enterprise Enterprise
        {
            get { return (Enterprise)Company; }
            set { Company = value; }
        }

        #endregion

        public EnterpriseComercialActivity()
        {
        }

        public EnterpriseComercialActivity(Company enterprise, ComercialActivity comercialActivity)
            : base(enterprise, comercialActivity)
        {
        }
    }
}