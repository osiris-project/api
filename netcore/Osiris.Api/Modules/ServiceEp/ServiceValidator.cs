﻿using FluentValidation;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.ServiceEp
{
    public class ServiceValidator
        : AbstractValidator<Service>
    {
        public ServiceValidator()
        {
            RuleFor(i => i.Name).Length(3, 100);
            RuleFor(i => i.UnitPrice).GreaterThan(0);
        }
    }
}
