﻿using log4net;
using LightInject;
using LightInject.Interception;
using Nancy;
using Nancy.ModelBinding;
using Zentom.Dependency;

namespace Zentom.Api
{
    public static class LightinjectConfig
    {
        public static void Configure(IServiceContainer container)
        {
            var log = LogManager.GetLogger(typeof(LightinjectConfig));
            log.Debug("Configuring lightinject.");
            var serviceContainer = (ServiceContainer)container;
            serviceContainer.RegisterFrom<Composition>();
            serviceContainer.Register<ISerializer, SecureJsonSerializer>();
            serviceContainer.Register<IBodyDeserializer, JsonDeserializer>();
            log.Debug("Done.");
        }

        public static void RegisterInterceptors(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Intercept(sr => sr.ServiceType == typeof(NancyModule),
                (sf, proxyDef) => ProxyTypeDefinition(proxyDef));
        }

        private static void ProxyTypeDefinition(ProxyDefinition proxyDef)
        {
            proxyDef.Implement(() => new LoggingInterceptor());
        }
    }
}