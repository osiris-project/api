﻿using System.Linq;
using Dobelik.Utils.Identity;
using Slf4Net;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Osiris.Core.Services;

namespace Osiris.Api.Modules.EmployeeEp
{
    public class EmployeeModule
        : CRUDLModule<Command, EmployeeCommand, EmployeeCommand>
    {
        private readonly IAuthenticationService authService;
        private readonly INationalIdentity identity;

        private readonly ILogger log = LoggerFactory.GetLogger(typeof(EmployeeModule));

        public EmployeeModule(ISessionFactory factory, IAuthenticationService authService, INationalIdentity identity)
            : base("employees", factory)
        {
            this.authService = authService;
            this.identity = identity;

            // Account endpoints
            this.Post<AccountCreateCmd>(BasePath + "/{id:guid}/account", CreateAccount);

            // Role endpoints
            this.Post<RoleCommand>(BasePath + "/{id:guid}/account/roles", SetRoles);
        }

        #region Employee Management

        [Permission(Permission = "employee-read")]
        public override Negotiator Read(Command cmd)
        {
            log.Debug("Reading Employee.");

            var entity = db.QueryOver<Employee>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Left.JoinQueryOver(i => i.Person)
                .Left.JoinQueryOver(i => i.Account)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                log.Info("Employee '{0}' not found.", cmd.Id);
                return Negotiate.WithNotFound(string.Format(I18n.Error.EmployeeNotFound, cmd.Id));
            }

            Account account = null;
            AuthorizedAccount authorizedAccount = null;
            Role role = null;
            var roles = db.QueryOver<AccountRole>()
                .Left.JoinAlias(i => i.Role, () => role)
                .Left.JoinAlias(i => i.AuthorizedAccount, () => authorizedAccount)
                .Left.JoinAlias(i => authorizedAccount.Account, () => account)
                .Where(i => authorizedAccount.Account == entity.Person.Account)
                .List();
            var dto = new
            {
                id = entity.Id,
                firstName = entity.Person.FirstName,
                lastName = entity.Person.LastName,
                nationalId = entity.Person.NationalId,
                account = new
                {
                    email = entity.Person.Account.Email,
                    login = entity.Person.Account.Login,
                    roles = roles.Select(i => new
                    {
                        name = i.Role.Name,
                        id = i.Role.Id,
                        displayName = i.Role.DisplayName,
                        description = i.Role.Description
                    })
                }
            };
            log.Debug("Role count: {0}", dto.account.roles.Count());
            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "employee-list")]
        public override Negotiator List(dynamic args)
        {
            var bound = this.BindTo(new EmployeeQuery());
            log.Debug("Query: {0}", bound);
            Person person = null;
            EmployeeDto dto = null;
            var query = db.QueryOver<Employee>()
                .Left.JoinAlias(i => i.Person, () => person)
                .Where(i => i.Tenant == currentTenant)
                .SelectList(i => i
                    .Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => person.FirstName).WithAlias(() => dto.FirstName)
                    .Select(j => person.LastName).WithAlias(() => dto.LastName)
                    .Select(j => person.NationalId).WithAlias(() => dto.NationalId));
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;

            if (!string.IsNullOrWhiteSpace(bound.FullName))
            {
                query.WhereRestrictionOn(i => person.FullName)
                    .IsInsensitiveLike(bound.FullName);
            }
            else if (!string.IsNullOrWhiteSpace(bound.LastName))
            {
                query.WhereRestrictionOn(i => person.LastName)
                    .IsInsensitiveLike(bound.LastName);
            }
            else if (!string.IsNullOrWhiteSpace(bound.NationalId))
            {
                bound.NationalId = identity.CleanId(bound.NationalId);
                query.Where(i => person.NationalId == bound.NationalId);
            }

            var items = query.TransformUsing(Transformers.AliasToBean<EmployeeDto>())
                .PaginateResults<Employee, EmployeeDto>(page, rpp);
            return items == null ? Negotiate.WithNotFound() : Negotiate.WithOkAndList(items, Request.Url.ToString());
        }

        [Permission(Permission = "employee-create")]
        public override Negotiator Create(EmployeeCommand cmd, dynamic args)
        {
            cmd.NationalId = identity.CleanId(cmd.NationalId);
            var person = db.QueryOver<Person>()
                .Where(i => i.NationalId == cmd.NationalId)
                .SingleOrDefault();

            if (person == null)
            {
                person = new Person
                {
                    FirstName = cmd.FirstName,
                    LastName = cmd.LastName,
                    NationalId = cmd.NationalId,
                    BirthDate = cmd.BirthDate
                };

                db.Save(person);
            }
            
            var employee = db.QueryOver<Employee>()
                .Where(i => i.Person == person)
                .SingleOrDefault();

            if (employee != null)
            {
                return Negotiate.WithCreated(Context.GetResourceUrl(employee.Id), employee.Id);
            }

            employee = new Employee
            {
                Tenant = currentTenant,
                Person = person
            };

            db.Save(employee);

            return Negotiate.WithCreated(Context.GetResourceUrl(employee.Id), employee.Id);
        }

        [Permission(Permission = "employee-delete")]
        public override Negotiator Remove(Command cmd)
        {
            var entity = db.QueryOver<Employee>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithOk();
            }

            db.Delete(entity);
            return Negotiate.WithOk();
        }

        #endregion

        #region Account Management

        [Permission(Permission = "account-create")]
        public Negotiator CreateAccount(AccountCreateCmd cmd)
        {
            log.Debug("Creating account: {0}", cmd);
            var entity = db.QueryOver<Account>()
                .Where(i => i.Login == cmd.Login)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                entity = new Account
                {
                    Login = cmd.Login,
                    Email = cmd.Email,
                    Person = db.Load<Person>(cmd.PersonId)
                };
                authService.HashPassword(entity, cmd.Password);
                db.Save(entity);
            }

            currentTenant.AuthorizeAccount(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        #endregion

        #region Role Assignment

        /// <summary>
        /// Adds or removes roles depending on the content passed. If the roles enumerable is empty,
        /// the roles will be cleared from this account.
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        [Permission(Permission = "account-update")]
        public Negotiator SetRoles(RoleCommand cmd)
        {
            var entity = db.QueryOver<AuthorizedAccount>()
                .Where(i => i.Id == cmd.AccountId && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Text.Account, cmd.AccountId);
            }

            if (cmd.Roles.Any())
            {
                // Remove roles
                for(var index = entity.AccountRoles.Count - 1; index >= 0; index--)
                {
                    var ar = entity.AccountRoles.ElementAt(index);
                    if (cmd.Roles.All(i => i.Id != ar.Role.Id))
                    {
                        entity.AccountRoles.Remove(ar);
                    }
                }

                foreach (var ar in cmd.Roles.Select(i => new AccountRole(db.Load<Role>(i.Id), entity)))
                {
                    entity.AccountRoles.Add(ar);
                }
            }
            else
            {
                entity.AccountRoles.Clear();
            }
            
            return Negotiate.WithNoContent();
        }

        #endregion
    }
}
