﻿using FluentValidation;

namespace Osiris.Api.Modules.EmployeeEp
{
    public class EmployeeValidator
        : AbstractValidator<EmployeeCommand>
    {
        public EmployeeValidator()
        {
            RuleFor(i => i.FirstName).NotEmpty();
            RuleFor(i => i.LastName).NotEmpty();
            RuleFor(i => i.NationalId).NotEmpty();
        }
    }
}
