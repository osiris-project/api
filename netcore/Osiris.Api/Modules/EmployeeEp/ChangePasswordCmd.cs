﻿namespace Osiris.Api.Modules.EmployeeEp
{
    public class ChangePasswordCmd
        : Command
    {
        public string Password { get; set; }

        public override string ToString()
        {
            return $"ChangePasswordCommand: [Password: {Password}]";
        }
    }
}
