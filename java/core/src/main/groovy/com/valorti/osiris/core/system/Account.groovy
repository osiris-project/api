/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationException

/**
 * An account only has an ID field to link to the other entities.
 *
 * The account has many relationship to other entities, but the bidirectional
 * relationship will not be set for now.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = "system")
class Account extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, unique = true, length = 20)
    String taxId

    /**
     * Defines a relationship with the tenants that have authorized
     * this account to represent them.
     */
    @OneToMany
    def authorizedBy = new HashSet<AuthorizedAccount>()

    /**
     *
     */
    protected Account() {
    }

    /**
     *
     * @param taxId
     * @throws ValidationException if there are validation errors.
     * @since 0.0.1
     */
    Account(String taxId) {
        def errors = []
        if(!taxId) {
            errors << ValidationBuilder.empty('account.taxId')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        this.taxId = taxId.trim()
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((taxId == null) ? 0 : taxId.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof Account)) {
            return false
        }
        final other = (Account) obj
        return Objects.equals(this.taxId, other.taxId)
    }
}
