/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.ValidationError
import com.valorti.osiris.core.taxation.documents.DocumentParser

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
abstract class AbstractDocumentParser
    implements DocumentParser {

    final List<ValidationError> errors

    /**
     * Default constructor
     */
    protected AbstractDocumentParser() {
        this.errors = []
    }
}
