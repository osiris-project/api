﻿using System;
using System.Collections.Generic;
using System.IO;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Core.Interfaces.Integration.TributaryDocuments
{
    public interface ITributaryDocumentFormatter<in TEntity>
        : IDisposable
    {
        /// <summary>
        /// Fetches the validation results for this instance.
        /// </summary>
        IList<IModelValidation> Errors { get; }

        /// <summary>
        /// Checks whether this instance is valid or not.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Date format to be used in the date fields.
        /// </summary>
        string DateFormat { get; }

        /// <summary>
        /// Formats the object.
        /// </summary>
        /// <param name="stream">The stream where the object will be written.</param>
        /// <param name="entity">The object that will be formatted.</param>
        void Format(StreamWriter stream, TEntity entity);
    }
}
