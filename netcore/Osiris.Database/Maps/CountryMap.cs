﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class CountryMap
        : ClassMap<Country>
    {
        public CountryMap()
        {
            Schema("location");
            Table("country");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(100).UniqueKey("country_un");
            Map(p => p.Code).Length(2);
            Map(p => p.PhoneCode).Length(5);

            HasMany(i => i.Cities)
                .Cascade.AllDeleteOrphan()
                .KeyColumn("country_id")
                .Inverse();

            HasMany(i => i.Locales)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("country_id");
        }
    }
}
