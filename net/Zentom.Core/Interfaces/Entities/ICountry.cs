﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ICountry
        : IUnique<int>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Code { get; set; }

        string PhoneCode { get; set; }
        
        ICollection<ICity> Cities { get; set; }
        
        ICollection<ICountryLocale> CountryLocales { get; set; }
    }
}
