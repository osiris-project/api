﻿using Dobelik.Utils.Identity;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Osiris.Core.Services;
using Slf4Net;

namespace Osiris.Api.Modules.ManagementEp {
    [Module (Name = "management")]
    public class ManagementModule
        : TenantApi {
            private readonly IAuthenticationService authenticationService;
            private readonly INationalIdentity identity;
            //private static readonly ILogger log = LoggerFactory.GetLogger(typeof(ManagementModule));

            public ManagementModule (ISessionFactory factory, IAuthenticationService authenticationService, INationalIdentity identity) : base ("management", factory) {
                this.authenticationService = authenticationService;
                this.identity = identity;
                this.Post<AccountCmd> (BasePath + "/accounts/", CreateAccount);
            }

            [Permission (Permission = "account-create")]
            public Negotiator CreateAccount (AccountCmd cmd) {
                var entity = db.QueryOver<Account> ()
                    .Where (i => i.Login == cmd.Login && i.Email == cmd.Email)
                    .Take (1)
                    .SingleOrDefault ();

                if (entity == null) {
                    entity = new Account {
                    Email = cmd.Email,
                    Login = cmd.Login,
                    Person = new Person {
                    FirstName = cmd.FirstName,
                    LastName = cmd.LastName,
                    NationalId = identity.CleanId (cmd.NationalId),
                    BirthDate = cmd.BirthDate,
                    Gender = cmd.Gender
                    }
                    };
                    authenticationService.HashPassword (entity, cmd.Password);
                    db.Save (entity);
                }
                return Negotiate.WithOkAndModel (new {
                    id = entity.Id,
                        login = entity.Login,
                        email = entity.Email,
                        person = new {
                            id = entity.Person.Id,
                                nationalId = entity.Person.NationalId,
                                firstName = entity.Person.FirstName,
                                lastName = entity.Person.LastName,
                                birthDate = entity.Person.BirthDate
                        }
                });
            }
        }
}
