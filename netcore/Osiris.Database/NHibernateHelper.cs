﻿using System;
using System.Diagnostics;
using FluentNHibernate.Cfg;
using Slf4Net;
using NHibernate;
using NHibernate.Cfg;
using Osiris.Database.Conventions;
using Osiris.Database.Maps;

namespace Osiris.Database
{
    public static class NHibernateHelper
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger("Osiris.Database.NHibernateHelper");

        private static ISessionFactory sessionFactory;

        public static ISessionFactory SessionFactory
        {
            get
            {
                Log.Debug("Retrieving sessionFactory.");

                if(sessionFactory == null)
                {
                    try
                    {
                        Log.Debug("Building Session Factory.");
                        var nhcfg = new Configuration();
                        nhcfg.Configure();
                        sessionFactory = Fluently.Configure(nhcfg)
                            .Mappings
                            (
                                i => i.FluentMappings.AddFromAssemblyOf<AccountMap>()
                                    .Conventions.AddFromAssemblyOf<OsirisIdConvention>()
                            )
                            .BuildSessionFactory();
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Failed to build nhibernate session factory - ", ex);
                        Debug.WriteLine("Exception: {0}", ex);
                        sessionFactory = null;
                    }
                }
                else
                {
                    Log.Debug("Session Factory already built.");
                }

                return sessionFactory;
            }
        }

        public static ISession Session
        {
            get
            {
                ISession s;

                try
                {
                    s = SessionFactory.GetCurrentSession();
                    Log.Debug("Retrieved session id '{0}'.",
                        s.GetSessionImplementation().SessionId);
                }
                catch (Exception)
                {
                    Log.Debug("Failed to retrieve current session. Opening one.");
                    try
                    {
                        s = SessionFactory.OpenSession();
                        Log.Debug("Opening Session id: {0}.", s.GetSessionImplementation().SessionId);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Probably session factory wasn't built.", ex);
                        Debug.WriteLine("Exception: {0}", ex);
                        s = null;
                    }
                }

                return s;
            }
        }
    }
}
