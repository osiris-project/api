﻿namespace Zentom.Core.Entities
{
    public class TaxDocumentIssuer
        : AbstractTaxDocumentEnterprise
    {
        public virtual string SectorCode { get; set; }

        public virtual string VatOwnerTaxationId { get; set; }

        public virtual string IssuerName { get; set; }

        public TaxDocumentIssuer() { }
    }
}
