/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.ValidationError
import groovy.util.slurpersupport.GPathResult

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 * @param <T>   The type that will be parsed.
 */
interface SectionParser<T> {
    List<ValidationError> getErrors()

    T parse(GPathResult path)
}
