/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

/**
 * Represents a basic entity.
 *
 * @author Nicolas Mancilla Vergara <nmancilla@valorti.com>
 */
interface Entity<IDType> extends Versioned {
	IDType getId()
}
