﻿using System;
using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocument
        : IUnique<long>, IVersioned, IRowGuid
    {
        string EmissionPoint { get; set; }

        long FolioNumber { get; set; }

        DateTime AccountingDate { get; set; }

        TimeSpan? AccountingTime { get; set; }

        DateTime? ExpireDate { get; set; }

        decimal PreviousDebtBalance { get; set; }

        string Comments { get; set; }

        string FilePath { get; set; }

        string DocumentStatus { get; set; }

        IDispatchType DispatchType { get; set; }

        IDocumentType DocumentType { get; set; }

        IEnterprise Provider { get; set; }

        IEnterprise Client { get; set; }

        ICollection<IPayment> Payments { get; set; }

        IPaymentType DefaultPaymentType { get; set; }

        ICollection<ITaxDocumentReference> ReferencedDocuments { get; set; }

        ICollection<ITaxDocumentReference> ReferencedBy { get; set; }

        ITaxDocument Parent { get; set; }

        ICollection<ITaxDocumentProduct> TaxDocumentProducts { get; set; }

        ICollection<ITaxDocumentService> TaxDocumentServices { get; set; }
    }
}
