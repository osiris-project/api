﻿using System;
using System.Collections.Generic;
using Zentom.Core.Pagination;

namespace Zentom.Api
{
    [Serializable]
    public class PagedResults<TEntity>
        : IPagedResult<TEntity>
    {
        #region Properties

        public IList<TEntity> Items
        {
            get;
        }

        public long ResultCount
        {
            get; private set;
        }

        public int CurrentPage
        {
            get; private set;
        }

        public int ResultsPerPage
        {
            get; private set;
        }

        public long PageCount
        {
            get; private set;
        }

        public bool HasPrevious
        {
            get; private set;
        }

        public bool HasNext
        {
            get; private set;
        }

        #endregion

        public PagedResults(IEnumerable<TEntity> src, int page, int resultsPerPage, int total)
            : this(src, page, resultsPerPage, (long)total)
        { }

        public PagedResults(IEnumerable<TEntity> src, int page, int resultsPerPage, long total)
        {
            Calculate(page, resultsPerPage, total);
            Items = new List<TEntity>(src);
        }

        public PagedResults(IList<TEntity> src, int page, int resultsPerPage, long total)
        {
            Calculate(page, resultsPerPage, total);
            Items = src;
        }

        private void Calculate(int page, int resultsPerPage, long total)
        {
            CurrentPage = page;
            ResultsPerPage = resultsPerPage;
            ResultCount = total;
            double mod = ResultCount % ResultsPerPage;
            var pageCnt = ResultCount/ResultsPerPage;
            PageCount = mod > 0 ? pageCnt + 1 : pageCnt;
            HasNext = (CurrentPage + 1) < ResultCount;
            HasPrevious = CurrentPage > 0;
        }
    }
}
