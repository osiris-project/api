﻿using System;
using System.Linq;

namespace Dobelik.Utils.Password.Extensions
{
    public static class ByteExtensions
    {
        /// <summary>
        /// Extension method to compare two byte[].
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>True if both contains the same sequence of bytes, or false
        /// if a or b are null or empty.</returns>
        public static bool CompareTo(this byte[] a, byte[] b)
        {
            if (a == null || b == null)
                return false;
            if (a.Length <= 0 || b.Length <= 0)
                return false;
            return a.SequenceEqual(b);
        }

        /// <summary>
        /// Converts a byte array to string using BitConverter.ToString().
        /// </summary>
        /// <param name="arr">A byte array to convert to string.</param>
        /// <param name="noHyphen">True if the returned string should not contain hyphens.</param>
        /// <returns>Returns a string representation of the hash or an empty string if the 
        /// hash was null or empty.</returns>
        public static string AsString(this byte[] arr, bool noHyphen = true)
        {
            var result = "";
            if (arr == null || arr.Length <= 0) return result;
            result = BitConverter.ToString(arr);
            if (noHyphen)
                result = result.Replace("-", string.Empty);
            return result;
        }
    }
}
