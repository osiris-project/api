﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Entities
{
    public abstract class VersionedEntity
        : IVersioned
    {
        #region Implementation of IVersioned

        public virtual byte[] RowVersion { get; protected set; }

        #endregion
    }
}
