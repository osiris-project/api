﻿using Dobelik.Utils.Identity;
using NHibernate;
using Osiris.Core.Services;

namespace Osiris.Services
{
    public class EnterpriseService
        : Service, IEnterpriseService
    {
        private readonly INationalIdentity identity;

        public EnterpriseService(ISessionFactory factory, INationalIdentity identity)
            : base(factory)
        {
            this.identity = identity;
        }
    }
}
