/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.location

import com.valorti.osiris.core.location.LocationService
import com.valorti.osiris.core.organization.City
import com.valorti.osiris.core.organization.CityRepository
import com.valorti.osiris.core.organization.Commune
import com.valorti.osiris.core.organization.CommuneRepository
import org.springframework.stereotype.Service

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Service
class LocationServiceImpl
    implements LocationService {

    private final CommuneRepository communeRepository
    private final CityRepository cityRepository


    LocationServiceImpl(CommuneRepository communeRepository, CityRepository cityRepository) {
        this.communeRepository = communeRepository
        this.cityRepository = cityRepository
    }

    @Override
    Commune retrieveCommune(String communeName, String cityName) {
        cityName = cityName?.trim()
        Commune commune = null
        City city = null
        if (cityName) {
            city = this.cityRepository.findOneByName(cityName)
        } else {
            cityName = '------'
            city = this.cityRepository.findOneByName(cityName)
        }

        if (!city) {
            city = new City(cityName)
        } else {
            commune = this.communeRepository.findOneByNameAndCity(communeName, city)
        }
        if (!commune) {
            commune = new Commune(communeName, '', city)
        }
        return commune
    }
}
