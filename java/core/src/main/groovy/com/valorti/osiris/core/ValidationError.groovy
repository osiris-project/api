/**
 * Copyright (c) Mar 7, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

/**
 * A validation error created when validating an entity.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface ValidationError {
    /**
     * The faulty property name.
     */
    String property

    /**
     * The error message of why the property is not valid.
     */
    String message
}
