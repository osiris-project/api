﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEmployee
        : IUnique<long>, IVersioned, IRowGuid
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Name { get; }

        string NationalId { get; set; }

        IAccount Account { get; set; }

        ICollection<IProduct> CreatedProducts { get; set; }

        ICollection<IService> CreatedServices { get; set; }

        ICollection<IAccount> CreatedAccounts { get; set; }

        ICollection<IEnterpriseStaff> EnterpriseStaff { get; set; }
    }
}
