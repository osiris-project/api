﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class Commune
        : Entity
    {
        #region Fields

        private string _name;

        private string _postCode;

        private City _city;

        private ISet<EnterpriseAddress> _enterpriseAddresses;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        public virtual City City
        {
            get { return _city; }
            set { _city = value; }
        }

        public virtual ISet<EnterpriseAddress> EnterpriseAddresses
        {
            get { return _enterpriseAddresses; }
            set { _enterpriseAddresses = value; }
        }

        #endregion

        public Commune()
        {
            _enterpriseAddresses = new HashSet<EnterpriseAddress>();
        }

        public Commune(string name, string postCode, City city, ISet<EnterpriseAddress> enterpriseAddresses)
            : this()
        {
            _name = name;
            _postCode = postCode;
            _city = city;

            if(enterpriseAddresses != null)
                _enterpriseAddresses = enterpriseAddresses;
        }
    }
}
