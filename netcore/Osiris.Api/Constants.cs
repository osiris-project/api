﻿namespace Osiris.Api
{
    public static class Constants
    {
        public const string ADMIN_ROLE_NAME = "administrator";

        public const string ANONYMOUS = "anonymous";

        public const string API_VERSION = "v1";

        public const string BASE_PATH = "/";
        
        public const string EMISSION_POINT = "vti_osiris";

        public const string ROOT_STORAGE_KEY = "osiris.root_storage";

        public const string RECEIVED_DOCUMENTS_FOLDER = "received-documents";

        public const string IS_TESTING_ENABLED_KEY = "osiris.is_testing_enabled";

        public const string SYSTEM_FILE_DATE_TIME_FORMAT = "yyyy-MM-dd_HH-mm-ss";

        // Setting constants

        public const string API_SETTINGS_KEY = "api_settings";

        public const string FOLDER_NAME_SETTING = "repository_folder_name";

        public const string SIGNING_PROVIDER_SETTING = "signing_provider";

        public const string FOLIO_RANGE_ALERT_PERCENT = "folio_range_alert";
    }
}
