﻿using System;

namespace Zentom.Api.Modules.AccountingEp
{
    public class AccountingAccountCreateCmd
    {
        public string Code { get; set; } 

        public string Description { get; set; }
    }

    public class AccountingAccountUpdateCmd
        : AccountingAccountCreateCmd
    {
        public Guid Id { get; set; }
    }
}