﻿using System.Collections.Generic;
using FluentValidation.Internal;
using FluentValidation.Validators;
using Nancy.Validation;
using Nancy.Validation.FluentValidation;

namespace Zentom.Api.Modules.Validators.NationalId
{
    public class NationalIdAdapter
        : AdapterBase
    {
        #region Overrides of AdapterBase

        public override bool CanHandle(IPropertyValidator validator)
        {
            return validator is NationalIdValidator;
        }

        public override IEnumerable<ModelValidationRule> GetRules(PropertyRule rule, IPropertyValidator validator)
        {
            yield return
                new NationalIdValidationRule(FormatMessage(rule, validator), GetMemberNames(rule));
        }

        #endregion
    }
}