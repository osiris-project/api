﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class LocaleMap : ClassMap<Locale>
    {
        public LocaleMap()
        {
            Schema("i18n");
            Table("locale");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(6).UniqueKey("locale_un");
            Map(p => p.FullName).Length(50).Nullable().Default("");

            HasMany(i => i.CountryLocales)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("locale_id");
        }
    }
}
