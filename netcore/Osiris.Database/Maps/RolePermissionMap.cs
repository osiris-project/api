﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class RolePermissionMap
        : ClassMap<RolePermission>
    {
        public RolePermissionMap()
        {
            Schema("system");
            Table("role_permission");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Role, "role_id")
                .UniqueKey("role_permission_un")
                .Cascade.SaveUpdate();

            References(i => i.Permission, "permission_id")
                .UniqueKey("role_permission_un")
                .Cascade.SaveUpdate();
        }
    }
}
