﻿using System;

namespace Osiris.Api.Modules.EnterpriseEp
{
    public class ComercialActivityDto
    {
        public Guid Id { get; set; } 

        public string Name { get; set; }
    }
}
