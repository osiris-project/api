﻿using System;

namespace Zentom.Api.Modules.AccountingEp
{
    public class VoucherQuery
    {
        public long? Number { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}