/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class ValidationBuilder {

    /**
     * Builds a validation error that appends the .empty string
     * to the property.
     *
     * @param property The name of the faulty property.
     * @param msg The error message
     * @since 0.0.1
     */
    static ValidationError build(String property, String msg) {
        return DefaultValidationError.build(property, msg)
    }

    /**
     * Builds a validation error that appends the .empty string
     * to the property.
     *
     * @param property The name of the faulty property.
     * @param msg The error message
     * @since 0.0.1
     */
    static ValidationError empty(String property, String msg) {
        return DefaultValidationError.build("${property}.empty", msg)
    }

    static ValidationError empty(String property) {
        return empty(property, '')
    }
}
