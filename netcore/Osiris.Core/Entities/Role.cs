﻿using System.Collections.Generic;
using System.Linq;

namespace Osiris.Core.Entities
{
    public class Role
        : TenantEntity
    {
        #region Fields

        private string name;

        private string displayName;

        private string description;

        private ISet<AccountRole> accountRoles;

        private ISet<RolePermission> rolePermissions;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual ISet<AccountRole> AccountRoles
        {
            get { return accountRoles; }
            set { accountRoles = value; }
        }

        public virtual ISet<RolePermission> RolePermissions
        {
            get { return rolePermissions; }
            set { rolePermissions = value; }
        }

        #endregion

        public Role()
        {
            accountRoles = new HashSet<AccountRole>();
            rolePermissions = new HashSet<RolePermission>();
        }

        public Role(Tenant tenant, string name, string displayName, string description, ISet<AccountRole> accountRoles,
            ISet<RolePermission> rolePermissions)
            : base(tenant)
        {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
            this.accountRoles = accountRoles;
            this.rolePermissions = rolePermissions;
        }

        #region Role Permissions

        public virtual void AddRolePermission(Permission item)
        {
            if (rolePermissions.Any(i => i.Permission.Id == item.Id))
                return;

            rolePermissions.Add(new RolePermission(this, item));
        }

        public virtual void RemoveRolePermission(Permission item)
        {
            if (rolePermissions.All(i => i.Permission.Id != item.Id))
                return;

            rolePermissions.Remove(new RolePermission(this, item));
        }

        public virtual void AddPermissions(IList<Permission> list)
        {
            if (list.Any())
            {
                if (rolePermissions.Any())
                {
                    var len = rolePermissions.Count - 1;
                    for (var index = len; index >= 0; index--)
                    {
                        var rp = rolePermissions.ElementAt(index);
                        if (list.All(i => i.Id != rp.Permission.Id)) continue;
                        rolePermissions.Remove(rp);
                    }
                }

                foreach (var permission in list)
                {
                    rolePermissions.Add(new RolePermission(this, permission));
                }
            }
            else
            {
                rolePermissions.Clear();
            }
        }

        #endregion
    }
}
