/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.dobelik.utils.nationalId.IdentityFactory
import com.dobelik.utils.nationalId.NationalIdentity
import groovy.util.slurpersupport.GPathResult
import org.junit.Before

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class BaseParserTests {

    protected final NationalIdentity identity = new IdentityFactory().GetInstance('CL')

    @Before
    void setup() {

    }

    GPathResult buildEBill() {
        return build('/761039156_231252_33.xml')
    }

    GPathResult buildECreditNote() {
        return build('/761039156_2262_61.xml')
    }

    GPathResult build(String classPathFile) {
        def result = new XmlSlurper().parseText(this.getClass().getResource(classPathFile).text)
        return result
    }
}
