﻿namespace Osiris.Core.Entities
{
    public class AccountRole
        : Entity
    {
        #region Fields

        private Role role;

        private AuthorizedAccount authorizedAuthorizedAccount;

        #endregion

        #region Properties

        public virtual Role Role
        {
            get { return role; }
            set { role = value; }
        }

        public virtual AuthorizedAccount AuthorizedAccount
        {
            get { return authorizedAuthorizedAccount; }
            set { authorizedAuthorizedAccount = value; }
        }

        #endregion

        public AccountRole() { }

        public AccountRole(Role role, AuthorizedAccount authorizedAuthorizedAccount)
        {
            this.role = role;
            this.authorizedAuthorizedAccount = authorizedAuthorizedAccount;
        }
    }
}
