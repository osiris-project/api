﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Osiris.Database.Conventions
{
    public class OsirisVersionConvention
        : IVersionConvention
    {
        public void Apply(IVersionInstance instance)
        {
            instance.Column("row_version");
            instance.Generated.Always();
            instance.Nullable();
        }
    }
}
