﻿using Nancy;
using Nancy.Responses.Negotiation;
using Osiris.Api.Extensions.Nancy;

namespace Osiris.Api.Modules
{
    public abstract class ApiModule : NancyModule
    {
        #region Properties
        
        public ApiSettings Settings { protected get; set; }

        #endregion

        protected ApiModule()
            : base(Constants.BASE_PATH)
        {
            Get("", Index);
            Get("welcome", Index);
            this.ParseSettings();
        }

        /// <summary>
        /// Simple welcome message. Public to anyone.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private Negotiator Index(dynamic args)
        {
            const string model = "Welcome to the Osiris API.";
            // TODO: Return an introduction and link to a tutorial.
            return Negotiate.WithModel(model)
                .WithStatusCode(HttpStatusCode.OK);
        }
    }
}
