﻿using FluentValidation.Validators;
using Slf4Net;

namespace Osiris.Api.Modules.Validators.Guid
{
    public class GuidValidator
        : PropertyValidator
    {
        public GuidValidator()
            : base("InvalidId", typeof(I18n.Error))
        {
        }

        #region Overrides of PropertyValidator

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var log = LoggerFactory.GetLogger(typeof (GuidValidator));
            log.Debug("property value: {0}", context.PropertyValue);
            var val = context.PropertyValue.ToString();
            System.Guid guid;
            System.Guid.TryParse(val, out guid);
            log.Debug("parsed guid: {0}", guid);
            return guid != System.Guid.Empty;
        }

        #endregion
    }
}
