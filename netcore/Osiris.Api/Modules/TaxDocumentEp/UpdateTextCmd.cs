﻿using System;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class UpdateTextCmd
    {
        public Guid ReferencedDocument { get; set; }
    }
}
