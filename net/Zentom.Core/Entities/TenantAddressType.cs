﻿namespace Zentom.Core.Entities
{
    public class TenantAddressType
        : CompanyAddressType
    {
        #region Properties

        public virtual TenantAddress TenantAddress
        {
            get { return (TenantAddress) CompanyAddress; }
            set { CompanyAddress = value; }
        }

        #endregion

        public TenantAddressType() { }

        public TenantAddressType(CompanyAddress companyAddress, AddressType addressType)
            : base(companyAddress, addressType)
        {
        }
    }
}