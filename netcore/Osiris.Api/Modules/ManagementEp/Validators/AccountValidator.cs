﻿using Dobelik.Utils.Identity;
using FluentValidation;
using Osiris.Api.Modules.Validators.NationalId;

namespace Osiris.Api.Modules.ManagementEp.Validators
{
    public class AccountValidator
        : AbstractValidator<AccountCmd>
    {
        public AccountValidator(INationalIdentity identity)
        {
            RuleFor(i => i.Login).Length(3, 50);
            RuleFor(i => i.FirstName).Length(1, 100);
            RuleFor(i => i.LastName).Length(1, 100);
            RuleFor(i => i.NationalId).SetValidator(new NationalIdValidator(identity));
            RuleFor(i => i.Email).NotEmpty();
            RuleFor(i => i.Password).Length(6, 200);
        }
    }
}
