﻿namespace Zentom.Api.Extensions.Nancy
{
    public enum ModelId
    {
        Error,
        TamperedToken,
        BadFormattedModel,
        ItemNotFound,
        MissingId,
        ExistingItem,
        ExpiredToken
    }

    public class ExceptionModel
    {
        public ModelId Code { get; set; }

        public string Error { get; set; }
    }
}