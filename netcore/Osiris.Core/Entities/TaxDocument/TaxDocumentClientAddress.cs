﻿using System.Collections.Generic;

namespace Osiris.Core.Entities.TaxDocument
{
    public class TaxDocumentClientAddress
        : Entity
    {
        #region Fields

        private string line1;

        private string line2;

        private string commune;

        private string city;

        private TaxDocumentClient taxDocumentClient;

        private ISet<TaxDocumentClientAddressType> addressTypes;

        #endregion

        #region Properties

        public virtual string Line1
        {
            get { return line1; }
            set { line1 = value; }
        }

        public virtual string Line2
        {
            get { return line2; }
            set { line2 = value; }
        }

        public virtual string Commune
        {
            get { return commune; }
            set { commune = value; }
        }

        public virtual string City
        {
            get { return city; }
            set { city = value; }
        }

        public virtual ISet<TaxDocumentClientAddressType> AddressTypes
        {
            get { return addressTypes; }
            set { addressTypes = value; }
        }

        public virtual TaxDocumentClient TaxDocumentClient
        {
            get { return taxDocumentClient; }
            set { taxDocumentClient = value; }
        }

        #endregion

        public TaxDocumentClientAddress() { }
        
        public TaxDocumentClientAddress(string line1, string line2, string commune, string city,
            TaxDocumentClient taxDocumentClient, ISet<TaxDocumentClientAddressType> addressTypes)
        {
            this.line1 = line1;
            this.line2 = line2;
            this.commune = commune;
            this.city = city;
            this.taxDocumentClient = taxDocumentClient;
            this.addressTypes = addressTypes;
        }
    }
}
