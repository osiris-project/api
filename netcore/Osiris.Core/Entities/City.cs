﻿using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public class City
        : Entity
    {
        #region Fields

        private string _name;

        private Country _country;

        private ISet<Commune> _communes;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual Country Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public virtual ISet<Commune> Communes
        {
            get { return _communes; }
            set { _communes = value; }
        }

        #endregion

        public City()
        {
            _communes = new HashSet<Commune>();
        }

        public City(string name, Country country, ISet<Commune> communes)
            : this()
        {
            _name = name;
            _country = country;

            if(communes != null)
                _communes = communes;
        }
    }
}
