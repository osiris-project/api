﻿using System;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentClientDto
    {
        public virtual Guid SelectedClientReceptionAddress { get; set; }

        public virtual Guid SelectedClientSector { get; set; }

        public virtual Guid SelectedClient { get; set; }
    }
}
