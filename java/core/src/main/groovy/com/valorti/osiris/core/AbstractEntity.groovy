/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import java.time.Instant

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.Transient
import javax.persistence.Version

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@MappedSuperclass
abstract class AbstractEntity<IdType>
extends AuditableEntity<IdType>
implements Entity<IdType> {
    @Transient
    private static final long serialVersionUID = 1L

    @Version
    @Column(columnDefinition = "TIMESTAMP", nullable = false)
    Instant version

    abstract IdType getId()
}
