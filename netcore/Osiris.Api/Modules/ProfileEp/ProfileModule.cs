﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Dobelik.Utils.Identity;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Osiris.Core.Services;
using Slf4Net;

namespace Osiris.Api.Modules.ProfileEp {
    [Permission (RequiresLogin = true)]
    public class ProfileModule : TenantApi {
        private readonly IAuthenticationService authService;

        private readonly INationalIdentity identity;

        private readonly ILogger log;

        private string templateFilePath;

        private bool deleteTemplateFile;

        public ProfileModule (ISessionFactory factory, IAuthenticationService authService, INationalIdentity identity) : base ("profile", factory) {
            pathsToSkip = new [] {
                // Skipped because we need the list of tenants before we can select one.
                BasePath + "/tenants/?"
            };
            // Hook executed after a successful database operation to delete template files.
            After.AddItemToEndOfPipeline (DeleteTemplateFileOnSuccess);
            // This hook is executed if the save of a template to the database fails.
            OnError.AddItemToStartOfPipeline (DeleteTemplateFileOnError);
            this.authService = authService;
            this.identity = identity;
            log = LoggerFactory.GetLogger (typeof (ProfileModule));

            // Profile Changes
            this.Patch<ChangePasswordCommand> (BasePath + "/change-password/", ChangePassword);

            // Roles and Tenants
            this.Get (BasePath + "/tenants/", TenantList);
            this.Get (BasePath + "/roles/", GetCurrentRoles);

            // Tax Document Templates
            var subPath = BasePath + "/tax-document-templates/";
            this.Post<TaxDocumentTemplateCmd> (subPath, CreateTaxDocumentTemplate);
            this.Get (subPath, TaxDocumentTemplateList);
            this.Get (subPath + "check-name/", CheckTemplateNameAvailability);
            this.Delete<Command> (subPath + "{id:guid}/", DeleteTaxDocumentTemplate);

            // Address
            this.Get (BasePath + "/address-types/", GetAddressTypes);
        }

        /// <summary>
        /// Changes the user password.
        /// </summary>
        /// <param name="cmd">The input command.</param>
        /// <param name="args"></param>
        /// <returns>409 - If the password doesn't match.
        /// 200 - And redirects to login.</returns>
        public Negotiator ChangePassword (ChangePasswordCommand cmd, dynamic args) {
            log.Debug ("Entering ChangePassword.");
            log.Debug ("Change password command: {0}", cmd);
            var toUpdate = Account;

            if (!authService.ChangePassword (toUpdate, cmd.OldPassword, cmd.NewPassword)) {
                return Negotiate.WithValidationFailure (authService.ErrorList);
            }

            // TODO: Invalidate token.
            log.Debug ("Leaving ChangePassword.");
            return Negotiate.WithStatusCode (HttpStatusCode.OK)
                .WithHeader ("Location", Constants.BASE_PATH + "auth");
        }

        /// <summary>
        /// Saves a tax document template.
        /// </summary>
        /// <param name="cmd">The input command. <see cref="TaxDocumentTemplateCmd"/></param>
        /// <returns>409 - if a template with the same name already exists.
        /// 201 - if the operation was successful.</returns>
        public Negotiator CreateTaxDocumentTemplate (TaxDocumentTemplateCmd cmd) {
            var entity = db.QueryOver<TaxDocumentTemplate> ()
                .Where (i => i.Account == Account && i.Name == cmd.Name)
                .Take (1)
                .SingleOrDefault ();

            if (entity != null) {
                return Negotiate.WithExistingItem (I18n.Text.TaxDocumentTemplate, "name");
            }

            entity = new TaxDocumentTemplate {
                Name = cmd.Name,
                DocumentType = cmd.DocumentType,
                CreatedAt = DateTime.UtcNow,
                Tenant = currentTenant
            };
            entity.FilePath = GetFileName (entity.DocumentType, entity.CreatedAt);
            entity.Account = Account;
            db.Save (entity);
            var serializedContent = JsonConvert.SerializeObject (cmd.Content);
            log.Info ("Saving serialized content to '{0}'.", entity.FilePath);
            entity.Save (serializedContent);
            var url = string.Concat (Context.Request.Url.BasePath.TrimEnd ("/".ToCharArray ()), "/", entity.Id);
            return Negotiate.WithCreated (url, entity.Id);
        }

        /// <summary>
        /// Retrieves a list of templates saved by the user.
        /// </summary>
        /// <param name="args">The query to filter the list.</param>
        /// <see cref="TaxDocumentTemplateQuery"/>
        /// <returns>200 - A list of the saved user templates (if any)</returns>
        public Negotiator TaxDocumentTemplateList (dynamic args) {
            var bound = this.BindTo (new TaxDocumentTemplateQuery ());
            var query = db.QueryOver<TaxDocumentTemplate> ()
                .Where (i => i.Account == Account);

            if (!string.IsNullOrWhiteSpace (bound.DocumentType)) {
                query.Where (i => i.DocumentType == bound.DocumentType);
            }

            if (!string.IsNullOrWhiteSpace (bound.Name)) {
                query.WhereRestrictionOn (i => i.Name)
                    .IsInsensitiveLike ("%" + bound.Name + "%");
            }

            TaxDocumentTemplateDto dto = null;
            var items = query
                .SelectList (i =>
                    i.Select (j => j.Id).WithAlias (() => dto.Id)
                    .Select (j => j.Name).WithAlias (() => dto.Name)
                    .Select (j => j.DocumentType).WithAlias (() => dto.DocumentType)
                    .Select (j => j.FilePath).WithAlias (() => dto.FilePath)
                    .Select (j => j.CreatedAt).WithAlias (() => dto.CreateAt))
                .OrderBy (Settings.OrderBy)
                .TransformToBean<TaxDocumentTemplate, TaxDocumentTemplateDto> ()
                .PaginateResults<TaxDocumentTemplate, TaxDocumentTemplateDto> (Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);

            log.Debug ("Including Json? {0}", bound.IncludeJson);
            if (!bound.IncludeJson) return Negotiate.WithOkAndList (items, Request.Url);

            var len = items.Items.Count;
            var list = new List<object> ();
            for (var index = 0; index < len; index++) {
                var item = items.Items[index];
                object taxDocumentData = null;
                if (!string.IsNullOrWhiteSpace (item.FilePath)) {
                    if (File.Exists (item.FilePath))
                        taxDocumentData = JsonConvert.DeserializeObject (File.ReadAllText (item.FilePath));
                }

                list.Add (new {
                    id = item.Id,
                        name = item.Name,
                        documentType = item.DocumentType,
                        createdAt = item.CreateAt,
                        taxDocumentData
                });
            }
            return Negotiate.WithOkAndList (list, Request.Url, items.ResultCount, items.CurrentPage, items.PageCount,
                items.ResultsPerPage, items.HasPrevious, items.HasNext);
        }

        /// <summary>
        /// Deletes a tax document template.
        /// </summary>
        /// <param name="cmd">The command containing the id of the template to delete</param>
        /// <returns>200 - either if the template doesn't exist or if it was correctly deleted.</returns>
        public Negotiator DeleteTaxDocumentTemplate (Command cmd) {
            log.Debug ("Trying to delete tax document template '{0}'", cmd.Id);
            var entity = db.QueryOver<TaxDocumentTemplate> ()
                .Where (i => i.Account.Id == Account.Id && i.Id == cmd.Id)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithOk ();
            }

            templateFilePath = entity.FilePath;
            deleteTemplateFile = true;
            db.Delete (entity);
            return Negotiate.WithOk ();
        }

        /// <summary>
        /// Validates that no duplicate template exist.
        /// </summary>
        /// <param name="args"><see cref="TaxDocumentTemplateQuery"/></param>
        /// <returns>200 - true if the name is available, false otherwise.</returns>
        public Negotiator CheckTemplateNameAvailability (dynamic args) {
            deleteTemplateFile = false;
            var bound = this.BindTo (new TaxDocumentTemplateQuery ());
            log.Debug ("Checking template name: {0}", bound);

            if (string.IsNullOrWhiteSpace (bound.Name)) {
                return Negotiate.WithOkAndModel (new {
                    isAvailable = false
                });
            }

            var entity = db.QueryOver<TaxDocumentTemplate> ()
                .Where (i => i.Account == Account && i.Name == bound.Name)
                .Take (1)
                .SingleOrDefault ();

            return Negotiate.WithOkAndModel (new {
                isAvailable = (entity == null)
            });
        }

        /// <summary>
        /// Retrieves a list of the tenants the current user can manage.
        /// </summary>
        /// <param name="args">The arguments to filter the list.</param>
        /// <see cref="TenantQuery"/>
        /// <returns>200 - the list of tenants this user can manage.</returns>
        public Negotiator TenantList (dynamic args) {
            Tenant tenant = null;
            TenantDto dto = null;
            var query = db.QueryOver<AuthorizedAccount> ()
                .Where (i => i.Account == Account)
                .Left.JoinAlias (i => i.Tenant, () => tenant)
                .SelectList (i => i
                    .Select (() => tenant.Id).WithAlias (() => dto.Id)
                    .Select (() => tenant.LegalName).WithAlias (() => dto.LegalName)
                    .Select (() => tenant.TaxId).WithAlias (() => dto.TaxId));

            var bound = this.BindTo (new TenantQuery ());
            if (!string.IsNullOrWhiteSpace (bound.TaxId)) {
                bound.TaxId = identity.CleanId (bound.TaxId);
                query.Where (() => tenant.TaxId == bound.TaxId);
            }

            if (!string.IsNullOrWhiteSpace (bound.LegalName)) {
                query.WhereRestrictionOn (() => tenant.LegalName)
                    .IsInsensitiveLike ("%" + bound.LegalName + "%");
            }

            var items = query
                .OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<TenantDto> ())
                .PaginateResults<AuthorizedAccount, TenantDto> (Settings.PaginationSettings.Page, Settings.PaginationSettings.ResultsPerPage);

            return Negotiate.WithOkAndList (items, Request.Url);
        }

        /// <summary>
        /// Retrieves the user roles a tenant has assigned to them.
        /// </summary>
        /// <returns>404 - if the user doesn't have any managed tenants.
        /// 200 - A list of roles for the specific tenant.</returns>
        public Negotiator GetCurrentRoles () {
            var entity = db.QueryOver<AuthorizedAccount> ()
                .Where (i => i.Account == Account && i.Tenant.Id == currentTenant.Id)
                .Take (1)
                .SingleOrDefault ();
            if (entity == null) {
                return Negotiate.WithNotFound (I18n.Error.NoManagedTenants);
            }
            log.Debug ("Retrieving roles for tenant {0}", currentTenant.Id);
            var accountRoles = db.QueryOver<AccountRole> ().List ();
            var roles = new Dictionary<string, List<object>> ();

            foreach (var role in accountRoles) {
                var list = new List<object> ();

                foreach (var permission in role.Role.RolePermissions) {
                    list.Add (new {
                        id = permission.Permission.Id,
                            name = permission.Permission.Name
                    });
                }

                if (!roles.ContainsKey (role.Role.Name)) {
                    roles.Add (role.Role.Name, list);
                } else {
                    roles[role.Role.Name].AddRange (list);
                }
            }
            return Negotiate.WithOkAndModel (roles);
        }

        /// <summary>
        /// Retrieves the list of address types for an invoice/bill.
        /// </summary>
        /// <returns>200 - A list of address types.</returns>
        public Negotiator GetAddressTypes () {
            var list = db.QueryOver<AddressType> ()
                .SelectList (i => i
                    .Select (j => j.Name)
                    .Select (j => j.Id))
                .List<object[]> ()
                .Select (i => new {
                    name = (string) i[0],
                        id = (Guid) i[1]
                });

            return Negotiate.WithOkAndModel (list);
        }

        private string GetFileName (string documentType, DateTime createdAt) {
            var path = Path.Combine (ConfigurationManager.AppSettings["zentom.root_storage"], currentTenant.TaxId, Account.Id.ToString ());
            if (!Directory.Exists (path)) {
                Directory.CreateDirectory (path);
            }
            var fname = string.Concat (documentType, "_", createdAt.ToString ("yyyy-MMMM-dd_HH-mm-ss"), ".json");
            templateFilePath = Path.Combine (path, fname);
            log.Debug ("Generated file path: {0}", templateFilePath);
            return templateFilePath;
        }

        private Response DeleteTemplateFileOnError (NancyContext context, Exception ex) {
            DeleteTemplateFile ();
            return null;
        }

        private void DeleteTemplateFileOnSuccess (NancyContext context) {
            DeleteTemplateFile ();
        }

        private void DeleteTemplateFile () {
            if (!deleteTemplateFile) return;
            if (string.IsNullOrWhiteSpace (templateFilePath)) {
                log.Warn ("The template file path was null or empty; nothing to delete.");
                return;
            }

            File.Delete (templateFilePath);

            if (File.Exists (templateFilePath)) {
                // TODO: create a system alert to delete the file.
                log.Error ("Failed to delete file '{0}'.", templateFilePath);
            }
        }
    }
}
