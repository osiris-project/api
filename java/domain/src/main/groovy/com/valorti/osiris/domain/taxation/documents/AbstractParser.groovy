/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.ValidationError
import com.valorti.osiris.core.taxation.documents.SectionParser

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
abstract class AbstractParser<T>
    implements SectionParser<T> {

    final List<ValidationError> errors

    /**
     * Default constructor
     */
    protected AbstractParser() {
        this.errors = []
    }
}
