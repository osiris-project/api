﻿using FluentNHibernate.Mapping;

namespace Zentom.Database.Maps.Tenant
{
    public class TenantMap
        : ClassMap<Core.Entities.Tenant>
    {
        public TenantMap()
        {
            Schema("organization");
            Table("tenant");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            HasMany(i => i.AccountingAccounts)
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id")
                .Inverse();

            HasMany(i => i.AccountingVouchers)
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id")
                .Inverse();

            HasMany(i => i.AuthorizedAccounts)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            Map(i => i.BankAccountNumber).Length(15).Default("''");
            Map(i => i.BillResolutionDate).Nullable();
            Map(i => i.BillResolutionNumber).Nullable();
            
            HasMany(i => i.CostsCenters)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            Map(i => i.DisplayName).Length(100);
            Map(i => i.Email).Length(1000);

            HasMany(i => i.Employees)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.Enterprises)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.FolioRanges)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.Groups)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tenant_id");

            Map(i => i.IsEnabled);
            Map(i => i.LegalName).Length(100).UniqueKey("tenant_un");
            Map(i => i.Phone).Length(50);

            HasMany(i => i.Products)
                .KeyColumn("tenant_id")
                .Inverse()
                .Cascade.AllDeleteOrphan();
            
            HasMany(i => i.ReceivedTaxDocuments)
                .Inverse()
                .KeyColumn("tenant_id")
                .Cascade.AllDeleteOrphan();

            HasMany(i => i.Roles)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.Services)
                .KeyColumn("tenant_id")
                .Inverse()
                .Cascade.AllDeleteOrphan();

            HasMany(i => i.Settings)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.TaxDocuments)
                .Inverse()
                .KeyColumn("tenant_id")
                .Cascade.AllDeleteOrphan();

            Map(i => i.TaxId).Length(20).UniqueKey("tenant_un");

            HasMany(i => i.TenantAddresses)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tenant_id");

            HasMany(i => i.TenantComercialActivities)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.TenantEconomicActivities)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");

            HasMany(i => i.Modules)
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyColumn("tenant_id");
        }
    }
}
