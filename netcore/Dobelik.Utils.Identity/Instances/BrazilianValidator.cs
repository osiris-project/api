﻿using System;
using Dobelik.Utils.Identity.Attributes;

namespace Dobelik.Utils.Identity.Instances
{
    [Descriptor("pt", "BR")]
    internal class BrazilianValidator
    {
        public object CalculateVerifierPart(string partialId)
        {
            throw new NotImplementedException();
        }

        public string CleanId(string id)
        {
            throw new NotImplementedException();
        }

        public string Normalize(string rawId)
        {
            throw new NotImplementedException();
        }

        public bool Validate(string id)
        {
            throw new NotImplementedException();
        }
    }
}
