﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class TaxDocumentTotalsMap
        : ClassMap<TaxDocumentTotals>
    {
        public TaxDocumentTotalsMap()
        {
            Schema("taxation");
            Table("tax_document_totals");

            Id(i => i.Id).Column("tax_document_id").Not.Nullable();
            Version(i => i.RowVersion).Column("row_version").Generated.Always();
            Map(i => i.NetAmount).Column("net_amount").Not.Nullable().Precision(13).Scale(4).Default("0.0000");
            Map(i => i.ExemptAmount).Column("exempt_amount").Not.Nullable().Precision(13).Scale(4).Default("0.0000");
            Map(i => i.VatAmount).Column("vat_amount").Not.Nullable().Precision(13).Scale(4).Default("0.0000");
            Map(i => i.TaxRate).Column("tax_rate").Not.Nullable().Precision(4).Scale(3).Default("0.000");
            Map(i => i.Total).Column("total").Not.Nullable().Precision(13).Scale(4).Default("0.0000");

            References<TaxDocument>(i => i.TaxDocument, "tax_document_id")
                .Unique()
                .Fetch.Join()
                .ForeignKey()
                .Cascade.All();
        }
    }
}
