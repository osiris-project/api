/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Embedded
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.MapsId
import javax.persistence.OneToOne

/**
 * Represents a client or provider's basic information.
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Embeddable
class EnterpriseData {
    @MapsId
    @OneToOne(mappedBy = 'document', fetch = FetchType.EAGER)
    @JoinColumn(name = 'id')
    Document document

    @Column(nullable = false, length = 20)
    String taxId

    @Column(nullable = false, length = 150)
    String name

    @Column(nullable = false, length = 50)
    String commercialActivity

    @Embedded
    Address legalAddress

    /**
     * Default constructor.
     */
    protected EnterpriseData() {
        this.taxId = ''
        this.name = ''
        this.commercialActivity = ''
        this.legalAddress = null
        this.document = null
    }

    /**
     * @param documentHeader
     * @param taxId
     * @param name
     * @param commercialActivity
     * @param legalAddress
     * @throws IllegalArgumentException If the taxId, name,
     * commercialActivity or legalAddress are null.
     * @since 0.0.1
     */
    EnterpriseData(String taxId, String name, String commercialActivity,
                   Address legalAddress) {
        if (!taxId.trim()) {
            throw new IllegalArgumentException('taxId')
        }
        if (!name.trim()) {
            throw new IllegalArgumentException('name')
        }
        if (!commercialActivity) {
            throw new IllegalArgumentException('commercialActivity')
        }
        if (!legalAddress) {
            throw new IllegalArgumentException('legalAddress')
        }
        this.taxId = taxId
        this.name = name
        this.commercialActivity = commercialActivity
        this.legalAddress = legalAddress
    }
}
