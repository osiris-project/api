﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class CostsCenterMap
        : ClassMap<CostsCenter>
    {
        public CostsCenterMap()
        {
            Schema("organization");
            Table("costs_center");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(24).UniqueKey("costs_center_un");
            HasMany(i => i.Enterprises)
                .KeyColumn("costs_center_id")
                .Inverse()
                .Cascade.SaveUpdate();
            Map(p => p.Description);
            Map(p => p.Name).Length(70).UniqueKey("costs_center_un");

            References(i => i.Tenant)
                .UniqueKey("costs_center_un")
                .Cascade.SaveUpdate();
        }
    }
}
