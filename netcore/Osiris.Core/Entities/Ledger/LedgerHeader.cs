﻿using System;

namespace Osiris.Core.Entities.Ledger
{
    public enum LedgerSubmissionType
    {
        Total, Partial, Final, Adjustment
    }

    public enum LedgerType
    {
        Monthly, Special
    }

    public enum LedgerOperation
    {
        Sales, Purchase
    }

    public class LedgerHeader
    {
        #region Fields

        private readonly string enterpriseTaxId;


        private readonly string authorizedEmployeeNationalId;

        /// <summary>
        /// 
        /// </summary>
        private readonly DateTime taxPeriod;

        /// <summary>
        /// 
        /// </summary>
        private readonly LedgerOperation ledgerOperation;

        /// <summary>
        /// 
        /// </summary>
        private readonly LedgerType type;

        /// <summary>
        /// Required.
        /// </summary>
        private readonly LedgerSubmissionType submissionType;

        /// <summary>
        /// Denotes the number of the segment if the submission type is partial.
        /// Optional.
        /// </summary>
        private readonly int segmentNumber;

        private readonly int notificationFolio;

        /// <summary>
        /// Required only if the current ledger will replace another.
        /// </summary>
        private readonly string amendCode;

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public LedgerOperation Operation
        {
            get { return ledgerOperation; }
        }

        public string EnterpriseTaxId
        {
            get { return enterpriseTaxId; }
        }

        public string AuthorizedEmployeeNationalId
        {
            get { return authorizedEmployeeNationalId; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime TaxPeriod
        {
            get { return taxPeriod; }
        }

        /// <summary>
        /// 
        /// </summary>
        public LedgerType Type1
        {
            get { return type; }
        }

        /// <summary>
        /// Required.
        /// </summary>
        public LedgerSubmissionType SubmissionType
        {
            get { return submissionType; }
        }

        /// <summary>
        /// Denotes the number of the segment if the submission type is partial.
        /// Optional.
        /// </summary>
        public int SegmentNumber
        {
            get { return segmentNumber; }
        }

        public int NotificationFolio
        {
            get { return notificationFolio; }
        }

        /// <summary>
        /// Required only if the current ledger will replace another.
        /// </summary>
        public string AmendCode
        {
            get { return amendCode; }
        }

        #endregion

        public LedgerHeader(string enterpriseTaxId, string authorizedEmployeeNationalId, DateTime taxPeriod,
            LedgerOperation ledgerOperation, LedgerType type, LedgerSubmissionType submissionType,
            int segmentNumber, int notificationFolio, string amendCode)
        {
            this.enterpriseTaxId = enterpriseTaxId;
            this.authorizedEmployeeNationalId = authorizedEmployeeNationalId;
            this.taxPeriod = taxPeriod;
            this.ledgerOperation = ledgerOperation;
            this.type = type;
            this.submissionType = submissionType;
            this.segmentNumber = segmentNumber;
            this.notificationFolio = notificationFolio;
            this.amendCode = amendCode;
        }
    }
}
