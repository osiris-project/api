﻿namespace Osiris.Core.Enums
{
    public enum LedgerOperation
    {
        Purchase,
        Sale
    }
}
