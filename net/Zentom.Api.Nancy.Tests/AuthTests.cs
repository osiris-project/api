﻿using System;
using System.Globalization;
using System.Linq;
using Dobelik.Logging;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace Zentom.Api.Nancy.Tests
{
    [TestClass]
    public class AuthTests
    {
        private Browser _browser;

        private Bootstrap _bootstrap;

        private const string javieraSpaPassword = "nomeacuerdo";

        [TestInitialize]
        public void Init()
        {
            XmlConfigurator.Configure();
            _bootstrap = new Bootstrap();
            _browser = new Browser(_bootstrap);
            var log = LoggerFactory.GetLogger("AuthTests");
            log.Debug("Initializing Authentication Tests");
        }

        [TestCleanup]
        public void End()
        {
            _browser = null;
            _bootstrap?.Dispose();
        }

        [TestMethod]
        public void StartTest()
        {
            var result = _browser.Get("/api", with =>
            {
                with.Header("Accept-Language", "es-cl");
                with.Accept("application/json");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void CreateRole()
        {
            const string roleName = "NoPriv";
            const string roleDisplayName = "Sin privilegios";
            var result = _browser.Post("/api/role", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.FormValue("displayName", roleDisplayName);
                with.FormValue("name", roleName);
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ListRoles()
        {
            var result = _browser.Get("/api/role.json", with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void GetRole()
        {
            var result = _browser.Get("/api/role/a8e81117-0b92-e511-80c6-080027018225", with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void CreateService()
        {
            const string name = "Faked Service 20";
            const decimal value = 100.39M;
            var result = _browser.Post("/api/service", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.FormValue("name", name);
                with.FormValue("unitPrice", value.ToString(new CultureInfo("es-CL")));
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
            Assert.IsNotNull(result.Headers["Location"]);
            result = _browser.Delete(result.Headers["Location"], with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ReadAccount()
        {
            var result = _browser.Get("/api/account/BFE81117-0B92-E511-80C6-080027018225", with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ReadService()
        {
            var result = _browser.Get("/api/service/F7C3E1B5-FB99-E511-80C6-080027018225.json", with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ReadServiceList()
        {
            var result = _browser.Get("/api/service.json", with => with.DefaultBrowserContext(_browser));
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void ChangePassword()
        {
            var result = _browser.Patch("/api/account/BFE81117-0B92-E511-80C6-080027018225/change-password", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.FormValue("password", "nomeacuerdo");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void CreateTestRoles()
        {
            var roles = new string[3][];
            roles[0] = new[]
            {
                "product_manager",
                "Administrador de Productos",
                "a0922a04-f199-e511-80c6-080027018225,a1922a04-f199-e511-80c6-080027018225,a2922a04-f199-e511-80c6-080027018225,a3922a04-f199-e511-80c6-080027018225,a4922a04-f199-e511-80c6-080027018225"
            };
            roles[1] = new[]
            {
                "service_manager", "Administrador de Servicios",
                "af922a04-f199-e511-80c6-080027018225,b0922a04-f199-e511-80c6-080027018225,b1922a04-f199-e511-80c6-080027018225,b2922a04-f199-e511-80c6-080027018225,b3922a04-f199-e511-80c6-080027018225"
            };
            roles[2] = new[]
            {
                "payer", "Pagador",
                "5a922a04-f199-e511-80c6-080027018225,5b922a04-f199-e511-80c6-080027018225,5c922a04-f199-e511-80c6-080027018225,5d922a04-f199-e511-80c6-080027018225,5e922a04-f199-e511-80c6-080027018225"
            };
            Log("Roles.length: {0}", roles.Length);
            for(var i = 0; i < roles.Length; i++)
            {
                var index = i;
                var result = _browser.Post("/api/role.json", with =>
                {
                    with.DefaultBrowserContext(_browser);
                    var split = roles[index][2].Split(",".ToCharArray());
                    Log("Split length: {0}", split.Length);
                    var list = split.ToList();
                    with.JsonBody(new
                    {
                        name = roles[index][0],
                        displayName = roles[index][1],
                        permissions = list
                    });
                });
                LogResponse(result);
                Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
            }
        }

        [TestMethod]
        public void CreateEmployeeAndAccount()
        {
            var result = _browser.Post("/api/employee/", with =>
            {
                with.DefaultBrowserContext(_browser);
                var employee = new
                {
                    firstName = "Juan",
                    lastName = "Perez",
                    nationalId = "94254353",
                    account = new
                    {
                        login = "94254353",
                        password = "nomeacuerdo",
                        email = "juan.perez@some.com",
                        isEnabled = true,
                        roles = new[]
                        {
                            "7eb4457c-7749-4c26-8405-a563011f087f",
                            "00802491-6ee2-4a1c-9ad9-a563011f08cb"
                        }
                    }
                };
                with.JsonBody(employee);
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }

        [TestMethod]
        public void OrderTesting()
        {
            var result = _browser.Get("/api/service.json", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.Query("page", "0");
                with.Query("rpp", "10");
                with.Query("orderBy", "unitPrice:asc,name:desc");
            });
            LogResponse(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void PermissionAttributeTest()
        {
            var result = _browser.Post("/api/employees", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.JsonBody(new
                {
                    firstName = "Aaaa",
                    lastName = "Bbbb",
                    nationalId = "1-9"
                });
            });
            Log("Executed:");
            LogResponse(result);
        }

        [TestMethod]
        public void ForceDuplicateExceptionTest()
        {
            var result = _browser.Post("/products/force-exception", with =>
            {
                with.DefaultBrowserContext(_browser);
                with.JsonBody(new
                {
                    name = "Producto Afecto 1",
                    unitPrice = 1000
                });
            });
            result.Log();
            Assert.AreEqual(result.StatusCode, HttpStatusCode.Conflict);
        }

        #region Utils

        private static void LogResponse(BrowserResponse response)
        {
            Log("Response: {0}", response.Body.AsString());
            Log("Headers:");
            foreach (var item in response.Context.Response.Headers)
            {
                Log("'{0}' => {1}", item.Key, item.Value);
            }
        }

        private static void Log(string msg, params object[] args)
        {
            Console.WriteLine(msg, args);
        }

        #endregion
    }
}
