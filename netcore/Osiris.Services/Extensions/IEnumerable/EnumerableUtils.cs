﻿using System.Linq;

namespace Osiris.Extensions.IEnumerable
{
    public static class EnumerableUtils
    {
        public static bool IsNullOrEmpty(this System.Collections.IEnumerable collection)
        {
            return collection == null || !collection.Cast<object>().Any();
        }

        public static bool IsNull(this System.Collections.IEnumerable collection)
        {
            return collection == null;
        }
    }
}
