﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class ServiceMap
        : SubclassMap<Service>
    {
        public ServiceMap()
        {
            Schema("organization");
            Table("service");
            Abstract();
        }
    }
}
