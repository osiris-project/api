﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Company
{
    public class CompanyAddressMap
        : SubclassMap<CompanyAddress>
    {
        public CompanyAddressMap()
        {
            Schema("organization");
        }
    }
}
