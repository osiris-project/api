/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.ValidationException
import groovy.util.slurpersupport.GPathResult
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.time.format.DateTimeFormatter

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DocumentReferenceParserTest
    extends BaseParserTests {

    private final Logger log = LoggerFactory.getLogger(getClass())

    @Test
    void test_parseECreditNoteReferenceSection() {
        log.debug('test_parseECreditNoteReferenceSection')
        def path = this.buildECreditNote()
        assert path != null

        def referencia = path.DTE.Documento.Referencia
        assert referencia != null

        def parser = new DefaultReferenceParser()

        def refs = []
        referencia.each {
            GPathResult it ->
                def ref = parser.parse(it)
                if (parser.errors) {
                    throw new ValidationException(parser.errors)
                }
                assert ref != null
                println ref
                assert ref.documentType == it.TpoDocRef.text()
                assert ref.folioNumber == it.FolioRef.text()
                XmlUtils.dateFromString(it.FchRef.text())
                assert ref.date.format(DateTimeFormatter.ofPattern(XmlUtils.dateFormat)) == it.FchRef.text()
                assert ref.type.value.toString() == it.CodRef.text()
                assert ref.reason == it.RazonRef.text()
                refs << ref
        }
        assert refs.size() == referencia.size()
    }
}
