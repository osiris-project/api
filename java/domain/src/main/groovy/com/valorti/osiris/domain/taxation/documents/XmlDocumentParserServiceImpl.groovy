/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.AbstractService
import com.valorti.osiris.core.location.LocationService
import com.valorti.osiris.core.taxation.EconomicActivity
import com.valorti.osiris.core.taxation.EconomicActivityRepository
import com.valorti.osiris.core.taxation.documents.DocumentParser
import com.valorti.osiris.core.taxation.documents.XmlDocumentParserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.springframework.stereotype.Service

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Service
class XmlDocumentParserServiceImpl
    extends AbstractService
    implements XmlDocumentParserService {

    private final Logger log = LoggerFactory.getLogger(XmlDocumentParserServiceImpl.class)

    private final DocumentParser documentParser

    XmlDocumentParserServiceImpl(EconomicActivityRepository economicActivityRepository,
                                 LocationService locationService,
                                 Marker marker) {
        def eaClosure = { String code ->
            def economicActivity = economicActivityRepository.findOneByCode(code)
            if (!economicActivity) {
                def uid = UUID.randomUUID() as String
                economicActivity = new EconomicActivity(uid + '_NF', code, true, EconomicActivity.TributaryCategory.PRIMARY, null)
                this.log.warn(marker, 'An economic activity was not found: {}', uid)
            }
            return economicActivity
        }
        def communeClosure = { String commune, String city ->
            locationService.retrieveCommune(commune, city)
        }
        this.documentParser = new DefaultDocumentParser(
            this.tenant,
            eaClosure,
            communeClosure
        )
    }

    XmlDocumentParserServiceImpl(DocumentParser documentParser) {
        this.documentParser = documentParser
    }
}
