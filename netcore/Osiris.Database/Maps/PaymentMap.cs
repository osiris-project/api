﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class PaymentMap
        : ClassMap<Payment>
    {
        public PaymentMap()
        {
            Schema("taxation");
            Table("payment");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Amount).Precision(13).Scale(4);
            Map(p => p.ExpireDate);

            References(i => i.PaymentType, "payment_type_id")
                .Cascade.SaveUpdate()
                .Unique();

            References(i => i.TaxDocument, "tax_document_id")
                .Cascade.SaveUpdate()
                .Unique();
        }
    }
}
