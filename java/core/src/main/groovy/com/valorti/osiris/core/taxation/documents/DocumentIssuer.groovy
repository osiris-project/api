/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.ValidationBuilder

import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.Table

import com.valorti.osiris.core.BigIntegerEntity
import com.valorti.osiris.core.ValidationException

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class DocumentIssuer
    extends BigIntegerEntity {
    @Embedded
    final EnterpriseData data

    @Column(nullable = false, length = 41)
    final String phones

    /**
     * The accepted email length is 254:
     * https://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
     */
    @Column(nullable = false, length = 254)
    final String email

    @Column(nullable = false, length = 27)
    private final String economicActivityCodes

    /**
     * Default constructor.
     */
    protected DocumentIssuer() {
    }

    /**
     * @param data
     * @param phones
     * @param email
     * @param economicActivityCodes
     * @throws IllegalArgumentException If the enterprise data is null, if the
     * @throws ValidationException
     */
    DocumentIssuer(EnterpriseData data, List<String> phones, String email, List<String> economicActivityCodes)
        throws IllegalArgumentException,
            ValidationException {
        if (!data) {
            throw new IllegalArgumentException('No enterprise data provided')
        }
        this.data = data
        this.phones = phones ? this.write(phones) : ''
        this.email = email || ''
        if (!economicActivityCodes) {
            throw new ValidationException([
                ValidationBuilder.empty('documentIssuer.economicActivityCodes')
            ])
        }
        this.economicActivityCodes = this.write(economicActivityCodes)
    }

    // Access

    List<String> getPhones() {
        return this.read(this.phones)
    }

    List<String> getEconomicActivityCodes() {
        return this.read(this.economicActivityCodes)
    }

    /**
     * Checks if the economic activity code exists
     * for this issuer.
     * @param code
     * @return true if it exists, false otherwise
     */
    boolean hasEconomicActivityCode(String code) {
        return this.getEconomicActivityCodes().contains(code)
    }

    // Utils
    private final List<String> read(CharSequence input) {
        return input.tokenize('|')
    }

    private final String write(List<String> input) {
        return input.join('|')
    }
}
