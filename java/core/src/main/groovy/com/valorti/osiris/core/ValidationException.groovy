/**
 * Copyright (c) Mar 7, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import groovy.transform.ToString

/**
 * Exception thrown when an entity property has one or
 * more validation errors.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@ToString(includeNames = true)
class ValidationException extends Exception {
    private static final long serialVersionUID = 1L

    final List<ValidationError> errors
    final Class clazz

    /**
     * Creates a validation exception with a list of errors.
     *
     * @param errors The validation errors.
     * @since 0.0.1
     */
    ValidationException(List<ValidationError> errors) {
        this.errors = errors
        this.clazz = null
    }
}
