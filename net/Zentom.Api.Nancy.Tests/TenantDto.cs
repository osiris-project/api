﻿using System;

namespace Zentom.Api.Nancy.Tests
{
    public class TenantDto
    {
        public Guid id { get; set; }

        public string legalName { get; set; }

        public string taxId { get; set; }
    }
}