﻿using System;

namespace Zentom.Core.Entities
{
    public class PasswordRequest
        : Entity
    {
        #region Fields

        private DateTime resetExpireDate;

        private DateTime requestDate;

        private string ipv4;

        private string ipv6;

        private Account account;

        #endregion

        #region Properties

        public virtual DateTime ResetExpireDate
        {
            get { return resetExpireDate; }
            set { resetExpireDate = value; }
        }

        public virtual DateTime RequestDate
        {
            get { return requestDate; }
            set { requestDate = value; }
        }

        public virtual string Ipv4
        {
            get { return ipv4; }
            set { ipv4 = value; }
        }

        public virtual string Ipv6
        {
            get { return ipv6; }
            set { ipv6 = value; }
        }

        public virtual Account Account
        {
            get { return account; }
            set { account = value; }
        }

        #endregion

        public PasswordRequest()
        {
        }

        public PasswordRequest(DateTime resetExpireDate, DateTime requestDate, string ipv4, string ipv6, Account account)
        {
            this.resetExpireDate = resetExpireDate;
            this.requestDate = requestDate;
            this.ipv4 = ipv4;
            this.ipv6 = ipv6;
            this.account = account;
        }
    }
}