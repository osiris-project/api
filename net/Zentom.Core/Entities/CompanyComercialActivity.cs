﻿namespace Zentom.Core.Entities
{
    public abstract class CompanyComercialActivity
        : CompanyEntity
    {
        #region Fields
        
        private ComercialActivity comercialActivity;

        #endregion

        #region Properties

        public virtual ComercialActivity ComercialActivity
        {
            get { return comercialActivity; }
            set { comercialActivity = value; }
        }

        #endregion

        protected CompanyComercialActivity()
        {
        }

        protected CompanyComercialActivity(Company enterprise, ComercialActivity comercialActivity)
            : base(enterprise)
        {
            this.comercialActivity = comercialActivity;
        }
    }
}
