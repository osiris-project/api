﻿using System.Configuration;
using System.IO;
using Dobelik.Logging;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace Zentom.Api.Nancy.Tests
{
    [TestClass]
    public class UploadTests
    {
        private Browser browser;

        private Bootstrap bootstrap;

        [TestInitialize]
        public void Init()
        {
            XmlConfigurator.Configure();
            bootstrap = new Bootstrap();
            browser = new Browser(bootstrap);
            var log = LoggerFactory.GetLogger("AuthTests");
            log.Debug("Initializing Authentication Tests");
        }

        [TestCleanup]
        public void End()
        {
            browser = null;
            bootstrap?.Dispose();
        }

        [TestMethod]
        public void UploadEBill()
        {
            var stream = new FileStream(@"D:\761039156_231252_33.xml", FileMode.Open);
            var multipart = new BrowserContextMultipartFormData(i =>
            {
                i.AddFile("761039156_231252_33", "761039156_231252_33.xml", "text/xml", stream);
            });

            var result = browser.Post("/received-tax-documents", with =>
            {
                with.DefaultBrowserContext(browser);
                with.MultiPartFormData(multipart);
            });

            result.Log();
            Assert.AreEqual(result.StatusCode, HttpStatusCode.OK);
        }

        [TestMethod]
        public void UploadECreditNote()
        {
            var stream = new FileStream(@"D:\761039156_2262_61.xml", FileMode.Open);
            var multipart = new BrowserContextMultipartFormData(i =>
            {
                i.AddFile("761039156_231252_33", "761039156_231252_33.xml", "text/xml", stream);
            });

            var result = browser.Post("/received-tax-documents", with =>
            {
                with.DefaultBrowserContext(browser);
                with.MultiPartFormData(multipart);
            });

            result.Log();
            Assert.AreEqual(result.StatusCode, HttpStatusCode.OK);
        }

        [TestMethod]
        public void BulkTaxDocumentTest()
        {
            var stream = new FileStream(@"D:\format.xlsx", FileMode.Open);
            var multipart = new BrowserContextMultipartFormData(i =>
            {
                i.AddFile("format", "format.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", stream);
            });

            var result = browser.Post("/resources/file-processors/bulk-import-tax-documents/", with =>
            {
                with.DefaultBrowserContext(browser);
                with.MultiPartFormData(multipart);
            });

            result.Log();
            Assert.AreEqual(result.StatusCode, HttpStatusCode.OK);
        }
    }
}
