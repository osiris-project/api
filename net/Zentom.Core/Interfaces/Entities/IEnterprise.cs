﻿using System;
using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterprise
        : IUnique<long>, IVersioned, IRowGuid
    {
        string TaxId { get; set; }

        string DisplayName { get; set; }

        string LegalName { get; set; }

        string BankAccountNumber { get; set; }

        DateTime? BillResolutionDate { get; set; }

        int? BillResolutionNumber { get; set; }

        ICollection<IEnterpriseStaff> EnterpriseStaff { get; set; }

        ICollection<IEnterpriseAddress> EnterpriseAddresses { get; set; }

        ICollection<IEnterpriseContactData> EnterpriseContactData { get; set; }

        ICollection<IEnterpriseSector> EnterpriseSectors { get; set; }

        ICollection<ITaxDocument> GeneratedDocuments { get; set; }

        ICollection<ITaxDocument> ReceivedDocuments { get; set; }

        ICollection<IClient> Providers { get; set; }

        ICollection<IClient> Clients { get; set; }

        ICollection<IEnterpriseProduct> EnterpriseProducts { get; set; }

        ICollection<IEnterpriseService> EnterpriseServices { get; set; }

        ICollection<ITaxDocumentFolio> TaxDocumentFolios { get; set; }
    }
}
