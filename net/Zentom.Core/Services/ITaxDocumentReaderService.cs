﻿using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Core.Services
{
    public interface ITaxDocumentReaderService
    {
        TaxDocument ReadDocument(string xmlFile);
    }
}