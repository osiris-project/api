using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Osiris.Core.Services {
    public class DefaultOperationResult<T, E> : IOperationResult<T, E> {

        private IList<E> errors;

        private T result;

        public DefaultOperationResult (T result, IList<E> errors) {
            this.result = result;
            this.errors = errors;
        }

        public DefaultOperationResult (T result) {
            this.result = result;
            this.errors = new Collection<E> ();
        }

        public DefaultOperationResult (IList<E> errors) {
            this.result = default(T);
            this.errors = errors;
        }

        public T Result {
            get {
                return result;
            }
        }

        public IReadOnlyCollection<E> Errors {
            get {
                return new ReadOnlyCollection<E>(this.errors);
            }
        }

        public bool HasErrors {
            get {
                if (this.errors == null) return false;
                return this.errors.Count > 0;
            }
        }
    }
}
