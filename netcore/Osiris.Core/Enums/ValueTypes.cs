﻿namespace Osiris.Core.Enums
{
    public enum ValueTypes
    {
        String,
        Integer,
        Decimal,
        Bool
    }
}
