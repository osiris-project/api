/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

import org.hibernate.annotations.GenericGenerator

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 *
 */
@MappedSuperclass
abstract class UUIDEntity
extends AbstractEntity<UUID> {
    @Transient
    private static final long serialVersionUID = 1L

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)", nullable = false)
    private UUID id

    /**
     * Retrieves this entity's id.
     * @since 0.0.1
     */
    @Override
    UUID getId() {
        return this.id
    }
}
