﻿using System;
using System.Collections.Generic;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Core.Interfaces.Integration.TributaryDocuments
{
    public interface ITributaryDocumentParser<out TEntity>
        : IDisposable
    {
        /// <summary>
        /// List of errors that ocurred in the proccess.
        /// </summary>
        IList<IError> Errors { get; }

        /// <summary>
        /// Parses the document specified by the path.
        /// </summary>
        /// <param name="path">Absolute path to the file to be parsed.</param>
        /// <returns></returns>
        TEntity Parse(string path);
    }
}
