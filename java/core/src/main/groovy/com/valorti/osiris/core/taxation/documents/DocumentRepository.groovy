/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.BigIntegerRepository

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface DocumentRepository
    extends BigIntegerRepository<Document> {
}
