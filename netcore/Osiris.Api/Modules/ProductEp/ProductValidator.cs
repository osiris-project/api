﻿using FluentValidation;
using Osiris.Core.Entities;

namespace Osiris.Api.Modules.ProductEp
{
    public class ProductValidator
        : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(i => i.Name).Length(3, 100);
            RuleFor(i => i.UnitPrice).GreaterThan(0);
        }
    }
}
