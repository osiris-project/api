﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class AddressTypeMap
        : ClassMap<AddressType>
    {
        public AddressTypeMap()
        {
            Schema("organization");
            Table("address_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            Map(i => i.Name).Length(100).UniqueKey("address_type_un");
            Map(i => i.Description).Length(1000).Default("''");
        }
    }
}
