﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Database.Maps.TaxDocument
{
    public class TaxDocumentLineMap
        : ClassMap<TaxDocumentLine>
    {
        public TaxDocumentLineMap()
        {
            Schema("taxation");
            Table("tax_document_line");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.LineNumber).UniqueKey("tax_document_line_un");
            Map(i => i.Text).Length(80);
            Map(i => i.UnitPrice).Precision(13).Scale(4).Default("0.0000");
            Map(i => i.Quantity).Precision(13).Scale(4).Default("1.0000");
            Map(i => i.AdditionalGloss).Length(1000).Default("\"\"");
            Map(i => i.MeasureUnit).Length(30).Default("\"\"");
            Map(i => i.TaxAmount).Precision(13).Scale(4);
            Map(i => i.ExtraTaxAmount).Precision(13).Scale(4);
            Map(i => i.LineTotal).Precision(13).Scale(4);
            Map(i => i.NetAmount).Not.Insert().Not.Update().Formula("quantity * unit_price");
            Map(i => i.TaxType).CustomType(typeof(TaxType));
            Map(i => i.TotalTaxAmount).Not.Insert().Not.Update().Formula("tax_amount + extra_tax_amount");

            References(i => i.TaxDocument, "tax_document_id")
                .UniqueKey("tax_document_line_un")
                .Cascade.None();
        }
    }
}
