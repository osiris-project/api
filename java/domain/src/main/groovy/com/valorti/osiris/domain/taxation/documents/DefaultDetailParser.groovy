/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.taxation.documents.DocumentDetail
import groovy.util.slurpersupport.GPathResult
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import static com.valorti.osiris.domain.taxation.documents.XmlUtils.dateFromString

import java.time.LocalDate

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DefaultDetailParser
    extends AbstractParser<DocumentDetail> {

    private final Logger log = LoggerFactory.getLogger(getClass())

    @Override
    DocumentDetail parse(GPathResult path) {
        this.errors.clear()
        String name = path.NmbItem?.text()?.trim()
        String liq = path.TpoDocLiq?.text()?.trim()
        def modifier = DocumentDetail.ItemModifier.fromInt(path.IndExe.isEmpty() ? 0 : path.IndExe.toInteger())
        String description = path.DscItem?.text()?.trim()
        Double refQty = !path.QtyRef.isEmpty() ? path.Qty.Ref.toDouble() : 0
        String refUnit = path.UnmdRef?.text()?.trim()
        Double refPrice = !path.PrcRef.isEmpty() ? path.PrcRef.toDouble() : 0
        Double quantity = !path.QtyItem.isEmpty() ? path.QtyItem.toDouble() : 0
        LocalDate manufDate = dateFromString(path.FchElabor?.text()?.trim())
        LocalDate expiryDate = dateFromString(path.FchVencim?.text()?.trim())
        String measureUnit = path.UnmdItem?.text()?.trim()
        Double unitPrice = !path.PrcItem.isEmpty() ? path.PrcItem.toDouble() : 0
        Double discountPercent = !path.DescuentoPct.isEmpty() ? path.DescuentoPct.toDouble() : 0
        Double discountAmount = !path.DescuentoMonto.isEmpty() ? path.DescuentoMonto.toDouble() : 0
        Double surchargePercent = !path.RecargoPct.isEmpty() ? path.RecargoPct.toDouble() : 0
        Double surchargeAmount = !path.RecargoMonto.isEmpty() ? path.RecargoMonto.toDouble() : 0
        Double total = !path.MontoItem.isEmpty() ? path.MontoItem.toDouble() : 0
        List<String> codes = []
        if (path.CodImpAdic) {
            path.CodImpAdic.each { taxCode ->
                codes << taxCode?.text()?.trim()
            }
        }
        try {
            def documentDetail = new DocumentDetail(liq, modifier, name, description, refQty, refUnit,
                refPrice, quantity, manufDate, expiryDate, measureUnit, unitPrice, discountPercent,
                discountAmount, surchargePercent, surchargeAmount, codes)
            if (documentDetail.lineTotal != total) {
                this.log.error('''The created document detail total and the total on the document doesn\'t 
match: [lineTotal={}, total={}]''',
                    documentDetail.lineTotal, total)
            }
            return documentDetail
        } catch (ValidationException ex) {
            this.log.error('Validation failed for a document detail. ', ex)
            this.errors.addAll(ex.errors)
        }
        return null
    }
}
