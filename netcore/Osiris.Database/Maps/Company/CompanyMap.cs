﻿using FluentNHibernate.Mapping;

namespace Osiris.Database.Maps.Company
{
    public class CompanyMap
        : ClassMap<Core.Entities.Company>
    {
        public CompanyMap()
        {
            UseUnionSubclassForInheritanceMapping();
            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.DisplayName).Length(100);
            Map(i => i.Email).Length(1000);
            Map(i => i.Phone).Length(50);
            Map(i => i.BankAccountNumber).Length(15).Default("''");
        }
    }
}
