﻿using FluentValidation.Validators;
using log4net;

namespace Zentom.Api.Modules.Validators.Guid
{
    public class GuidValidator
        : PropertyValidator
    {
        public GuidValidator()
            : base("InvalidId", typeof(I18n.Error))
        {
        }

        #region Overrides of PropertyValidator

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var log = LogManager.GetLogger(typeof (GuidValidator));
            log.DebugFormat("property value: {0}", context.PropertyValue);
            var val = context.PropertyValue.ToString();
            System.Guid guid;
            System.Guid.TryParse(val, out guid);
            log.DebugFormat("parsed guid: {0}", guid);
            return guid != System.Guid.Empty;
        }

        #endregion
    }
}