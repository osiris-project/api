﻿namespace Zentom.Api.Modules.EmployeeEp
{
    public class EmployeeDto
        : Command
    {
        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string NationalId { get; set; }
    }
}