﻿using System;
using System.Collections.Generic;
using System.IO;
using Zentom.Core.Entities;
using Zentom.Core.Enums;
using Zentom.Core.Interfaces.Integration.TributaryDocuments;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Integration.TributaryDocuments.Artikos
{
    public abstract class AbstractLedger
        : ITributaryDocumentFormatter<TaxDocument>
    {
        protected StreamWriter streamWriter;

        public AbstractLedger()
        {
            Validate();
        }

        public string DateFormat { get; protected set; }

        public IList<IModelValidation> Errors  { get; protected set; }

        public bool IsValid
        {
            get
            {
                return (Errors != null && Errors.Count > 0);
            }
        }

        public virtual void Format(StreamWriter streamWriter, TaxDocument entity)
        {
            this.streamWriter = streamWriter;
        }

        protected abstract void SetSuggestedFileName();

        protected abstract void Validate();

        public void Dispose()
        {
            Errors.Clear();
        }

        public class LedgerCover
        {
            private string emitterId;

            private string authorizedUserId;

            private DateTime periodTax;

            private DateTime resolutionDate;

            private int resolutionNumber;

            private LedgerOperation ledgerOperation;

            public LedgerCover(string emitterId, string authorizedUserId, DateTime periodTax,
                DateTime resolutionDate, int resolutionNumber,
                LedgerOperation ledgerOperation, LedgerType ledgerType,
                LedgerFormat ledgerFormat, int segmentNumber, long notificationFolio)
            {

            }
        }
    }
}
