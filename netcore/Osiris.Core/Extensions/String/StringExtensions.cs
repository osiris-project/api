﻿using System;
using System.Text;

namespace Osiris.Core.Extensions.String
{
    public static class StringExtensions
    {
        private static readonly Encoding AsciiEncoding = Encoding.GetEncoding("us-ascii",
            new EncoderReplacementFallback(string.Empty),
            new DecoderExceptionFallback());

        private static readonly Encoding Iso88591 = Encoding.GetEncoding("ISO-8859-1");
        private static readonly Encoding Utf8 = Encoding.UTF8;

        public static string ToAscii(this string input)
        {
            var bytes = AsciiEncoding.GetBytes(input);
            return AsciiEncoding.GetString(bytes);
        }

        public static string ToIso88591(this string input)
        {
            var utf8Bytes = Utf8.GetBytes(input);
            var isoBytes = Encoding.Convert(Utf8, Iso88591, utf8Bytes);

            return Iso88591.GetString(isoBytes);
        }

        /// <summary>
        /// Splits a string by its separator and converts every item to long, 
        /// removing empty items and sorting the result (ascending or increasing).
        /// </summary>
        /// <param name="input">The string to split.</param>
        /// <param name="separator">The character used to split.</param>
        /// <returns>A long[] sorted or an empty long[].</returns>
        public static long[] LongSplit(this string input, char[] separator)
        {
            var r = new long[] { };

            if (input == null) return r;

            var split = input.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            r = new long[split.Length];

            for(var i = 0; i < split.Length; i++)
            {
                long l;

                if (long.TryParse(split[i], out l))
                    r[i] = l;
            }

            Array.Sort(r);

            return r;
        }
    }
}
