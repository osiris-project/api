/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import com.dobelik.utils.nationalId.IdentityFactory
import com.dobelik.utils.nationalId.NationalIdentity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationError
import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.organization.Tenant
import com.valorti.osiris.core.taxation.documents.*
import groovy.util.slurpersupport.GPathResult
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DefaultDocumentParser
    extends AbstractDocumentParser {

    private final Logger log = LoggerFactory.getLogger(getClass())

    SectionParser<DocumentDetail> detailParser

    SectionParser<DocumentReference> referenceParser

    SectionParser<DocumentIssuer> issuerParser

    SectionParser<DocumentReceiver> receptorParser

    private List<ValidationError> errors

    final Tenant tenant

    private final NationalIdentity identity = new IdentityFactory().GetInstance('CL')

    /**
     *
     * @param tenant
     * @param detailParser
     * @param referenceParser
     * @param issuerParser
     * @param receptorParser
     * @throws IllegalArgumentException If the tenant is null
     */
    private DefaultDocumentParser(Tenant tenant,
                                  SectionParser detailParser,
                                  SectionParser referenceParser,
                                  SectionParser issuerParser,
                                  SectionParser receptorParser)
        throws IllegalArgumentException {
        if (!tenant) {
            throw new IllegalArgumentException('No tenant provided')
        }
        this.tenant = tenant
        if (!detailParser) {
            detailParser = new DefaultDetailParser()
        }
        if (!referenceParser) {
            referenceParser = new DefaultReferenceParser()
        }
        if (!issuerParser) {
            issuerParser = new DefaultDocumentIssuerParser(this.identity)
        }
        if (!receptorParser) {
            receptorParser = new DefaultDocumentReceptorParser(this.identity)
        }
        this.detailParser = detailParser
        this.referenceParser = referenceParser
        this.issuerParser = issuerParser
        this.receptorParser = receptorParser
    }

    private DefaultDocumentParser(Tenant tenant) {
        this(tenant, null, null, null, null)
    }

    DefaultDocumentParser(Tenant tenant,
                          SectionParser<DocumentDetail> detailParser,
                          SectionParser<DocumentReference> referenceParser,
                          SectionParser<DocumentIssuer> issuerParser,
                          SectionParser<DocumentReceiver> receptorParser) {
        this(tenant, detailParser, referenceParser, issuerParser, receptorParser)
    }

    Document parse(String filePath)
        throws ValidationException, UnexpectedReceiverException {
        return parseDocument(new XmlSlurper().parse(filePath))
    }

    Document parse(File file)
        throws ValidationException, UnexpectedReceiverException {
        return parseDocument(new XmlSlurper().parse(file))
    }

    private Document parseDocument(GPathResult path)
        throws ValidationException {
        this.errors = []
        if (!this.validateRequiredSections(path)) {
            throw new ValidationException(this.errors)
        }
        def document = this.parseDocumentSection(path.Documento.Encabezado)
        // End execution early if the document is null, since we cannot continue.
        if (!document) {
            throw new ValidationException(this.errors)
        }
        this.parseDetails(path.DTE.Documento, document)
        this.parseReferences(path.DTE.Documento, document)

        // This must be the last statement.
        if (this.errors) {
            throw new ValidationException(this.errors)
        }
        return document
    }

    private Document parseDocumentSection(GPathResult path) {
        def idDoc = path.IdDoc as GPathResult
        def docType = Document.DocumentType.fromInt(idDoc.TipoDTE.text() ? idDoc.TipoDTE.toInteger() : 0)
        if (!docType) {
            this.errors << ValidationBuilder.build(idDoc.TipoDTE.text() as String,
                'El tipo de documento no es válido o no se reconoce')
        }
        BigInteger folio = (idDoc.Folio.text() ? idDoc.toBigInteger() : 0)
        if (!folio) {
            this.errors << ValidationBuilder.build('', 'El folio del documento no existe o no es un número válido')
        }
        def accountingDate = XmlUtils.dateFromString(idDoc.FchEmision.text() ? idDoc.FchEmision.text().trim() : '')
        if (!accountingDate) {
            this.errors << ValidationBuilder.build('', 'La fecha de emisión no es válida')
        }
        def expirationDate = XmlUtils.dateFromString(idDoc.FchVenc?.text() ? '' : idDoc.FchVenc.text().trim())
        def isGrossAmount = (idDoc.MntBruto.text() ? idDoc.MntBruto.text() == '1' : null)
        def discountAllowed = (idDoc.IndNoRebaja.text() ? idDoc.text() == '1' : null)
        def dispatchType = Document.DispatchType.fromInt(idDoc.TipoDespacho.text() ? idDoc.TipoDespacho.toInteger() : 0)
        def serviceType = Document.ServiceType.fromInt(idDoc.IndServicio.text() ? idDoc.IndServicio.toInteger() : 0)
        def paymentType = Document.PaymentType.fromInt(idDoc.FmaPago.text() ? Short.parseShort(idDoc.text()) : 0)
        def principalTaxId = this.identity.clean(path.RUTMandante.text())
        def requesterTaxId = this.identity.clean(path.RUTSolicita.text())
        def issuer = this.parseIssuer(path.Emisor)
        def document = new Document(docType, folio, accountingDate, expirationDate, isGrossAmount,
            discountAllowed, dispatchType, serviceType, paymentType, principalTaxId, requesterTaxId,
            issuer,
            this.tenant)
        this.parseReceiver(path.Receptor, document)
        return document
    }

    private DocumentIssuer parseIssuer(GPathResult path) {
        def issuer = this.issuerParser.parse(path)
        if (!issuer) {
            this.log.error('Failed to parse the document issuer section!')
            this.errors.addAll(this.issuerParser.errors)
        }
        // TODO: this is wrong. The issuer must be checked by the caller.
        //this.checkStoredIssuer(issuer)
        return issuer
    }

    private void parseReceiver(GPathResult path, Document document) {
        def receiver = this.receptorParser.parse(path)
        if (receiver) {
            document.receiver = receiver
            if (this.tenant.taxId != document.receiver.data.taxId) {
                throw new UnexpectedReceiverException(this.tenant.taxId, document.receiver.data.taxId)
            }
        } else {
            this.log.error('No receiver parsed!')
            this.errors.addAll(this.receptorParser.errors)
        }
    }

    private void parseDetails(GPathResult path, Document document) {
        this.log.debug('Detail line count: {}', path.Detalle?.size())
        if (!path.Detalle) {
            this.log.error('No detail lines to parse. This shouldn\'t have triggered')
        }
        def count = 0
        def details = []
        path.Detalle.each {
            def detail = this.detailParser.parse(it)
            if (this.detailParser.errors) {
                this.errors.addAll(this.detailParser.errors)
            } else {
                if (detail) {
                    detail.document = document
                    details << detail
                }
            }
            count++
        }
        document.details = details
        this.log.info('{} lines of detail parsed.', details.size())
    }

    private void parseReferences(GPathResult path, Document document) {
        this.log.debug('Reference count: {}', path.Referencia?.size())
        if (!path.Referencia) {
            this.log.info('No references to parse.')
            return
        }
        def refs = []
        path.Referencia.each {
            def ref = this.referenceParser.parse(path)
            if (this.referenceParser.errors) {
                this.errors.addAll(this.referenceParser.errors)
            } else {
                if (ref) {
                    ref.document = document
                    refs << ref
                }
            }
        }
        document.references = refs
        this.log.info('{} lines of references parsed.', refs.size())
    }

    private boolean validateRequiredSections(GPathResult path) {
        if (!path.DTE) {
            this.errors << ValidationBuilder.build('', 'Falta la sección \'DTE\'')
            return false
        }
        if (!path.DTE.Documento) {
            this.errors << ValidationBuilder.build('', 'Falta la sección \'Documento\'')
            return false
        }
        if (!path.DTE.Documento.Encabezado) {
            this.errors << ValidationBuilder.build('', 'Falta la sección \'Encabezado\'')
            return false
        }
        if (!path.DTE.Documento.Encabezado.IdDoc) {
            this.errors << ValidationBuilder.build('', 'Falta la sección \'IdDoc\'')
            return false
        }
        if (!path.DTE.Documento.Encabezado.Emisor) {
            this.errors << ValidationBuilder.build('',
                'La sección \'Emisor\' no está presente')
            return false
        }
        if (!path.DTE.Documento.Encabezado.Receptor) {
            this.errors << ValidationBuilder.build('',
                'Falta la sección \'Receptor\'')
            return false
        }
        if (!path.DTE.Documento.Encabezado.Totales) {
            this.errors << ValidationBuilder.build('',
                'Falta la sección \'Totales\'')
            return false
        }
        if (!path.DTE.Documento.Detalle) {
            this.errors << ValidationBuilder.build('',
                "No hay ningún item de detalle")
            return false
        }
        return true
    }

    /*private void checkStoredIssuer(DocumentIssuer issuer) {
        Enterprise enterprise
        try {
            enterprise = this.tenant.addEnterprise(issuer.data.taxId, issuer.data.name, issuer.data.name)
        } catch (IllegalArgumentException ex) {
            this.log.error('', ex)
            return
        } catch (ValidationException ex) {
            this.log.error('Validation failed for document issuer')
            this.errors.addAll(ex.errors)
            return
        } catch (DuplicateEntityException ex) {
            this.log.info('Duplicate entity found: {}', ex.clazz)
            enterprise = this.tenant.enterprises.find {
                return it.taxId == issuer.data.taxId
            }
        }
        def commune = this.readCommune(issuer.data.legalAddress.commune, issuer.data.legalAddress.city)
        enterprise.addAddress(issuer.data.legalAddress.line, '', commune, CompanyAddress.AddressType.PRIMARY)
        this.addIssuerEconomicActivities(issuer.economicActivityCodes, enterprise)
    }

    private Commune readCommune(String name, String cityName) {
        return this.communeClosure(name, cityName)
    }

    private void addIssuerEconomicActivities(List<String> activities, Enterprise enterprise) {
        activities.each {
            String code -> enterprise.addEconomicActivity(this.economicActivityClosure(code))
        }
    }*/
}
