﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Database.Maps.TaxDocument
{
    public class TaxDocumentClientAddressTypeMap
        : ClassMap<TaxDocumentClientAddressType>
    {
        public TaxDocumentClientAddressTypeMap()
        {
            Schema("taxation");
            Table("taxdoc_client_address_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Address, "taxdoc_client_address_id")
                .Cascade.All()
                .UniqueKey("taxdoc_client_address_type_un");

            References(i => i.AddressType, "address_type_id")
                .Cascade.None()
                .UniqueKey("taxdoc_client_address_type_un");
        }
    }
}