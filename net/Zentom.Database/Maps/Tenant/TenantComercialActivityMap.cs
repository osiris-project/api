﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Tenant
{
    public class TenantComercialActivityMap
        : ClassMap<TenantComercialActivity>
    {
        public TenantComercialActivityMap()
        {
            Schema("taxation");
            Table("tenant_comercial_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("tenant_comercial_activity_un");

            References(i => i.ComercialActivity, "comercial_activity_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("tenant_comercial_activity_un");
        }
    }
}