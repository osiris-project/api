﻿namespace Osiris.Api.Modules.EmployeeEp
{
    public class EmployeeQuery
    {
        public string LastName { get; set; }

        public string FullName { get; set; }

        public string NationalId { get; set; }

        public override string ToString()
        {
            return $"LastName: {LastName}, FullName: {FullName}, NationalId: {NationalId}";
        }
    }
}
