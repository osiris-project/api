﻿using System;
using System.Linq;
using Dobelik.Utils.Identity;
using log4net;
using Zentom.Core.Enums;
using Zentom.I18n;

namespace Zentom.Integration.Artikos.TributaryDocuments.Artikos
{
    internal class DebitNote
        : AbstractArtikosFormatter
    {
        #region Fields

        private static readonly ILog Log = LogManager.GetLogger(typeof(DebitNote));

        protected CorrectionCode CorrectionCode;

        #endregion

        #region Constructors

        public DebitNote(INationalIdentity idUtils)
            : base(idUtils)
        { }

        #endregion

        protected override void Setup()
        {
            base.Setup();
            CorrectionCode = entity.CorrectionCode;
        }

        protected override void Validate()
        {
            if (entity.TaxDocumentLines == null || entity?.TaxDocumentLines?.Count <= 0)
            {
                Errors.Add(new ModelValidation("Bill.NoDetail", Error.NoDetails));
            }

            if (entity.ReferencedDocuments == null || entity.ReferencedDocuments.Count <= 0)
            {
                Errors.Add(new ModelValidation("ReferencedDocuments", Error.NoReferencedDocuments));
            }

            if (CorrectionCode == CorrectionCode.AmendText)
            {
                Errors.Add(new ModelValidation("CorrectionCode", Error.InvalidCorrectionCode));
            }
            else
            {
                if (CorrectionCode == CorrectionCode.VoidDocument
                    && !OnlyReferencesCreditNotes())
                {
                    Errors.Add(new ModelValidation("ReferencedDocuments", Error.InvalidDebitNoteVoidableDocument));
                }
            }
        }

        protected override void WriteDocumentInfo()
        {
            Log.DebugFormat("- Document Info. -");
            var tmp = "";
            StreamWriter.Write("0100");

            // Punto de emisión
            StreamWriter.Write(CorrectString(entity.EmissionPoint, 10));
            Log.DebugFormat("User: {0}", entity.EmissionPoint);

            // Bill type Code
            StreamWriter.Write(CorrectString(entity.DocumentType, 3));
            Log.DebugFormat("Bill type Code:  {0}", entity.DocumentType);

            // Número de folio
            StreamWriter.Write(CorrectNumber(entity.FolioNumber.ToString(), 10));
            Log.DebugFormat("Folio number:  {0}", entity.FolioNumber);

            // Número de documento interno
            StreamWriter.Write(CorrectString(entity.DocumentNumber.ToString(), 15));
            Log.DebugFormat("Document number:  {0}", entity.DocumentNumber);

            // Fecha de emisión contable
            StreamWriter.Write(entity.AccountingDate.ToString(DateFormat));
            Log.DebugFormat("Date issued:  {0}", entity.AccountingDate.ToString(DateFormat));

            // Tipo de servicio
            StreamWriter.Write(serviceType);
            Log.DebugFormat("Service type Code:  {0}", serviceType);

            // Indicador de monto neto (sólo para boletas)
            if (entity.DocumentType == "39")
            {
                StreamWriter.Write("1");
                Log.DebugFormat("Net amount: 1");
            }
            else if (entity.DocumentType == "41")
            {
                StreamWriter.Write("0");
                Log.DebugFormat("Net amount: 0");
            }
            else
            {
                StreamWriter.Write(" ");
                Log.DebugFormat("Net amount: empty");
            }

            // Fecha de vencimiento del pago
            if (entity.ExpireDate != null)
            {
                StreamWriter.Write(entity.ExpireDate.Value.ToString(DateFormat));
                Log.DebugFormat("Payment expiration date:  {0}", entity.ExpireDate.Value.ToString(DateFormat));
            }
            else
            {
                StreamWriter.Write(GetSpaces(10));
            }


            // Tipo/forma de pago
            if (entity.DefaultPaymentType != null)
            {
                StreamWriter.Write(CorrectString(entity.DefaultPaymentType.Code, 3));
                Log.DebugFormat("Payment type:  {0}", tmp);
            }
            else
            {
                StreamWriter.Write(GetSpaces(3));
                Log.DebugFormat("Payment type: empty.");
            }

            // Formato de impresión
            StreamWriter.Write(CorrectNumber("", 5));

            // Cantidad de líneas de detalle (opcional - omitido).
            StreamWriter.Write(GetZeroes(3));

            // Clase de documento
            StreamWriter.Write(GetSpaces(3));

            // Código de envío (forma en que se despachan los documentos)
            if (entity.DispatchType != null)
            {
                StreamWriter.Write(CorrectString(entity.DispatchType.Code, 4));
                Log.DebugFormat("Dispatch Code:  {0}", entity.DispatchType.Code);
            }
            else
            {
                StreamWriter.Write(GetSpaces(4));
                Log.DebugFormat("Dispatch Code:  {0}", "none");
            }

            // Indicador de trasaldo - sólo para guías de despacho
            // Versión 1.0: no soportado.
            if (entity.DocumentType == "52")
            {
                throw new NotImplementedException("This type of document (type 52) is not supported yet.");
            }
            else
            {
                StreamWriter.Write("0");
            }

            StreamWriter.Write("0");
            Log.DebugFormat("Discount indicator:  0");

            // END
            StreamWriter.WriteLine();
            Log.DebugFormat("- END -");
            registryCount++;
        }

        protected override void WriteTotals()
        {
            Log.DebugFormat("- Totals -");
            // Start
            StreamWriter.Write("0301");

            Log.DebugFormat("Total (net + vat) = {0}", (NetAmount + VatAmount));
            var exempt = IsExemptCheck();
            string netAmountStr;
            string vatAmountStr;
            decimal total;

            if (exempt)
            {
                Log.DebugFormat("There are exempt bills.");
                total = ExemptAmount;
                netAmountStr = GetZeroes(18);
                vatAmountStr = GetZeroes(18);
            }
            else
            {
                Log.DebugFormat("No exempt bills. Vat Amount: {0}", VatAmount);
                total = NetAmount + VatAmount;
                netAmountStr = CorrectNumber(Convert.ToInt32(NetAmount).ToString(), 18);
                vatAmountStr = CorrectNumber(Convert.ToInt32(VatAmount).ToString(), 18);
            }

            // Monto neto
            StreamWriter.Write(netAmountStr);
            Log.DebugFormat("Net Amount: {0}", netAmountStr);

            // Monto exento (boleta electrónica exenta)
            StreamWriter.Write(CorrectNumber(Convert.ToInt32(ExemptAmount).ToString(), 18));
            Log.DebugFormat("Exempt Amount: {0}", ExemptAmount);

            // Tasa de IVA
            StreamWriter.Write(GetZeroes(6));

            // Monto de IVA
            StreamWriter.Write(vatAmountStr);
            Log.DebugFormat("Tax amount: {0}", vatAmountStr);

            // Monto total (+ IVA)
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));
            Log.DebugFormat("Total + Tax: {0}", total);

            // Monto no facturable
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(nonBillableAmount).ToString(), 18));
            // TODO - Total Periodo
            // TODO - Saldo anterior electrónico
            // TODO - Saldo anterior otras
            StreamWriter.Write(GetZeroes(72));

            // TODO - Valor a pagar total del documento
            StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));

            // TODO - Saldo anterior total
            // TODO - Valor comisión neto
            // TODO - " exento
            // TODO - " IVA
            // TODO - Total comisión
            // TODO - Total venta a tercero
            // TODO - Iva propio o de terceros
            // TODO - Total venta terceros
            // TODO - Iva no retornable (IVANoRet)
            // TODO - CredEC
            // TODO - GrntDep
            StreamWriter.Write(GetZeroes(198));

            // END
            StreamWriter.WriteLine();
            Log.DebugFormat("- END -");
            registryCount++;
        }

        protected virtual bool IsExemptCheck()
        {
            foreach (var i in entity.ReferencedDocuments)
            {
                if (i.ReferencedDocument.DocumentType == "30" ||
                    i.ReferencedDocument.DocumentType == "33")
                {
                    return false;
                }
                else
                {
                    if (i.ReferencedDocument.ReferencedDocuments?.Count > 0)
                    {
                        // Does the referenced document have any subject to tax document?
                        if (i.ReferencedDocument.ReferencedDocuments.
                            Any(j =>
                            j.ReferencedDocument.DocumentType == "30" ||
                            j.ReferencedDocument.DocumentType == "33"))
                            return false; // If there is any, then the referenced document is not exempt.
                    }
                }
            }

            return true; // The document is exempt.
        }

        protected virtual bool OnlyReferencesCreditNotes()
        {
            return entity.ReferencedDocuments.All(reference => reference.ReferencedDocument.DocumentType == "61");
        }
    }
}
