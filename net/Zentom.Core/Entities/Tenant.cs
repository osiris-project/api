﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zentom.Core.Entities.Accounting;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Core.Entities
{
    public class Tenant
        : Company
    {
        #region Fields

        private DateTime? billResolutionDate;

        private int? billResolutionNumber;

        private bool isEnabled;

        private ISet<TaxDocument.TaxDocument> generatedTaxDocuments = new HashSet<TaxDocument.TaxDocument>();

        private ISet<ReceivedTaxDocument> receivedTaxDocuments = new HashSet<ReceivedTaxDocument>();

        private ISet<Product> products = new HashSet<Product>();

        private ISet<Service> services = new HashSet<Service>();

        private ISet<Employee> employees = new HashSet<Employee>();

        private ISet<Role> roles = new HashSet<Role>();

        private ISet<TenantSetting> settings = new HashSet<TenantSetting>();

        private ISet<CostsCenter> costsCenters = new HashSet<CostsCenter>();

        private ISet<Enterprise> enterprises = new HashSet<Enterprise>();

        private ISet<TenantGroup> groups = new HashSet<TenantGroup>();

        private ISet<AuthorizedAccount> authorizedAccounts = new HashSet<AuthorizedAccount>();

        private ISet<FolioRange> folioRanges = new HashSet<FolioRange>();

        private ISet<TenantModule> modules = new HashSet<TenantModule>();

        private ISet<AccountingVoucher> accountingVouchers = new HashSet<AccountingVoucher>();

        private ISet<AccountingAccount> accountingAccounts = new HashSet<AccountingAccount>(); 

        #endregion

        #region Properties

        public virtual DateTime? BillResolutionDate
        {
            get { return billResolutionDate; }
            set { billResolutionDate = value; }
        }

        public virtual int? BillResolutionNumber
        {
            get { return billResolutionNumber; }
            set { billResolutionNumber = value; }
        }

        public virtual bool IsEnabled
        {
            get { return isEnabled; }
            set { isEnabled = value; }
        }

        public virtual ISet<TaxDocument.TaxDocument> TaxDocuments
        {
            get { return generatedTaxDocuments; }
            set { generatedTaxDocuments = value; }
        }

        public virtual ISet<Product> Products
        {
            get { return products; }
            set { products = value; }
        }

        public virtual ISet<Service> Services
        {
            get { return services; }
            set { services = value; }
        }

        public virtual ISet<Employee> Employees
        {
            get { return employees; }
            set { employees = value; }
        }

        public virtual ISet<Role> Roles
        {
            get { return roles; }
            set { roles = value; }
        }

        public virtual ISet<TenantSetting> Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        public virtual ISet<CostsCenter> CostsCenters
        {
            get { return costsCenters; }
            set { costsCenters = value; }
        }

        public virtual ISet<TenantAddress> TenantAddresses
        {
            get { return (ISet<TenantAddress>)Addresses; }
            set { Addresses = value; }
        }

        public virtual ISet<TenantComercialActivity> TenantComercialActivities
        {
            get { return (ISet<TenantComercialActivity>) ComercialActivities; }
            set { ComercialActivities = value; }
        }

        public virtual ISet<TenantEconomicActivity> TenantEconomicActivities
        {
            get { return (ISet<TenantEconomicActivity>) EconomicActivities; }
            set { EconomicActivities = value; }
        } 

        public virtual ISet<Enterprise> Enterprises
        {
            get { return enterprises; }
            set { enterprises = value; }
        }

        public virtual ISet<ReceivedTaxDocument> ReceivedTaxDocuments
        {
            get { return receivedTaxDocuments; }
            set { receivedTaxDocuments = value; }
        }

        public virtual ISet<TaxDocument.TaxDocument> GeneratedTaxDocuments
        {
            get { return generatedTaxDocuments; }
            set { generatedTaxDocuments = value; }
        }

        public virtual ISet<AuthorizedAccount> AuthorizedAccounts
        {
            get { return authorizedAccounts; }
            set { authorizedAccounts = value; }
        }

        public virtual ISet<TenantGroup> Groups
        {
            get { return groups; }
            set { groups = value; }
        }

        public virtual ISet<FolioRange> FolioRanges
        {
            get { return folioRanges; }
            set { folioRanges = value; }
        }

        public virtual ISet<TenantModule> Modules
        {
            get { return modules; }
            set { modules = value; }
        }

        public virtual ISet<AccountingVoucher> AccountingVouchers
        {
            get { return accountingVouchers; }
            set { accountingVouchers = value; }
        }

        public virtual ISet<AccountingAccount> AccountingAccounts
        {
            get { return accountingAccounts; }
            set { accountingAccounts = value; }
        }

        #endregion

        #region CTORs.

        public Tenant() { }

        public Tenant(string taxId, string displayName, string legalName, string bankAccountNumber, string email,
            string phone, IEnumerable<CompanyAddress> addresses, DateTime? billResolutionDate, int? billResolutionNumber,
            bool isEnabled) : base(taxId, displayName, legalName, bankAccountNumber, email, phone, addresses)
        {
            this.billResolutionDate = billResolutionDate;
            this.billResolutionNumber = billResolutionNumber;
            this.isEnabled = isEnabled;
        }

        public Tenant(string taxId, string displayName, string legalName, string bankAccountNumber, string email,
            string phone, IEnumerable<CompanyAddress> addresses,
            IEnumerable<CompanyComercialActivity> comercialActivities,
            IEnumerable<CompanyEconomicActivity> economicActivities,
            DateTime? billResolutionDate,
            ISet<FolioRange> folioRanges,
            int? billResolutionNumber, bool isEnabled)
            : base(
                taxId, displayName, legalName, bankAccountNumber, email, phone, addresses, comercialActivities,
                economicActivities)
        {
            this.billResolutionDate = billResolutionDate;
            this.billResolutionNumber = billResolutionNumber;
            this.isEnabled = isEnabled;
            this.folioRanges = folioRanges;
        }

        #endregion

        public virtual void AuthorizeAccount(Account account)
        {
            if (authorizedAccounts.Any(i => i.Account == account))
            {
                return;
            }

            authorizedAccounts.Add(new AuthorizedAccount(this, account));
        }

        public virtual void SetComercialActivities(List<ComercialActivity> activities)
        {
            if (activities != null)
            {
                var len = TenantComercialActivities.Count() - 1;

                for (var index = len; index >= 0; index--)
                {
                    var el = TenantComercialActivities.ElementAt(index);
                    if (activities.All(i => i.Name != el.ComercialActivity.Name)) continue;
                    TenantComercialActivities.Remove(el);
                }

                foreach (var comercialActivity in activities)
                {
                    TenantComercialActivities.Add(new TenantComercialActivity(this, comercialActivity));
                }
            }
            else
            {
                TenantComercialActivities.Clear();
            }
        }

        public virtual void SetEconomicActivities(List<EconomicActivity> activities)
        {
            if (activities != null)
            {
                var len = TenantEconomicActivities.Count - 1;

                for (var index = len; index >= 0; index--)
                {
                    var el = TenantEconomicActivities.ElementAt(index);
                    if (activities.All(i => i.Code != el.EconomicActivity.Code)) continue;
                    TenantEconomicActivities.Remove(el);
                }

                foreach (var economicActivity in activities)
                {
                    TenantEconomicActivities.Add(new TenantEconomicActivity(this, economicActivity));
                }
            }
            else
            {
                TenantEconomicActivities.Clear();
            }
        }
    }
}