﻿using System;

namespace Osiris.Api.Modules.TenantEp
{
    public class FolioRangeCmd
    {
        public string DocumentType { get; set; }

        public long RangeStart { get; set; }

        public long RangeEnd { get; set; }

        public bool IsCurrentRange { get; set; }

        public DateTime RequestDate { get; set; }
    }
}
