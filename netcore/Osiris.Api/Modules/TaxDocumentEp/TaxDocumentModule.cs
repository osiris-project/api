﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Dobelik.Utils.Identity;
using Slf4Net;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Enums;
using Osiris.Core.Interfaces.Validation;
using Osiris.Core.Pagination;
using Osiris.Core.Services;
using Osiris.Integration.Artikos;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentModule
        : TenantApi
    {
        #region Fields

        private static readonly ILogger log = LoggerFactory.GetLogger(typeof(TaxDocumentModule));

        private readonly INationalIdentity identity;

        private readonly ArtikosFactory artikosFactory;
        private readonly IFolioManagerService folioManagerService;

        private readonly Dictionary<string, string> paths = new Dictionary<string, string>();

        #endregion

        public TaxDocumentModule(ISessionFactory factory, INationalIdentity identity, ArtikosFactory artikosFactory,
            IFolioManagerService folioManagerService)
            : base("tax-documents", factory)
        {
            OnError.AddItemToEndOfPipeline(DeleteDocuments);
            After.AddItemToEndOfPipeline(PushDocuments);
            this.identity = identity;
            this.artikosFactory = artikosFactory;
            this.folioManagerService = folioManagerService;

            // Reading
            this.Get(BasePath, List);
            this.Get<Command>(BasePath + "/{id:guid}/", Read);
            this.Get(BasePath + "/get-folio/{documentType}/", GetFolioNumber);
            this.Get<Command>(BasePath + "/{id:guid}/references/", GetDocumentReferences);
            this.Get<ReferenceQuery>(BasePath + "/reference-query/", GetReferenceData);

            // Writing
            this.Post<TaxDocumentCmd>(BasePath, Create);
            this.Post<VoidDocumentCmd>(BasePath + "/void/", VoidDocument);
            this.Post<WriteFileListCmd>(BasePath + "/write-file-list/", WriteFileList);
            this.Patch<Command>(BasePath + "/{id:guid}/", WriteFile);
            this.Patch<Command>(BasePath + "/{id:guid}/approve/", ApproveDocument);
            this.Patch<Command>(BasePath + "/{id:guid}/reject/", RejectDocument);
        }

        #region Overrides of CRUDModule<TaxDocumentDto>

        [Permission(Permission = "tax-document-read")]
        public Negotiator Read(Command cmd)
        {
            var entity = db.QueryOver<TaxDocument>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.TaxDocumentNotFound);
            }

            TaxDocument alias = null;

            // Documents referenced by this:
            var refs = db.QueryOver<TaxDocumentReference>()
                .Where(i => i.Parent == entity)
                .Left.JoinQueryOver(i => i.ReferencedDocument, () => alias)
                .SelectList(i => i
                    .Select(j => alias.Id)
                    .Select(j => j.ReferenceComment)
                    .Select(j => alias.FolioNumber)
                    .Select(j => alias.DocumentType))
                .Take(30)
                .Future<object[]>();

            // Documents that reference this:
            var refsBy = db.QueryOver<TaxDocumentReference>()
                .Where(i => i.ReferencedDocument == entity)
                .Left.JoinQueryOver(i => i.Parent, () => alias)
                .SelectList(i => i
                    .Select(j => j.Parent.Id)
                    .Select(j => j.ReferenceComment)
                    .Select(j => alias.FolioNumber)
                    .Select(j => alias.DocumentType))
                .Take(30)
                .Future<object[]>();

            // Lines
            var lines = db.QueryOver<TaxDocumentLine>()
                .Where(i => i.TaxDocument == entity)
                .Take(60)
                .Future();

            // Client Info

            // Client addresses
            var addressList = entity.Client.Addresses.Select(i => new
            {
                line1 = i.Line1,
                line2 = i.Line2,
                commune = i.Commune,
                city = i.City
            }).ToList();

            var dto = new
            {
                folioNumber = entity.FolioNumber,
                accountingDate = entity.AccountingDate,
                dateGenerated = entity.GenerationDate,
                expireDate = entity.ExpireDate,
                documentType = entity.DocumentType,
                documentStatus = entity.DocumentStatus,
                taxRate = entity.TaxRate,
                extraTaxRate = entity.ExtraTaxRate,
                extraTaxCode = entity.ExtraTaxCode,
                isRetainedExtraTax = entity.IsRetainedExtraTax,
                total = entity.DocumentTotal,
                creditNoteDiscount = entity.CreditNoteDiscount,
                exemptAmount = entity.ExemptTotal,
                subjectAmount = entity.AffectTotal,
                tenant = new
                {
                    taxId = entity.TenantTaxId,
                    legalName = entity.TenantLegalName,
                    address = entity.TenantAddress,
                    commune = entity.TenantCommune,
                    city = entity.TenantCity,
                    sector = new
                    {
                        name = entity.TenantComercialActivity,
                        code = entity.TenantEconomicActivityCode
                    }
                },
                // TODO: add payments
                //payments,
                referencedDocuments = refs.ToList().Select(i => new
                {
                    referenceId = (Guid)i[0],
                    comment = (string)i[1],
                    folio = (long)i[2],
                    documentType = (string)i[3]
                }),
                referencedBy = refsBy.ToList().Select(i => new
                {
                    parentId = (Guid)i[0],
                    reason = (string)i[1],
                    folio = (long)i[2],
                    documentType = (string)i[3]
                }),
                lines = lines.ToList().Select(i => new
                {
                    quantity = i.Quantity,
                    unitPrice = i.UnitPrice,
                    netAmount = i.NetAmount,
                    totalTaxAmount = i.TotalTaxAmount,
                    totalExtraTaxAmount = i.ExtraTaxAmount,
                    text = i.Text,
                    subtotal = i.LineTotal
                }),
                correctionCode = entity.CorrectionCode,
                // TODO: add cost centers - not sure
                //costsCenters,
                vatOwnerTaxId = entity.VatOwnerTaxId,
                client = new
                {
                    taxId = entity.Client.TaxId,
                    legalName = entity.Client.LegalName,
                    phone = entity.Client.Phone,
                    addresses = addressList,
                    comercialActivity = entity.Client.ComercialActivity
                },
                clientId = entity.CurrentClient.Id
            };

            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "tax-document-read")]
        public Negotiator GetFolioNumber(dynamic args)
        {
            string doctype = args.documentType;

            if (string.IsNullOrWhiteSpace(doctype))
            {
                return Negotiate.WithNotFound(string.Format(I18n.Error.DocTypeFolioNotFound, doctype));
            }
            var folio = folioManagerService.GetNextFolioNumber(doctype, currentTenant);

            if (folio > 0)
            {
                return Negotiate.WithOkAndModel(new
                {
                    folioNumber = folio
                });
            }

            log.Error("No folio number found for document type {0}.", doctype);
            return Negotiate.WithNotFound(string.Format(I18n.Error.DocTypeFolioNotFound, doctype));
        }

        [Permission(Permission = "tax-document-list")]
        public Negotiator List(dynamic args)
        {
            var bound = this.BindTo(new TaxDocumentQuery());
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            log.Debug("Tax Document Query: {0}", bound);

            TaxDocument alias = null;
            TaxDocumentClient client = null;
            Enterprise enterprise = null;

            var query = db.QueryOver(() => alias)
                .Where(() => alias.Tenant == currentTenant)
                .Left.JoinAlias(i => i.Client, () => client)
                .Left.JoinAlias(i => i.CurrentClient, () => enterprise);

            if (bound.Folio1 > 0)
            {
                if (string.IsNullOrWhiteSpace(bound.FolioOperator))
                {
                    bound.FolioOperator = "";
                }

                switch (bound.FolioOperator)
                {
                    case "lessThan":
                        query.Where(() => alias.FolioNumber < bound.Folio1);
                        break;
                    case "greaterThan":
                        query.Where(() => alias.FolioNumber > bound.Folio1);
                        break;

                    case "between":
                        if (bound.Folio2 <= 0) break;
                        if (bound.Folio1 == bound.Folio2)
                        {
                            query.Where(() => alias.FolioNumber == bound.Folio1);
                            break;
                        }

                        long left, right;
                        if (bound.Folio1 > bound.Folio2)
                        {
                            left = bound.Folio2;
                            right = bound.Folio1;
                        }
                        else
                        {
                            left = bound.Folio1;
                            right = bound.Folio2;
                        }
                        query.Where(() => alias.FolioNumber >= left && alias.FolioNumber <= right);
                        break;

                    default:
                        query.Where(() => alias.FolioNumber == bound.Folio1);
                        break;
                }
            }

            if (bound.AccountingDate1.HasValue)
            {
                if (bound.AccountingDate2.HasValue)
                {
                    if (bound.AccountingDate1 == bound.AccountingDate2)
                    {
                        query.Where(() => alias.AccountingDate == bound.AccountingDate1);
                    }
                    else
                    {
                        DateTime left, right;

                        if (bound.AccountingDate1 > bound.AccountingDate2)
                        {
                            left = bound.AccountingDate2.Value;
                            right = bound.AccountingDate1.Value;
                        }
                        else
                        {
                            left = bound.AccountingDate1.Value;
                            right = bound.AccountingDate2.Value;
                        }

                        query.Where(() => alias.AccountingDate >= left && alias.AccountingDate <= right);
                    }
                }
                else
                {
                    query.Where(() => alias.AccountingDate >= bound.AccountingDate1);
                }
            }

            if (!string.IsNullOrWhiteSpace(bound.ClientTaxId))
            {
                var taxid = identity.CleanId(bound.ClientTaxId);
                query.Where(() => client.TaxId == taxid);
            }

            if (!string.IsNullOrWhiteSpace(bound.ClientName))
            {
                query.WhereRestrictionOn(() => client.LegalName)
                    .IsInsensitiveLike("%" + bound.ClientName + "%");
            }

            if (!string.IsNullOrWhiteSpace(bound.DocumentType))
            {
                query.Where(() => alias.DocumentType == bound.DocumentType);
            }

            TaxDocumentDto dto = null;
            TaxDocumentLine lines = null;
            TaxDocumentReference referencedBy = null;

            var totalCount = query.Clone()
                .ToRowCountInt64Query().FutureValue<long>();
            var items = query
                .Left.JoinAlias(() => alias.TaxDocumentLines, () => lines)
                .Left.JoinAlias(() => alias.ReferencedBy, () => referencedBy)
                .Select(
                    Projections.Group<TaxDocument>(i => alias.TaxRate).WithAlias(() => dto.TaxRate),
                    Projections.Group<TaxDocument>(i => alias.FolioNumber).WithAlias(() => dto.FolioNumber),
                    Projections.Group<TaxDocument>(i => alias.Id).WithAlias(() => dto.Id),
                    Projections.Group<TaxDocument>(i => alias.AccountingDate).WithAlias(() => dto.AccountingDate),
                    Projections.Group<TaxDocument>(i => alias.DocumentType).WithAlias(() => dto.DocTypeCode),
                    Projections.Group<TaxDocument>(i => alias.DocumentStatus).WithAlias(() => dto.DocumentStatus),
                    Projections.Group<TaxDocument>(i => alias.ExemptTotal).WithAlias(() => dto.ExemptAmount),
                    Projections.Group<TaxDocument>(i => alias.AffectTotal).WithAlias(() => dto.SubjectAmount),
                    Projections.Group<TaxDocumentClient>(i => client.LegalName).WithAlias(() => dto.ClientLegalName),
                    Projections.Group<TaxDocumentClient>(i => client.TaxId).WithAlias(() => dto.ClientTaxId),
                    Projections.Group<TaxDocumentClient>(i => client.Id).WithAlias(() => dto.ClientId),
                    Projections.Group<TaxDocumentClient>(i => client.ComercialActivity)
                        .WithAlias(() => dto.ClientSectorName),
                    Projections.Conditional(Subqueries.Exists(QueryOver.Of<TaxDocumentReference>()
                        .Where(
                            i =>
                                i.ReferencedDocument.Id == alias.Id &&
                                i.ReferenceReason == CorrectionCode.VoidDocument.ToString())
                        .Select(i => i.Id)
                        .Take(1).DetachedCriteria), Projections.Constant(true), Projections.Constant(false))
                        .WithAlias(() => dto.IsVoided),
                    Projections.Group<Enterprise>(i => i.Id).WithAlias(() => dto.ClientId),
                    Projections.Sum(() => lines.TotalTaxAmount).WithAlias(() => dto.TotalTaxes),
                    Projections.Sum(() => lines.NetAmount).WithAlias(() => dto.TotalNetAmount),
                    Projections.Sum(() => lines.LineTotal).WithAlias(() => dto.GrossTotal)
                //Projections.SubQuery(
                //    QueryOver.Of(() => lineSum).Where(i => lineSum.TaxType == TaxType.Exempt)
                //        .Select(Projections.Sum(() => lineSum.NetAmount)).DetachedCriteria)
                //    .WithAlias(() => dto.ExemptAmount),
                //Projections.SubQuery(
                //    QueryOver.Of(() => lineSum).Where(i => lineSum.TaxType == TaxType.Subject)
                //        .Select(Projections.Sum(() => lineSum.NetAmount)).DetachedCriteria)
                //    .WithAlias(() => dto.SubjectAmount)
                )
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<TaxDocumentDto>())
                .Skip((page - 1) * rpp)
                .Take(rpp)
                .Future<TaxDocumentDto>();
            var paginated = new Paginate<TaxDocumentDto>(items.ToList(), page, rpp, totalCount.Value);
            return Negotiate.WithOkAndList(paginated, Request.Url.ToString());
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator Create(TaxDocumentCmd cmd, dynamic args)
        {
            long folio = 0;

            if (!IsManualDocument(cmd.DocumentType))
            {
                log.Debug("Not a manual document. Trying to obtain a new folio number.");
                var response = SelectFolioNumber(ref folio, cmd.DocumentType);
                if (response != null)
                {
                    return response;
                }
                log.Debug("Selected folio number: {0}", folio);
            }
            else
            {
                log.Debug("Manual document. Verifying that the folio number is not empty or 0.");
                if (!cmd.FolioNumber.HasValue || cmd.FolioNumber.Value <= 0)
                {
                    return Negotiate.WithValidationFailure(new ModelError("folioNumber", "missing"));
                }
                folio = cmd.FolioNumber.Value;
            }

            var client = db.Get<Enterprise>(cmd.Client.Id);

            if (client == null)
            {
                log.Error("No enterprise with id '{0}' found.", cmd.Client.Id);
                return Negotiate.WithNotFound(cmd.Client.Id, I18n.Error.ClientNotFound);
            }

            var entity = new TaxDocument(folio, cmd.DocumentType, Constants.EMISSION_POINT, cmd.AccountingDate,
                null, cmd.ExpireDate, cmd.PreviousDebt, cmd.Comments, cmd.TaxRate, cmd.ExtraTaxRate,
                cmd.IsRetainedTax,
                cmd.ExtraTaxCode, 0M, Account.Person.FullName, null, cmd.PaymentType, null, cmd.ServiceType,
                cmd.CreditNoteDiscount, cmd.CorrectionCode, cmd.VatOwnerTaxId)
            {
                DocumentStatus = TaxDocumentStatus.Saved
            };
            entity.AddTenant(currentTenant, cmd.Tenant.EconomicActivity.Code, cmd.Tenant.ComercialActivity,
                cmd.Tenant.EconomicActivity.Name);
            entity.AddClient(client, cmd.Client.ComercialActivity);
            var lineNumber = 1;

            foreach (var item in cmd.Lines)
            {
                entity.AddTaxDocumentLine(lineNumber, item.Text, item.Quantity, item.AdditionalGloss,
                    item.UnitPrice, item.MeasureUnit, item.TaxType);
                lineNumber++;

                var netAmount = item.UnitPrice * item.Quantity;
                if (item.TaxType == TaxType.Exempt)
                {
                    entity.ExemptTotal += netAmount;
                }
                else
                {
                    entity.AffectTotal += netAmount;
                }
            }

            if (cmd.ReferencedDocuments != null)
            {
                var count = 0;
                foreach (
                    var docRef in
                        cmd.ReferencedDocuments)
                {

                    if (IsWeakReference(docRef.DocumentType))
                    {
                        entity.AddReference(docRef.Folio, docRef.DocumentType, docRef.ReferenceDate, docRef.Comment);
                    }
                    else
                    {
                        if (docRef.ReferencedDocumentId == Guid.Empty)
                        {
                            return
                                Negotiate.WithValidationFailure(new ModelError("references[" + count + "]",
                                    I18n.Error.ReferenceNotFound));
                        }
                        entity.AddReference(db.Load<TaxDocument>(docRef.ReferencedDocumentId),
                            docRef.ReferenceReason, docRef.Comment);
                    }
                    count++;
                }
            }

            if (cmd.GenerateFile)
            {
                SetDocumentFilePath(entity);
                entity.DocumentStatus = TaxDocumentStatus.Pending;
            }
            db.Save(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator VoidDocument(VoidDocumentCmd cmd, dynamic args)
        {
            log.Debug("VoidCommand: {0}", cmd);
            var referencedDocument = db.QueryOver<TaxDocument>()
                .Where(i => i.Id == cmd.ReferencedDocumentId && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (referencedDocument == null)
            {
                return Negotiate.WithNotFound(I18n.Error.ReferencedDocumentNotFound);
            }

            var documentType = SelectDocumentType(referencedDocument.DocumentType);

            var folio = cmd.FolioNumber ?? 0L;
            var response = SelectFolioNumber(ref folio, documentType);

            if (response != null)
            {
                return response;
            }

            var entity = new TaxDocument
            {
                AccountingDate = referencedDocument.AccountingDate,
                AccountingTime = null,
                Comments = "",
                CorrectionCode = CorrectionCode.VoidDocument,
                CostsCenter = null,
                FolioNumber = folio,
                DocumentType = documentType,
                DocumentResponsible = Account.Person.FullName,
                CreditNoteDiscount = false,
                ServiceType = 2,
                TaxRate = referencedDocument.TaxRate,
                ExtraTaxRate = referencedDocument.ExtraTaxRate,
                ExtraTaxCode = referencedDocument.ExtraTaxCode,
                IsRetainedExtraTax = referencedDocument.IsRetainedExtraTax,
                EmissionPoint = Constants.EMISSION_POINT
            };

            entity.AddTenant(currentTenant, referencedDocument.TenantEconomicActivityCode, referencedDocument.TenantComercialActivity, referencedDocument.TenantEconomicActivityName);
            var client = db.QueryOver<Enterprise>()
                .Where(i => i.TaxId == referencedDocument.Client.TaxId && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            entity.AddClient(client, referencedDocument.Client.ComercialActivity);
            entity.VatOwnerTaxId = referencedDocument.VatOwnerTaxId;
            entity.AddReference(referencedDocument, CorrectionCode.VoidDocument, cmd.VoidReason);

            foreach (var line in referencedDocument.TaxDocumentLines)
            {
                entity.AddTaxDocumentLine(line.LineNumber, line.Text, line.Quantity, line.AdditionalGloss, line.UnitPrice, line.MeasureUnit);
            }

            SetDocumentFilePath(entity);
            db.Save(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator BulkCreate(IEnumerable<TaxDocumentCmd> list)
        {
            if (list == null)
            {
                return Negotiate.WithOk();
            }
            return Negotiate.WithOk();
        }

        [Permission(Permission = "tax-document-update")]
        public Negotiator ApproveDocument(Command cmd)
        {
            var entity = db.QueryOver<TaxDocument>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.TaxDocumentNotFound);
            }

            entity.DocumentStatus = TaxDocumentStatus.Approved;
            return Negotiate.WithOk();
        }

        [Permission(Permission = "tax-document-update")]
        public Negotiator RejectDocument(Command cmd)
        {
            var entity = db.QueryOver<TaxDocument>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.TaxDocumentNotFound);
            }

            entity.DocumentStatus = TaxDocumentStatus.Rejected;
            SaveRejectedFolio(entity.FolioNumber, entity.DocumentType);
            return Negotiate.WithOk();
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator UpdateTaxDocumentData(UpdateTextCmd cmd)
        {
            return null;
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator UpdateTaxDocumentValues(UpdateValuesCmd cmd)
        {
            return null;
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator WriteFile(Command cmd)
        {
            var entity = db.QueryOver<TaxDocument>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(cmd.Id, I18n.Error.TaxDocumentNotFound);
            }

            SetDocumentFilePath(entity);
            entity.DocumentStatus = TaxDocumentStatus.Pending;
            return Negotiate.WithOk();
        }

        [Permission(Permission = "tax-document-create")]
        public Negotiator WriteFileList(WriteFileListCmd cmd)
        {
            if (cmd.Documents.Length <= 0)
            {
                return Negotiate.WithOk();
            }

            var len = cmd.Documents.Length;
            var result = new Dictionary<Guid, IList<IModelValidation>>();
            for (var i = 0; i < len; i++)
            {
                var id = cmd.Documents[i];
                var doc = db.Load<TaxDocument>(id);
                if (!CanBeWritten(doc.DocumentType)) continue;
                var errors = SetDocumentFilePath(doc);
                if (errors != null && errors.Count > 0)
                    result.Add(id, errors);
                else
                    doc.DocumentStatus = TaxDocumentStatus.Pending;
            }
            return result.Count > 0 ? Negotiate.WithStatusCode(HttpStatusCode.Conflict).WithModel(new
            {
                validation = result
            }) : Negotiate.WithOk();
        }

        [Permission(Permission = "tax-document-read")]
        public Negotiator GetDocumentReferences(Command cmd)
        {
            TaxDocumentReference referencedDocuments = null;
            TaxDocument referencedDocument = null;
            ReferenceDto dto = null;
            var items = db.QueryOver<TaxDocument>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Left.JoinAlias(i => i.ReferencedDocuments, () => referencedDocuments)
                .Left.JoinAlias(i => referencedDocuments.ReferencedDocument, () => referencedDocument)
                .SelectList(i => i.Select(j => referencedDocuments.ReferenceReason).WithAlias(() => dto.ReferenceReason)
                    .Select(j => referencedDocuments.ReferenceComment).WithAlias(() => dto.Comment)
                    .Select(j => referencedDocument.FolioNumber).WithAlias(() => dto.ReferencedFolio)
                    .Select(j => referencedDocument.DocumentType).WithAlias(() => dto.ReferencedDocumentType))
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<ReferenceDto>())
                .PaginateResults<TaxDocument, ReferenceDto>();

            return Negotiate.WithOkAndList(items, Request.Url);
        }

        [Permission(Permission = "tax-document-read")]
        public Negotiator GetReferenceData(ReferenceQuery bound)
        {
            var entity = db.QueryOver<TaxDocument>()
                .Where(
                    i =>
                        i.Tenant == currentTenant && i.DocumentType == bound.DocumentType &&
                        i.FolioNumber == bound.Folio)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound();
            }

            var dto = new
            {
                id = entity.Id,
                folio = entity.FolioNumber,
                documentType = entity.DocumentType,
                referenceDate = entity.AccountingDate
            };

            return Negotiate.WithOkAndModel(dto);
        }

        #endregion

        private void PushDocuments(NancyContext context)
        {
            if (paths.Count == 0)
            {
                return;
            }

            foreach (var path in paths.Where(path => File.Exists(path.Key)))
            {
                File.Move(path.Key, path.Value);
            }
        }

        private Negotiator DeleteDocuments(NancyContext context, Exception ex)
        {
            if (paths.Count == 0) return null;
            foreach (var path in paths.Where(i => File.Exists(i.Key)))
            {
                File.Delete(path.Key);
            }
            return null;
        }

        //private long GetNextFolioNumber(string documentType)
        //{
        //    long folio;
        //    var locker = new object();

        //    lock (locker)
        //    {
        //        var entity = db.QueryOver<FolioRange>()
        //            .Where(i => i.Tenant.Id == currentTenant.Id && i.DocumentType == documentType && i.IsCurrentRange && i.Status == RangeStatus.Enabled)
        //            .Take(1)
        //            .SingleOrDefault();

        //        if (entity == null)
        //        {
        //            return 0;
        //        }

        //        FolioRange range;
        //        // Try to change to the newest next folio range
        //        if (entity.IsUsed)
        //        {
        //            entity.IsCurrentRange = false;
        //            entity.Status = RangeStatus.Disabled;
        //            range = db.QueryOver<FolioRange>()
        //                .Where(i => i.Tenant.Id == currentTenant.Id && i.DocumentType == documentType && i.CreatedDate > entity.CreatedDate)
        //                .Take(1)
        //                .SingleOrDefault();

        //            if (range == null)
        //            {
        //                return 0;
        //            }

        //            range.IsCurrentRange = true;
        //            range.Status = RangeStatus.Enabled;
        //        }
        //        else
        //        {
        //            range = entity;
        //        }

        //        //if (entity.IsUsed) return -1;
        //        if (!string.IsNullOrWhiteSpace(range.UnusedFolios)) return range.CurrentFolio;
        //        folio = GetLastUnusedFolio(range.UnusedFolios);

        //        if (folio > 0)
        //            return folio;

        //        folio = entity.CurrentFolio;
        //        range.CurrentFolio++;
        //    }

        //    return folio;
        //}

        //private static long GetLastUnusedFolio(string unusedFolios)
        //{
        //    if (string.IsNullOrWhiteSpace(unusedFolios))
        //        return -1;
        //    var split = unusedFolios.Trim().Split(";".ToCharArray());
        //    if (split.Length <= 0)
        //    {
        //        return -1;
        //    }

        //    if (split.Length == 1)
        //    {
        //        return long.Parse(split[0]);
        //    }

        //    var longs = Array.ConvertAll(split, long.Parse);
        //    Array.Sort(longs);
        //    return longs[0];
        //}

        private IList<IModelValidation> WriteFile(TaxDocument taxDoc, string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return null;
            }
            var instance = artikosFactory.Resolve(taxDoc.DocumentType);
            using (var stream = new StreamWriter(path, false, Encoding.GetEncoding("ISO-8859-1")))
            {
                instance.Format(stream, taxDoc);
            }
            return instance.IsValid ? null : instance.Errors;
        }

        private Negotiator SelectFolioNumber(ref long folioIn, string documentType)
        {
            var folio = folioIn;
            if (folio <= 0)
            {
                log.Info("Retrieving managed folio number.");
                folio = folioManagerService.GetNextFolioNumber(documentType, currentTenant);

                switch (folio)
                {
                    case 0:
                        log.Error("Failed: No folio number found for document type {0}.", documentType);
                        return Negotiate.WithNotFound(new
                        {
                            error = string.Format(I18n.Error.NoFolioNumberForDoctype, documentType)
                        });
                    case -1:
                        log.Error("The folio range for the document type {0} has been consumed.", documentType);
                        return Negotiate.WithConflict(I18n.Error.FolioRangeUsed);
                }
            }
            else
            {
                log.Debug("Folio number has been provided; checking duplicates:");
                // Validate that there is no other document with this folio:
                var entity = db.QueryOver<TaxDocument>()
                    .Where(i => i.Tenant == currentTenant &&
                                i.FolioNumber == folio &&
                                i.DocumentType == documentType &&
                                i.DocumentStatus != TaxDocumentStatus.Rejected)
                    .Take(1)
                    .SingleOrDefault();

                if (entity != null)
                {
                    log.Error("A tax document with folio {0} and {{status != rejected}} already exists.", folio);
                    return Negotiate.WithDuplicateItem(I18n.Error.DuplicateTaxDocument, "folioNumber");
                }
            }

            folioIn = folio;
            log.Info("Using folio '{0}'", folio);
            return null;
        }

        private IList<IModelValidation> SetDocumentFilePath(TaxDocument document)
        {
            if (!CanBeWritten(document.DocumentType))
            {
                return null;
            }
            var signingProvider = GetSigningProvider();
            if (signingProvider == null)
            {
                log.Info("The current tenant (tax id = {0}) does not have a signing provider.", document.TenantTaxId);
                paths.Clear();
                return null;
            }
            var fname = ArtikosFactory.BuildFileName(currentTenant.TaxId, document.FolioNumber, document.DocumentType);
            var srcPath = GetTmpFilePath(fname);
            var dstPath = string.Concat(GetRootStoragePath(),
                ArtikosFactory.GetRepositoryFolder(signingProvider.FolderName, document.DocumentType, IsTestingEnabled()),
                "\\",
                fname);
            log.Debug("File Name: {0}, Src: {1}, Dst: {2}", fname, srcPath, dstPath);
            paths.Add(srcPath, dstPath);
            return WriteFile(document, srcPath);
        }

        private static string GetTmpFilePath(string fname)
        {
            var tmpFolder = GetTempFolder();
            return Path.Combine(tmpFolder, fname);
        }

        /// <summary>
        /// Selects the document type that can void the voidableDocumentType.
        /// </summary>
        /// <param name="voidableDocumentType">The document type that can be voided.</param>
        /// <returns></returns>
        private static string SelectDocumentType(string voidableDocumentType)
        {
            switch (voidableDocumentType)
            {
                case "61":
                    return "56";
                case "56":
                    return "61";
                default:
                    return "61";
            }
        }

        private void SaveRejectedFolio(long folio, string documentType)
        {
            var entity = db.QueryOver<FolioRange>()
                .Where(i => i.Tenant == Tenant && i.DocumentType == documentType && i.IsCurrentRange)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                log.Error("Trying to store a rejected folio when there is no folio defined.");
                return;
            }

            entity.UnusedFolios = string.Concat(entity.UnusedFolios, ",", folio);
        }

        private static bool IsManualDocument(string documentType)
        {
            switch (documentType)
            {
                case "30":
                case "32":
                case "55":
                case "60":
                    return true;

                case "33":
                case "34":
                case "56":
                case "61":
                    return false;
            }

            return true;
        }

        private static bool CanBeWritten(string documentType)
        {
            switch (documentType)
            {
                case "30":
                case "32":
                case "801":
                    return false;
            }

            return true;
        }

        private static bool IsWeakReference(string documentType)
        {
            switch (documentType)
            {
                case "801":
                    return true;
            }

            return false;
        }
    }
}
