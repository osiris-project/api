﻿using System;

namespace Osiris.Core.Entities.TaxDocument
{
    public class TaxDocumentWeakReference
        : Entity
    {
        #region Fields

        private long folio;

        private string documentType;

        private DateTime date;

        private string reason;

        private TaxDocument taxDocument;

        #endregion

        #region Properties

        public virtual long Folio
        {
            get { return folio; }
            set { folio = value; }
        }

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual TaxDocument TaxDocument
        {
            get { return taxDocument; }
            set { taxDocument = value; }
        }

        #endregion

        #region Ctors.

        public TaxDocumentWeakReference()
        {
        }

        public TaxDocumentWeakReference(long folio, string documentType, DateTime date, string reason, TaxDocument taxDocument)
        {
            this.folio = folio;
            this.documentType = documentType;
            this.date = date;
            this.reason = reason;
            this.taxDocument = taxDocument;
        }

        #endregion
    }
}
