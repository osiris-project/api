﻿using System;

namespace Osiris.Core.Interfaces.Data
{
    public interface ITransactionScopeWrapper
        : IDisposable
    {
        void Begin();

        void Complete();

        void Rollback();
    }
}
