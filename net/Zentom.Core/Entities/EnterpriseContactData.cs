﻿namespace Zentom.Core.Entities
{
    public class EnterpriseContactData
        : ContactInfo
    {
        #region Fields

        private Enterprise enterprise;

#endregion

        #region Properties

        public virtual Enterprise Enterprise
        {
            get { return enterprise; }
            set { enterprise = value; }
        }

        #endregion

        public EnterpriseContactData() { }

        public EnterpriseContactData(int contactType, string value, Enterprise enterprise)
            : base(contactType, value)
        {
            this.enterprise = enterprise;
        }
    }
}
