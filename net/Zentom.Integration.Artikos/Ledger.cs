﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Zentom.Core.Entities;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Integration.Artikos
{
    public enum LedgerPeriod
    {
        MENSUAL,
        ESPECIAL
    }
    public enum LedgerType
    {
        COMPRA,
        VENTA
    }
    public enum SendType
    {
        PARCIAL,
        AJUSTE,
        FINAL,
        TOTAL
    }
    class Ledger
    {
        public LedgerType tipo;
        public LedgerPeriod period;
        public SendType tipoEnvio;
        private string versionLibro = "1.0";
        private string idEnvioLibro = "ID 1";

        public string periodoTributario;
        public string fechaResolucion;
        public int nroResolucion;
        public int nroSegmento;
        public string codAutrec;
        public long folioNotificacion;
        //-------------------------
        public LCaratula caratula;
        public LResumenSegmento resumenSegmento;
        public LResumenPeriodo resumenPeriodo;
        public List<LDetalle> detalle;
        public Tenant _tentat;
        public IList<TaxDocument> _taxdoc;
        public IList<ReceivedTaxDocument> _recTaxdoc;
        public string rutEnvia;
        public Ledger()
        {
            //test
            this.tipo = LedgerType.VENTA;
            caratula = new LCaratula(this);
            resumenSegmento = new LResumenSegmento(this);
            resumenPeriodo = new LResumenPeriodo(this);
            //detalle = new LDetalle(this);

            caratula.testdata();
            resumenSegmento.testdata();
            resumenPeriodo.testdata();
            //detalle.testdata();
        }
        public Ledger(Tenant tentat, IList<TaxDocument> taxdoc, LedgerPeriod ledgerPeriod, SendType sendT,
            string authorizedRut,
            DateTime tributaryDate, int? segmentNumber = null, long? folioNotificacion = null, string codAutrec = null)
        {
            ValidateData(ledgerPeriod, sendT, authorizedRut, segmentNumber, folioNotificacion, codAutrec);
            tipo = LedgerType.VENTA;
            period = ledgerPeriod;
            tipoEnvio = sendT;
            _tentat = tentat;
            _taxdoc = taxdoc;

            caratula = new LCaratula(this);
            detalle = new List<LDetalle> { new LDetalle(this) };
            periodoTributario = tributaryDate.ToString("yyyy-MM");

            setData();
            //caratula.setData();
            //detalle.setData();
        }

        public Ledger(Tenant tentat, IList<ReceivedTaxDocument> recTaxdoc, LedgerPeriod ledgerPeriod, SendType sendT,
            string authorizedRut, DateTime tributaryDate, int? segmentNumber = null, long? folioNotificacion = null,
            string codAutrec = null)
        {
            // #### MODIFICADO
            ValidateData(ledgerPeriod, sendT, authorizedRut, segmentNumber, folioNotificacion, codAutrec);

            periodoTributario = tributaryDate.ToString("yyyy-MM");
            tipo = LedgerType.COMPRA;
            period = ledgerPeriod;
            tipoEnvio = sendT;
            _tentat = tentat;
            _recTaxdoc = recTaxdoc;

            caratula = new LCaratula(this);

            detalle = from doc in recTaxdoc select new LDetalle(this, doc);

            detalle = new List<LDetalle> { new LDetalle(this) };


            setData();
            caratula.setData();

        }

        private void ValidateData(LedgerPeriod ledgerPeriod, SendType type, string authorizedRut,
            int? segmentNumber = null, long? notificationFolio = null, string cod = null)
        {
            if (ledgerPeriod == LedgerPeriod.ESPECIAL)
            {
                if (!notificationFolio.HasValue)
                {
                    throw new Exception("No notification folio in Especial Ledger.");
                }

                folioNotificacion = notificationFolio.Value;

                if (string.IsNullOrWhiteSpace(codAutrec))
                {
                    throw new Exception("No rectification code in Especial Ledger.");
                }

                codAutrec = cod;
            }

            if (type != SendType.PARCIAL) return;

            if (!segmentNumber.HasValue)
            {
                throw new Exception("No segment number present in a Partial Ledger.");
            }

            nroSegmento = segmentNumber.Value;

            if (string.IsNullOrWhiteSpace(authorizedRut))
            {
                throw new Exception("Authorized RUT missing.");
            }

            rutEnvia = authorizedRut;
        }

        public void setData()
        {
            if (!_tentat.BillResolutionDate.HasValue)
                throw new Exception("No bill resolution date present.");

            if (!_tentat.BillResolutionNumber.HasValue)
                throw new Exception("No bill resolution number present.");

            //periodoTributario = "2001-12"; // Te lo paso por el constructor
            fechaResolucion = _tentat.BillResolutionDate.Value.ToString("yyyy-MM-dd"); // de donde lo saco ? #### MODIFICADO
            nroResolucion = _tentat.BillResolutionNumber.Value; // de donde lo saco ? #### MODIFICADO
                                                                //nroSegmento = 123456; // Esto es sólo SendType == Parcial
                                                                //folioNotificacion = 123456789123456789; // Sólo se usa cuando LedgerPeriod == Especial
                                                                //codAutrec = "aaaaaaaa";// Relacionado al anterior - Te lo paso en el constructor

            if (tipoEnvio == SendType.TOTAL) //TOTAL: Información Total del Mes;
            {
                resumenPeriodo = new LResumenPeriodo(this);
                resumenPeriodo.setData();
            }
            else if (tipoEnvio == SendType.PARCIAL || tipoEnvio == SendType.FINAL) //PARCIAL: Se está enviando un segmento que tiene parte de la información; FINAL: Corresponde al último Segmento; 
            {
                resumenSegmento = new LResumenSegmento(this);
                resumenSegmento.setData();
            }
            else if (tipoEnvio == SendType.AJUSTE) //AJUSTE: Movimientos Ingresados con Posterioridad a un envío FINAL o TOTAL
            {
                resumenSegmento = new LResumenSegmento(this);
                resumenSegmento.setData();
                resumenPeriodo = new LResumenPeriodo(this);
                resumenPeriodo.setData();
            }
        }

        public Boolean doXML()
        {
            getXDoc().Save("D:/xml" + DateTime.Now.ToString("hmmss") + ".xml");
            return true;
        }
        public XDocument getXDoc()
        {
            XDocument xdoc = new XDocument(
                                new XElement("LibroCompraVenta",
                                new XAttribute("version", this.versionLibro),
                                new XElement("EnvioLibro",
                                    new XAttribute("ID", this.idEnvioLibro),
                                    caratula.getXEl(),
     tipoEnvio == SendType.TOTAL ? resumenPeriodo.getXEl() :
                                    resumenSegmento.getXEl(),
                                    from det in detalle select det.getXEl()
                                ))
                         );

            return xdoc;
        }
    }
    class LCaratula
    {
        public Ledger libro;
        public string rutEmisorLibro { get; set; }
        public string rutEnvia { get; set; }
        public string periodoTributario { get; set; }
        public string fchResol { get; set; }
        public int nroResol { get; set; }
        public string tipoOperacion { get; set; }
        public string tipoLibro { get; set; }
        public string tipoEnvio { get; set; }
        public int nroSegmento { get; set; }
        public long folioNotificacion { get; set; }
        public string codAutRec { get; set; }
        public LCaratula(Ledger libro)
        {
            this.libro = libro;
        }
        public void setData()
        {
            rutEmisorLibro = libro._tentat.TaxId;
            rutEnvia = libro.rutEnvia;
            periodoTributario = libro.periodoTributario;
            fchResol = libro.fechaResolucion;
            nroResol = libro.nroResolucion;
            tipoOperacion = "COMPRA";
            tipoLibro = Enum.GetName(typeof(LedgerType), libro.tipo);
            tipoEnvio = Enum.GetName(typeof(SendType), libro.tipoEnvio);
            nroSegmento = libro.nroSegmento;
            folioNotificacion = libro.folioNotificacion;
            codAutRec = libro.codAutrec;
        }
        public void testdata()
        {
            rutEmisorLibro = "0-0";
            rutEnvia = "0-0";
            periodoTributario = "2001-12";
            fchResol = "1967-08-13";
            nroResol = 123456;
            tipoOperacion = "COMPRA";
            tipoLibro = "MENSUAL";
            tipoEnvio = "PARCIAL";
            nroSegmento = 123456;
            folioNotificacion = 123456789123456789;
            codAutRec = "aaaaaaaaaa";
        }
        public XElement getXEl()
        {
            XElement t = new XElement("Caratula",
                            new XElement("RutEmisorLibro", rutEmisorLibro),
                            new XElement("RutEnvia", rutEnvia),
                            new XElement("PeriodoTributario", periodoTributario),
                            new XElement("FchResol", fchResol),
                            new XElement("TipoOperacion", tipoOperacion),
                            new XElement("TipoLibro", tipoLibro),
                            new XElement("TipoEnvio", tipoEnvio),
                            (libro.tipoEnvio == SendType.PARCIAL) ? new XElement("NroSegmento", nroSegmento) : null,
                            new XElement("FolioNotificacion", folioNotificacion),
                            new XElement("CodAutRec", codAutRec)
                );
            return t;
        }
    }
    class LResumenSegmento //segmento se utiliza cuando se envían libros segmentados.
    {
        Ledger libro;
        public List<LTotalesSegmento> totalesSegmento { get; set; } //Tantos como tipos de documento Haya
        public LResumenSegmento(Ledger libro)
        {
            this.libro = libro;


        }
        public void testdata()
        {
            totalesSegmento = new List<LTotalesSegmento>() { new LTotalesSegmento(this.libro), new LTotalesSegmento(this.libro) };

        }
        public void setData()
        {

        }
        public XElement getXEl()
        {
            if (totalesSegmento == null || totalesSegmento.Count == 0) return null; //si no hay segmentos

            XElement t = new XElement("ResumenSegmento",
                           from totseg in totalesSegmento
                           select totseg.getXEl()
                );
            return t;
        }
    }
    class LTotalesSegmento
    {
        public Ledger libro;
        public bool _compra;
        public bool _venta;
        public short tpoDoc { get; set; }
        public short tpoImp { get; set; }
        public int totDoc { get; set; }
        public int totAnulado { get; set; }
        public long totOpExe { get; set; }
        public long totMntExe { get; set; }
        public long totMntNeto { get; set; }
        public int totOpIVARec { get; set; }
        public long totMntIVA { get; set; }
        public int totOpActivoFijo { get; set; }
        public long totMntActivoFijo { get; set; }
        public long totMntIVAActivoFijo { get; set; }
        public List<LTotIVANoRec> totIVANoRec { get; set; } //Tabla de Iva No recuperable permite hasta 5 repeticiones. De no haber IVA No Recuperable no se generan los TAGs
        public int totOpIVAUsoComun { get; set; }
        public long totIVAUsoComun { get; set; }
        public long totIVAFueraPlazo { get; set; }
        public long totIVAPropio { get; set; }
        public long totIVATerceros { get; set; }
        public long totLey18211 { get; set; }
        public List<LRSTotOtrosImp> totOtrosImp { get; set; } //Tabla De Otros Impuesto permite hasta 20 repeticiones. De no haber otros impuestos no se generan los TAGs
        public long totImpSinCredito { get; set; }
        public long totOpIVARetTotal { get; set; }
        public long totIVARetTotal { get; set; }
        public long totOpIVARetParcial { get; set; }
        public long totIVARetParcial { get; set; }
        public long totCredEC { get; set; }
        public long totDepEnvase { get; set; }
        public LTotLiquidaciones totLiquidaciones { get; set; } //Se coloca este subconjunto solamente si hay liquidaciones. De lo contrario no se deben colocar los TAGs
        public long totMntTotal { get; set; }
        public long totOpIVANoRetenido { get; set; }
        public long totIVANoRetenido { get; set; }
        public long totMntNoFact { get; set; }
        public long totMntPeriodo { get; set; }
        public long totPsjNac { get; set; }
        public long totPsjInt { get; set; }
        public long totTabPuros { get; set; }
        public long totTabCigarrillos { get; set; }
        public long totTabElaborado { get; set; }

        public LTotalesSegmento(Ledger libro)
        {
            this.libro = libro;
            _compra = libro.tipo == LedgerType.COMPRA;
            _venta = !_compra;
            testdata();
        }
        public void testdata()
        {
            //test data:
            tpoDoc = 30;
            tpoImp = 1;
            totDoc = 1234567891;
            totAnulado = 1234567891;
            totOpExe = 1234567891;
            totMntExe = 123465789123456789;
            totMntNeto = 123456789123456789;
            totOpIVARec = 1234567891;
            totMntIVA = 123456789123456789;
            totOpActivoFijo = 1234567891;
            totMntActivoFijo = 123456789123456789;
            totMntIVAActivoFijo = 123456789123456789;
            totIVANoRec = new List<LTotIVANoRec>() { new LTotIVANoRec() { codIVANoRec = 1, totOpIVANoRec = 1234567891, totMntIVANoRec = 123456789123456789 } };
            totOpIVAUsoComun = 1234567891;
            totIVAUsoComun = 123456789123456789;
            totIVAFueraPlazo = 123456789123456789;
            totIVAPropio = 123456789123456789;
            totIVATerceros = 123456789123456789;
            totLey18211 = 123456789123456789;
            totOtrosImp = new List<LRSTotOtrosImp>() { new LRSTotOtrosImp() { codImp = 14, totMntImp = 123456789123456789 } };
            totImpSinCredito = 123456789123456789;
            totOpIVARetTotal = 1234567891;
            totIVARetTotal = 123456789123456789;
            totOpIVARetParcial = 1234567891;
            totIVARetParcial = 123456789123456789;
            totCredEC = 123456789123456789;
            totDepEnvase = 123456789123456789;
            totLiquidaciones = new LTotLiquidaciones() { totValComNeto = 123456789132456789, totValComExe = 123456789123456789, totValComIVA = 123456789123456789 };
            totMntTotal = 123456789123456789;
            totOpIVANoRetenido = 1234567891;
            totIVANoRetenido = 123456789123456789;
            totMntNoFact = 123456789123456789;
            totMntPeriodo = 123456789123456789;
            totPsjNac = 123456789123456789;
            totPsjInt = 123456789123456789;
            totTabPuros = 123456789123456789;
            totTabCigarrillos = 123456789123456789;
            totTabElaborado = 123456789123456789;
        }
        public XElement getXEl()
        {
            XElement t = new XElement("TotalesSegmento",
                            new XElement("TpoDoc", tpoDoc),
                  _compra ? new XElement("TpoImp", tpoImp) : null,
                            new XElement("TotDoc", totDoc),
                            new XElement("TotAnulado", totAnulado),
                            new XElement("TotOpExe", totOpExe),
                            new XElement("TotMntExe", totMntExe),
                            new XElement("TotMntNeto", totMntNeto),
                  _compra ? new XElement("TotOpIVARec", totOpIVARec) : null,
                            new XElement("TotMntIVA", totMntIVA),
                  _compra ? new XElement("TotOpActivoFijo", totOpActivoFijo) : null,
                  _compra ? new XElement("TotMntActivoFijo", totMntActivoFijo) : null,
                  _compra ? new XElement("TotMntIVAActivoFijo", totMntIVAActivoFijo) : null,
                  _compra ? from totiva in totIVANoRec select totiva.getXEl() : null,
                  _compra ? new XElement("TotOpIVAUsoComun", totOpIVAUsoComun) : null,
                  _compra ? new XElement("TotIVAUsoComun", totIVAUsoComun) : null,
                   _venta ? new XElement("TotIVAFueraPlazo", totIVAFueraPlazo) : null,
                   _venta ? new XElement("TotIVAPropio", totIVAPropio) : null,
                   _venta ? new XElement("TotIVATerceros", totIVATerceros) : null,
                   _venta ? new XElement("TotLey18211", totLey18211) : null,
                            from tototrimp in totOtrosImp select tototrimp.getXEl(),
                  _compra ? new XElement("TotImpSinCredito", totImpSinCredito) : null,
                   _venta ? new XElement("TotOpIVARetTotal", totOpIVARetTotal) : null,
                   _venta ? new XElement("TotIVARetTotal", totIVARetTotal) : null,
                   _venta ? new XElement("TotOpIVARetParcial", totOpIVARetParcial) : null,
                   _venta ? new XElement("TotIVARetParcial", totIVARetParcial) : null,
                   _venta ? new XElement("TotCredEC", totCredEC) : null,
                   _venta ? new XElement("TotDepEnvase", totDepEnvase) : null,
                   _venta && (totLiquidaciones != null) ? totLiquidaciones.getXEl() : null,
                            new XElement("TotMntTotal", totMntTotal),
                   _venta ? new XElement("TotOpIVANoRetenido", totOpIVANoRetenido) : null,
                   _venta ? new XElement("TotIVANoRetenido", totIVANoRetenido) : null,
                   _venta ? new XElement("TotMntNoFact", totMntNoFact) : null,
                   _venta ? new XElement("TotMntPeriodo", totMntPeriodo) : null,
                   _venta ? new XElement("TotPsjNac", totPsjNac) : null,
                   _venta ? new XElement("TotPsjInt", totPsjInt) : null,
                  _compra ? new XElement("TotTabPuros", totTabPuros) : null,
                  _compra ? new XElement("TotTabCigarrillos", totTabCigarrillos) : null,
                  _compra ? new XElement("TotTabElaborado", totTabElaborado) : null
                );
            return t;
        }
    }
    class LResumenPeriodo //Resumen Periodo se utiliza cuando se envían libros Finales sin segmentación.
    {
        public Ledger libro;
        public List<LTotalesPeriodo> totalesPeriodo { get; set; } //Con un máximo de 40.
        public LResumenPeriodo(Ledger lib)
        {
            this.libro = lib;

        }
        public void testdata()
        {
            totalesPeriodo = new List<LTotalesPeriodo>() { new LTotalesPeriodo(this.libro) };
        }
        public void setData()
        {

        }
        public XElement getXEl()
        {
            if (totalesPeriodo == null || totalesPeriodo.Count == 0) return null; //si no hay segmentos

            XElement t = new XElement("ResumenPeriodo",
                           from totseg in totalesPeriodo
                           select totseg.getXEl()
                );
            return t;
        }
    }
    class LTotalesPeriodo
    {
        public Ledger libro;
        public bool _compra;
        public bool _venta;

        public int tpoDoc { get; set; }
        public int tpoImp { get; set; }
        public long totDoc { get; set; }
        public long totAnulado { get; set; }
        public long totOpExe { get; set; }
        public long totMntExe { get; set; }
        public long totMntNeto { get; set; }
        public long totOpIVARec { get; set; }
        public long totMntIVA { get; set; }
        public long totOpActivoFijo { get; set; }
        public long totMntActivoFijo { get; set; }
        public long totMntIVAActivoFijo { get; set; }
        public List<LTotIVANoRec> totIVANoRec { get; set; } //Tabla de Iva No recuperable permite hasta 5 repeticiones. De no haber IVA No Recuperable no se generan los TAGs
        public long totOpIVAUsoComun { get; set; }
        public decimal fctProp { get; set; } //2 Enteros 3 decimales
        public long totCredIVAUsoComun { get; set; }
        public long totIVAFueraPlazo { get; set; }
        public long totIVAPropio { get; set; }
        public long totIVATerceros { get; set; }
        public long totLey18211 { get; set; }
        public List<LRPTotOtrosImp> totOtrosImp { get; set; } //Tabla De Otros Impuesto permite hasta 20 repeticiones. De no haber otros impuestos no se generan los TAGs                                             
        public long totImpSinCredito { get; set; }
        public long totOpIVARetTotal { get; set; }
        public long totIVARetTotal { get; set; }
        public long totOpIVARetParcial { get; set; }
        public long totIVARetParcial { get; set; }
        public long totCredEC { get; set; }
        public long totDepEnvase { get; set; }
        public LTotLiquidaciones totLiquidaciones { get; set; }
        public long totMntTotal { get; set; }
        public long totOpIVANoRetenido { get; set; }
        public long totMntNoFact { get; set; }
        public long totMntPeriodo { get; set; }
        public long totPsjNac { get; set; }
        public long totPsjInt { get; set; }
        public long totTabPuros { get; set; }
        public long totTabCigarrillos { get; set; }
        public long totTabElaborado { get; set; }
        public long totImpVehiculo { get; set; }
        public LTotalesPeriodo(Ledger libro)
        {
            this.libro = libro;
            _compra = this.libro.tipo == LedgerType.COMPRA;
            _venta = !_compra;

            testdata();
        }
        public void testdata()
        {
            tpoDoc = 30;
            tpoImp = 1;
            totDoc = 1234567891;
            totAnulado = 1234567891;
            totOpExe = 1234567891;
            totMntExe = 123465789123456789;
            totMntNeto = 123456789123456789;
            totOpIVARec = 1234567891;
            totMntIVA = 123456789123456789;
            totOpActivoFijo = 1234567891;
            totMntActivoFijo = 123456789123456789;
            totMntIVAActivoFijo = 123456789123456789;
            totIVANoRec = new List<LTotIVANoRec>() { new LTotIVANoRec() { codIVANoRec = 1, totOpIVANoRec = 1234567891, totMntIVANoRec = 123456789123456789 } };
            totOpIVAUsoComun = 1234567891;
            totIVAFueraPlazo = 123456789123456789;
            totIVAPropio = 123456789123456789;
            totIVATerceros = 123456789123456789;
            totLey18211 = 123456789123456789;
            totOtrosImp = new List<LRPTotOtrosImp>() { new LRPTotOtrosImp(libro) { codImp = 14, totMntImp = 123456789123456789 } };
            totImpSinCredito = 123456789123456789;
            totOpIVARetTotal = 1234567891;
            totIVARetTotal = 123456789123456789;
            totOpIVARetParcial = 1234567891;
            totIVARetParcial = 123456789123456789;
            totCredEC = 123456789123456789;
            totDepEnvase = 123456789123456789;
            totLiquidaciones = new LTotLiquidaciones() { totValComNeto = 123456789132456789, totValComExe = 123456789123456789, totValComIVA = 123456789123456789 };
            totMntTotal = 123456789123456789;
            totOpIVANoRetenido = 1234567891;
            totMntNoFact = 123456789123456789;
            totMntPeriodo = 123456789123456789;
            totPsjNac = 123456789123456789;
            totPsjInt = 123456789123456789;
            totTabPuros = 123456789123456789;
            totTabCigarrillos = 123456789123456789;
            totTabElaborado = 123456789123456789;
        }
        public XElement getXEl()
        {

            XElement t = new XElement("TotalesPeriodo",
                            new XElement("TotDoc", totDoc),
                  _compra ? new XElement("TpoImp", tpoImp) : null,
                            new XElement("TotAnulado", totAnulado),
                            new XElement("TotOpExe", totOpExe),
                            new XElement("TotMntExe", totMntExe),
                            new XElement("TotMntNeto", totMntNeto),
                  _compra ? new XElement("TotOpIVARec", totOpIVARec) : null,
                            new XElement("TotMntIVA", totMntIVA),
                  _compra ? new XElement("TotOpActivoFijo", totOpActivoFijo) : null,
                  _compra ? new XElement("TotMntActivoFijo", totMntActivoFijo) : null,
                  _compra ? new XElement("TotMntIVAActivoFijo", totMntIVAActivoFijo) : null,
                  _compra ? new XElement("TotIVANoRec", totIVANoRec) : null,
                  _compra ? new XElement("TotOpIVAUsoComun", totOpIVAUsoComun) : null,
                  _compra ? new XElement("FctProp", fctProp.ToString("00.000")) : null,
                  _compra ? new XElement("TotCredIVAUsoComun", totCredIVAUsoComun) : null,
                   _venta ? new XElement("TotIVAFueraPlazo", totIVAFueraPlazo) : null,
                   _venta ? new XElement("TotIVAPropio", totIVAPropio) : null,
                   _venta ? new XElement("TotIVATerceros", totIVATerceros) : null,
                   _venta ? new XElement("TotLey18211", totLey18211) : null,
                            from tototrimp in totOtrosImp select tototrimp.getXEl(),
                  _compra ? new XElement("TotImpSinCredito", totImpSinCredito) : null,
                   _venta ? new XElement("TotOpIVARetTotal", totOpIVARetTotal) : null,
                   _venta ? new XElement("TotIVARetTotal", totIVARetTotal) : null,
                   _venta ? new XElement("TotOpIVARetParcial", totOpIVARetParcial) : null,
                   _venta ? new XElement("TotIVARetParcial", totIVARetParcial) : null,
                   _venta ? new XElement("TotCredEC", totCredEC) : null,
                   _venta ? new XElement("TotDepEnvase", totDepEnvase) : null,
                   _venta && (totLiquidaciones != null) ? totLiquidaciones.getXEl() : null,
                            new XElement("TotMntTotal", totMntTotal),
                   _venta ? new XElement("TotOpIVANoRetenido", totOpIVANoRetenido) : null,
                   _venta ? new XElement("TotMntNoFact", totMntNoFact) : null,
                   _venta ? new XElement("TotMntPeriodo", totMntPeriodo) : null,
                   _venta ? new XElement("TotPsjNac", totPsjNac) : null,
                   _venta ? new XElement("TotPsjInt", totPsjInt) : null,
                  _compra ? new XElement("TotTabPuros", totTabPuros) : null,
                  _compra ? new XElement("TotTabCigarrillos", totTabCigarrillos) : null,
                  _compra ? new XElement("TotTabElaborado", totTabElaborado) : null,
                  _compra ? new XElement("TotImpVehiculo", totImpVehiculo) : null
                );
            return t;
        }
    }
    class LTotLiquidaciones
    {
        public long totValComNeto { get; set; }
        public long totValComExe { get; set; }
        public long totValComIVA { get; set; }
        public XElement getXEl()
        {
            XElement t = new XElement("TotLiquidaciones",
                           new XElement("TotValComNeto", totValComNeto),
                           new XElement("TotValComExe", totValComExe),
                           new XElement("TotValComIVA", totValComIVA)
                );
            return t;
        }
    }
    class LRSTotOtrosImp
    {
        public short codImp { get; set; }
        public long totMntImp { get; set; }
        public XElement getXEl()
        {
            XElement t = new XElement("TotOtrosImp",
                           new XElement("CodImp", codImp),
                           new XElement("TotMntImp", totMntImp)
                );
            return t;
        }
    }
    class LRPTotOtrosImp
    {
        public Ledger libro;
        public bool _compra;
        public bool _venta;
        // ResumenPeriodo /TotalesPeriodo
        //Tabla De Otros Impuesto permite hasta 20 repeticiones. De no haber otros impuestos no se generan los TAGs
        public short codImp { get; set; }
        public long totMntImp { get; set; }
        public decimal fctImpAdic { get; set; } //2 Enteros 3 decimales
        public long totCredImp { get; set; }
        public LRPTotOtrosImp(Ledger libro)
        {
            this.libro = libro;
            _compra = libro.tipo == LedgerType.COMPRA;
            _venta = !_compra;
        }
        public XElement getXEl()
        {
            XElement t = new XElement("TotOtrosImp",
                           new XElement("CodImp", codImp),
                           new XElement("TotMntImp", totMntImp),
                 _compra ? new XElement("FctImpAdic", fctImpAdic) : null,
                 _compra ? new XElement("TotCredImp", totCredImp) : null
                );
            return t;
        }
    }
    class LTotIVANoRec
    {
        //Tabla de Iva No recuperable permite hasta 5 repeticiones. De no haber IVA No Recuperable no se generan los TAGs
        public int codIVANoRec { get; set; }
        public long totOpIVANoRec { get; set; }
        public long totMntIVANoRec { get; set; }
        public XElement getXEl()
        {
            XElement t = new XElement("TotIVANoRec",
                            new XElement("CodIVANoRec", codIVANoRec),
                            new XElement("TotOpIVANoRec", totOpIVANoRec),
                            new XElement("TotMntIVANoRec", totMntIVANoRec)
                );
            return t;
        }
    }
    class LDetalle
    {
        public Ledger libro;
        public bool _compra;
        public bool _venta;

        public short tpoDoc { get; set; }
        public short emisor { get; set; }
        public short indFactCompra { get; set; }
        public int nroDoc { get; set; }
        public string anulado { get; set; }
        public short operacion { get; set; }
        public short tpoImp { get; set; }
        public decimal tasaImp { get; set; }
        public string numInt { get; set; }
        public short indServicio { get; set; }
        public short indSinCosto { get; set; }
        public string fchDoc { get; set; }
        public int cdgSIISucur { get; set; }
        public string rUTDoc { get; set; }
        public string rznSoc { get; set; }
        public LExtranjero extranjero { get; set; }//Solo Aplica a Clientes Extranjeros 
        public short tpoDocRef { get; set; }
        public int folioDocRef { get; set; }
        public long mntExe { get; set; }
        public long mntNeto { get; set; }
        public long mntIVA { get; set; }
        public long mntActivoFijo { get; set; }
        public long MntIVAActivoFijo { get; set; }
        public List<LIVANoRec> iVANoRec { get; set; }//Tabla de Iva No recuperable permite hasta 5 repeticiones. De no haber IVA No Recuperable no se generan los TAGs. De informarlos deben coincidir con los declarados en el resumen
        public long iVAUsoComun { get; set; }
        public long iVAFueraPlazo { get; set; }
        public long iVAPropio { get; set; }
        public long iVATerceros { get; set; }
        public long ley18211 { get; set; }
        public List<LOtrosImp> otrosImp { get; set; } //Tabla De Otros Impuesto permite hasta 20 repeticiones. De no haber otros impuestos no se generan los TAGs. De informarlos deben coincidir con los declarados en el resumen
        public long mntSinCred { get; set; }
        public long iVARetTotal { get; set; }
        public long iVARetParcial { get; set; }
        public long credEC { get; set; }
        public long depEnvase { get; set; }
        public List<LLiquidaciones> liquidaciones { get; set; }  //Se coloca este subconjunto solamente si hay liquidaciones. De lo contrario no se deben colocar los TAGs. De informarlos deben coincidir con los declarados en el resumen	
        public long mntTotal { get; set; }
        public long iVANoRetenido { get; set; }
        public long mntNoFact { get; set; }
        public long mntPeriodo { get; set; }
        public long psjNac { get; set; }
        public long psjInt { get; set; }
        public long tabPuros { get; set; }
        public long tabCigarrillos { get; set; }
        public long tabElaborado { get; set; }
        public long impVehiculo { get; set; }

        public LDetalle(Ledger libro, TaxDocument documento)
        {
            this.libro = libro;
            _compra = libro.tipo == LedgerType.COMPRA;
            _venta = !_compra;
        }
        public void testdata()
        {
            tpoDoc = 30;
            emisor = 1;
            indFactCompra = 1;
            nroDoc = 1234567891;
            anulado = "A";
            operacion = 1;
            tpoImp = 1;
            tasaImp = 0;
            numInt = "aaaaaaaaaa";
            indServicio = 1;
            indSinCosto = 1;
            fchDoc = "1967-08-13";
            cdgSIISucur = 12345678;
            rUTDoc = "0-0";
            rznSoc = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            extranjero = new LExtranjero() { numId = "a", nacionalidad = "a" };
            tpoDocRef = 30;
            folioDocRef = 1234567891;
            mntExe = 123456789123456789;
            mntNeto = 123456789123456789;
            mntIVA = 123456789123456789;
            mntActivoFijo = 123456789123456789;
            MntIVAActivoFijo = 123456789123456789;
            iVANoRec = new List<LIVANoRec> { new LIVANoRec() { codIVANoRec = 1, mntIVANoRec = 123456789123456789 } };
            iVAUsoComun = 123456789123456789;
            iVAFueraPlazo = 123456789123456789;
            iVAPropio = 123456789123456789;
            iVATerceros = 123456789123456789;
            ley18211 = 123456789123456789;
            otrosImp = new List<LOtrosImp> { new LOtrosImp() { codImp = 14, tasaImp = 0, mntImp = 123456789123456789 } };
            mntSinCred = 123456789123456789;
            iVARetTotal = 123456789123456789;
            iVARetParcial = 123456789123456789;
            credEC = 123456789123456789;
            depEnvase = 123456789123456789;
            liquidaciones = new List<LLiquidaciones> { new LLiquidaciones() { rutEmisor = "0-0", valComNeto = 123456789123456789, valComExe = 123456789123456789, valComIVA = 123456789123456789 } };
            mntTotal = 123456789123456789;
            iVANoRetenido = 123456789123456789;
            mntNoFact = 123456789123456789;
            mntPeriodo = 123456789123456789;
            psjNac = 123456789123456789;
            psjInt = 123456789123456789;
            tabPuros = 123456789123456789;
            tabCigarrillos = 123456789123456789;
            tabElaborado = 123456789123456789;
            impVehiculo = 123456789123456789;
        }
        public void setData()
        {

        }
        public XElement getXEl()
        {
            XElement t = new XElement("Detalle",
                            new XElement("TpoDoc", tpoDoc),
                  _compra ? new XElement("Emisor", emisor) : null,
                  _compra ? new XElement("IndFactCompra", indFactCompra) : null,
                            new XElement("NroDoc", nroDoc),
                            new XElement("Anulado", anulado),
                            new XElement("Operacion", operacion),
                  _compra ? new XElement("TpoImp", tpoImp) : null,
                            new XElement("TasaImp", tasaImp),
                            new XElement("NumInt", numInt),
                   _venta ? new XElement("IndServicio", indServicio) : null,
                   _venta ? new XElement("IndSinCosto", indSinCosto) : null,
                            new XElement("FchDoc", fchDoc),
                            new XElement("CdgSIISucur", cdgSIISucur),
                            new XElement("RUTDoc", rUTDoc),
                            new XElement("RznSoc", rznSoc),
                   _venta ? extranjero.getXEl() : null,
                   _venta ? new XElement("TpoDocRef", tpoDocRef) : null,
                   _venta ? new XElement("FolioDocRef", folioDocRef) : null,
                            new XElement("MntExe", mntExe),
                            new XElement("MntNeto", mntNeto),
                            new XElement("MntIVA", mntIVA),
                  _compra ? new XElement("mntActivoFijo", mntActivoFijo) : null,
                  _compra ? new XElement("MntIVAActivoFijo", MntIVAActivoFijo) : null,
                  _compra ? from ivn in iVANoRec select ivn.getXEl() : null,
                  _compra ? new XElement("IVAUsoComun", iVAUsoComun) : null,
                   _venta ? new XElement("IVAFueraPlazo", iVAFueraPlazo) : null,
                   _venta ? new XElement("IVAPropio", iVAPropio) : null,
                   _venta ? new XElement("IVATerceros", iVATerceros) : null,
                            new XElement("Ley18211", ley18211),
                            from otr in otrosImp select otr.getXEl(),
                  _compra ? new XElement("MntSinCred", mntSinCred) : null,
                   _venta ? new XElement("IVARetTotal", iVARetTotal) : null,
                   _venta ? new XElement("IVARetParcial", iVARetParcial) : null,
                   _venta ? new XElement("CredEC", credEC) : null,
                   _venta ? new XElement("DepEnvase", depEnvase) : null,
                   _venta ? from liq in liquidaciones select liq.getXEl() : null,
                            new XElement("MntTotal", mntTotal),
                            new XElement("IVANoRetenido", iVANoRetenido),
                   _venta ? new XElement("MntNoFact", mntNoFact) : null,
                   _venta ? new XElement("MntPeriodo", mntPeriodo) : null,
                   _venta ? new XElement("PsjNac", psjNac) : null,
                   _venta ? new XElement("PsjInt", psjInt) : null,
                  _compra ? new XElement("TabPuros", tabPuros) : null,
                  _compra ? new XElement("TabCigarrillos", tabCigarrillos) : null,
                  _compra ? new XElement("TabElaborado", tabElaborado) : null,
                  _compra ? new XElement("ImpVehiculo", impVehiculo) : null
                );
            return t;
        }
    }

    class LExtranjero
    {
        public string numId { get; set; }
        public string nacionalidad { get; set; }
        public XElement getXEl()
        {
            XElement t = new XElement("Extranjero",
                            new XElement("NumId", numId),
                            new XElement("Nacionalidad", nacionalidad)
                );
            return t;
        }
    }
    class LIVANoRec
    {
        public short codIVANoRec { get; set; }
        public long mntIVANoRec { get; set; }
        public XElement getXEl()
        {
            XElement t = new XElement("IVANoRec",
                            new XElement("CodIVANoRec", codIVANoRec),
                            new XElement("MntIVANoRec", mntIVANoRec)
                );
            return t;
        }
    }
    class LOtrosImp
    {
        public short codImp { get; set; }
        public decimal tasaImp { get; set; }
        public long mntImp { get; set; }

        public XElement getXEl()
        {
            XElement t = new XElement("OtrosImp",
                            new XElement("CodImp", codImp),
                            new XElement("TasaImp", tasaImp),
                            new XElement("MntImp", mntImp)
                );
            return t;
        }
    }
    class LLiquidaciones
    {
        public string rutEmisor { get; set; }
        public long valComNeto { get; set; }
        public long valComExe { get; set; }
        public long valComIVA { get; set; }

        public XElement getXEl()
        {
            XElement t = new XElement("Liquidaciones",
                            new XElement("rutEmisor", rutEmisor),
                            new XElement("valComNeto", valComNeto),
                            new XElement("valComExe", valComExe),
                            new XElement("valComIVA", valComIVA)
                );
            return t;
        }
    }
}
