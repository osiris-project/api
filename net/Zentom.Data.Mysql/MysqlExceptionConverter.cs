﻿using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using NHibernate.Exceptions;
using Zentom.Data.Exceptions;

namespace Zentom.Data.Mysql
{
    public class MysqlExceptionConverter
        : ISQLExceptionConverter
    {
        public IViolatedConstraintNameExtracter ConstraintExtractor { get; set; }

        private static readonly string ValueExtractor = @"Duplicate entry '(.*)' for key '(.*)'";

        public MysqlExceptionConverter(IViolatedConstraintNameExtracter constraintExtractor)
        {
            ConstraintExtractor = constraintExtractor;
        }

        public Exception Convert(AdoExceptionContextInfo context)
        {
            var sqlException = ADOExceptionHelper.ExtractDbException(context.SqlException) as SqlException;

            if (sqlException != null)
            {
                switch (sqlException.Number)
                {
                    case 1062:
                        var extractedValues = ExtractValueFromExceptionMessage(sqlException.Message);
                        return new UniqueKeyException(sqlException, extractedValues[0], extractedValues[1]);
                }
            }

            return SQLStateConverter.HandledNonSpecificException(context.SqlException,
                            context.Message, context.Sql);
        }

        private static string[] ExtractValueFromExceptionMessage(string exceptionMessage)
        {
            var result = new[] { "", "" };

            if (!string.IsNullOrEmpty(exceptionMessage))
            {
                var match = Regex.Match(exceptionMessage, ValueExtractor, RegexOptions.Compiled);

                if (!match.Success) return result;

                result[0] = match.Groups[1].Value;
                result[1] = match.Groups[2].Value;
            }

            return result;
        }
    }
}
