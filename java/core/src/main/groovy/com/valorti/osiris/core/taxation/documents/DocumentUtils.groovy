/**
 * Copyright (c) Mar 10, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DocumentUtils {

    /**
     * Checks if a document type is a debit note.
     *
     * @param code The document type
     * @return true if it is, false otherwise.
     */
    static boolean isDebitNote(String code) {
        switch(code) {
            // Manual
            case '55':
            // Electronic
            case '56':
            // Exports
            case '111':
                return true
        }
        return false
    }

    /**
     * Checks if the document is a credit note.
     *
     * @param code Document type
     * @return true if it is, false otherwise.
     */
    static boolean isCreditNote(String code) {
        switch(code) {
            case '60':
            case '61':
            case '':
                return true
        }
        return false
    }

    /**
     * Checks if this is a debit or credit note, without
     * taking into account if it's electronic, manual or
     * for exports.
     *
     * @param code The document type code.
     * @return true if it's a note, false otherwise
     */
    static boolean isNote(String code){
        return isDebitNote(code) || isCreditNote(code)
    }
}
