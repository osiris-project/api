﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Criterion.Lambda;
using NHibernate.Transform;
using Zentom.Core.Entities;

namespace Zentom.Nhibernate.Tests
{
    public static class QueryOverHelper
    {
        private const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

        private static readonly ILog Log = LogManager.GetLogger(typeof(QueryOverHelper));

        public static bool Exists<T>(this ISession session, Expression<Func<T, bool>> whereExpression)
            where T : Entity
        {
            return session.QueryOver<T>()
                .Where(whereExpression)
                .Take(1)
                .SingleOrDefault() != null;
        }

        /// <summary>
        /// Projects and transforms the root object (TRoot) into the output object (TOut).
        /// </summary>
        /// <typeparam name="TRoot">The base object that's going to be transformed.</typeparam>
        /// <typeparam name="TSub">Sub type object of the query.</typeparam>
        /// <typeparam name="TOut">The resulting object.</typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TSub> ProjectInto<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>();
        }

        /// <summary>
        /// Internally it calls <see cref="ProjectInto&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt; TRoot, TSub&gt;)"/>
        /// using the TRoot as TSub.
        /// </summary>
        /// <typeparam name="TRoot">The base object of the query.</typeparam>
        /// <typeparam name="TOut">The resulting object.</typeparam>
        /// <param name="query">IQueryOver object.</param>
        /// <returns>An IQueryOver object.</returns>
        public static IQueryOver<TRoot, TRoot> ProjectInto<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.ProjectInto<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Projects, transforms and executes the query, returning an instance of TOut
        /// or the default values.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static TOut ProjectToObject<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>()
                .SingleOrDefault<TOut>();
        }

        /// <summary>
        /// <see cref="ProjectToObject&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static TOut ProjectToObject<T, TOut>(this IQueryOver<T, T> query)
        {
            return query.ProjectToObject<T, T, TOut>();
        }

        /// <summary>
        /// Projects, transforms and generates an IEnumerable&lt;TOut&gt;.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns>An IEnumerable&lt;TOut&gt; on success.</returns>
        public static IEnumerable<TOut> ProjectToList<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>()
                .List<TOut>();
        }

        /// <summary>
        /// <see cref="ProjectToList&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)"/>
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IEnumerable<TOut> ProjectToList<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.ProjectToList<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Projects a TRoot into a TOut object.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        private static IQueryOver<TRoot, TSub> SelectAs<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            Console.WriteLine("Converting from [{0}] to [{1}].", typeof(TRoot), typeof(TOut));
            var projectionBuilder = new QueryOverProjectionBuilder<TRoot>();
            var toTypeProps = typeof(TOut).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var fromTypeProps = typeof(TRoot).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var toProp in toTypeProps)
            {
                var fromProp = fromTypeProps.SingleOrDefault(i => i.Name == toProp.Name);
                //Console.WriteLine("To Property: {0}", toProp);
                //Console.WriteLine("From Property: {0}", fromProp);
                if (fromProp == null)
                {
                    //Console.WriteLine("Property {0} skipped.", toProp);
                    continue;
                }

                //Console.WriteLine("Projecting property: {0}", toProp);
                projectionBuilder.Select(Projections.Property(fromProp.Name).As(toProp.Name));
            }

            query.SelectList(i => projectionBuilder);
            return query;
        }

        public static IQueryOver<TRoot, TSub> SelectAs<TRoot, TSub>(this IQueryOver<TRoot, TSub> query, Type outType)
        {
            var projection = new QueryOverProjectionBuilder<TRoot>();
            var inType = typeof(TRoot);
            var toProps = outType.GetProperties(bindingFlags);
            var inProps = inType.GetProperties(bindingFlags);
            //Console.WriteLine("ToTypeProperties: {0}", toProps.Count());
            //Console.WriteLine("FromTypeProperties: {0}", inProps.Count());

            foreach (var property in toProps)
            {
                var foundProperty = inProps.SingleOrDefault(i => i.Name == property.Name);
                //Console.WriteLine("To Property: {0}", property);
                //Console.WriteLine("From Property: {0}", foundProperty);
                if (foundProperty == null) continue;
                //Console.WriteLine("Projecting property: {0}", property);
                projection.Select(Projections.Property(foundProperty.Name).As(property.Name));
            }

            query.SelectList(i => projection);
            return query;
        }

        /// <summary>
        /// <see cref="SelectAs&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)" />
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TRoot> SelectAs<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.SelectAs<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Shorcut method to transform a TRoot into a TOut.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TSub> TransformToBean<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.TransformUsing(Transformers.AliasToBean<TOut>());
        }

        /// <summary>
        /// <see cref="TransformToBean&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)" />
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TRoot> TransformToBean<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.TransformToBean<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Applies the "Order By" clause to the query based on string containing the column names and order.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <param name="query"></param>
        /// <param name="orderBy">The order by columns and order.</param>
        /// <returns></returns>
        /// <example>An example of the orderBy string: "name:asc;lastName:asc", asuming that the entity has the columns Name and LastName,
        /// the query would be ordered by the "Name" in ascending order and then by "LastName" in ascending order.</example>
        public static IQueryOver<TRoot, TRoot> OrderBy<TRoot>(this IQueryOver<TRoot, TRoot> query, string orderBy)
        {
            Log.Debug("Entering ParseOrderBy.");
            if (string.IsNullOrWhiteSpace(orderBy))
                return query;

            Console.WriteLine("OrderBy: {0}", orderBy);
            var type = typeof(TRoot);
            var props = type.GetProperties(bindingFlags);
            try
            {
                var split = orderBy.Split(';');
                var count = 0;
                //QueryOverOrderBuilderBase<TRoot, TSub> orderBuilder;

                foreach (var element in split)
                {
                    var elementSplit = element.Split(':');
                    // Parse left value:
                    var propertyName = elementSplit[0];
                    var prop = NormalizeProperty(props, propertyName);
                    if (prop == null) continue;
                    count++;

                    var orderBuilder = count <= 0
                        ? query.OrderBy(Projections.Property(prop))
                        : query.ThenBy(Projections.Property(prop));

                    // Set order
                    var asc = true;

                    if (elementSplit.Length == 2)
                    {
                        asc = elementSplit[1].ToLower() == "asc";
                    }
                    query = asc ? orderBuilder.Asc() : orderBuilder.Desc();

                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to apply order to the query.", ex);
            }

            Log.Debug("Leaving ParseOrderBy.");
            return query;
        }

        private static PropertyInfo StringToProperty(PropertyInfo[] properties, string toParse)
        {
            Log.Debug("Entering StringToProperty");
            if (properties == null)
                return null;
            if (string.IsNullOrWhiteSpace(toParse))
                return null;

            PropertyInfo result = null;
            var currentProperties = properties;
            try
            {
                var split = toParse.Split('.');
                Console.WriteLine("Parsing: {0}", toParse);

                foreach (var propertyName in split)
                {
                    var currentProperty = currentProperties.SingleOrDefault(i => string.Equals(i.Name, propertyName, StringComparison.InvariantCultureIgnoreCase));
                    Console.WriteLine("CurrentProperty: {0}", currentProperty);
                    if (currentProperty == null)
                        break;
                    result = currentProperty;
                    Console.WriteLine("ReflectedType: {0}", currentProperty.ReflectedType);
                    if (currentProperty.ReflectedType == null) return currentProperty;
                    currentProperties = currentProperty.ReflectedType.GetProperties(bindingFlags);
                }
            }
            catch (Exception ex)
            {
                Log.Error("String to Property failed.", ex);
            }

            Log.Debug("Leaving StringToProperty");
            return result;
        }

        private static string NormalizeProperty(IReadOnlyCollection<PropertyInfo> properties, string toParse)
        {
            Log.Debug("Entering NormalizeProperty");
            if (properties == null)
                return null;
            if (string.IsNullOrWhiteSpace(toParse))
                return null;

            string result = null;
            var currentProperties = properties;
            try
            {
                var split = toParse.Split('.');
                Console.WriteLine("Parsing: {0}", toParse);
                var isFirst = true;

                foreach (var propertyName in split)
                {
                    var currentProperty = currentProperties.SingleOrDefault(i => string.Equals(i.Name, propertyName, StringComparison.InvariantCultureIgnoreCase));
                    Console.WriteLine("CurrentProperty: {0}", currentProperty);

                    if (currentProperty == null)
                    {
                        break;
                    }

                    var current = currentProperty.Name;
                    Console.WriteLine("Current Property: {0}", current);

                    if (currentProperty.ReflectedType == null)
                    {
                        Console.WriteLine("No reflected type. IsFirst: {0}", isFirst);

                        if (isFirst)
                        {
                            result = current;
                            break;
                        }
                    }
                    else
                    {
                        Log.Debug("Reflected type not null.");
                        if (isFirst && currentProperty.PropertyType.BaseType == typeof(Entity))
                        {
                            current = char.ToLowerInvariant(current[0]) + current.Substring(1);
                        }
                        currentProperties = currentProperty.ReflectedType.GetProperties(bindingFlags);
                    }

                    result = isFirst ? current : string.Concat(result, ".", current);
                    Console.WriteLine("Current result: {0}", result);
                    isFirst = false;
                }
            }
            catch (Exception ex)
            {
                Log.Error("String to Property failed.", ex);
            }

            Console.WriteLine("Result: {0}", result);
            Log.Debug("Leaving StringToProperty");
            return result;
        }
    }
}