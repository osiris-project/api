﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class ReceivedTaxDocumentWeakReferenceMap
        : ClassMap<ReceivedTaxDocumentWeakReference>
    {
        public ReceivedTaxDocumentWeakReferenceMap()
        {
            Schema("taxation");
            Table("received_taxdoc_weak_reference");
            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Date);
            Map(i => i.DocumentType);
            Map(i => i.Folio);
            Map(i => i.Reason);

            References(i => i.ReceivedTaxDocument, "received_document_id")
                .Not.Nullable()
                .Cascade.SaveUpdate();
        }
    }
}
