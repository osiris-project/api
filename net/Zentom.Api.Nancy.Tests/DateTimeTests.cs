﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Zentom.Api.Nancy.Tests
{
    [TestClass]
    public class DateTimeTests
    {
        [TestMethod]
        public void Test()
        {
            Console.WriteLine("Date(0): {0}", new DateTime(0));
            var utc = DateTime.UtcNow;
            Console.WriteLine("Utc: {0}", utc);
            var culture = CultureInfo.CreateSpecificCulture("es-ES");
            Thread.CurrentThread.CurrentCulture = culture;
            Console.WriteLine("es_ES date: {0}", DateTime.Now.ToLocalTime());
            culture = CultureInfo.CreateSpecificCulture("es-CL");
            Thread.CurrentThread.CurrentCulture = culture;
            Console.WriteLine("es_CL date: {0}", DateTime.Now.ToLocalTime());
        }
    }
}
