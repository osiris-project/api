﻿namespace Osiris.Api.Modules.TenantEp
{
    public class FolioRangeAlertDto
    {
        public string DocumentType { get; set; }

        public long FoliosLeft { get; set; } 
    }
}
