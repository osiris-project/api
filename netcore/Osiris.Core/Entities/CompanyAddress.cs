﻿using System.Collections.Generic;

namespace Osiris.Core.Entities
{
    public abstract class CompanyAddress
        : Address
    {
        #region Fields

        private Company company;

        private IEnumerable<CompanyAddressType> companyAddressTypes;

        #endregion

        #region Properties

        protected virtual Company Company
        {
            get { return company; }
            set { company = value; }
        }

        protected virtual IEnumerable<CompanyAddressType> CompanyAddressTypes
        {
            get { return companyAddressTypes; }
            set { companyAddressTypes = value; }
        }

        #endregion

        protected CompanyAddress() { }

        protected CompanyAddress(string line1, string line2, Commune commune, Company company,
            IEnumerable<CompanyAddressType> companyAddressTypes)
            : base(line1, line2, commune)
        {
            this.company = company;
            this.companyAddressTypes = companyAddressTypes;
        }
    }
}
