﻿using System;

namespace Zentom.Core.Interfaces.Data
{
    public interface IAuditable
    {
        /// <summary>
        /// The time that the instance was created.
        /// </summary>
        DateTime CreatedAt { get; set; }

        /// <summary>
        /// Id of this instance's creator.
        /// </summary>
        object CreatedBy { get; set; }

        /// <summary>
        /// A date when the instance was updated.
        /// </summary>
        DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Id of the entity that updated the instance.
        /// </summary>
        object UpdatedBy { get; set; }
    }
}
