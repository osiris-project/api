﻿namespace Osiris.Api.Modules.ProductEp
{
    public class ProductDto
        : Command
    {
        public string Name { get; set; }

        public decimal UnitPrice { get; set; }

        public bool IsDiscontinued { get; set; }
    }
}
