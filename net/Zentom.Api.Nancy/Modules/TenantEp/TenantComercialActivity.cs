﻿using System;

namespace Zentom.Api.Modules.TenantEp
{
    public class TenantComercialActivityDto
    {
        public Guid Id { get; set; } 

        public string Name { get; set; }
    }
}