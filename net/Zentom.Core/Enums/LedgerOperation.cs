﻿namespace Zentom.Core.Enums
{
    public enum LedgerOperation
    {
        Purchase,
        Sale
    }
}
