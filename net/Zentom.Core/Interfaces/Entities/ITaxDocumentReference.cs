﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocumentReference
        : IVersioned
    {
        string ReferenceReason { get; set; }

        string ReferenceComment { get; set; }

        ITaxDocument Parent { get; set; }

        ITaxDocument ReferencedDocument { get; set; }
    }

    public interface ITaxDocumentReferenceId
    {
        ITaxDocument Parent { get; set; }

        ITaxDocument ReferencedDocument { get; set; }
    }
}
