﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Mail;

namespace Rnwood.SmtpServer.Tests
{
    [TestClass]
    public class ClientTests
    {
        [TestMethod]
        public void SmtpClient_NonSSL()
        {
            SmtpClient client = new SmtpClient("localhost", 25);
            client.Send("from@from.com", "to@to.com", "subject", "body");
        }
    }
}