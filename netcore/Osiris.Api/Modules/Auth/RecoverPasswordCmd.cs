﻿namespace Osiris.Api.Modules.Auth
{
    public class RecoverPasswordCmd
    {
        public string Login { get; set; }

        public string ReturnUri { get; set; }

        public override string ToString()
        {
            return $"Login: {Login}, ReturnUri: {ReturnUri}";
        }
    }
}
