﻿using System;
using log4net;
using Nancy.ModelBinding;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Modules.EnterpriseEp;
using Zentom.Api.Security;
using Zentom.Core.Entities;
using Zentom.Core.Pagination;

namespace Zentom.Api.Modules.SectorEp
{
    public class SectorModule
        : TenantApi
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SectorModule));

        private static int callCount;

        private string orderByList;

        public SectorModule(ISessionFactory factory)
            : base("sectors", factory)
        {
            this.Get(BasePath, List);
            this.Post<SectorCmd>(BasePath, Create);
            callCount = 0;
            orderByList = "";
        }

        [Permission(RequiresLogin = true)]
        public object List(dynamic args)
        {
            var bound = this.BindTo(new SectorQuery());
            log.DebugFormat("SectorQuery: {0}", bound);
            var query = db.QueryOver<Sector>();

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.Name + "%");
            }

            if (!string.IsNullOrWhiteSpace(bound.Code))
            {
                query.WhereRestrictionOn(i => i.Code)
                    .IsInsensitiveLike("%" + bound.Code + "%");
            }

            IPagedResult<SectorDto> items;

            if (!bound.EconomicGroupsOnly)
            {
                if (bound.OnlyExemptSectors)
                {
                    query.Where(i => i.IsSubjectToTax == false);
                }
                items = GetSectors(query);
            }
            else
            {
                items = GetEconomicGroups(query);
            }

            return Negotiate.WithOkAndList(items, Request.Url, orderByList);
        }

        [Permission(RequiresLogin = true)]
        public object Create(SectorCmd cmd)
        {
            var query = db.QueryOver<Sector>()
                .Where(i => i.Name == cmd.Name && i.Code == cmd.Code);

            if (cmd.Parent != Guid.Empty)
            {
                query.Where(i => i.ParentSector.Id == cmd.Parent);
            }

            var entity = query.Take(1).SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithOk();
            }

            entity = new Sector
            {
                Name = cmd.Name,
                Code = cmd.Code,
                ParentSector = db.Get<Sector>(cmd.Parent),
                TributaryCategory = cmd.TributaryCategory
            };

            db.Save(entity);
            return Negotiate.WithPostSuccess(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        private IPagedResult<SectorDto> GetEconomicGroups(IQueryOver<Sector, Sector> query)
        {
            SectorDto dto = null;
            orderByList = "name";
            return query
                .SelectList(i => i
                    .Select(j => j.ParentSector.Name).WithAlias(() => dto.ParentSectorName)
                    .Select(j => j.ParentSector.Id).WithAlias(() => dto.ParentSectorId))
                .Where(i => i.ParentSector == null)
                .OrderBy(Settings.OrderBy)
                .ProjectInto<Sector, SectorDto>()
                .PaginateResults<Sector, SectorDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
        }

        private IPagedResult<SectorDto> GetSectors(IQueryOver<Sector, Sector> query)
        {
            callCount++;
            log.DebugFormat("Call count: {0}", callCount);
            SectorDto dto = null;
            Sector parentSector = null;
            orderByList = "name, code, parentSector.name";

            return query
                .SelectList(i => i
                    .Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.Name).WithAlias(() => dto.Name)
                    .Select(j => j.Code).WithAlias(() => dto.Code)
                    .Select(j => j.IsSubjectToTax).WithAlias(() => dto.IsSubjectToTax))
                .Where(i => i.ParentSector != null)
                .Left.JoinAlias(i => i.ParentSector, () => parentSector)
                .SelectList(i => i.Select(j => parentSector.Name).WithAlias(() => dto.ParentSectorName))
                .OrderBy(Settings.OrderBy)
                .ProjectInto<Sector, Sector, SectorDto>()
                .PaginateResults<Sector, SectorDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
        }
    }
}