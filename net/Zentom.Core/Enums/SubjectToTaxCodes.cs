﻿namespace Zentom.Core.Enums
{
    public enum SubjectToTaxCode
        : byte
    {
        //[Display(Name = "Exent", ResourceType = typeof(Text))]
        Exent = 0,
        //[Display(Name = "SubjectToTax", ResourceType = typeof(Text))]
        Subject = 1,
        //[Display(Name = "Undetermined", ResourceType = typeof(Text))]
        Undetermined = 255
    }
}