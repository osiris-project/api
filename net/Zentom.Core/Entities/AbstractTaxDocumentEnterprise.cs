﻿using System;

namespace Zentom.Core.Entities
{
    public abstract class AbstractTaxDocumentEnterprise
        : AbstractEntity
    {
        public virtual string TaxationId { get; set; }

        public virtual string LegalName { get; set; }

        public virtual string SectorName { get; set; }

        public virtual string AddressStreet { get; set; }

        public virtual string AddressAdminDivisionLevel2 { get; set; }

        public virtual string AddressAdminDivisionLevel3 { get; set; }

        public virtual TaxDocument TaxDocument { get; set; }

        public AbstractTaxDocumentEnterprise() {}
    }
}
