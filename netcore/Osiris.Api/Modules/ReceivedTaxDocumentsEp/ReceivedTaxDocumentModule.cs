﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dobelik.Utils.Identity;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Extensions.NHibernate;
using Osiris.Api.Responses;
using Osiris.Api.Security;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Pagination;
using Osiris.Core.Services;
using Slf4Net;

namespace Osiris.Api.Modules.ReceivedTaxDocumentsEp {
    public class ReceivedTaxDocumentModule : TenantApi {
        private readonly IReceivedTaxDocumentService receivedTaxDocumentService;
        private readonly INationalIdentity identity;
        private readonly IReceivedDocumentValidationService validationService;

        private readonly ILogger log = LoggerFactory.GetLogger (typeof (ReceivedTaxDocumentModule));

        private const int MAX_UPLOADS_PER_REQUEST = 10;

        public ReceivedTaxDocumentModule (ISessionFactory factory, IReceivedTaxDocumentService receivedTaxDocumentService,
            INationalIdentity identity, IReceivedDocumentValidationService validationService) : base ("received-tax-documents", factory) {
            this.receivedTaxDocumentService = receivedTaxDocumentService;
            this.identity = identity;
            this.validationService = validationService;

            // Document Reception
            this.Get (BasePath, List);
            this.Post (BasePath, ReceiveDocuments);
            this.Get<Command> (BasePath + "/{id:guid}/references/", DocumentReferences);
            this.Post<ReceivedDocumentCmd> (BasePath + "/create/", Create);
            //this.Post(BasePath + "/receive", ReceiveDocuments);

            // Document Reception Tracing
            this.Patch (BasePath + "/validate-documents/", ValidateReception);
            this.Get (BasePath + "/trace-documents/", ReceivedDocumentList);
            this.Patch<Command> (BasePath + "/{id:guid}/accept-for-accounting/", AcceptForAccounting);
        }

        [Permission (Permission = "received-document-create")]
        public Negotiator Create (ReceivedDocumentCmd cmd) {
            var document = db.QueryOver<ReceivedTaxDocument> ()
                .Where (i => i.DocumentType == cmd.DocumentType && i.Tenant == currentTenant && i.FolioNumber == cmd.FolioNumber)
                .Take (1)
                .SingleOrDefault ();

            if (document != null) {
                return Negotiate.WithDuplicateItem (new [] {
                    "documentType",
                    "folioNumber"
                });
            }
            var tenantAddress = GetTenantLegalAddress ();
            var propertyInfo = tenantAddress.GetType ();
            var address = (string) propertyInfo.GetProperty ("line1").GetValue (tenantAddress) +
                " " + (string) propertyInfo.GetProperty ("line2").GetValue (tenantAddress);
            document = new ReceivedTaxDocument {
                FolioNumber = cmd.FolioNumber,
                DocumentType = cmd.DocumentType,
                Tenant = currentTenant,
                AccountingDate = cmd.AccountingDate,
                ReceptionTime = DateTime.Now,
                Comments = "",
                FilePath = "",
                TaxRate = cmd.TaxRate,
                ExtraTaxCode = cmd.ExtraTaxCode,
                ExtraTaxRate = cmd.ExtraTaxRate,
                ProviderTaxId = identity.CleanId (cmd.Provider.TaxId),
                ProviderLegalName = cmd.Provider.LegalName,
                ProviderAddresLine1 = cmd.Provider.Address.Line1,
                ProviderAddressLine2 = cmd.Provider.Address.Line2,
                ProviderAddressCity = cmd.Provider.Address.City,
                ProviderAddressCommune = cmd.Provider.Address.Commune,
                ProviderBankAccountNumber = cmd.Provider.BankAccountNumber,
                ProviderComercialActivity = cmd.Provider.CommercialActivity.Name,
                ProviderEconomicActivity = cmd.Provider.EconomicActivity.Code,
                ProviderPhone = cmd.Provider.Phone,
                ReceptionStatus = ReceptionStatus.Received,
                ReceptorTaxId = identity.CleanId (currentTenant.TaxId),
                ReceptorLegalName = currentTenant.LegalName,
                ReceptorAddress = address.Trim (),
                ReceptorCommune = (string) propertyInfo.GetProperty ("commune").GetValue (tenantAddress),
                ReceptorComercialActivity = cmd.Tenant.CommercialActivity.Name,
                AcceptedForAccounting = true,
                Enterprise = db.Load<Enterprise> (cmd.Provider.Id),
                ExemptAmount = 0.0M,
                Voucher = null
            };

            int lineCount = 1;

            foreach (var item in cmd.Lines) {
                var line = new ReceivedTaxDocumentLine (lineCount, item.Text, document, item.Quantity, item.AdditionalGloss, item.UnitPrice, item.MeasureUnit);
                if (item.TaxType == TaxType.Exempt) {
                    document.ExemptAmount += line.NetAmount;
                } else {
                    document.SubjectAmount += line.NetAmount;
                }
                document.TaxAmount += line.TotalTaxAmount;
                document.Total += line.LineTotal;
            }

            if (cmd.References != null) {
                var count = 0;
                foreach (
                    var docRef in
                        cmd.References) {
                    if (docRef.DocumentType == "801") {
                        document.AddReference (docRef.Folio, docRef.DocumentType, docRef.ReferenceDate, docRef.Comment);
                    } else {
                        if (docRef.ReferencedDocumentId == Guid.Empty) {
                            return
                            Negotiate.WithValidationFailure (new ModelError ("references[" + count + "]",
                                I18n.Error.ReferenceNotFound));
                        }
                        document.AddReference (db.Load<ReceivedTaxDocument> (docRef.ReferencedDocumentId),
                            docRef.ReferenceReason, docRef.Comment);
                    }
                    count++;
                }
            }

            db.Save (document);
            return Negotiate.WithCreated (Context.GetResourceUrl (document.Id), document.Id);
        }

        [Permission (Permission = "received-tax-documents-list")]
        [Module (Name = "document_reception_trace")]
        public Negotiator ReceivedDocumentList (dynamic args) {
            var bound = this.BindTo (new ReceivedDocumentQuery ());
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            ReceivedDocumentDto dto = null;
            var query = db.QueryOver<ReceivedTaxDocument> ()
                .Where (
                    i =>
                    i.Tenant == currentTenant)
                .SelectList (i => i
                    .Select (j => j.Id).WithAlias (() => dto.Id)
                    .Select (j => j.AccountingDate).WithAlias (() => dto.EmissionDate)
                    .Select (j => j.ReceptionStatus).WithAlias (() => dto.ReceptionStatus)
                    .Select (j => j.DocumentType).WithAlias (() => dto.DocumentType)
                    .Select (j => j.FolioNumber).WithAlias (() => dto.FolioNumber)
                    .Select (j => j.ProviderTaxId).WithAlias (() => dto.ProviderTaxId)
                    .Select (j => j.ProviderLegalName).WithAlias (() => dto.ProviderLegalName)
                    .Select (j => j.ReceptionTime).WithAlias (() => dto.ReceptionDate)
                    .Select (j => j.AcceptedForAccounting).WithAlias (() => dto.AcceptedForAccounting));

            if (bound.ReceptionStatuses != null && bound.ReceptionStatuses.Any ()) {
                query.WhereRestrictionOn (i => i.ReceptionStatus)
                    .IsIn (bound.ReceptionStatuses.ToArray ());
            }
            if (bound.Folio1 > 0) {
                if (string.IsNullOrWhiteSpace (bound.FolioOperator)) {
                    bound.FolioOperator = "";
                }

                switch (bound.FolioOperator) {
                    case "lessThan":
                        query.Where (i => i.FolioNumber < bound.Folio1);
                        break;
                    case "greaterThan":
                        query.Where (i => i.FolioNumber > bound.Folio1);
                        break;

                    case "between":
                        if (bound.Folio2 <= 0) break;
                        if (bound.Folio1 == bound.Folio2) {
                            query.Where (i => i.FolioNumber == bound.Folio1);
                            break;
                        }

                        long left, right;
                        if (bound.Folio1 > bound.Folio2) {
                            left = bound.Folio2;
                            right = bound.Folio1;
                        } else {
                            left = bound.Folio1;
                            right = bound.Folio2;
                        }
                        query.Where (i => i.FolioNumber >= left && i.FolioNumber <= right);
                        break;

                    default:
                        query.Where (i => i.FolioNumber == bound.Folio1);
                        break;
                }
            }

            if (bound.AccountingDate1.HasValue) {
                if (bound.AccountingDate2.HasValue) {
                    if (bound.AccountingDate1 == bound.AccountingDate2) {
                        query.Where (i => i.AccountingDate == bound.AccountingDate1);
                    } else {
                        DateTime left, right;

                        if (bound.AccountingDate1 > bound.AccountingDate2) {
                            left = bound.AccountingDate2.Value;
                            right = bound.AccountingDate1.Value;
                        } else {
                            left = bound.AccountingDate1.Value;
                            right = bound.AccountingDate2.Value;
                        }

                        query.Where (i => i.AccountingDate >= left && i.AccountingDate <= right);
                    }
                } else {
                    query.Where (i => i.AccountingDate >= bound.AccountingDate1);
                }
            }

            if (!string.IsNullOrWhiteSpace (bound.ProviderTaxId)) {
                var taxid = identity.CleanId (bound.ProviderTaxId);
                query.Where (i => i.ProviderTaxId == taxid);
            }

            if (!string.IsNullOrWhiteSpace (bound.ProviderName)) {
                query.WhereRestrictionOn (i => i.ProviderLegalName)
                    .IsInsensitiveLike ("%" + bound.ProviderName + "%");
            }

            if (!string.IsNullOrWhiteSpace (bound.DocumentType)) {
                query.Where (i => i.DocumentType == bound.DocumentType);
            }

            if (bound.AcceptedForAccounting.HasValue) {
                query.Where (i => i.AcceptedForAccounting == bound.AcceptedForAccounting);
            }
            var list = query.OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<ReceivedDocumentDto> ())
                .PaginateResults<ReceivedTaxDocument, ReceivedDocumentDto> (page, rpp);
            return Negotiate.WithOkAndList (list, Request.Url);
        }

        [Permission (Permission = "received-tax-documents-list")]
        public Negotiator List (dynamic args) {
            var bound = this.BindTo (new ReceivedDocumentQuery ());
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            CostsCenter costsCenter = null;
            Enterprise provider = null;

            var query = db.QueryOver<ReceivedTaxDocument> ()
                .Where (i => i.Tenant == currentTenant)
                .Left.JoinAlias (i => i.Enterprise, () => provider)
                .Left.JoinAlias (i => provider.CostsCenter, () => costsCenter);

            if (bound.Folio1 > 0) {
                if (string.IsNullOrWhiteSpace (bound.FolioOperator)) {
                    bound.FolioOperator = "";
                }

                switch (bound.FolioOperator) {
                    case "lessThan":
                        query.Where (i => i.FolioNumber < bound.Folio1);
                        break;
                    case "greaterThan":
                        query.Where (i => i.FolioNumber > bound.Folio1);
                        break;

                    case "between":
                        if (bound.Folio2 <= 0) break;
                        if (bound.Folio1 == bound.Folio2) {
                            query.Where (i => i.FolioNumber == bound.Folio1);
                            break;
                        }

                        long left, right;
                        if (bound.Folio1 > bound.Folio2) {
                            left = bound.Folio2;
                            right = bound.Folio1;
                        } else {
                            left = bound.Folio1;
                            right = bound.Folio2;
                        }
                        query.Where (i => i.FolioNumber >= left && i.FolioNumber <= right);
                        break;

                    default:
                        query.Where (i => i.FolioNumber == bound.Folio1);
                        break;
                }
            }

            if (bound.AccountingDate1.HasValue) {
                if (bound.AccountingDate2.HasValue) {
                    if (bound.AccountingDate1 == bound.AccountingDate2) {
                        query.Where (i => i.AccountingDate == bound.AccountingDate1);
                    } else {
                        DateTime left, right;

                        if (bound.AccountingDate1 > bound.AccountingDate2) {
                            left = bound.AccountingDate2.Value;
                            right = bound.AccountingDate1.Value;
                        } else {
                            left = bound.AccountingDate1.Value;
                            right = bound.AccountingDate2.Value;
                        }

                        query.Where (i => i.AccountingDate >= left && i.AccountingDate <= right);
                    }
                } else {
                    query.Where (i => i.AccountingDate >= bound.AccountingDate1);
                }
            }

            if (!string.IsNullOrWhiteSpace (bound.ProviderTaxId)) {
                var taxid = identity.CleanId (bound.ProviderTaxId);
                query.Where (() => provider.TaxId == taxid);
            }

            if (!string.IsNullOrWhiteSpace (bound.ProviderName)) {
                query.WhereRestrictionOn (() => provider.LegalName)
                    .IsInsensitiveLike ("%" + bound.ProviderName + "%");
            }

            if (!string.IsNullOrWhiteSpace (bound.DocumentType)) {
                query.Where (i => i.DocumentType == bound.DocumentType);
            }

            if (bound.NotInVoucher) {
                query.Where (i => i.Voucher == null);
            }

            if (bound.AcceptedForAccounting.HasValue) {
                query.Where (i => i.AcceptedForAccounting == bound.AcceptedForAccounting);
            }

            if (bound.ReceptionStatuses != null && bound.ReceptionStatuses.Any ()) {
                query.WhereRestrictionOn (i => i.ReceptionStatus)
                    .IsIn (bound.ReceptionStatuses.ToArray ());
            }

            ReceivedTaxDocumentDto dto = null;
            ReceivedTaxDocumentLine lines = null;
            var totalCount = query.Clone ()
                .ToRowCountInt64Query ().FutureValue<long> ();
            var items = query
                .Left.JoinAlias (i => i.TaxDocumentLines, () => lines)
                .Select (
                    // Document Data
                    Projections.Group<ReceivedTaxDocument> (i => i.FolioNumber).WithAlias (() => dto.FolioNumber),
                    Projections.Group<ReceivedTaxDocument> (i => i.Id).WithAlias (() => dto.Id),
                    Projections.Group<ReceivedTaxDocument> (i => i.AccountingDate).WithAlias (() => dto.AccountingDate),
                    Projections.Group<ReceivedTaxDocument> (i => i.ExpiryDate).WithAlias (() => dto.ExpiryDate),
                    Projections.Group<ReceivedTaxDocument> (i => i.ReceptionTime).WithAlias (() => dto.ReceptionDate),
                    Projections.Group<ReceivedTaxDocument> (i => i.DocumentType).WithAlias (() => dto.DocTypeCode),
                    Projections.Group<ReceivedTaxDocument> (i => i.AcceptedForAccounting).WithAlias (() => dto.AcceptedForAccounting),
                    Projections.Group<ReceivedTaxDocument> (i => i.ExemptAmount).WithAlias (() => dto.ExemptAmount),
                    // Provider Data
                    Projections.Group<ReceivedTaxDocument> (i => i.Enterprise.Id).WithAlias (() => dto.ProviderId),
                    Projections.Group<ReceivedTaxDocument> (i => i.ProviderTaxId).WithAlias (() => dto.ProviderTaxId),
                    Projections.Group<ReceivedTaxDocument> (i => i.ProviderLegalName).WithAlias (() => dto.ProviderLegalName),
                    // Costs Center
                    Projections.Group<CostsCenter> (i => costsCenter.Id).WithAlias (() => dto.CostsCenterId),
                    Projections.Group<CostsCenter> (i => costsCenter.Name).WithAlias (() => dto.CostsCenterName),
                    Projections.Group<CostsCenter> (i => costsCenter.Code).WithAlias (() => dto.CostsCenterCode),
                    Projections.Sum (() => lines.TotalTaxAmount).WithAlias (() => dto.TotalTaxes),
                    Projections.Sum (() => lines.NetAmount).WithAlias (() => dto.TotalNetAmount),
                    Projections.Sum (() => lines.LineTotal).WithAlias (() => dto.GrossTotal)
                )
                .OrderBy (Settings.OrderBy)
                .TransformUsing (Transformers.AliasToBean<ReceivedTaxDocumentDto> ())
                .Skip ((page - 1) * rpp)
                .Take (rpp)
                .Future<ReceivedTaxDocumentDto> ();
            var paginated = new Paginate<ReceivedTaxDocumentDto> (items.ToList (), page, rpp, totalCount.Value);
            return Negotiate.WithOkAndList (paginated, Request.Url.ToString ());
        }

        [Permission (Permission = "received-tax-documents-create")]
        public async Task<dynamic> ReceiveDocumentsAsync (dynamic args, CancellationToken ct) {
            var errors = await HandleUploadAsync (Request.Files);
            return errors.Count > 0 ? Negotiate.WithValidationFailure (errors) : Negotiate.WithOk ();
        }

        [Permission (Permission = "received-tax-documents-create")]
        public Negotiator ReceiveDocuments (dynamic args) {
            var errors = HandleUpload (Request.Files);
            return errors.Count > 0 ? Negotiate.WithValidationFailure (errors) : Negotiate.WithOk ();
        }

        private async Task<IList<FileErrorValidation>> HandleUploadAsync (IEnumerable<HttpFile> files) {
            var httpFiles = files as IList<HttpFile> ?? files.ToList ();
            if (httpFiles.Count > MAX_UPLOADS_PER_REQUEST) {
                log.Warn ("Max uploads per request reached: {0}. The number of uploaded files is: {1}", MAX_UPLOADS_PER_REQUEST, httpFiles.Count);
            }
            var list = new List<FileErrorValidation> ();
            var path = Path.Combine (RootPath (), Constants.RECEIVED_DOCUMENTS_FOLDER);

            for (var i = 0; i < httpFiles.Count () && i < MAX_UPLOADS_PER_REQUEST; i++) {
                var file = httpFiles[i];

                if (file == null) continue;

                await receivedTaxDocumentService.ReceiveDocumentAsync (file.Value, currentTenant);

                if (!receivedTaxDocumentService.IsValid) {
                    list.Add (new FileErrorValidation (file.Key, receivedTaxDocumentService.ErrorList));
                    continue;
                }

                if (!Directory.Exists (path)) {
                    Directory.CreateDirectory (path);
                }

                var filePath = Path.Combine (path, GetFileName ());
                SaveDocuments (receivedTaxDocumentService.ReceivedTaxDocuments, filePath);
                await receivedTaxDocumentService.SaveDocumentAsync (filePath);
            }

            return list;
        }

        private IList<FileErrorValidation> HandleUpload (IEnumerable<HttpFile> files) {
            var httpFiles = files as IList<HttpFile> ?? files.ToList ();
            if (httpFiles.Count > MAX_UPLOADS_PER_REQUEST) {
                log.Warn ("Max uploads per request reached: {0}", httpFiles.Count);
            }
            var list = new List<FileErrorValidation> ();
            var path = Path.Combine (RootPath (), Constants.RECEIVED_DOCUMENTS_FOLDER);

            for (var i = 0; i < httpFiles.Count () && i < MAX_UPLOADS_PER_REQUEST; i++) {
                var file = httpFiles[i];

                if (file == null) continue;

                receivedTaxDocumentService.ReceiveDocument (file.Value, currentTenant);

                if (!receivedTaxDocumentService.IsValid) {
                    list.Add (new FileErrorValidation (file.Name, receivedTaxDocumentService.ErrorList));
                    continue;
                }

                if (!Directory.Exists (path)) {
                    Directory.CreateDirectory (path);
                }

                var filePath = Path.Combine (path, GetFileName ());
                SaveDocuments (receivedTaxDocumentService.ReceivedTaxDocuments, filePath);
                receivedTaxDocumentService.SaveDocument (filePath);
            }

            return list;
        }

        private void SaveDocuments (IEnumerable<ReceivedTaxDocument> list, string filePath) {
            foreach (var item in list) {
                item.FilePath = filePath;
                db.Save (item);
            }
        }

        private static string GetFileName () {
            return string.Concat (DateTime.UtcNow.ToString (Constants.SYSTEM_FILE_DATE_TIME_FORMAT), ".xml");
        }

        [Permission (Permission = "validate-document-reception")]
        [Module (Name = "document_reception_trace")]
        public Negotiator ValidateReception (dynamic args) {
            var fileList = Request.Files as IList<HttpFile>;
            if (fileList == null) {
                log.Error ("Failed to convert Request.Files to IList<HttpFile>.");
                return Negotiate.WithStatusCode (HttpStatusCode.InternalServerError)
                    .WithReasonPhrase ("An internal error ocurred. If the problems persist, contact your technical support.");
            }
            var file = fileList[0];
            if (file == null) {
                return Negotiate.WithConflict (I18n.Error.NoFileUploaded);
            }
            validationService.UpdateDocuments (file.Value, currentTenant);
            return validationService.IsValid ?
                Negotiate.WithNoContent () :
                Negotiate.WithValidationFailure (validationService.ErrorList);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator DocumentReferences (Command cmd) {
            var entity = db.QueryOver<ReceivedTaxDocument> ()
                .Where (i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithNotFound (string.Format (I18n.Error.ReceivedDocumentNotFound, cmd.Id));
            }

            var query = db.QueryOver<ReceivedTaxDocumentReference> ()
                .Where (i => i.Parent == entity)
                .Left.JoinQueryOver (i => i.ReferencedDocument)
                .List ();
            var list = query.Select (i => new {
                id = i.ReferencedDocument.DocumentType,
                    documentType = i.ReferencedDocument.DocumentType,
                    folioNumber = i.ReferencedDocument.FolioNumber
            });
            return Negotiate.WithOkAndModel (list);
        }

        [Permission (Permission = "accept-for-accounting")]
        public Negotiator AcceptForAccounting (Command cmd) {
            return ChangeAccountingStatus (cmd.Id, true);
        }

        private Negotiator ChangeAccountingStatus (Guid id, bool status) {
            var entity = db.QueryOver<ReceivedTaxDocument> ()
                .Where (i => i.Tenant == currentTenant && i.Id == id)
                .Take (1)
                .SingleOrDefault ();

            if (entity == null) {
                return Negotiate.WithNotFound (string.Format (I18n.Error.ReceivedDocumentNotFound, id));
            }
            entity.AcceptedForAccounting = status;
            return Negotiate.WithNoContent ();
        }

        // TODO: Remove duplicate code. The code in the following function
        // is the same as in Osiris.Api.Modules.TenantEp.TenantModule
        private object GetTenantLegalAddress () {
            TenantAddressType tenantAddressTypes = null;
            AddressType addressType = null;
            Commune commune = null;
            City city = null;
            var entity = db.QueryOver<TenantAddress> ()
                .Inner.JoinAlias (i => i.TenantAddressTypes, () => tenantAddressTypes)
                .Inner.JoinAlias (i => tenantAddressTypes.AddressType, () => addressType)
                .Inner.JoinAlias (i => i.Commune, () => commune)
                .Left.JoinAlias (i => commune.City, () => city)
                .Where (i => addressType.Name == "legal" && i.Tenant == currentTenant)
                .SelectList (i => i.Select (j => j.Id)
                    .Select (j => j.Line1)
                    .Select (j => j.Line2)
                    .Select (j => commune.Name)
                    .Select (j => city.Name))
                .Take (1)
                .SingleOrDefault<object[]> ();

            if (entity == null) {
                return Negotiate.WithNotFound (I18n.Error.NoLegalAddressDefined);
            }

            var dto = new {
                id = (Guid) entity[0],
                line1 = (string) entity[1],
                line2 = (string) entity[2],
                commune = (string) entity[3],
                city = (string) entity[4]
            };

            return dto;
        }
    }
}
