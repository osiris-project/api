﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ISpecificTax
        : IUnique<int>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        int Code { get; set; }

        bool IsRetained { get; set; }

        decimal TaxRate { get; set; }

        ICollection<IProduct> Products { get; set; }

        ICollection<IService> Services { get; set; }
    }
}
