﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Dobelik.Utils.Identity.Attributes;
using Slf4Net;

namespace Dobelik.Utils.Identity.Instances
{
    [Descriptor("es", "CL")]
    internal class ChileanIdValidator
        : INationalIdentity
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(ChileanIdValidator));

        private readonly Regex _cleaner = new Regex(@"[^0-9k]", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public bool Validate(string id)
        {
            Log.Debug("Entering Validate.", id);
            Log.Debug("Paramters: [id = {0}]", id);
            if (string.IsNullOrWhiteSpace(id)) return false;
            id = id.Trim();
            if (!Regex.IsMatch(id, @"[k0-9]$", RegexOptions.IgnoreCase | RegexOptions.Compiled))
            {
                Log.Debug("Id didn't end with the expected value.");
                return false;
            }
            id = CleanId(id);
            var digit = id.Last();
            id = id.Substring(0, id.Length - 1);
            var o = CalculateVerifierPart(id);
            bool result;

            if (o == null)
                result = false;
            else
                result = (char) o == digit;

            Log.Debug("Result = {0}", result);
            Log.Debug("Leaving Validate.");
            return result;
        }

        public object CalculateVerifierPart(string partialId)
        {
            Log.Debug("Entering CalculateVerifierPart.");
            Log.Debug("Paramters: [partialId = {0}].", partialId);
            if (string.IsNullOrEmpty(partialId)) return null;
            partialId = CleanId(partialId);
            partialId = partialId.ToLower();
            char digit;
            int sum = 0, sequence = 2;

            foreach (var c in partialId.Reverse())
            {
                sum += sequence * int.Parse(c.ToString());
                sequence++;

                if (sequence > 7)
                    sequence = 2;
            }

            sequence = (11 - (sum % 11));

            switch (sequence)
            {
                case 10:
                    digit = 'k';
                    break;

                case 11:
                    digit = '0';
                    break;

                default:
                    digit = sequence.ToString()[0];
                    break;
            }
            Log.Debug("Result = {0}", digit);
            Log.Debug("Leaving CalculateVerifierPart.");
            return digit;
        }

        public string Normalize(string rawId)
        {
            Log.Debug("Enterign Normalize.");
            Log.Debug("Parameters: [rawId = {0}]", rawId);

            if (string.IsNullOrEmpty(rawId)) return rawId;

            rawId = CleanId(rawId);
            char digit;

            try
            {
                digit = rawId[rawId.Length - 1];
                Log.Debug("Digit: {0}", digit);
                rawId = rawId.Substring(0, rawId.Length - 1);
            }
            catch (IndexOutOfRangeException ex)
            {
                Log.Error("Failed to remove verifier part from string.", ex);
                return rawId;
            }

            var sb = new StringBuilder();
            sb.Append(digit).Append("-");
            var i = 0;

            foreach (var c in rawId.Reverse())
            {
                if (i > 0 &&
                    (i % 3) == 0)
                {
                    sb.Append(".");
                }

                sb.Append(c);
                i++;
            }
            var charArr = sb.ToString().ToCharArray();
            Array.Reverse(charArr);
            var result = new string(charArr).ToLower();
            Log.Debug("Result = {0}", result);
            Log.Debug("Leaving Normalize.");
            return result;
        }

        public string CleanId(string id)
        {
            Log.Debug("Entering CleanId(id = {0})", id);
            if (string.IsNullOrEmpty(id)) return id;
            var result = _cleaner.Replace(id, "").ToLower().Trim();
            Log.Debug("Clear string = {0}", result);
            Log.Debug("Leaving CleanId");
            return result;
        }
    }
}
