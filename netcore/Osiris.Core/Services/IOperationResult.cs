using System.Collections.Generic;

namespace Osiris.Core.Services {
    /// <summary>
    /// Represents the result of a operation (method call).
    /// </summary>
    /// <typeparam name="T">The operation result type.</typeparam>
    /// <typeparam name="E">The type of errors to expect.</typeparam>
    public interface IOperationResult<T, E> {
        /// <summary>
        /// Retrieves the result of the operation.
        /// </summary>
        /// <returns></returns>
        T Result { get; }

        /// <summary>
        /// Retrieves a list of errors that occured during the operation.
        /// </summary>
        /// <returns>A list of errors.</returns>
        IReadOnlyCollection<E> Errors { get; }

        /// <summary>
        /// Checks if there are errors.
        /// </summary>
        /// <returns>True if there are errores, false otherwise.</returns>
        bool HasErrors { get; }
    }
}
