﻿using System;

namespace Zentom.Api.Modules.AccountingEp
{
    public class AccountingAccountDto
    {
        public Guid Id { get; set; }

        public string Code { get; set; } 

        public string Description { get; set; }
    }
}