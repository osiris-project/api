﻿using System;
using System.Linq;
using Nancy.ModelBinding;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;
using Zentom.Core.Entities.Accounting;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Api.Modules.AccountingEp
{
    public class AccountingModule
        : TenantApi
    {
        public AccountingModule(ISessionFactory factory)
            : base("accounting", factory)
        {
            // Accounting Vouchers
            this.Post<CreateVoucherCmd>(BasePath + "/vouchers/", VoucherCreate);
            this.Get(BasePath + "/vouchers/", VoucherList);
            this.Get<Command>(BasePath + "/vouchers/{id:guid}/", VoucherRead);
            this.Patch<Command>(BasePath + "/vouchers/{id:guid}/approve/", ApproveVoucher);
            this.Patch<Command>(BasePath + "/vouchers/{id:guid}/reject/", RejectVoucher);
            this.Delete<Command>(BasePath + "/vouchers/{id:guid}/", DeleteVoucher);

            // Accounting Accounts
            this.Get<Command>(BasePath + "/accounting-accounts/{id:guid}", AccountingAccountRead);
            this.Get(BasePath + "/accounting-accounts/", AccountingAccountList);
            this.Post<AccountingAccountCreateCmd>(BasePath + "/accounting-accounts/", AccountingAccountCreate);
            this.Put<AccountingAccountUpdateCmd>(BasePath + "/accounting-accounts/{id:guid}", AccountingAccountUpdate);
            this.Delete<Command>(BasePath + "/accounting-accounts/{id:guid}", AccountingAccountDelete);

            // Costs Centers
            this.Get<Command>(BasePath + "/costs-centers/{id:guid}", CostsCenterRead);
            this.Get(BasePath + "/costs-centers/", CostsCenterList);
            this.Post<CostsCenterCreateCmd>(BasePath + "/costs-centers/", CostsCenterCreate);
            this.Put<CostsCenterUpdateCmd>(BasePath + "/costs-centers/{id:guid}", CostsCenterUpdate);
            this.Delete<Command>(BasePath + "/costs-centers/{id:guid}", CostsCenterDelete);
        }

        #region Vouchers

        [Permission(Permission = "accounting-voucher-create")]
        [Module(Name = "accounting_voucher")]
        public object VoucherCreate(CreateVoucherCmd cmd)
        {
            var entity = new AccountingVoucher
            {
                Tenant = currentTenant,
                Date = cmd.Date,
                Description = cmd.Description,
                Status = AccountingVoucherStatus.Saved
            };
            foreach (var document in cmd.Documents.Select(id => db.Get<ReceivedTaxDocument>(id)))
            {
                if (document != null && document.Voucher == null)
                {
                    document.Voucher = entity;
                }
                else if (document?.Voucher != null)
                {
                    for (var i = cmd.Lines.Count - 1; i >= 0; i--)
                    {
                        var el = cmd.Lines.ElementAt(i);
                        if (el.DocumentType != document.DocumentType || el.FolioNumber != document.FolioNumber)
                            continue;
                        cmd.Lines.RemoveAt(i);
                        break;
                    }
                }
            }
            foreach (var voucherLine in cmd.Lines.Select(line => new AccountingVoucherLine
            {
                AccountingAccount = db.Load<AccountingAccount>(line.AccountingAccount.Id),
                AccountingVoucher = entity,
                Asset = line.Asset,
                CostsCenter = db.Load<CostsCenter>(line.CostsCenter.Id),
                CustomField1 = line.CustomField1,
                CustomField2 = line.CustomField2,
                CustomField3 = line.CustomField3,
                Debit = line.Debit,
                Description = line.Description,
                DocumentType = line.DocumentType,
                EmissionDate = line.EmissionDate,
                ExpiryDate = line.ExpiryDate,
                FolioNumber = line.FolioNumber,
                NetAmount = line.NetAmount,
                NetFixedAsset = line.NetFixedAsset,
                ReferencedDocumentType = line.ReferencedDocumentType,
                ReferencedDocumentFolio = line.ReferencedDocumentFolio,
                Total = line.Total,
                TotalExemptAmount = line.TotalExemptAmount,
                TotalTaxAmount = line.TotalTaxAmount,
                TotalUnrecoverableAmount = line.TotalUnrecoverableAmount,
                Provider = db.Load<Enterprise>(line.Provider.Id)
            }))
            {
                voucherLine.ProviderTaxId = voucherLine.Provider.TaxId;
                entity.Lines.Add(voucherLine);
            }
            db.Save(entity);
            return Negotiate.WithPostSuccess(entity.Id, Request.Url);
        }

        [Permission(Permission = "accounting-voucher-read")]
        [Module(Name = "accounting_voucher")]
        public object VoucherList(dynamic args)
        {
            var bound = this.BindTo(new VoucherQuery());
            var query = db.QueryOver<AccountingVoucher>()
                .Where(i => i.Tenant == currentTenant);

            if (bound.Number.HasValue && bound.Number.Value > 0)
            {
                query.Where(i => i.Number == bound.Number.Value);
            }

            if (bound.StartDate.HasValue)
            {
                if (bound.EndDate.HasValue)
                {
                    if (bound.StartDate == bound.EndDate)
                    {
                        query.Where(i => i.Date == bound.StartDate);
                    }
                    else
                    {
                        DateTime left, right;
                        if (bound.StartDate > bound.EndDate)
                        {
                            left = bound.EndDate.Value;
                            right = bound.StartDate.Value;
                        }
                        else
                        {
                            left = bound.StartDate.Value;
                            right = bound.EndDate.Value;
                        }
                        query.Where(i => i.Date >= left && i.Date <= right);
                    }
                }
                else
                {
                    query.Where(i => i.Date >= bound.StartDate);
                }
            }

            VoucherDto dto = null;
            Tenant tenant = null;
            AccountingVoucherLine lines = null;
            var list = query
                .Left.JoinAlias(i => i.Tenant, () => tenant)
                .Left.JoinAlias(i => i.Lines, () => lines)
                .Select(
                Projections.Group<AccountingVoucher>(i => i.Id).WithAlias(() => dto.Id),
                Projections.Group<AccountingVoucher>(i => i.Number).WithAlias(() => dto.Number),
                Projections.Group<AccountingVoucher>(i => i.Status).WithAlias(() => dto.Status),
                Projections.Group<AccountingVoucher>(i => i.Date).WithAlias(() => dto.Date),
                Projections.Group<AccountingVoucher>(i => i.Description).WithAlias(() => dto.Description),
                Projections.Sum<AccountingVoucherLine>(i => lines.Debit).WithAlias(() => dto.Debit),
                Projections.Sum<AccountingVoucherLine>(i => lines.Asset).WithAlias(() => dto.Asset),
                Projections.Sum<AccountingVoucherLine>(i => lines.NetAmount).WithAlias(() => dto.TotalNetAmount),
                Projections.Sum<AccountingVoucherLine>(i => lines.TotalTaxAmount).WithAlias(() => dto.TotalTaxes),
                Projections.Sum<AccountingVoucherLine>(i => lines.Total).WithAlias(() => dto.Total))
                .OrderBy(Settings.OrderBy)
                .TransformUsing(Transformers.AliasToBean<VoucherDto>())
                .PaginateResults<AccountingVoucher, VoucherDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(list, Request.Url);
        }

        [Permission(Permission = "accounting-voucher-read")]
        [Module(Name = "accounting_voucher")]
        public object VoucherRead(Command cmd)
        {
            var voucher = db.QueryOver<AccountingVoucher>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (voucher == null)
            {
                return Negotiate.WithNotFound(I18n.Error.AccountingVoucherNotFound);
            }
            AccountingAccount accountingAccount = null;
            Enterprise provider = null;
            CostsCenter costsCenter = null;
            var lines = db.QueryOver<AccountingVoucherLine>()
                .Where(i => i.AccountingVoucher == voucher)
                .Left.JoinAlias(i => i.AccountingAccount, () => accountingAccount)
                .Left.JoinAlias(i => i.Provider, () => provider)
                .Left.JoinAlias(i => provider.CostsCenter, () => costsCenter)
                .Take(1000)
                .List();
            var dto = new
            {
                id = voucher.Id,
                number = voucher.Number,
                date = voucher.Date,
                description = voucher.Description,
                lines = lines.Select(i => new
                {
                    accountingAccount = new
                    {
                        id = i.AccountingAccount.Id,
                        code = i.AccountingAccount.Code
                    },
                    description = i.Description,
                    asset = i.Asset,
                    debit = i.Debit,
                    costsCenter = new
                    {
                        id = i.CostsCenter.Id,
                        code = i.CostsCenter.Code
                    },
                    customField1 = i.CustomField1,
                    customField2 = i.CustomField2,
                    customField3 = i.CustomField3,
                    documentType = i.DocumentType,
                    emissionDate = i.EmissionDate,
                    expiryDate = i.ExpiryDate,
                    folioNumber = i.FolioNumber,
                    netAmount = i.NetAmount,
                    netFixedAsset = i.NetFixedAsset,
                    provider = new
                    {
                        id = i.Provider.Id,
                        taxId = i.Provider.TaxId
                    },
                    referencedDocumentFolio = i.ReferencedDocumentFolio,
                    referencedDocumentType = i.ReferencedDocumentType,
                    total = i.Total,
                    totalExemptAmount = i.TotalExemptAmount,
                    totalTaxAmount = i.TotalTaxAmount,
                    totalUnrecoverableAmount = i.TotalUnrecoverableAmount
                })
            };
            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "accounting-voucher-update-status")]
        [Module(Name = "accounting_voucher")]
        public object ApproveVoucher(Command cmd)
        {
            return UpdateStatus(cmd, AccountingVoucherStatus.Accepted);
        }

        [Permission(Permission = "accounting-voucher-update-status")]
        [Module(Name = "accounting_voucher")]
        public object RejectVoucher(Command cmd)
        {
            return UpdateStatus(cmd, AccountingVoucherStatus.Rejected);
        }

        private object UpdateStatus(Command cmd, AccountingVoucherStatus status)
        {
            var entity = db.QueryOver<AccountingVoucher>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNotFound(string.Format(I18n.Error.AccountingVoucherNotFound, cmd.Id));
            }
            if (entity.Status != AccountingVoucherStatus.Saved)
            {
                return Negotiate.WithValidationFailure(new ModelError("unmodifiable", string.Format(I18n.Error.AccountingVoucherUnmodifiable, entity.Number)));
            }
            entity.Status = status;
            return Negotiate.WithNoContent();
        }

        [Permission(Permission = "accounting-voucher-update-status")]
        [Module(Name = "accounting_voucher")]
        public object DeleteVoucher(Command cmd)
        {
            var entity = db.QueryOver<AccountingVoucher>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();

            if (entity == null)
            {
                return Negotiate.WithNoContent();
            }
            db.Delete(entity);
            return Negotiate.WithNoContent();
        }

        #endregion

        #region Accounting Accounts

        [Permission(Permission = "accounting-account-read")]
        public object AccountingAccountRead(Command cmd)
        {
            var entity = db.QueryOver<AccountingAccount>()
                 .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                 .Take(1)
                 .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Error.AccountingAccountNotFound);
            }
            var dto = new
            {
                id = entity.Id,
                code = entity.Code,
                description = entity.Description
            };
            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "accounting-account-create")]
        public object AccountingAccountCreate(AccountingAccountCreateCmd cmd)
        {
            var entity = db.QueryOver<AccountingAccount>()
                .Where(i => i.Tenant == currentTenant && i.Code == cmd.Code)
                .Take(1)
                .SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithExistingItem("duplicate", "code");
            }
            entity = new AccountingAccount(currentTenant, cmd.Code, cmd.Description);
            db.Save(entity);
            return Negotiate.WithPostSuccess(entity.Id, Request.Url);
        }

        [Permission(Permission = "accounting-account-update")]
        public object AccountingAccountUpdate(AccountingAccountUpdateCmd cmd)
        {
            var entity = db.QueryOver<AccountingAccount>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Error.AccountingAccountNotFound);
            }

            if (entity.Code != cmd.Code)
            {
                var duplicate = db.QueryOver<AccountingAccount>()
                    .Where(i => i.Tenant == currentTenant && i.Code == cmd.Code)
                    .Take(1)
                    .SingleOrDefault();

                if (duplicate != null)
                {
                    return Negotiate.WithDuplicateItem("duplicate", "code");
                }
            }
            entity.Code = cmd.Code;
            entity.Description = cmd.Description;
            return Negotiate.WithNoContent();
        }

        [Permission(RequiresLogin = true)]
        public object AccountingAccountList()
        {
            var bound = this.BindTo(new AccountingAccountQuery());
            var query = db.QueryOver<AccountingAccount>()
                .Where(i => i.Tenant == currentTenant);
            if (!string.IsNullOrWhiteSpace(bound.Code))
            {
                query.Where(i => i.Code == bound.Code);
            }

            AccountingAccountDto dto = null;
            var items = query
                .SelectList(
                    i => i.Select(j => j.Id).WithAlias(() => dto.Id).Select(j => j.Code).WithAlias(() => dto.Code)
                        .Select(j => j.Description).WithAlias(() => dto.Description))
                .OrderBy(Settings.OrderBy)
                .ProjectInto<AccountingAccount, AccountingAccountDto>()
                .PaginateResults<AccountingAccount, AccountingAccountDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(items, Request.Url);
        }

        [Permission(Permission = "accounting-account-delete")]
        public object AccountingAccountDelete(Command arg)
        {
            var entity = db.QueryOver<AccountingAccount>()
                 .Where(i => i.Tenant == currentTenant && i.Id == arg.Id)
                 .Take(1)
                 .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNoContent();
            }
            db.Delete(entity);
            return Negotiate.WithNoContent();
        }

        #endregion

        #region Costs Centers

        [Permission(Permission = "costs-center-read")]
        public object CostsCenterRead(Command cmd)
        {
            var entity = db.QueryOver<CostsCenter>()
                 .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                 .Take(1)
                 .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Error.CostsCenterNotFound);
            }
            var dto = new
            {
                id = entity.Id,
                code = entity.Code,
                description = entity.Description
            };
            return Negotiate.WithOkAndModel(dto);
        }

        [Permission(Permission = "costs-center-create")]
        public object CostsCenterCreate(CostsCenterCreateCmd cmd)
        {
            var entity = db.QueryOver<CostsCenter>()
                .Where(i => i.Tenant == currentTenant && i.Code == cmd.Code)
                .Take(1)
                .SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithExistingItem("duplicate", "code");
            }
            entity = new CostsCenter(currentTenant, cmd.Code, cmd.Name, cmd.Description);
            db.Save(entity);
            return Negotiate.WithPostSuccess(entity.Id, Request.Url);
        }

        [Permission(Permission = "costs-center-update")]
        public object CostsCenterUpdate(CostsCenterUpdateCmd cmd)
        {
            var entity = db.QueryOver<CostsCenter>()
                .Where(i => i.Tenant == currentTenant && i.Id == cmd.Id)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNotFound(I18n.Error.CostsCenterNotFound);
            }

            if (entity.Code != cmd.Code)
            {
                var duplicate = db.QueryOver<CostsCenter>()
                    .Where(i => i.Tenant == currentTenant && i.Code == cmd.Code)
                    .Take(1)
                    .SingleOrDefault();

                if (duplicate != null)
                {
                    return Negotiate.WithDuplicateItem("duplicate", "code");
                }
            }
            entity.Code = cmd.Code;
            entity.Name = cmd.Name;
            entity.Description = cmd.Description;
            return Negotiate.WithNoContent();
        }

        [Permission(RequiresLogin = true)]
        public object CostsCenterList()
        {
            var bound = this.BindTo(new CostsCenterQuery());
            var query = db.QueryOver<CostsCenter>()
                .Where(i => i.Tenant == currentTenant);
            if (!string.IsNullOrWhiteSpace(bound.Code))
            {
                query.Where(i => i.Code == bound.Code);
            }

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.Where(i => i.Name == bound.Name);
            }

            CostsCenterDto dto = null;
            var items = query
                .SelectList(
                    i => i.Select(j => j.Id).WithAlias(() => dto.Id).Select(j => j.Code).WithAlias(() => dto.Code)
                        .Select(j => j.Description).WithAlias(() => dto.Description))
                .OrderBy(Settings.OrderBy)
                .ProjectInto<CostsCenter, CostsCenterDto>()
                .PaginateResults<CostsCenter, CostsCenterDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
            return Negotiate.WithOkAndList(items, Request.Url);
        }

        [Permission(Permission = "costs-center-delete")]
        public object CostsCenterDelete(Command arg)
        {
            var entity = db.QueryOver<CostsCenter>()
                .Where(i => i.Tenant == currentTenant && i.Id == arg.Id)
                .Take(1)
                .SingleOrDefault();
            if (entity == null)
            {
                return Negotiate.WithNoContent();
            }
            db.Delete(entity);
            return Negotiate.WithNoContent();
        }

        #endregion
    }
}