﻿using System;

namespace Zentom.Api.Modules.EnterpriseEp
{
    public class UpdateCostsCenterCmd
    {
        public Guid Id { get; set; }

        public Guid CostsCenterId { get; set; }
    }
}