﻿namespace Osiris.Core.Entities
{
    public class TenantComercialActivity
        : CompanyComercialActivity
    {
        #region Properties

        public virtual Tenant Tenant
        {
            get { return (Tenant)Company; }
            set { Company = value; }
        }

        #endregion

        public TenantComercialActivity()
        {
        }

        public TenantComercialActivity(Company tenant, ComercialActivity comercialActivity)
            : base(tenant, comercialActivity)
        {
        }
    }
}
