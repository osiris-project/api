/**
 * Copyright (c) Mar 7, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import groovy.transform.ToString

/**
 * A single validation error.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@ToString
class DefaultValidationError
implements ValidationError {
    final String property
    final String message


    private DefaultValidationError(String property) {
        this(property, '')
    }

    private DefaultValidationError(String property, String message) {
        this.property = property
        this.message = message
    }

    /**
     * Builds a validation error with a property and an optional message.
     *
     * @param property The name of the faulty property.
     * @param message An error message.
     * @since 0.0.1
     */
    static DefaultValidationError build(String property, String message) {
        return new DefaultValidationError(property, message)
    }

    /**
     * Builds a validation error with an empty message.
     *
     * @param property The name of the faulty property.
     * @since 0.0.1
     */
    static DefaultValidationError build(String property) {
        return build(property, '')
    }
}
