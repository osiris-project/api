﻿using System.Collections.Generic;
using Osiris.Core.Interception;
using Osiris.Core.Interfaces.Validation;

namespace Osiris.Core.Services
{
    public interface IService
        : IInterceptable
    {
        bool IsValid { get; }

        IList<IModelValidation> ErrorList { get; }
    }
}
