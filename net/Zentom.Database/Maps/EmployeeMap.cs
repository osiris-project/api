﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class EmployeeMap
        : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Schema("organization");
            Table("employee");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Person)
                .UniqueKey("employee_un")
                .Cascade.SaveUpdate();

            References(i => i.Tenant, "tenant_id")
                .UniqueKey("employee_un")
                .Cascade.SaveUpdate();
        }
    }
}
