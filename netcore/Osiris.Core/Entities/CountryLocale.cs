﻿namespace Osiris.Core.Entities
{
    public class CountryLocale
        : Entity
    {
        #region Fields

        private Country _country;

        private Locale _locale;

        #endregion

        #region Properties

        public virtual Country Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public virtual Locale Locale
        {
            get { return _locale; }
            set { _locale = value; }
        }

        #endregion

        public CountryLocale() { }

        public CountryLocale(Country country, Locale locale)
        {
            _country = country;
            _locale = locale;
        }
    }
}
