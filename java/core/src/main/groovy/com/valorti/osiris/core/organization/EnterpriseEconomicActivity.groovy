/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.taxation.EconomicActivity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization',
uniqueConstraints = [
    @UniqueConstraint(columnNames = [ 'enterprise_id', 'economic_activity_id' ])
])
class EnterpriseEconomicActivity extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Enterprise enterprise

    @ManyToOne
    EconomicActivity economicActivity

    /**
     *
     */
    protected EnterpriseEconomicActivity() {
    }

    /**
     * @param enterprise
     * @param economicActivity
     */
    EnterpriseEconomicActivity(Enterprise enterprise, EconomicActivity economicActivity) {
        if(!enterprise) {
            throw new IllegalArgumentException('No enterprise provided!')
        }
        if(!economicActivity) {
            throw new IllegalArgumentException('No economic activity provided!')
        }
        this.enterprise = enterprise
        this.economicActivity = economicActivity
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((economicActivity == null) ? 0 : economicActivity.hashCode())
        result = prime * result + ((enterprise == null) ? 0 : enterprise.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof EnterpriseEconomicActivity)) {
            return false
        }
        final other = (EnterpriseEconomicActivity) obj
        if(!Objects.equals(this.enterprise, other.enterprise)) {
            return false
        }
        return Objects.equals(this.economicActivity, other.economicActivity)
    }
}
