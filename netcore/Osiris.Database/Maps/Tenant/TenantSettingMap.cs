﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    class TenantSettingMap
        : ClassMap<TenantSetting>
    {
        public TenantSettingMap()
        {
            Schema("system");
            Table("tenant_settings");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Name, "setting_name").UniqueKey("tenant_setting_un").Length(255);
            Map(i => i.Value, "setting_value").CustomSqlType("NVARCHAR(MAX)");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_setting_un");
        }
    }
}
