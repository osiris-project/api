﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class PaymentTypeMap
        : ClassMap<PaymentType>
    {
        public PaymentTypeMap()
        {
            Schema("taxation");
            Table("payment_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(6).Unique();
            Map(p => p.Description).Length(70).Default("");

            HasMany(i => i.TaxDocuments)
                .Inverse()
                .KeyColumn("payment_type_id")
                .Cascade.SaveUpdate();

            HasMany(i => i.Payments)
                .KeyColumn("payment_type_id")
                .Cascade.SaveUpdate()
                .Inverse();
        }
    }
}
