/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.AttributeOverride
import javax.persistence.AttributeOverrides
import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class DocumentReceiver
    extends BigIntegerEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Embedded
    final EnterpriseData data

    @Embedded
    @AttributeOverrides([
        @AttributeOverride(name = 'line', column = @Column(name = 'postal_line', nullable = true)),
        @AttributeOverride(name = 'commune', column = @Column(name = 'postal_commune', nullable = true)),
        @AttributeOverride(name = 'city', column = @Column(name = 'postal_city', nullable = true))
    ])
    final Address postalAddress

    @Column(nullable = false, length = 20)
    final String phone

    @Column(nullable = false, length = 254)
    final String email

    protected DocumentReceiver() {
    }
    /**
     *
     * @param data The enterprise basic data.
     * @throws IllegalArgumentException If the data is null.
     */
    DocumentReceiver(EnterpriseData data)
        throws IllegalArgumentException {
        this(data, null, '', '')
    }

    /**
     *
     * @param data The enterprise basic data.
     * @param postalAddress An optional postal address.
     * @param phone An optional contact phone.
     * @param email An optional contact name.
     * @throws IllegalArgumentException If the data is null.
     */
    DocumentReceiver(EnterpriseData data, Address postalAddress, String phone, String email)
        throws IllegalArgumentException {
        if (!data) {
            throw new IllegalArgumentException('data')
        }
        this.data = data
        this.postalAddress = postalAddress
        this.phone = phone?.trim()
        this.email = email?.trim()
    }
}
