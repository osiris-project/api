﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ICommune : IUnique<int>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string PostalCode { get; set; }

        ICity City { get; set; }

        ICollection<IEnterpriseAddress> EnterpriseAddresses { get; set; }
    }
}
