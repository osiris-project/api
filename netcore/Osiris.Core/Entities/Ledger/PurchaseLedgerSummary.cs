﻿namespace Osiris.Core.Entities.Ledger
{
    public enum TaxType
    {
        VAT, L18211
    }

    public class PurchaseLedgerSummary
        : LedgerSummary
    {
        private readonly TaxType taxType;
        private readonly decimal recoverableVatAmount;
        private readonly int fixedAssetsOperations;
        private readonly decimal fixedAssetsAmount;
        private readonly int commonVatOperations;
        private readonly decimal commonVatAmount;

        public PurchaseLedgerSummary()
        {
        }

        public PurchaseLedgerSummary(int docType, TaxType taxType, int docCount, int nullDocs, int exemptDocs,
            decimal exemptAmount, decimal netAmount, decimal recoverableVatAmount, decimal vatAmount,
            int fixedAssetsOperations, decimal fixedAssetsAmount,
            int commonVatOperations, decimal commonVatAmount, 
            decimal total)
            : base(docType, docCount, nullDocs, exemptDocs, exemptAmount, netAmount, vatAmount, total)
        {
            this.taxType = taxType;
            this.recoverableVatAmount = recoverableVatAmount;
            this.fixedAssetsOperations = fixedAssetsOperations;
            this.fixedAssetsAmount = fixedAssetsAmount;
            this.commonVatOperations = commonVatOperations;
            this.commonVatAmount = commonVatAmount;
        }
    }
}
