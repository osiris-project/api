﻿using System;
using System.Transactions;
using log4net;
using NHibernate;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Database
{
    public class TransactionScopeWrapper
        : ITransactionScopeWrapper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(TransactionScopeWrapper));
        private readonly TransactionScope _scope;
        private ITransaction _transaction;
        private bool _isDisposed;
        private ISession _session;

        private readonly Lazy<ISession> _lazySession;

        public TransactionScopeWrapper(Lazy<ISession> lazySession)
        {
            _lazySession = lazySession;
            _scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.MaximumTimeout
                });
        }

        public void Begin()
        {
            Log.Debug("Beginning transaction.");
            _session = _lazySession.Value;
            _transaction = _session.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
        }

        public void Complete()
        {
            Log.Debug("Entering Complete.");

            if (_transaction != null && _transaction.IsActive)
            {
                Log.Debug("Commiting transaction.");
                _transaction.Commit();
                Log.Debug("Transaction commited.");
                _scope.Complete();
                Log.Debug("Scope complete.");
            }

            Log.Debug("Leaving Complete.");
        }

        public void Rollback()
        {
            Log.Debug("Entering Rollback");

            if (_transaction != null && _transaction.IsActive)
            {
                Log.Debug("Trying to rollback changes.");
                _transaction.Rollback();
                Log.Debug("Success!");
            }

            Log.Debug("Leaving Rollback");
        }

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    try
                    {
                        _transaction?.Dispose();
                    }
                    finally
                    {
                        _scope?.Dispose();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _transaction = null;
                _isDisposed = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TransactionScopeWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
