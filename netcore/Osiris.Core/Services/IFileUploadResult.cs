﻿namespace Osiris.Core.Services
{
    public interface IFileUploadResult
    {
        string Identifier { get; }
    }
}
