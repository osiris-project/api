﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class TenantAddress
        : CompanyAddress
    {
        #region Properties

        public virtual Tenant Tenant
        {
            get { return (Tenant)Company; }
            set { Company = value; }
        }

        public virtual ISet<TenantAddressType> TenantAddressTypes
        {
            get { return (ISet<TenantAddressType>) CompanyAddressTypes; }
            set { CompanyAddressTypes = value; }
        }

        #endregion
        
        public TenantAddress() { }

        public TenantAddress(string line1, string line2, Commune commune, Company company,
            IEnumerable<CompanyAddressType> companyAddressTypes)
            : base(line1, line2, commune, company, companyAddressTypes)
        {
        }
    }
}