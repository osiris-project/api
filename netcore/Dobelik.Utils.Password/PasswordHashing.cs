﻿using Dobelik.Utils.Password.Enums;
using Dobelik.Utils.Password.Exceptions;
using Dobelik.Utils.Password.Interfaces;
using System;
using System.Security.Cryptography;
using Dobelik.Utils.Password.Extensions;
using Slf4Net;

namespace Dobelik.Utils.Password
{
    public class PasswordHashing
        : IPasswordHasher
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(PasswordHashing));

        public IHashInfo HashInformation { get; set; }

        public byte[] CurrentHash { get; private set; }

        public byte[] CurrentSalt { get; private set; }

        public PasswordHashing(IHashInfo hashInfo)
        {
            Log.Debug("Entering PasswordHashing.");
            HashInformation = hashInfo;
            Log.Debug("Current HashInfo: {0}", HashInformation);
            Log.Debug("Leaving PasswordHashing.");
        }

        public byte[] HashPassword(string raw, byte[] salt = null)
        {
            if (salt == null)
            {
                salt = GenerateSalt();
            }
            else
            {
                if (salt.Length % 2 != 0)
                    throw new ArgumentException("The salt length must be even!");
            }

            HashParts r = CreateHash(raw, salt);
            MergeHashes(r);

            return r.Hash;
        }

        public byte[] GenerateSalt()
        {
            var salt = new byte[HashInformation.SaltLength];
            var csprng = new RNGCryptoServiceProvider();
            csprng.GetBytes(salt);
            csprng.Dispose();
            Log.Debug("Generated Salt: {0}", salt.AsString());
            return salt;
        }

        public string Concat(byte[] b1, byte[] b2)
        {
            var sB1 = b1.AsString();
            var sB2 = b2.AsString();
            return string.Concat(sB1, HashInformation.SaltSeparator, sB2);
        }

        public bool CompareHashes(byte[] hashedPassword, string plainTextPassword)
        {
            if (HashInformation.SaltPosition == SaltPosition.None)
                throw new InvalidSaltPositionException("The salt position cannot be None.");

            var main = SplitHash(hashedPassword);
            var compare = CreateHash(plainTextPassword, main.Salt);
            Log.Debug("Hash1: {0}", main.Hash.AsString());
            Log.Debug("Hash2: {0}", compare.Hash.AsString());
            return main.Hash.CompareTo(compare.Hash);
        }

        public bool CompareHashes(byte[] hashedPassword,
            string plainTextPassword, byte[] salt)
        {
            if (HashInformation.SaltPosition != SaltPosition.None)
                throw new InvalidSaltPositionException(
                    "The salt position must be set to None, but " +
                    HashInformation.SaltPosition + " was set.");

            if (salt == null || salt.LongLength <= 0)
                throw new ArgumentException("SaltPosition is None and salt was null or empty. Please, provide a salt or set the correct SaltPosition.");

            var main = new HashParts
            {
                Salt = salt,
                Hash = hashedPassword
            };
            var compare = CreateHash(plainTextPassword, main.Salt);
            Log.Debug("Hash1: {0}", main.Hash.AsString());
            Log.Debug("Hash2: {0}", compare.Hash.AsString());
            return main.Hash.CompareTo(compare.Hash);
        }

        public bool CompareHashes(byte[] hash1, byte[] hash2)
        {
            Log.Debug("Hash1: {0}", hash1.AsString());
            Log.Debug("Hash2: {0}", hash2.AsString());
            return hash1.CompareTo(hash2);
        }

        public byte[] ExtractSalt(byte[] hash)
        {
            return SplitHash(hash).Salt;
        }
        
        private HashParts CreateHash(string raw, byte[] salt)
        {
            Log.Debug("Entering CreateHash.");
            Log.Debug("Raw: {0}", raw);
            Log.Debug("Salt: {0}", salt.AsString());
            var bytes = new Rfc2898DeriveBytes(raw, salt) { IterationCount = HashInformation.IterationCount };
            var hp = new HashParts
            {
                Salt = salt,
                Hash = bytes.GetBytes(HashInformation.HashLength)
            };
            Log.Debug("Created Hash: {0}", hp.Hash.AsString());
            Log.Debug("Used Salt: {0}", hp.Salt.AsString());
            bytes.Dispose();
            return hp;
        }

        private void MergeHashes(HashParts hp)
        {
            if (HashInformation.SaltPosition == SaltPosition.None) return;

            var r = new byte[hp.Hash.Length + hp.Salt.Length];
            int h;

            switch (HashInformation.SaltPosition)
            {
                case SaltPosition.Append:
                    Array.Copy(hp.Hash, 0, r, 0, hp.Hash.Length);
                    Array.Copy(hp.Salt, 0, r, hp.Hash.Length, hp.Salt.Length);
                    break;

                case SaltPosition.Prepend:
                    Array.Copy(hp.Salt, 0, r, 0, hp.Salt.Length);
                    Array.Copy(hp.Hash, 0, r, hp.Salt.Length, hp.Hash.Length);
                    break;

                case SaltPosition.SplitHalf:
                    h = hp.Salt.Length / 2;
                    Array.Copy(hp.Salt, 0, r, 0, h);
                    Array.Copy(hp.Hash, 0, r, h, hp.Hash.Length);
                    Array.Copy(hp.Salt, h, r, (h + hp.Hash.Length), h);
                    break;

                case SaltPosition.SplitHalfReverse:
                    h = hp.Salt.Length / 2;
                    Array.Copy(hp.Salt, h, r, 0, h);
                    Array.Copy(hp.Hash, 0, r, h, hp.Hash.Length);
                    Array.Copy(hp.Salt, 0, r, (h + hp.Hash.Length), h);
                    break;
            }

            hp.Hash = r;
            Log.Debug("Hash: {0}", hp.Hash.AsString());
            Log.Debug("Salt: {0}", hp.Salt.AsString());
            Log.Debug("Merged Hash: {0}", hp.Hash.AsString());
        }

        private HashParts SplitHash(byte[] hash)
        {
            var hp = new HashParts
            {
                Hash = new byte[HashInformation.HashLength],
                Salt = new byte[HashInformation.SaltLength]
            };
            int h;

            switch (HashInformation.SaltPosition)
            {
                case SaltPosition.Append:
                    Array.Copy(hash, 0, hp.Hash, 0, hp.Hash.Length);
                    Array.Copy(hash, hp.Hash.Length, hp.Salt, 0, hp.Salt.Length);
                    break;

                case SaltPosition.Prepend:
                    Array.Copy(hash, 0, hp.Salt, 0, HashInformation.SaltLength);
                    Array.Copy(hash, HashInformation.SaltLength, hp.Hash, 0, HashInformation.HashLength);
                    break;

                case SaltPosition.SplitHalf:
                    h = HashInformation.SaltLength / 2;
                    Array.Copy(hash, 0, hp.Salt, 0, h);
                    Array.Copy(hash, (HashInformation.HashLength + h), hp.Salt, h, h);
                    Array.Copy(hash, h, hp.Hash, 0, HashInformation.HashLength);
                    break;

                case SaltPosition.SplitHalfReverse:
                    h = HashInformation.SaltLength / 2;
                    Array.Copy(hash, (HashInformation.HashLength + h), hp.Salt, 0, h);
                    Array.Copy(hash, 0, hp.Salt, h, h);
                    Array.Copy(hash, h, hp.Hash, 0, HashInformation.HashLength);
                    break;
            }

            CurrentHash = hp.Hash;
            CurrentSalt = hp.Salt;

            if (Log.IsDebugEnabled())
            {
                Log.Debug("Splitted Hash: {0}", hp.Hash.AsString());
                Log.Debug("Splitted Salt: {0}", hp.Salt.AsString());
            }
            return hp;
        }

        #region Inner Classess

        #region Class HashParts

        private class HashParts
        {
            public byte[] Hash { get; set; }
            public byte[] Salt { get; set; }
        }

        #endregion

        #endregion
    }
}
