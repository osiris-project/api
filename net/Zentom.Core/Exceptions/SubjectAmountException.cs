﻿using System;

namespace Zentom.Core.Exceptions
{
    [Serializable]
    public class SubjectAmountException
        : Exception
    {
        public SubjectAmountException(string message)
            : base(message) {}

        public SubjectAmountException()
            : base("A subject amount has been received in a context where there should not be any.") {}
    }
}
