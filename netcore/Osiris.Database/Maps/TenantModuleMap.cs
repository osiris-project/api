﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class TenantModuleMap
        : ClassMap<TenantModule>
    {
        public TenantModuleMap()
        {
            Schema("system");
            Table("tenant_module");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            Map(i => i.IsEnabled);
            Map(i => i.Settings).CustomSqlType("NVARCHAR(MAX)").Default("\"\"");
            Map(i => i.Version).Default("1.0.0");

            References(i => i.Module, "module_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_module_un");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_module_un");
        }
    }
}
