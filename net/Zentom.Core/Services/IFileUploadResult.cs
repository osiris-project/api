﻿namespace Zentom.Core.Services
{
    public interface IFileUploadResult
    {
        string Identifier { get; }
    }
}
