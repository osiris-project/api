﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class ModuleMap
        : ClassMap<Module>
    {
        public ModuleMap()
        {
            Schema("system");
            Table("module");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Name).UniqueKey("module_un");
            Map(i => i.Description).Default("\"\"");
        }
    }
}
