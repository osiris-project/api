﻿using Dobelik.Utils.Identity;
using Zentom.I18n;

namespace Zentom.Integration.Artikos.TributaryDocuments.Artikos
{
    internal class ExemptBill
        : AbstractArtikosFormatter
    {
        #region Constructors

        public ExemptBill(INationalIdentity idUtils)
            : base(idUtils) { }

        #endregion

        #region AbstractArtikosFormatter Implementation

        protected override void Validate()
        {
            if(entity.TaxDocumentLines == null || entity?.TaxDocumentLines?.Count <= 0)
            {
                Errors.Add(new ModelValidation("ExemptBill.NoDetail", Error.NoDetails));
            }

            if(entity.TaxRate > 0 || entity.ExtraTaxRate > 0 || entity.ExtraTaxCode > 0)
            {
                Errors.Add(new ModelValidation("ExemptBill.SubjectAmount", Error.SubjectAmountException));
            }
            else
            {
                isExempt = true;
            }
        }

        #endregion
    }
}
