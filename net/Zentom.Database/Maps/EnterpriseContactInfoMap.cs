﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class EnterpriseContactInfoMap
        : SubclassMap<EnterpriseContactData>
    {
        public EnterpriseContactInfoMap()
        {
            Abstract();
            Schema("organization");
            Table("enterprise_contact_info");

            References(i => i.Enterprise, "enterprise_id")
                .Unique()
                .Cascade.All();
        }
    }
}
