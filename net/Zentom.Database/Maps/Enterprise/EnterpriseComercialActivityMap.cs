﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Enterprise
{
    public class EnterpriseComercialActivityMap
        : ClassMap<EnterpriseComercialActivity>
    {
        public EnterpriseComercialActivityMap()
        {
            Schema("taxation");
            Table("enterprise_comercial_activity");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Enterprise, "enterprise_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("enterprise_comercial_activity");

            References(i => i.ComercialActivity, "comercial_activity_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("enterprise_comercial_activity");
        }
    }
}