﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class DocumentType
        : Entity
    {
        #region Fields

        private string _code;

        private string _name;

        private decimal _taxRate;

        private ISet<TaxDocument> _taxDocuments;

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual decimal TaxRate
        {
            get { return _taxRate; }
            set { _taxRate = value; }
        }

        public virtual ISet<TaxDocument> TaxDocuments
        {
            get { return _taxDocuments; }
            set { _taxDocuments = value; }
        }

        #endregion

        public DocumentType()
        {
            _taxDocuments = new HashSet<TaxDocument>();
        }

        public DocumentType(string code, string name, decimal taxRate, ISet<TaxDocument> taxDocuments)
            : this()
        {
            _code = code;
            _name = name;
            _taxRate = taxRate;

            if(taxDocuments != null)
                _taxDocuments = taxDocuments;
        }
    }
}
