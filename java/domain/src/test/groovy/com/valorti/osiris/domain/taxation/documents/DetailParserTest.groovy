/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DetailParserTest
    extends BaseParserTests {

    @Test
    void test_parseDetailItems() {
        println 'test_readIssuerSection'
        def path = this.buildEBill()
        assert path != null

        def detalle = path.DTE.Documento.Detalle
        assert detalle.size() >= 1 && detalle.size() <= 60

        def details = []
        def parser = new DefaultDetailParser()
        detalle.each {
            def detail = parser.parse(it)
            assert detail != null
            assert detail.name == it.NmbItem.text()
            assert detail.description == it.DscItem.text()
            assert detail.quantity == it.QtyItem.toDouble()
            assert detail.unitPrice == it.PrcItem.toDouble()
            assert detail.lineTotal == it.MontoItem.toDouble()
            details << detail
        }
        assert details.size() == detalle.size()
    }
}
