/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation.documents

/**
 * Thrown when the tenant tax id doesn't match with the one in receptor.taxId field.
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class UnexpectedReceiverException
    extends Exception {

    private static final long serialVersionUID = 1L

    final String expectedTaxId
    final String currentTaxId

    /**
     *
     * @param expectedTaxId The tax id of the current tenant.
     * @param currentTaxId The receptor tax id in the document.
     */
    UnexpectedReceiverException(String expectedTaxId, String currentTaxId) {
        super("The current tenant's tax id (${expectedTaxId}) doesn't match the one on the receptor (${currentTaxId}). " +
            "Switch to the appropiate tenant or make sure that the document is to be received by this tenant.")
        this.expectedTaxId = expectedTaxId
        this.currentTaxId = currentTaxId
    }
}
