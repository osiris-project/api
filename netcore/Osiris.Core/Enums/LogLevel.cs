﻿namespace Osiris.Core.Enums
{
    public enum LogLevel
    {
        Fatal,
        Error,
        Warn,
        Info,
        Debug
    }
}
