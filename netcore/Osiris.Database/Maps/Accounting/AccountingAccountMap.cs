﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.Accounting;

namespace Osiris.Database.Maps.Accounting
{
    public class AccountingAccountMap
        : ClassMap<AccountingAccount>
    {
        public AccountingAccountMap()
        {
            Schema("accounting");
            Table("accounting_account");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Code).UniqueKey("accounting_account_un");
            Map(i => i.Description).Default("\"\"");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("accounting_account_un");
        }
    }
}
