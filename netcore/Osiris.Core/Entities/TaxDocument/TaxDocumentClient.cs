﻿using System.Collections.Generic;

namespace Osiris.Core.Entities.TaxDocument
{
    public class TaxDocumentClient
        : Entity
    {
        #region Fields

        private string taxId;

        private string legalName;

        private string phone;

        private string bankAccountNumber;

        private string comercialActivity;

        private TaxDocument taxDocument;

        private ISet<TaxDocumentClientAddress> addresses;

        #endregion

        #region Properties

        public virtual string TaxId
        {
            get { return taxId; }
            set { taxId = value; }
        }

        public virtual string LegalName
        {
            get { return legalName; }
            set { legalName = value; }
        }

        public virtual string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public virtual string BankAccountNumber
        {
            get { return bankAccountNumber; }
            set { bankAccountNumber = value; }
        }

        public virtual string ComercialActivity
        {
            get { return comercialActivity; }
            set { comercialActivity = value; }
        }

        public virtual ISet<TaxDocumentClientAddress> Addresses
        {
            get { return addresses; }
            set { addresses = value; }
        }

        public virtual TaxDocument TaxDocument
        {
            get { return taxDocument; }
            set { taxDocument = value; }
        }

        #endregion

        public TaxDocumentClient()
        {
            bankAccountNumber = "";
            phone = "";
            comercialActivity = "";
            addresses = new HashSet<TaxDocumentClientAddress>();
        }

        public TaxDocumentClient(TaxDocument taxDocument, string taxId, string legalName, string phone,
            string bankAccountNumber, string comercialActivity, ISet<TaxDocumentClientAddress> addresses)
        {
            this.taxDocument = taxDocument;
            this.taxId = taxId;
            this.legalName = legalName;
            this.phone = phone;
            this.bankAccountNumber = bankAccountNumber;
            this.comercialActivity = comercialActivity;
            this.addresses = addresses;
        }
    }
}
