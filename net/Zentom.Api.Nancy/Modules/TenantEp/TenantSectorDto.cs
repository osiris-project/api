﻿using System;

namespace Zentom.Api.Modules.TenantEp
{
    public class TenantEconomicActivityDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Code { get; set; }
    }
}