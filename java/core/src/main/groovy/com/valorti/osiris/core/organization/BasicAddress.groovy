/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Column
import javax.persistence.ManyToOne
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

import groovy.transform.ToString

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@MappedSuperclass
@ToString(includeNames = true)
abstract class BasicAddress
    extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 70)
    String line1

    @Column(nullable = false, length = 150)
    String line2

    @ManyToOne
    Commune commune

    /**
     * Default constructor.
     */
    protected BasicAddress() {
    }

    /**
     * @param line1
     * @param line2
     * @param commune
     * @throws ValidationException
     * @throws IllegalArgumentException if the commune is null.
     * @since 0.0.1
     */
    BasicAddress(String line1, String line2, Commune commune)
        throws ValidationException, IllegalArgumentException {
        def errors = []
        if (!commune) {
            throw new IllegalArgumentException('No commune provided!')
        }
        if (!line1?.trim()) {
            errors << ValidationBuilder.empty('address.line1')
        }
        if (errors) {
            throw new ValidationException(errors)
        }
        this.line1 = line1
        this.line2 = line2
        this.commune = commune
    }
}
