﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Osiris.Database.Conventions
{
    public class OsirisIdConvention
        : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            instance.Column("id");
            instance.GeneratedBy.GuidComb();
            instance.Not.Nullable();
        }
    }
}
