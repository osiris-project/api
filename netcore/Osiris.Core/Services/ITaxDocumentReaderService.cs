﻿using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Core.Services
{
    public interface ITaxDocumentReaderService
    {
        TaxDocument ReadDocument(string xmlFile);
    }
}
