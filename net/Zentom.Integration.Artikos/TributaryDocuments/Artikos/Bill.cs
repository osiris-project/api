﻿using Dobelik.Utils.Identity;
using Zentom.I18n;

namespace Zentom.Integration.Artikos.TributaryDocuments.Artikos
{
    internal class Bill
        : AbstractArtikosFormatter
    {
        #region Fields

        #endregion

        #region Ctors

        public Bill(INationalIdentity idUtils)
            : base(idUtils) { }

        #endregion

        protected override void Validate()
        {
            if (entity.TaxDocumentLines == null || entity?.TaxDocumentLines?.Count <= 0)
            {
                Errors.Add(new ModelValidation("Bill.NoDetail", Error.NoDetails));
            }
        }
    }
}
