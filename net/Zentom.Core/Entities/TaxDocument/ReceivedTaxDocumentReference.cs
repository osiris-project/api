﻿using System;

namespace Zentom.Core.Entities.TaxDocument
{
    public class ReceivedTaxDocumentReference
        : Entity
    {
        #region Fields

        private string referenceReason;

        private string referenceComment;

        private DateTime? referenceDate;

        private ReceivedTaxDocument parent;

        private ReceivedTaxDocument referencedDocument;

        #endregion

        #region Properties

        public virtual string ReferenceComment
        {
            get { return referenceComment; }
            set { referenceComment = value; }
        }

        public virtual DateTime? ReferenceDate
        {
            get { return referenceDate; }
            set { referenceDate = value; }
        }

        public virtual ReceivedTaxDocument Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public virtual ReceivedTaxDocument ReferencedDocument
        {
            get { return referencedDocument; }
            set { referencedDocument = value; }
        }

        public virtual string ReferenceReason
        {
            get { return referenceReason; }
            set { referenceReason = value; }
        }

        #endregion

        public ReceivedTaxDocumentReference()
        {
        }

        public ReceivedTaxDocumentReference(string referenceReason, string referenceComment, DateTime? referenceDate,
            ReceivedTaxDocument parent, ReceivedTaxDocument referencedDocument)
        {
            this.referenceReason = referenceReason;
            this.referenceComment = referenceComment;
            this.referenceDate = referenceDate;
            this.parent = parent;
            this.referencedDocument = referencedDocument;
        }
    }
}
