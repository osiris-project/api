﻿using FluentValidation;

namespace Osiris.Api.Modules.RoleEp
{
    public class RoleValidator
        : AbstractValidator<RoleCommand>
    {
        public RoleValidator()
        {
            RuleFor(i => i.Name).Length(3, 100);
            RuleFor(i => i.DisplayName).Length(3, 100);
        }
    }
}
