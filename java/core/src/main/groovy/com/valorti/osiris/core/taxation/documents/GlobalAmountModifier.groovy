/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class GlobalAmountModifier extends BigIntegerEntity {
    enum GlobalAmountModifierType {
        DISCOUNT,
        SURCHARGE
    }

    enum GlobalAmountModifierTarget {
        SUBJECT_TO_TAX,
        TAX_FREE,
        NON_BILLABLE,
        ALL
    }

    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false)
    GlobalAmountModifierType type

    @Column(nullable = true, length = 45)
    String description

    @Column(nullable = false)
    boolean isValueInPercent = false

    @Column(precision = 13, scale = 4, nullable = false)
    double value

    @Column(precision = 13, scale = 4, nullable = false)
    double foreignMoneyAmount

    @Column(nullable = false)
    GlobalAmountModifierTarget target

    @ManyToOne
    Document document

    /**
     * Default constructor.
     */
    protected GlobalAmountModifier() {
    }

    /**
     * @param type
     * @param description
     * @param isPercentValue
     * @param value
     * @param foreignMoneyAmount
     * @param target
     * @param document
     */
    GlobalAmountModifier(GlobalAmountModifierType type, String description, boolean isValueInPercent, double value, double foreignMoneyAmount,
    GlobalAmountModifierTarget target, Document document) {
        this.type = type
        this.description = description
        this.isValueInPercent = isValueInPercent
        this.value = value
        this.foreignMoneyAmount = foreignMoneyAmount
        this.target = target
        this.document = document
    }
}
