﻿using System.Collections.Generic;
using System.Linq;

namespace Osiris.Core.Entities
{
    public class CostsCenter
        : TenantEntity
    {
        #region Fields

        private string code;

        private string name;

        private string description;

        private ISet<Enterprise> enterprises = new HashSet<Enterprise>();

        #endregion

        #region Properties

        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual ISet<Enterprise> Enterprises
        {
            get { return enterprises; }
            set { enterprises = value; }
        }

        #endregion

        #region Ctors.

        public CostsCenter()
        {
            description = "";
        }

        public CostsCenter(Tenant tenant, string code, string name, string description)
            : base(tenant)
        {
            this.code = code;
            this.name = name;
            this.description = description;
        }

        #endregion

        public virtual void AddEnterprise(Enterprise enterprise)
        {
            if (enterprises.Any(i => i == enterprise))
            {
                return;
            }
            enterprises.Add(enterprise);
            enterprise.CostsCenter = this;
        }
    }
}
