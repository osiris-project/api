﻿using System;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Api.Modules.ReceivedTaxDocumentsEp
{
    public class ReceivedDocumentDto
    {
        public Guid Id { get; set; }

        public DateTime EmissionDate { get; set; }

        public DateTime ReceptionDate { get; set; }

        public ReceptionStatus ReceptionStatus { get; set; }

        public string DocumentType { get; set; }

        public long FolioNumber { get; set; }

        public string ProviderTaxId { get; set; }

        public string ProviderLegalName { get; set; }

        public bool AcceptedForAccounting { get; set; }
    }
}
