﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class PasswordRequestMap
        : ClassMap<PasswordRequest>
    {
        public PasswordRequestMap()
        {
            Schema("system");
            Table("password_request");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Account)
                .Cascade.SaveUpdate()
                .Not.Nullable();

            Map(i => i.Ipv4).Length(12);
            Map(i => i.Ipv6).Length(39).Nullable();
            Map(i => i.ResetExpireDate);
            Map(i => i.RequestDate);
        }
    }
}
