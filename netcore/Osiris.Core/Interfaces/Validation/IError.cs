﻿using System;

namespace Osiris.Core.Interfaces.Validation
{
    public interface IError
    {
        string Title { get; }

        string Message { get; }

        Exception Exception { get; }
    }
}
