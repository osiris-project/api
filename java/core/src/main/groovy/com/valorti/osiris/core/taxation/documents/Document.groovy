/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.BigIntegerEntity
import com.valorti.osiris.core.ValidationException
import com.valorti.osiris.core.organization.Tenant
import com.valorti.osiris.core.taxation.documents.DocumentDetail.ItemModifier
import com.valorti.osiris.core.taxation.documents.DocumentReference.ReferenceType
import com.valorti.osiris.core.taxation.documents.GlobalAmountModifier.GlobalAmountModifierTarget
import com.valorti.osiris.core.taxation.documents.GlobalAmountModifier.GlobalAmountModifierType
import groovy.transform.ToString

import javax.persistence.*
import java.time.LocalDate

/**
 * Represents a taxation document.
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation', uniqueConstraints = [
    @UniqueConstraint(columnNames = ['document_type', 'folio_number', 'is_tenant_issuer', 'tenant_id'])])
@ToString
class Document
    extends BigIntegerEntity {

    /**
     * Represents the type a document can have.
     */
    enum DocumentType {
        NOT_DEFINED(0),
        START_BILL(29),
        BILL(30),
        TAX_FREE_BILL(32),
        EBILL(33),
        TAX_FREE_EBILL(34),
        BILL_SETTLEMENT(40),
        EBILL_SETTLEMENT(43),
        PURCHASE_BILL(45),
        PURCHASE_EBILL(46),
        DEBIT_NOTE(55),
        EDEBIT_NOTE(56),
        CREDIT_NOTE(60),
        ECREDIT_NOTE(61),
        EXPORTS_BILL(101),
        EXPORTS_EBILL(110)

        final int value

        DocumentType(int value) {
            this.value = value
        }

        static DocumentType fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }
    }

    /**
     * Represents the type of service the document is billing for.
     *
     * @author Nicolas Mancilla <nmancilla@valorti.com>
     * @version 0.0.1
     * @since 0.0.1
     */
    enum ServiceType {
        NOT_DEFINED(0),
        MONTHLY_HOME_SERVICES(1),
        OTHER_MONTHLY_SERVICES(2),
        SERVICES(3),
        HOTEL_SERVICES(4),
        TRANSPORT_SERVICES(5)

        final int value

        ServiceType(int value) {
            this.value = value
        }

        static ServiceType fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }
    }

    /**
     * @author Nicolas Mancilla <nmancilla@valorti.com>
     * @version 0.0.1
     * @since 0.0.1
     */
    enum DispatchType {
        NOT_DEFINED(0),
        RECEIVER(1),
        ISSUER_TO_CLIENT(2),
        ISSUER_TO_CLIENT_OTHER(3)

        final int value

        DispatchType(int value) {
            this.value = value
        }

        static DispatchType fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }
    }

    /**
     * Represents the payment type of the document total value.
     * <ul>
     *     <li>NOT_DEFINED: default value.</li>
     *     <li>CASH: if the document is paid in cash</li>
     *     <li>CREDIT: if the document is credited</li>
     *     <li>FREE</li>
     * </ul>
     * @author Nicolas Mancilla <nmancilla@valorti.com>
     * @since 0.0.1
     */
    enum PaymentType {
        NOT_DEFINED(0),
        CASH(1),
        CREDIT(2),
        FREE(3)

        final int value

        PaymentType(int value) {
            this.value = value
        }

        static PaymentType fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }
    }

    @Transient
    private static final long serialVersionUID = 1L

    public static final int MAX_DETAIL = 60

    @Column(nullable = false, length = 3)
    final DocumentType documentType

    @Column(nullable = false)
    final BigInteger folioNumber

    @Column(nullable = false)
    private boolean isTenantIssuer = false

    @Column(nullable = false, columnDefinition = 'DATE')
    final LocalDate accountingDate

    @Column(nullable = true, columnDefinition = 'DATE')
    final LocalDate expirationDate

    @Column(nullable = true)
    final Boolean isGrossAmount

    /**
     * Only for credit notes that
     * have right to discount taxes.
     */
    @Column(nullable = true)
    final Boolean discountAllowed

    /**
     * The dispatch type of the products.
     */
    @Column(nullable = true)
    final DispatchType dispatchType

    /**
     * The service type.
     */
    @Column(nullable = true)
    final ServiceType serviceType

    @Column(nullable = true)
    final PaymentType paymentType

    @Column(nullable = false, length = 20)
    final String principalTaxId

    @Column(nullable = false, length = 20)
    final String requesterTaxId

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = 'id', nullable = false)
    DocumentIssuer issuer

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = 'id', nullable = false)
    DocumentReceiver receiver

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = 'id', nullable = false)
    private DocumentSummary summary

    @OneToMany(mappedBy = 'document', cascade = CascadeType.ALL, orphanRemoval = true)
    def details = new HashSet<DocumentDetail>()

    @OneToMany(mappedBy = 'document', cascade = CascadeType.ALL, orphanRemoval = true)
    def references = new HashSet<DocumentReference>()

    // TODO: implement sub total info

    @OneToMany(mappedBy = 'document', cascade = CascadeType.ALL, orphanRemoval = true)
    def globalAmountModifiers = new HashSet<GlobalAmountModifier>()

    @OneToMany(mappedBy = 'document', cascade = CascadeType.ALL, orphanRemoval = true)
    def globalCommissions = new HashSet<GlobalCommission>()

    @ManyToOne
    final Tenant tenant

    /**
     * Default constructor.
     */
    protected Document() {
        this.folioNumber = -1
        this.documentType = BILL
        this.accountingDate = LocalDate.now()
        this.expirationDate = LocalDate.now()
        this.isTenantIssuer = false
        this.isGrossAmount = null
        this.requesterTaxId = ''
        this.principalTaxId = ''
        this.receiver = null
        this.issuer = null
        this.tenant = null
    }

    /**
     * @param documentType
     * @param folioNumber
     * @param accountingDate
     * @param requesterTaxId
     * @param issuer
     * @param tenant
     * @throws IllegalArgumentException
     */
    Document(DocumentType documentType, BigInteger folioNumber, LocalDate accountingDate,
             String requesterTaxId, DocumentIssuer issuer, Tenant tenant)
        throws IllegalArgumentException {
        this(documentType, folioNumber, accountingDate, null, false,
            (documentType == CREDIT_NOTE), null, null,
            null, '', requesterTaxId, issuer, tenant)
    }

    /**
     *
     * @param documentType
     * @param folioNumber A number issued by the taxation authority.
     * @param accountingDate A date that represents when this document
     * is accounted for.
     * @param expirationDate The payment expiration date.
     * @param isGrossAmount Specifies if the detail lines contain net or gross amounts
     * in the total.
     * @param discountAllowed <b>Only for credit notes</b>. Specifies if this document
     * is allowed to discount taxes.
     * @param dispatchType Included only if the document has products attached. Indicates the
     * dispatch type of the products. Default: DispatchType.NONE.
     * @param serviceType The service type this document is charging for. Default: ServiceType.NONE.
     * @param paymentType Used to specify the payment type the receptor will use
     * to pay the total of the document. Default: PaymentType.NONE.
     * @param principalTaxId The benefited with the tax credit.
     * @param requesterTaxId The requester of the document.
     * @param issuer The issuer of the document. It can be the current tenant or an enterprise.
     * @param tenant The tenant owner of the object. This is just for relationship.
     * @throws IllegalArgumentException If the any of the following is satisfied:
     * <ul>
     *     <li>The document type is null</li>
     *     <li>The folio number is either null or 0</li>
     *     <li>The accounting date is null</li>
     *     <li>The issuer is null</li>
     *     </li> The tenant is null</li>
     * </ul>
     */
    Document(DocumentType documentType, BigInteger folioNumber,
             LocalDate accountingDate, LocalDate expirationDate, Boolean isGrossAmount, Boolean discountAllowed,
             DispatchType dispatchType, ServiceType serviceType, PaymentType paymentType,
             String principalTaxId, String requesterTaxId, DocumentIssuer issuer,
             Tenant tenant)
        throws IllegalArgumentException {
        if (documentType == NOT_DEFINED) {
            throw new IllegalArgumentException('documentType')
        }
        if (!folioNumber) {
            throw new IllegalArgumentException('folioNumber')
        }
        if (!accountingDate) {
            throw new IllegalArgumentException('accountingDate')
        }
        if (!issuer) {
            throw new IllegalArgumentException('issuer')
        }
        if (!tenant) {
            throw new IllegalArgumentException('tenant')
        }
        this.documentType = documentType
        this.folioNumber = folioNumber
        this.accountingDate = accountingDate
        this.expirationDate = expirationDate
        this.isGrossAmount = isGrossAmount
        this.discountAllowed = discountAllowed
        this.dispatchType = dispatchType
        this.serviceType = serviceType
        this.paymentType = paymentType
        this.principalTaxId = principalTaxId
        this.requesterTaxId = requesterTaxId
        this.issuer = issuer
        this.tenant = tenant
        this.isTenantIssuer = issuer.data.taxId == tenant.taxId
    }

    /**
     * Retrieves the built document summary.
     * @return The document summary.
     */
    DocumentSummary getSummary() {
        return this.summary
    }

    /**
     * Sets the detail lines.
     * @param details The detail lines.
     * @throws IllegalArgumentException If the details is null or empty.
     * @throws MaxDetailExceededException If the details.size() > MAX_DETAIL
     */
    void setDetails(Set<DocumentDetail> details)
        throws IllegalArgumentException, MaxDetailExceededException {
        if (!details) {
            throw new IllegalArgumentException('No details provided')
        }
        if (details.size() > MAX_DETAIL) {
            throw new MaxDetailExceededException(MAX_DETAIL)
        }
        this.details = details
        this.buildSummary()
    }

    /**
     * Specifies if this tenant was the issuer of the document,
     * to differentiate clients from providers.
     * @return True if it was the issuer, false otherwise.
     */
    boolean isTenantIssuer() {
        return this.isTenantIssuer
    }

    /**
     * Sets the document issuer.
     * @param issuer The current document issuer.
     * @throws IllegalArgumentException If the issuer is null.
     */
    void setIssuer(DocumentIssuer issuer)
        throws IllegalArgumentException {
        if (!issuer) {
            throw new IllegalArgumentException('No document issuer provided')
        }
        this.issuer = issuer
        this.isTenantIssuer = this.tenant.taxId == issuer.data.taxId
    }

    /**
     * Sets the document receiver.
     * @param receiver The current document receptor.
     * @throws IllegalArgumentException if the receiver is null.
     */
    void setReceiver(DocumentReceiver receiver)
        throws IllegalArgumentException {
        if (!receiver) {
            throw new IllegalArgumentException('No document receiver provided')
        }
        this.receiver = receiver
    }

    /**
     * Builds a document receiver with the specified data.
     * @param taxId The receiver's taxation id.
     * @param name The name of the receiver.
     * @param commercialActivity The commercial activity the receiver can
     * execute.
     * @param legalAddress The legal address.
     * @param phones A list of phones, if any.
     * @param email A contact email, if any.
     * @param economicActivityCodes A list of economic activities this
     * document is related to.
     * @throws IllegalArgumentException
     */
    void setIssuer(String taxId, String name, String commercialActivity,
                   Address legalAddress, List<String> phones, String email,
                   List<String> economicActivityCodes)
        throws IllegalArgumentException {
        this.issuer = new DocumentIssuer(
            new EnterpriseData(taxId, name, commercialActivity, legalAddress),
            phones, email, economicActivityCodes)
        this.issuer.data.document = this
    }

    /**
     *
     * @param taxId The receiver's taxation id.
     * @param name The name of the receiver.
     * @param commercialActivity The commercial activity the receiver can
     * execute.
     * @param legalAddress The legal address.
     * @param phone Optional contact phone.
     * @param email Optional contact email.
     * @param postalAddress Optional postal address.
     * @throws IllegalArgumentException
     */
    void setReceiver(String taxId, String name, String commercialActivity,
                     Address legalAddress, String phone, String email,
                     Address postalAddress)
        throws IllegalArgumentException {
        this.receiver = new DocumentReceiver(
            new EnterpriseData(taxId, name, commercialActivity, legalAddress),
            postalAddress, phone, email)
        this.receiver.data.document = this
    }

    /**
     *
     * @param taxId
     * @param name
     * @param commercialActivity
     * @param legalAddress
     * @throws IllegalArgumentException
     */
    void setReceiver(String taxId, String name, String commercialActivity,
                     Address legalAddress)
        throws IllegalArgumentException {
        this.setReceiver(new EnterpriseData(taxId, name, commercialActivity, legalAddress))
    }

    /**
     *
     * @param data
     * @throws IllegalArgumentException If the data is null.
     */
    void setReceiver(EnterpriseData data) {
        this.receiver = new DocumentReceiver(data)
        this.receiver.data.document = this
    }

    /**
     *
     * @param documentType
     * @param isGlobalReference
     * @param referenceFolio
     * @param type
     * @param date
     * @param reason
     * @throws ValidationException
     */
    void addReference(String documentType, boolean isGlobalReference, String referenceFolio,
                      ReferenceType type, LocalDate date, String reason)
        throws ValidationException {
        def reference = new DocumentReference(documentType, isGlobalReference,
            referenceFolio, type, date, reason)
        reference.document = this
        this.references.add(reference)
    }

    /**
     * Utils
     */
    protected void buildSummary() {
        def taxFree = 0
        def subjectToTax = 0
        def nonBillable = 0
        def deposit = 0
        def noSale = 0
        def toLower = 0
        def negNonBillable = 0
        this.details.each {
            def value = it.lineTotal
            switch (it.modifier) {
                case ItemModifier.TAX_FREE:
                    taxFree += value
                    break
                case ItemModifier.NON_BILLABLE:
                    nonBillable += value
                    break
                case ItemModifier.DEPOSIT_GUARANTEE:
                    deposit += value
                    break
                case ItemModifier.NO_SALE:
                    noSale += value
                    break
                case ItemModifier.TO_LOWER:
                    toLower += value
                    break
                case ItemModifier.NEGATIVE_NON_BILLABLE:
                    negNonBillable += value
                    break
                default:
                    subjectToTax += value
                    break
            }
        }
        this.globalAmountModifiers.each {
            switch (it.target) {
                case GlobalAmountModifierTarget.ALL:
                    apply(taxFree, it)
                    apply(subjectToTax, it)
                    break
                case GlobalAmountModifierTarget.TAX_FREE:
                    apply(taxFree, it)
                    break
                case GlobalAmountModifierTarget.SUBJECT_TO_TAX:
                    apply(subjectToTax, it)
                    break
                case GlobalAmountModifierTarget.NON_BILLABLE:
                    break
            }
        }
    }

    private static double apply(double amount, GlobalAmountModifier modifier) {
        def amountToApply = 0
        if (modifier.isValueInPercent) {
            amountToApply = amount * modifier.value
        } else {
            amountToApply = modifier.value
        }
        if (modifier.type == GlobalAmountModifierType.SURCHARGE) {
            return plus(amount, amountToApply)
        }
        return minus(amount, amountToApply)
    }

    private static double minus(double amount, double credit) {
        return amount - credit
    }

    private static double plus(double amount, double debit) {
        return amount + debit
    }
}
