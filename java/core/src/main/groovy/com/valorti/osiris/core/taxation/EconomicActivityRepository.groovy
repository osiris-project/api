/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation

import com.valorti.osiris.core.UUIDRepository

import java.util.stream.Stream

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface EconomicActivityRepository
    extends UUIDRepository<EconomicActivity> {
    EconomicActivity findOneByCode(String code)

    Stream<EconomicActivity> findByName(String name)
}
