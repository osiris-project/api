﻿using System;
using System.Collections.Generic;

namespace Zentom.Core.Entities.Accounting
{
    public enum AccountingVoucherStatus
        : short
    {
        Saved = 0,
        Accepted,
        Rejected
    }

    public class AccountingVoucher
        : TenantEntity
    {
        #region Fields
        
        private int number;

        private DateTime date;

        private string description;

        private AccountingVoucherStatus status;

        private ISet<AccountingVoucherLine> lines = new HashSet<AccountingVoucherLine>();

        #endregion

        #region Properties

        public virtual int Number
        {
            get { return number; }
            set { number = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        public virtual AccountingVoucherStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public virtual ISet<AccountingVoucherLine> Lines
        {
            get { return lines; }
            set { lines = value; }
        }

        #endregion

        #region Ctors.

        public AccountingVoucher()
        {
        }

        public AccountingVoucher(Tenant tenant, int number, DateTime date, string description, AccountingVoucherStatus status)
            : base(tenant)
        {
            this.number = number;
            this.date = date;
            this.description = description;
            this.status = status;
        }

        #endregion
    }
}