﻿using System;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentDto
        : Command
    {
        public long FolioNumber { get; set; }

        public DateTime AccountingDate { get; set; }

        public TaxDocumentStatus DocumentStatus { get; set; }

        public decimal TotalNetAmount { get; set; }

        public decimal ExemptAmount { get; set; }

        public decimal SubjectAmount { get; set; }

        public decimal TotalTaxes { get; set; }

        public decimal GrossTotal { get; set; }

        public string DocTypeCode { get; set; }

        public Guid ClientId { get; set; }

        public string ClientLegalName { get; set; }

        public string ClientTaxId { get; set; }

        public string ClientSectorName { get; set; }

        public bool IsVoided { get; set; }

        public decimal TaxRate { get; set; }
    }
}
