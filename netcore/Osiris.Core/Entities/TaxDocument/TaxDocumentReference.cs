﻿using System;
using System.Runtime.CompilerServices;

namespace Osiris.Core.Entities.TaxDocument
{
    public class TaxDocumentReference
        : Entity
    {
        private string referenceReason;

        private string referenceComment;

        private TaxDocument parent;

        private TaxDocument referencedDocument;

        private DateTime date;

        #region Properties

        public virtual string ReferenceComment
        {
            get { return referenceComment; }
            set { referenceComment = value; }
        }

        public virtual string ReferenceReason
        {
            get { return referenceReason; }
            set { referenceReason = value; }
        }

        public virtual TaxDocument Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public virtual TaxDocument ReferencedDocument
        {
            get { return referencedDocument; }
            set { referencedDocument = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        #endregion

        public TaxDocumentReference() { }

        public TaxDocumentReference(string referenceReason, string referenceComment,
            TaxDocument parent, TaxDocument referencedDocument)
        {
            this.referenceReason = referenceReason;
            this.referenceComment = referenceComment;
            this.parent = parent;
            this.referencedDocument = referencedDocument;
            this.date = referencedDocument.AccountingDate;
        }

        public TaxDocumentReference(string referenceReason, string referenceComment,
            TaxDocument parent, TaxDocument referencedDocument, DateTime date)
            : this(referenceReason, referenceComment, parent, referencedDocument)
        {
            this.date = date;
        }
    }
}
