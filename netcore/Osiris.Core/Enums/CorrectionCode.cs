﻿namespace Osiris.Core.Enums
{
    public enum CorrectionCode
    {
        None = 0,
        //[Display(Name = "VoidDocument", ResourceType = typeof(Text))]
        VoidDocument = 1,
        //[Display(Name = "AmmendText", ResourceType = typeof(Text))]
        AmendText = 2,
        //[Display(Name = "AmmendValues", ResourceType = typeof(Text))]
        AmendValues = 3
    }
}
