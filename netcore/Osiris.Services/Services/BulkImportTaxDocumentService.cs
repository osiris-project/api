﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using ExcelDataReader;
using Osiris.Core.Interfaces.Validation;
using Osiris.Core.Services;
using Slf4Net;

namespace Osiris.Services {
    // TODO: change Process(stream, contentType) to use an enum instead.

    public class BulkImportTaxDocumentService : IBulkImportTaxDocumentService, IDisposable {
        private readonly ILogger log = LoggerFactory.GetLogger (typeof (BulkImportTaxDocumentService));

        #region Implementation of IService

        public bool IsValid => ErrorList == null || ErrorList.Count <= 0;

        public IList<IModelValidation> ErrorList { get; }

        #endregion

        public BulkImportTaxDocumentService () {
            ErrorList = new List<IModelValidation> ();
        }

        #region Implementation of IBulkImportTaxDocumentService

        public IOperationResult<IList<IBulkImportRow>, string> Process (FileInfo fileInfo) {
            IOperationResult<IList<IBulkImportRow>, string> result = null;
            using (var stream = File.Open (fileInfo.FullName, FileMode.Open, FileAccess.Read)) {
                result = ParseDocument (stream);
            }
            return result;
        }

        public IOperationResult<IList<IBulkImportRow>, string> Process (string filePath) {
            IOperationResult<IList<IBulkImportRow>, string> result = null;
            using (var stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
                result = ParseDocument (stream);
            }
            return result;
        }

        public IOperationResult<IList<IBulkImportRow>, string> Process (Stream stream) {
            IOperationResult<IList<IBulkImportRow>, string> result = null;
            using (stream) {
                result = ParseDocument (stream);
            }
            return result;
        }

        #endregion

        private IOperationResult<IList<IBulkImportRow>, string> ParseDocument (Stream stream) {
            var config = new ExcelReaderConfiguration () {
                FallbackEncoding = Encoding.GetEncoding (65001) // UTF-8
            };

            using (var reader = ExcelReaderFactory.CreateReader (stream, config)) {
                if (reader.ResultsCount < 1) {
                    log.Error ("The book doesn't have any sheets; Nothing to import.");
                    return
                    new DefaultOperationResult<IList<IBulkImportRow>, string> (
                        new List<IBulkImportRow>(),
                        new List<string> {
                            I18n.Error.EmptyBook
                        });
                }
                return ParseDocument (reader);
            }
        }

        private IOperationResult<IList<IBulkImportRow>, string> ParseDocument (IExcelDataReader reader) {
            var rows = new List<IBulkImportRow>();
            var errors = new List<string>();

            for (var sheetIndex = 0; sheetIndex < reader.ResultsCount; sheetIndex++) {
                log.Debug ("Parsing sheet {2} (name '{0}') of {1}",
                    reader.Name, reader.ResultsCount, sheetIndex);
                var skipCount = 0;
                var rowNum = 1;
                while (reader.Read ()) {
                    rowNum++;
                    if (skipCount < 2) continue;
                    var row = BuildRow (reader, rowNum);
                    if (row == null) {
                        errors.Add (string.Format (
                            I18n.Error.BookImportError,
                            row));
                        continue;
                    }
                    rows.Add (row);
                }
                reader.NextResult ();
            }
            return new DefaultOperationResult<IList<IBulkImportRow>, string>(rows, errors);
        }

        private IBulkImportRow BuildRow (IExcelDataReader dataReader, int row) {
            try {
                var gloss = dataReader.GetString (0);
                var amount = dataReader.GetDouble (1);
                var doctype = dataReader.GetValue (2);
                return new BulkImportRow (
                    gloss,
                    amount,
                    doctype
                );
            } catch (Exception ex) {
                log.Error ("Failed to create row.", ex);
            }

            return null;
        }

        #region Implementation of IDisposable

        public void Dispose () { }

        #endregion

        private class BulkImportRow
            : IBulkImportRow {
                public string Gloss { get; }

                public double NetAmount { get; }

                public string DocumentType { get; set; }

                public BulkImportRow (
                    string gloss, double netAmount, object documentType = null) {
                    Gloss = gloss;
                    NetAmount = netAmount;

                    if (documentType != null) {
                        DocumentType = documentType.ToString ().Trim ();
                    }
                    DocumentType = null;
                }
            }
    }
}
