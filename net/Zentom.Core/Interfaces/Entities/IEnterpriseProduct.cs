﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseProduct
        : IVersioned
    {
        IProduct Product { get; set; }

        IEnterprise Enterprise { get; set; }
    }

    public interface IEnterpriseProductId
    {
        IEnterprise Enterprise { get; set; }

        IProduct Product { get; set; }
    }
}
