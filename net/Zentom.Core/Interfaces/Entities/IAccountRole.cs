﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IAccountRole
        : IVersioned
    {
        IRole Role { get; set; }

        IAccount Account { get; set; }
    }

    public interface IAccountRoleId
    {
        IRole Role { get; set; }

        IAccount Account { get; set; }
    }
}
