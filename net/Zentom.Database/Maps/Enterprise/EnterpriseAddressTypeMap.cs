﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Enterprise
{
    public class EnterpriseAddressTypeMap
        : ClassMap<EnterpriseAddressType>
    {
        public EnterpriseAddressTypeMap()
        {
            Schema("organization");
            Table("enterprise_address_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.EnterpriseAddress, "enterprise_address_id")
                .UniqueKey("enterprise_address_type_un")
                .Cascade.All();

            References(i => i.AddressType, "address_type_id")
                .UniqueKey("enterprise_address_type_un")
                .Cascade.SaveUpdate();
        }
    }
}
