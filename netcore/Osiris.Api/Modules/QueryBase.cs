﻿namespace Osiris.Api.Modules
{
    public class QueryBase<T>
    {
        public object Id { get; set; }

        public T Property { get; set; }
    }
}
