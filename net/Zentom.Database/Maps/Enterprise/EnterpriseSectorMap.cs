﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Enterprise
{
    public class EnterpriseSectorMap
        : ClassMap<EnterpriseSector>
    {
        public EnterpriseSectorMap()
        {
            Schema("taxation");
            Table("enterprise_sector");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Enterprise, "enterprise_id")
                .UniqueKey("enterprise_sector_un")
                .Cascade.SaveUpdate();

            References(i => i.Sector, "sector_id")
                .UniqueKey("enterprise_sector_un")
                .Cascade.SaveUpdate();
        }
    }
}
