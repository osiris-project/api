﻿using Nancy.ModelBinding;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.SpecificTaxEp
{
    public class SpecificTaxModule
        : TenantApi
    {
        public SpecificTaxModule(ISessionFactory factory)
            : base("specific-taxes", factory)
        {
            this.Get(BasePath, List);
            this.Get<Command>(BasePath + "/{id:guid}", Read);
        }

        [Permission(Permission = "tax-document-read")]
        private object Read(Command cmd)
        {
            var entity = db.Get<SpecificTax>(cmd.Id);
            return entity == null
                ? Negotiate.WithNotFound(I18n.Text.SpecificTax, cmd.Id)
                : Negotiate.WithOkAndModel(entity);
        }

        [Permission(Permission = "tax-document-list")]
        private object List(object arg)
        {
            var bound = this.BindTo(new SpecificTaxQuery());
            var query = db.QueryOver<SpecificTax>();
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var page = Settings.PaginationSettings.Page;

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.Name + "%");
            }

            if (bound.Code > 0)
            {
                query.Where(i => i.Code >= bound.Code || i.Code <= bound.Code);
            }

            var items = query.PageResults(page, rpp);
            return Negotiate.WithOkAndList(items, Context.Request.Url);
        }
    }
}