/**
 * Copyright (c) Mar 10, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import groovy.transform.ToString

import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
@ToString(includeNames = true)
class DocumentReference
    extends BigIntegerEntity {
    /**
     * Specifies the action that a document reference performs.
     * <ul>
     *     <li>NOT_DEFINED: this exists only for the cases where the
     *     reference type could not be parsed from an integer value</li>
     *     <li>VOID: voids a document.</li>
     *     <li><AMEND_TEXT: amends text of the referenced document./li>
     *     <li>AMEND_VALUES: amends values of the referenced document.</li>
     * </ul>
     *
     * @author Nicolas Mancilla <nmancilla@valorti.com>
     * @version 0.0.1
     * @since 0.0.1
     */
    enum ReferenceType {
        NOT_DEFINED(0),
        VOID(1),
        AMEND_TEXT(2),
        AMEND_VALUES(3)

        final int value

        ReferenceType(int value) {
            this.value = value
        }

        static ReferenceType fromInt(int value) {
            return values().find {
                if (it.value == value) {
                    return it
                }
            }
        }

        static ReferenceType fromString(String str) {
            return values().find {
                if (it.name() == str) {
                    return it
                }
            }
        }
    }

    @Transient
    private static final long serialVersionUID = 1L

    /**
     * The referenced document type.
     */
    @Column(nullable = false, length = 4)
    final String documentType

    @Column(nullable = false)
    final boolean isGlobalReference

    @Column(nullable = false)
    final String folioNumber

    @Column(nullable = false)
    final ReferenceType type

    @Column(columnDefinition = 'DATE', nullable = false)
    final LocalDate date

    @Column(nullable = false, length = 90)
    final String reason

    @ManyToOne
    Document document

    @ManyToOne
    Document referencedDocument

    /**
     * Default constructor.
     */
    protected DocumentReference() {
        this.documentType = ''
        this.isGlobalReference = false
        this.folioNumber = ''
        this.date = null
        this.reason = ''
        this.document = null
        this.type = ReferenceType.NONE
        this.referencedDocument = null
    }

    /**
     * @param documentType
     * @param isGlobalReference
     * @param folioNumber
     * @param referenceType
     * @param date
     * @param reason
     * @param references
     * @throws ValidationException
     * @since 0.0.1
     */
    DocumentReference(String documentType, boolean isGlobalReference, String folioNumber,
                      ReferenceType type, LocalDate date, String reason)
        throws ValidationException {
        def e = []
        if (!documentType.trim()) {
            e << ValidationBuilder.empty('documentReference.documentType')
        }

        if (type == ReferenceType.AMEND_VALUES && isGlobalReference) {
            e << ValidationBuilder.build('documentReference.isGlobalReference.invalid', 'The document cannot be a global reference and amend values')
            e << ValidationBuilder.build('documentReference.type.invalid', 'The document cannot be a global reference and amend values')
        }
        if (isGlobalReference) {
            if (folioNumber != '0') {
                e << ValidationBuilder.build('documentReference.referenceFolio.invalid', 'This document references many others, so the folio must be 0')
            }
        } else {
            if (!folioNumber || folioNumber == '0') {
                e << ValidationBuilder.build('documentReference.referenceFolio.empty', 'The document must have a valid reference folio')
            }
        }
        if (!reason.trim()) {
            e << ValidationBuilder.empty('documentReference.reason')
        }
        if (e) {
            throw new ValidationException(e)
        }
        this.documentType = documentType.trim()
        this.isGlobalReference = isGlobalReference
        this.folioNumber = folioNumber.trim()
        this.type = type
        this.date = date
        this.reason = reason.trim()
        this.referencedDocument = null
    }

    /**
     * Builds a document reference from an existing document.
     *
     * @param referencedDocument The referenced document upon which this
     * object is built.
     * @param type The type of reference (see {@link ReferenceType})
     * @param reason Why is the referenced document modified by the reference type.
     * @throws IllegalArgumentException if the referencedDocument is null.
     * @throws ValidationException
     */
    DocumentReference(Document referencedDocument, ReferenceType type, String reason)
        throws IllegalArgumentException, ValidationException {
        if (!referencedDocument) {
            throw new IllegalArgumentException('No referenced document provided')
        }
        def e = []
        this.documentType = referencedDocument.documentType
        this.isGlobalReference = false
        this.folioNumber = referencedDocument.folioNumber.toString()
        this.date = referencedDocument.accountingDate
        if (type == ReferenceType.NONE) {
            e << ValidationBuilder.empty('documentReference.type')
        }
        if (!reason.trim()) {
            e << ValidationBuilder.empty('documentReference.reason')
        }
        if (e) {
            throw new ValidationException(e)
        }
        this.type = type
        this.reason = reason.trim()
        this.referencedDocument = referencedDocument
    }
}
