/**
 * Copyright (c) Mar 7, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.organization.Tenant

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 *
 */
@Entity
@Table(schema = '', uniqueConstraints = [
    @UniqueConstraint(columnNames = ['account_id', 'tenant_id'])])
class AuthorizedAccount extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Account account

    @ManyToOne
    Tenant tenant

    /**
     *
     */
    protected AuthorizedAccount() {
    }

    /**
     * @param account
     * @param tenant
     */
    AuthorizedAccount(Account account, Tenant tenant) {
        this.account = account
        this.tenant = tenant
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((account == null) ? 0 : account.hashCode())
        result = prime * result + ((tenant == null) ? 0 : tenant.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof AuthorizedAccount)) {
            return false
        }
        final other = (AuthorizedAccount) obj
        if(!Objects.equals(this.tenant, other.tenant)) {
            return false
        }
        return Objects.equals(this.account, other.account)
    }
}
