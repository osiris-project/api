﻿namespace Osiris.Api.Modules.ProfileEp
{
    public class TaxDocumentTemplateQuery
    {
        public string Name { get; set; }

        public string DocumentType { get; set; }

        public bool IncludeJson { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
