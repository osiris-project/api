/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface BigIntegerRepository<T>
extends JpaRepository<T, BigInteger> {
}
