﻿namespace Zentom.Core.Enums
{
    public enum StringMatchMode
    {
        Start,
        End,
        Anywhere,
        Exact
    }
}
