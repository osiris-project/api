﻿using System;
using System.ComponentModel.DataAnnotations;
using Osiris.I18n;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentLineVm
    {
        [Display(ResourceType = typeof(Text), Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(I18n.Error), ErrorMessageResourceName = "RequiredField")]
        public string Text { get; set; }

        [Display(ResourceType = typeof(Text), Name = "Quantity")]
        [Required(ErrorMessageResourceType = typeof(I18n.Error), ErrorMessageResourceName = "RequiredField")]
        public decimal Quantity { get; set; }

        [Display(ResourceType = typeof(Text), Name = "UnitPrice")]
        [Required(ErrorMessageResourceType = typeof(I18n.Error), ErrorMessageResourceName = "RequiredField")]
        public decimal UnitPrice { get; set; }

        [Display(ResourceType = typeof(Text), Name = "AdditionalGloss")]
        [Required(ErrorMessageResourceType = typeof(I18n.Error), ErrorMessageResourceName = "RequiredField")]
        [StringLength(1000, ErrorMessageResourceName = "AdditionalGlossLength", ErrorMessageResourceType = typeof(I18n.Error))]
        public string AdditionalGloss { get; set; }

        #region Product/Service

        [Display(ResourceType = typeof(Text), Name = "Product")]
        public Guid SelectedProductId { get; set; }

        [Display(ResourceType = typeof(Text), Name = "Service")]
        public Guid SelectedServiceId { get; set; }

        #endregion
        
        [Display(ResourceType = typeof(Text), Name = "HasExtraTaxes")]
        public bool HasExtraTaxes { get; set; }

        [Display(ResourceType = typeof(Text), Name = "MeasureUnit")]
        public string MeasureUnit { get; set; }

        [Display(ResourceType = typeof(Text), Name = "LineTotal")]
        public decimal LineTotal { get; set; }

        [Display(ResourceType = typeof(Text), Name = "TaxAmount")]
        public decimal TaxAmount { get; set; }

        [Display(ResourceType = typeof(Text), Name = "ExtraTaxAmount")]
        public decimal ExtraTaxAmount { get; set; }

        [Display(ResourceType = typeof(Text), Name = "TotalTaxes")]
        public decimal TotalTaxAmount { get; set; }

        [Display(ResourceType = typeof(Text), Name = "NetAmount")]
        public decimal NetAmount { get; set; }
    }
}
