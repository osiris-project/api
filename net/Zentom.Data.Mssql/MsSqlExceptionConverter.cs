﻿using log4net;
using NHibernate.Exceptions;
using System;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Zentom.Data.Exceptions;

namespace Zentom.Data.Mssql
{
    public class MsSqlExceptionConverter
        : ISQLExceptionConverter
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MsSqlExceptionConverter));

        public Exception Convert(AdoExceptionContextInfo context)
        {
            Log.Debug("Entering Convert.");
            var sqlException = ADOExceptionHelper.ExtractDbException(context.SqlException) as SqlException;

            if (sqlException == null) return null;
            Exception result;

            switch (sqlException.Number)
            {
                case 2601:
                case 2627:
                    result = GetUniqueKeyException(sqlException);
                    break;
                default:
                    result = SQLStateConverter.HandledNonSpecificException(context.SqlException,
                        context.Message, context.Sql);
                    break;
            }

            Log.Debug("Leaving Convert.");
            return result;
        }

        private static UniqueKeyException GetUniqueKeyException(SqlException ex)
        {
            Log.Debug("Entering UniqueKeyException.");
            string constraint, value;
            ExtractUniqueKeyViolationData(ex.Message, out constraint, out value);
            Log.Debug("Leaving UniqueKeyException.");
            return new UniqueKeyException(ex, value, constraint);
        }

        private static void ExtractUniqueKeyViolationData(string message, out string constraint, out string value)
        {
            Log.Debug("Entering ExtractUniqueKeyViolationData.");
            Log.DebugFormat("Message: {0}", message);
            var match = Regex.Match(message, @"('[a-z0-9_\-]*')|\((.*?)\)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);
            constraint = match.Groups[1].Value;
            match = match.NextMatch();
            value = match.Groups[2].Value;
            Log.DebugFormat("Constraint: {0}, Value: {1}", constraint, value);
            Log.Debug("Leaving ExtractUniqueKeyViolationData.");
        }
    }
}
