﻿using System;

namespace Osiris.Api.Modules.Auth
{
    public class RoleCommand
    {
        public Guid AccountId { get; set; }

        public Guid TenantId { get; set; }
    }
}
