﻿using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocumentProductBase
        : IVersioned
    {
        /// <summary>
        /// How much of this product (or service) will be in the document.
        /// </summary>
        decimal Quantity { get; set; }

        /// <summary>
        /// An optional comment for this product.
        /// </summary>
        string Comment { get; set; }

        /// <summary>
        /// The referenced tax document object.
        /// </summary>
        ITaxDocument TaxDocument { get; set; }

        IBaseProduct BaseProduct { get; set; }
    }

    public interface ITaxDocumentProductBaseId
    {
        ITaxDocument TaxDocument { get; set; }

        IBaseProduct BaseProduct { get; set; }
    }
}
