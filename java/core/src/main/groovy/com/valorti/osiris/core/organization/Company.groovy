/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.Transient

import com.valorti.osiris.core.DefaultValidationError
import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationException

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @since 0.0.1
 */
@MappedSuperclass
abstract class Company extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 20)
    String taxId

    @Column(nullable = false, length = 100)
    String legalName

    @Column(nullable = false, length  = 100)
    String displayName

    @Column(nullable = false, length = 254)
    String email

    @Column(nullable = false, length = 50)
    String phone

    @Column(nullable = false, length = 15)
    String bankAccountNumber

    protected Company() {
    }

    /**
     *
     * @param taxId
     * @param legalName
     * @param displayName
     */
    protected Company(String taxId, String legalName, String displayName) {
        def errors = []
        if(taxId?.length() <= 0) {
            errors << DefaultValidationError.build('taxId.empty', '')
        }
        if(!legalName?.trim()) {
            errors << DefaultValidationError.build('legalName.empty')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        if(!displayName) {
            displayName = ''
        }
        this.taxId = taxId.trim()
        this.legalName = legalName.trim()
        this.displayName = displayName.trim()
    }

    protected Company(String taxId, String legalName, String displayName, String email,
    String phone, String bankAccountNumber) {
        this(taxId, legalName, displayName)
        if(!email) {
            email = ''
        }
        if(!phone) {
            phone = ''
        }
        if(!bankAccountNumber) {
            bankAccountNumber = ''
        }
        this.email = email.trim()
        this.phone = phone.trim()
        this.bankAccountNumber = bankAccountNumber.trim()
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((legalName == null) ? 0 : legalName.hashCode())
        result = prime * result + ((taxId == null) ? 0 : taxId.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof Company)) {
            return false
        }
        final other = (Company) obj
        if(!Objects.equals(this.taxId, other.taxId)) {
            return false
        }
        return Objects.equals(this.legalName, other.legalName)
    }
}
