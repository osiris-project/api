﻿using System;

namespace Osiris.Api.Modules.TaxDocumentEp
{
    public class VoidDocumentCmd
    {
        public long? FolioNumber { get; set; }

        public DateTime AccountingDate { get; set; }

        public Guid ReferencedDocumentId { get; set; }

        public string VoidReason { get; set; }

        public override string ToString()
        {
            return $"FolioNumber: {FolioNumber}, AccountingDate: {AccountingDate}, ReferencedDocumentId: {ReferencedDocumentId}, VoidReason: {VoidReason}";
        }
    }
}
