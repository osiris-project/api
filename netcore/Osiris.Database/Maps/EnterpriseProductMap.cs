﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class EnterpriseProductMap
        : ClassMap<EnterpriseProduct>
    {
        public EnterpriseProductMap()
        {
            Schema("organization");
            Table("enterprise_product");

            Id(i => i.Id).Column("id").Not.Nullable().GeneratedBy.GuidComb();
            Version(i => i.RowVersion).Column("row_version").Generated.Always();

            References(i => i.Enterprise, "enterprise_id")
                .Not.LazyLoad()
                .Fetch.Join()
                .Unique()
                .UniqueKey("enterprise_product_un")
                .Not.Nullable()
                .ForeignKey()
                .Cascade.SaveUpdate();

            References(i => i.Product, "product_id")
                .Not.LazyLoad()
                .Fetch.Join()
                .Unique()
                .UniqueKey("enterprise_product_un")
                .Not.Nullable()
                .ForeignKey()
                .Cascade.SaveUpdate();
        }
    }
}
