﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class SpecificTaxMap
        : ClassMap<SpecificTax>
    {
        public SpecificTaxMap()
        {
            Schema("taxation");
            Table("specific_tax");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(100).UniqueKey("specific_tax_un");
            Map(p => p.Code).UniqueKey("specific_tax_un");
            Map(p => p.IsRetained);
            Map(p => p.TaxRate).Precision(4).Scale(3);
        }
    }
}
