﻿namespace Osiris.Core.Enums
{
    public enum StringMatchMode
    {
        Start,
        End,
        Anywhere,
        Exact
    }
}
