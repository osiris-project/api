﻿using System;
using Dobelik.Utils.Identity;
using FluentValidation.Validators;
using Slf4Net;

namespace Osiris.Api.Modules.Validators.NationalId
{
    public class NationalIdValidator
        : PropertyValidator
    {
        private readonly INationalIdentity idUtils;

        private NationalIdValidator()
            : base("InvalidTaxId", typeof(I18n.Error))
        {
        }

        public NationalIdValidator(INationalIdentity identity)
            : this()
        {
            idUtils = identity;
        }

        #region Overrides of PropertyValidator

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (idUtils == null)
                throw new ArgumentNullException(nameof(idUtils));
            var log = LoggerFactory.GetLogger(typeof (NationalIdValidator));
            log.Debug("IdUtils instance: {0}", idUtils.GetType());
            return idUtils.Validate(context.PropertyValue.ToString());
        }

        #endregion
    }
}
