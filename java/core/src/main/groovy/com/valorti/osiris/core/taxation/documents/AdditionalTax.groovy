/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class AdditionalTax
    extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 10)
    final String code

    @Column(precision = 4, scale = 3, nullable = false)
    final Double taxRate

    @Column(precision = 13, scale = 4, nullable = false)
    final Double taxAmount

    @ManyToOne
    final DocumentSummary summary

    /**
     * Default constructor.
     */
    protected AdditionalTax() {
        this.code = ''
        this.taxRate = 0.000
        this.taxAmount = 0.0000
        this.summary = null
    }

    /**
     * @param code The code of this tax.
     * @param taxRate The tax rate of this additional tax.
     * @param taxAmount The amount of the tax.
     * @param summary The summary that this additional tax is related to.
     * @throws IllegalArgumentException if the code, taxRate, taxAmount or summary are null.
     * @since 0.0.1
     */
    AdditionalTax(String code, Double taxRate, Double taxAmount, DocumentSummary summary)
        throws IllegalArgumentException {
        def errors = []
        if (!code.trim()) {
            throw new IllegalArgumentException('code')
        }
        if (!taxRate) {
            throw new IllegalArgumentException('taxRate')
        }
        if (!taxAmount) {
            throw new IllegalArgumentException('taxAmount')
        }
        if (!summary) {
            throw new IllegalArgumentException('summary')
        }
        this.code = code
        this.taxRate = taxRate
        this.taxAmount = taxAmount
        this.summary = summary
    }
}
