﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Enterprise
{
    public class EnterpriseAddressMap
        : ClassMap<EnterpriseAddress>
    {
        public EnterpriseAddressMap()
        {
            Schema("organization");
            Table("enterprise_address");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Line1).Length(70).UniqueKey("enterprise_address_un");
            Map(i => i.Line2).Length(150);

            References(i => i.Commune, "commune_id")
                .Not.Nullable()
                .Cascade.SaveUpdate()
                .UniqueKey("enterprise_address_un");

            References(x => x.Enterprise, "enterprise_id")
                .Unique()
                .Cascade.SaveUpdate()
                .UniqueKey("enterprise_address_un");

            HasMany(i => i.EnterpriseAddressTypes)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("enterprise_address_id");
        }
    }
}
