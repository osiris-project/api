/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

import groovy.transform.ToString

/**
 * Represents the permission each role can have.
 * This are application wide, not per tenant.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @since 0.0.1
 */
@Entity
@Table(schema = 'system')
@ToString(includeNames = true)
class Permission extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    /**
     * This permission name.
     *
     * @since 0.0.1
     */
    @Column(nullable = false, length = 50, unique = true)
    String name

    /**
     * The permission description (what it does)
     *
     * @since 0.0.1
     */
    @Column(nullable = false, length = 100)
    String description

    /**
     * A list of roles this permission is linked to.
     *
     * @since 0.0.1
     */
    @OneToMany
    def roles = new HashSet<RolePermission>()

    /**
     *
     */
    protected Permission() {
    }

    /**
     *
     * @param name A required name for the permission
     * @param description What does this permission do.
     * @since 0.0.1
     * @throws ValidationException if a validation error occurs.
     */
    Permission(String name, String description) {
        def errors = []
        if(!name?.trim()) {
            errors << ValidationBuilder.empty('permission.name')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        this.name = name
        this.description = description
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof Permission)) {
            return false
        }
        final other = (Permission) obj
        return Objects.equals(this.name, other.name)
    }
}
