/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import com.valorti.osiris.core.ValidationException

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.DuplicateEntityException
import com.valorti.osiris.core.system.Account
import com.valorti.osiris.core.system.AuthorizedAccount
import com.valorti.osiris.core.system.Role
import com.valorti.osiris.core.taxation.CommercialActivity
import com.valorti.osiris.core.taxation.EconomicActivity


/**
 * Represents the tenant whose taxation documents will be checked.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization')
class Tenant extends Company {
    @Transient
    private static final long serialVersionUID = 1L

    /**
     * This tenant can be enabled or disabled depending on the payment status or the
     * subscription
     */
    @Column(nullable = false)
    Boolean isEnabled

    /**
     * The clients and providers of this tenant
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tenant", orphanRemoval = true)
    Set<Enterprise> enterprises = []

    /**
     * Tenant economic activities
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tenant", orphanRemoval = true)
    Set<TenantEconomicActivity> economicActivities = []

    /**
     * Tenant commercial activities
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tenant", orphanRemoval = true)
    Set<CommercialActivity> commercialActivities = []

    /**
     * The created roles for this tenant that authorized accounts can use
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'tenant', orphanRemoval = true)
    Set<Role> roles = []

    /**
     * Authorized accounts for this tenant
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'tenant', orphanRemoval = true)
    Set<AuthorizedAccount> accounts = []

    /**
     *
     * @param taxId
     * @param legalName
     * @param displayName
     * @throws ValidationException if there are validation errors.
     * @since 0.0.1
     */
    Tenant(String taxId, String legalName, String displayName) {
        super(taxId, legalName, displayName)
    }

    /**
     *
     * @param taxId
     * @param legalName
     * @param displayName
     * @param phone
     * @param email
     * @param bankAccountNumber
     * @throws ValidationException if there are validation errors.
     * @since 0.0.1
     */
    Tenant(String taxId, String legalName, String displayName, String phone, String email,
           String bankAccountNumber) {
        super(taxId, legalName, displayName, email, phone, bankAccountNumber)
    }

    /**
     * Authorizes an account to use the system with this tenant's data.
     *
     * @param account The account to be authorized.
     * @return The authorized account.
     * @throws DuplicateEntityException if the account is already
     * authorized.
     * @since 0.0.1
     */
    AuthorizedAccount authorizeAccount(Account account) {
        this.accounts.each { authorizedAccount ->
            if (authorizedAccount.account == account) {
                throw new DuplicateEntityException(AuthorizedAccount.class)
            }
        }
        final authorizedAccount = new AuthorizedAccount(account, this)
        this.accounts.add(authorizedAccount)
        return authorizedAccount
    }

    /**
     * Removes the account from the authorized accounts, if found.
     *
     * @param accountId The account to be removed
     * @return true if the account was found, false otherwise
     * @since 0.0.1
     */
    boolean removeAccount(UUID accountId) {
        return this.accounts.removeIf {
            if (authorizedAccount.account.id == id) {
                it.tenant = null
                return true
            }
            return false
        }
    }

    /**
     * Creates a new enterprise to this tenant.
     *
     * @param enterprise The enterprise to add
     * @return The created enterprise
     * @throws DuplicateEntityException if an enterprise with the same tax id already exists.
     * @since 0.0.1
     */
    Enterprise addEnterprise(Enterprise enterprise) {
        this.enterprises.each { e ->
            if (e.taxId == enterprise.taxId) {
                throw new DuplicateEntityException(Enterprise.class)
            }
        }
        this.enterprises.add(enterprise)
        enterprise.tenant = this
        return enterprise
    }

    /**
     * See {@link Enterprise ( String , String , String , Tenant )}
     *
     * @param taxId
     * @param legalName
     * @param displayName
     * @return The created enterprise object.
     * @throws DuplicateEntityException if the enterprise already exists.
     */
    Enterprise addEnterprise(String taxId, String legalName, String displayName) {
        this.enterprises.each {
            if (it.taxId == taxId) {
                throw new DuplicateEntityException(Enterprise.class, 'An enterprise with this tax id already exists')
            }
        }
        final enterprise = new Enterprise(taxId, legalName, displayName, this)
        this.enterprises.add(enterprise)
        return enterprise
    }

    /**
     * See {@link Enterprise}
     * @param taxId
     * @param legalName
     * @param displayName
     * @param email
     * @param phone
     * @param bankAccountNumber
     * @return The created enterprise object.
     */
    Enterprise addEnterprise(String taxId, String legalName, String displayName, String email,
                             String phone, String bankAccountNumber) {
        this.enterprises.each {
            if (it.taxId == taxId) {
                throw new DuplicateEntityException(Enterprise.class, 'An enterprise with this tax id already exists')
            }
        }
        final enterprise = new Enterprise(taxId, legalName, displayName, email, phone, bankAccountNumber, this)
        this.enterprises.add(enterprise)
        return enterprise
    }

    /**
     * Removes the specified enterprise.
     *
     * @param enterpriseId
     * @return true if removed, false otherwise.
     * @since 0.0.1
     */
    boolean removeEnterprise(UUID enterpriseId) {
        return this.enterprises.removeIf {
            if (it.id == enterpriseId) {
                it.tenant = null
                return true
            }
        }
    }

    /**
     * Creates a role with the specified name and description.
     *
     * @param name The role name.
     * @param description The role description.
     * @return The created role.
     * @throws DuplicateEntityException If a role with the same
     * name already exists.
     * @throws ValidationException
     * @since 0.0.1
     */
    Role addRole(String name, String description)
        throws ValidationException {
        this.roles.each {
            if (it.name == name && it.tenant == this) {
                throw new DuplicateEntityException(Role.class)
            }
        }
        final role = new Role(name, description, this)
        this.roles.add(role)
        return role
    }

    /**
     * Removes the specified role.
     *
     * @param roleId The role id to be removed.
     * @return true if removed, false otherwise.
     * @since 0.0.1
     */
    boolean removeRole(UUID roleId) {
        return this.roles.removeIf {
            if (it.id == roleId) {
                it.tenant = null
                return true
            }
            return false
        }
    }

    /**
     * Removes a list of roles.
     *
     * @param roleIds A list of role IDs.
     * @return true if the roles were removed, false otherwise.
     * @since 0.0.1
     */
    boolean removeRoles(UUID[] roleIds) {
        return this.roles.removeAll { role ->
            if (roleIds.find { it == role.id }) {
                role.tenant = null
                return true
            }
            return false
        }
    }

    /**
     * Adds an economic activity to this enterprise.
     *
     * @param ea The economic activity.
     * @throws IllegalArgumentException if the economic activity is null.
     * @since 0.0.1
     */
    void addEconomicActivity(EconomicActivity ea) {
        def tmp = new TenantEconomicActivity(this, ea)
        def found = false
        this.economicActivities.each {
            if (it == tmp) {
                found = true
                return
            }
        }
        if (found) {
            return
        }
        this.economicActivities.add(tmp)
    }

    /**
     * Add a commercial activity to the tenant.
     * @param ca The commercial activity.
     * @throws IllegalArgumentException If the commercial activity is null.
     * @since 0.0.1
     */
    void addCommercialActivity(CommercialActivity ca) {
        def found = false
        this.commercialActivities.each {
            if (it.commercialActivity == ca) {
                found = true
                return
            }
        }
        if (found) {
            return
        }
        final tmp = new TenantCommercialActivity(this, ca)
        this.commercialActivities.add(tmp)
    }
}
