﻿using System;
using Osiris.Core.Entities.Accounting;

namespace Osiris.Api.Modules
{
    public class VoucherDto
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public string TaxId { get; set; }

        public long Number { get; set; }

        public DateTime Date { get; set; }

        public AccountingVoucherStatus Status { get; set; }

        public decimal Asset { get; set; }

        public decimal Debit { get; set; }

        public decimal Total { get; set; }

        public decimal TotalTaxes { get; set; }

        public decimal TotalNetAmount { get; set; }
    }
}
