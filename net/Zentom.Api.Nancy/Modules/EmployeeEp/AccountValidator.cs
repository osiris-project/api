﻿using FluentValidation;

namespace Zentom.Api.Modules.EmployeeEp
{
    public class ChangePasswordValidator
        : AbstractValidator<ChangePasswordCmd>
    {
        public ChangePasswordValidator()
        {
            RuleFor(i => i.Password).Length(6, 100);
        }
    }

    public class AccountCreateValidator
        : AbstractValidator<AccountCreateCmd>
    {
        public AccountCreateValidator()
        {
            RuleFor(i => i.Login).Length(3, 100);
            RuleFor(i => i.Email).EmailAddress();
            RuleFor(i => i.Password).Length(6, 100);
        }
    }

    public class AccountUpdateValidator
        : AbstractValidator<AccountCmd>
    {
        public AccountUpdateValidator()
        {
            RuleFor(i => i.Login).Length(3, 100);
            RuleFor(i => i.Email).EmailAddress();
        }
    }
}