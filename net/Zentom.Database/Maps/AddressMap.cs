﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class AddressMap
        : ClassMap<Address>
    {
        public AddressMap()
        {
            Id(i => i.Id);
            Version(i => i.RowVersion);
            
            Map(i => i.Line1).Length(70);
            Map(i => i.Line2).Length(150);
        }
    }
}
