﻿namespace Zentom.Core.Entities
{
    public class EnterpriseService
        : Entity
    {
        #region Properties

        public virtual Enterprise Enterprise { get; set; }

        public virtual Service Service { get; set; }

        #endregion

        public EnterpriseService() { }

        #region Object

        public override bool Equals(object obj)
        {
            var t = obj as EnterpriseService;

            if (ReferenceEquals(null, t))
                return false;

            if (ReferenceEquals(this, t))
                return true;

            return (Enterprise.Equals(t.Enterprise) &&
                Service.Equals(t.Service));
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) + (!ReferenceEquals(null, Enterprise) ? Enterprise.Id.GetHashCode() : 0);
                hash = (hash * 31) + (!ReferenceEquals(null, Service) ? Service.Id.GetHashCode() : 0);

                return hash;
            }
        }

        #endregion
    }
}
