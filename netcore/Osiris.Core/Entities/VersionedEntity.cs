﻿using Osiris.Core.Interfaces.Data;

namespace Osiris.Core.Entities
{
    public abstract class VersionedEntity
        : IVersioned
    {
        #region Implementation of IVersioned

        public virtual byte[] RowVersion { get; protected set; }

        #endregion
    }
}
