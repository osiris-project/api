﻿using System;

namespace Osiris.Core.Entities.TaxDocument
{
    public class ReceivedTaxDocumentWeakReference
        : Entity
    {
        #region Fields

        private long folio;

        private string documentType;

        private DateTime date;

        private string reason;

        private ReceivedTaxDocument receivedTaxDocument;

        #endregion

        #region Properties

        public virtual long Folio
        {
            get { return folio; }
            set { folio = value; }
        }

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public virtual string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public virtual ReceivedTaxDocument ReceivedTaxDocument
        {
            get { return receivedTaxDocument; }
            set { receivedTaxDocument = value; }
        }

        #endregion

        #region Ctors.

        public ReceivedTaxDocumentWeakReference()
        {
        }

        public ReceivedTaxDocumentWeakReference(long folio, string documentType, DateTime date, string reason, ReceivedTaxDocument receivedTaxDocument)
        {
            this.folio = folio;
            this.documentType = documentType;
            this.date = date;
            this.reason = reason;
            this.receivedTaxDocument = receivedTaxDocument;
        }

        #endregion
    }
}
