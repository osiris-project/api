/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

/**
 * Represents the commercial activities tha a tenant or enterprise can do.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'taxation')
class CommercialActivity extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(unique = true, nullable = false, length = 200)
    String name

    protected CommercialActivity() {
    }

    /**
     * @param name The name of this commercial activity
     * @since 0.0.1
     */
    CommercialActivity(String name) {
        def errors = []
        if(!name?.trim()) {
            errors << ValidationBuilder.empty('comercialActivity.name')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        this.name = name.trim()
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true
        }
        if (!super.equals(obj)) {
            return false
        }
        if (!(obj instanceof CommercialActivity)) {
            return false
        }
        CommercialActivity other = (CommercialActivity) obj
        if (name == null) {
            if (other.name != null)
                return false
        } else if (!name.equals(other.name)) {
            return false
        }
        return true
    }
}
