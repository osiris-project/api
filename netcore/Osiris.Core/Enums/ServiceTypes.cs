﻿namespace Osiris.Core.Enums
{
    public enum ServiceTypes
    {
        PeriodicService = 1,
        OtherService = 2,
        ServiceBill = 3,
        HospitalityService = 4,
        InternationalTransport = 5 
    }
}
