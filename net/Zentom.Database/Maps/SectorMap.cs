﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class SectorMap
        : ClassMap<Sector>
    {
        public SectorMap()
        {
            Schema("taxation");
            Table("sector");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Name).Length(110);
            Map(p => p.Code).Length(6);
            Map(p => p.IsSubjectToTax).Default("1");
            Map(p => p.TributaryCategory).Nullable();

            //HasMany(i => i.Products)
            //    .Inverse()
            //    .Cascade.SaveUpdate()
            //    .KeyColumn("sector_id");

            //HasMany(i => i.Services)
            //    .Inverse()
            //    .KeyColumn("sector_id")
            //    .Cascade.SaveUpdate();

            HasMany(i => i.Sectors)
                .Cascade.SaveUpdate()
                .KeyNullable()
                .KeyColumn("sector_group_id")
                .Inverse();

            References(i => i.ParentSector, "sector_group_id")
                .Unique()
                .Cascade.SaveUpdate()
                .Nullable();

            HasMany(i => i.EnterpriseSectors)
                .Inverse()
                .Cascade.All()
                .KeyColumn("sector_id");
        }
    }
}
