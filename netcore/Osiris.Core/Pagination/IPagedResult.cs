﻿using System.Collections.Generic;

namespace Osiris.Core.Pagination
{
    public interface IPagedResult<TEntity>
    {
        int CurrentPage { get; }

        int ResultsPerPage { get; }

        long ResultCount { get; }

        long PageCount { get; }

        bool HasPrevious { get; }

        bool HasNext { get; }

        IList<TEntity> Items { get; }
    }
}
