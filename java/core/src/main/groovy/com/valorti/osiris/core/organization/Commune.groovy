/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationBuilder
import com.valorti.osiris.core.ValidationException

import groovy.transform.ToString

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization',
    uniqueConstraints = [
        @UniqueConstraint(columnNames = ['name', 'city'])])
@ToString(includeNames = true)
class Commune extends UUIDEntity {
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 20)
    String name

    @Column(nullable = false, length = 6)
    String postCode

    @ManyToOne
    City city

    protected Commune() {
    }

    /**
     * @param name
     * @param postalCode
     */
    Commune(String name, String postalCode, City city) {
        def errors = []
        if (!name?.trim()) {
            errors << ValidationBuilder.empty('commune.name')
        }
        if (city == null) {
            throw new IllegalArgumentException('No city provided!')
        }
        if (errors) {
            throw new ValidationException(errors)
        }
        this.name = name
        this.postalCode = postalCode
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((city == null) ? 0 : city.hashCode())
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this.is(obj)) {
            return true
        }
        if (!(obj instanceof Commune)) {
            return false
        }
        final other = (Commune) obj
        if (!Objects.equals(this.city, other.city)) {
            return false
        }
        return Objects.equals(this.name, other.name)
    }
}
