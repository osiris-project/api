﻿namespace Osiris.Api.Modules.RoleEp
{
    public class RoleDto
        : Command
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public RoleDto()
        {
            Description = "";
        }
    }
}
