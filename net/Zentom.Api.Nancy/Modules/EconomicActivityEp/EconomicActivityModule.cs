﻿using System;
using log4net;
using Nancy.ModelBinding;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;
using Zentom.Core.Pagination;

namespace Zentom.Api.Modules.EconomicActivityEp
{
    [Permission(RequiresLogin = true)]
    public class EconomicActivityModule
        : TenantApi
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EconomicActivityModule));

        private string orderByList;

        public EconomicActivityModule(ISessionFactory factory)
            : base("economic-activities", factory)
        {
            this.Get<Command>(BasePath + "/{id:guid}/", List);
            this.Get(BasePath + "/groups/", GetEconomicGroups);
            this.Get(BasePath + "/groups/{parentGroupId:guid}/", GetEconomicGroups);
            this.Post<EconomicActivityCmd>(BasePath, Create);
            orderByList = "";
        }
        
        public object List(Command cmd)
        {
            var bound = this.BindTo(new EconomicActivityQuery());
            var query = db.QueryOver<EconomicActivity>();

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.Name + "%");
            }

            if (!string.IsNullOrWhiteSpace(bound.Code))
            {
                query.WhereRestrictionOn(i => i.Code)
                    .IsInsensitiveLike("%" + bound.Code + "%");
            }

            IPagedResult<EconomicActivityDto> items;

            if (bound.OnlyExemptSectors)
            {
                query.Where(i => i.IsSubjectToTax == false);
            }

            items = GetEconomicActivities(query, cmd.Id);
            return Negotiate.WithOkAndList(items, Request.Url, orderByList);
        }

        public object Create(EconomicActivityCmd cmd)
        {
            var query = db.QueryOver<EconomicActivity>()
                .Where(i => i.Name == cmd.Name && i.Code == cmd.Code);

            if (cmd.Parent != Guid.Empty)
            {
                query.Where(i => i.Parent.Id == cmd.Parent);
            }

            var entity = query.Take(1).SingleOrDefault();

            if (entity != null)
            {
                return Negotiate.WithOk();
            }

            entity = new EconomicActivity
            {
                Name = cmd.Name,
                Code = cmd.Code,
                Parent = db.Get<EconomicActivity>(cmd.Parent),
                TributaryCategory = cmd.TributaryCategory
            };

            db.Save(entity);
            return Negotiate.WithCreated(Context.GetResourceUrl(entity.Id), entity.Id);
        }

        private object GetEconomicGroups(dynamic args)
        {
            log.DebugFormat("Entering GetEconomicGroups");
            EconomicActivityDto dto = null;
            var query = db.QueryOver<EconomicActivity>();
            query
                .SelectList(i => i
                    .Select(j => j.Parent.Name).WithAlias(() => dto.Name)
                    .Select(j => j.Parent.Id).WithAlias(() => dto.Id))
                .Where(i => i.Code == "0");

            Guid parentGroupId = args.parentGroupId;
            log.DebugFormat("Parent Group Id: {0}", parentGroupId);
            if (parentGroupId != Guid.Empty)
            {
                query.Where(i => i.Parent.Id == parentGroupId);
            }
            else
            {
                query.Where(i => i.Parent == null);
            }

            orderByList = "name";
            log.DebugFormat("Leaving GetEconomicGroups");
            return query
                .OrderBy(Settings.OrderBy)
                .ProjectInto<EconomicActivity, EconomicActivityDto>()
                .List<EconomicActivityDto>();
        }

        private IPagedResult<EconomicActivityDto> GetEconomicActivities(IQueryOver<EconomicActivity, EconomicActivity> query, Guid parentId)
        {
            EconomicActivityDto dto = null;
            EconomicActivity parent = null;
            orderByList = "name, code, parent.name";

            return query.SelectList(i => i
                    .Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.Name).WithAlias(() => dto.Name)
                    .Select(j => j.Code).WithAlias(() => dto.Code)
                    .Select(j => j.IsSubjectToTax).WithAlias(() => dto.IsSubjectToTax))
                .Where(i => i.Parent != null && i.Code != "0")
                .Left.JoinAlias(i => i.Parent, () => parent)
                .Where(i => parent.Id == parentId)
                .SelectList(i => i.Select(j => parent.Name).WithAlias(() => dto.ParentName))
                .OrderBy(Settings.OrderBy)
                .ProjectInto<EconomicActivity, EconomicActivity, EconomicActivityDto>()
                .PaginateResults<EconomicActivity, EconomicActivityDto>(Settings.PaginationSettings.Page,
                    Settings.PaginationSettings.ResultsPerPage);
        }
    }
}