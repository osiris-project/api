﻿using System;
using System.Collections.Generic;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IService : IBaseProduct
    {
        ICollection<ITaxDocumentService> TaxDocumentServices { get; set; }

        ICollection<IEnterpriseService> EnterpriseServices { get; set; }
    }
}
