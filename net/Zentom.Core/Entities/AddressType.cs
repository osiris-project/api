﻿namespace Zentom.Core.Entities
{
    public class AddressType
        : Entity
    {
        #region Fields

        private string name;

        private string description;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion

        public AddressType()
        {
        }

        public AddressType(string name, string description)
        {
            this.name = name;
            this.description = description;
        }
    }
}
