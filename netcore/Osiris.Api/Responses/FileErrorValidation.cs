﻿using System.Collections.Generic;
using Osiris.Core.Interfaces.Validation;

namespace Osiris.Api.Responses
{
    public class FileErrorValidation
    {
        public string Key { get; }

        public IList<IModelValidation> ErrorList { get; }

        public FileErrorValidation(string key, IList<IModelValidation> errorList)
        {
            Key = key;
            ErrorList = errorList;
        }
    }
}
