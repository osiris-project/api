﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zentom.Integration.Artikos
{
    internal static class StringHelper
    {
        private static CharConvert[] converts;

        static StringHelper()
        {
            converts = new CharConvert[]
            {
                new CharConvert("&", new [] { "y" }),
                new CharConvert("a", new []{ "á", "ä" }),
                new CharConvert("A", new []{"Á", "Ä"}), 
            };
        }
        internal static string RemoveSpecialChars()
        {
            char[] latinChars =
            {
                '&', 'á', 'Á', 'é', 'É', 'í', 'Í', 'ó', 'Ó', 'ú', 'Ú', 'ñ', 'Ñ'
            };
            char[] replaces =
            {
                'y', 'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'n', 'N'
            };
        }

        private class CharConvert
        {
            private readonly string name;
            private readonly string[] input;

            internal CharConvert(string name, string[] input)
            {
                this.name = name;
                this.input = input;
            }

            internal string Convert(string search)
            {
                var found = input.SingleOrDefault(i => i == name);
                return found == null ? search : name;
            }
        }
    }
}
