﻿using System;
using System.Collections.Generic;
using Nancy.Validation;

namespace Osiris.Api.Modules.Validators.Guid
{
    public class GuidValidationRule
        : ModelValidationRule
    {
        public GuidValidationRule(Func<string, string> errorMessageFormatter, IEnumerable<string> memberNames) :
            base("GuidValidator", errorMessageFormatter, memberNames)
        {
        }
    }
}
