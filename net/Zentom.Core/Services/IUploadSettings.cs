﻿namespace Zentom.Core.Services
{
    public interface IUploadSettings
    {
        string UploadPath { get; }
    }
}
