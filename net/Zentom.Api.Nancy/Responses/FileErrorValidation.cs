﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Api.Responses
{
    public class FileErrorValidation
    {
        public string Key { get; }

        public IList<IModelValidation> ErrorList { get; }

        public FileErrorValidation(string key, IList<IModelValidation> errorList)
        {
            Key = key;
            ErrorList = errorList;
        }
    }
}