﻿namespace Zentom.Core.Entities
{
    public class EnterpriseSector
        : CompanySector
    {
        #region

        private Enterprise _enterprise;

        #endregion

        #region Properties

        public virtual Enterprise Enterprise
        {
            get { return _enterprise; }
            set { _enterprise = value; }
        }

        #endregion

        public EnterpriseSector() { }

        public EnterpriseSector(Enterprise enterprise, Sector sector)
        {
            _enterprise = enterprise;
        }
    }
}
