/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import org.junit.After
import org.junit.Before
import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class OtherTests {
    def list = []

    /**
     * @throws java.lang.Exception
     */
    @Before
    void setUp() throws Exception {
        list << 1
        list << 2
        list << 3
        list << 4
        list << 5
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    void tearDown() throws Exception {
    }

    @Test
    void test_subList() {
        def subl = this.list.subList(0, 2)
        subl.each { println "subl: ${it}" }
        this.list.each { println "list: ${it}" }
    }

    @Test
    void test_listReturn() {
        this.doReturnOnceFound()
    }

    @Test
    void test_splitAndJoin() {
        def arr = ['abc', '123']
        def test = new SJTest(arr)
        assert test.str == arr
        arr = ['002', 'xyz']
        test.str = arr
        assert test.str == arr
        arr = null
        test = null
    }

    private void doReturnOnceFound() {
        println 'doReturnOnceFound'
        this.list.forEach {
            println "Current value: ${it}"
            if(it == 2) {
                println 'Value found'
                return
            }
        }
        println 'Not found'
    }

    protected class SJTest {
        private str = ''

        SJTest(List<String> list) {
            this.setStr(list)
        }

        @Override
        String toString() {
            return "Test [str=${this.str}]"
        }

        final List<String> getStr() {
            return this.str.split('\\|')
        }

        final void setStr(List<String> list) {
            this.str = list.join('|')
        }
    }
}
