﻿using Zentom.Core.Entities;

namespace Zentom.Core.Services
{
    public interface IFolioManagerService
    {
        long GetNextFolioNumber(string documentType, Tenant tenant);
    }
}