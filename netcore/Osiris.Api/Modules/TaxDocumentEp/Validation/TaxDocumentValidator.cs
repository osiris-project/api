﻿using FluentValidation;
using Osiris.Core.Enums;

namespace Osiris.Api.Modules.TaxDocumentEp.Validation
{
    public class TaxDocumentValidator
        : AbstractValidator<TaxDocumentCmd>
    {
        public TaxDocumentValidator()
        {
            RuleFor(i => i.AccountingDate).NotNull();
            RuleFor(i => i.DocumentType).NotEmpty();
            RuleFor(i => i.Tenant.ComercialActivity).NotNull();
            RuleFor(i => i.Tenant.EconomicActivity.Code).NotNull();
            RuleFor(i => i.Client.ComercialActivity).NotNull();
            RuleFor(i => i.Lines).NotEmpty();

            When(i => i.DocumentType == "33", () =>
            {
                RuleFor(i => i.TaxRate).GreaterThan(0M);
            });

            When(i => i.DocumentType == "34", () =>
            {
                RuleFor(i => i.TaxRate).LessThanOrEqualTo(0M);
            });

            When(i => i.ExtraTaxCode > 0, () =>
            {
                RuleFor(i => i.ExtraTaxRate).GreaterThan(0M);
            });

            When(
                i =>
                    i.DocumentType == "30" || i.DocumentType == "32" || i.DocumentType == "55" || i.DocumentType == "60",
                () =>
                {
                    RuleFor(i => i.FolioNumber).GreaterThan(0);
                });

            When(i => i.DocumentType == "801", () =>
            {
                RuleFor(i => i.FolioNumber).GreaterThan(0);
            });

            // Credit/Debit note
            When(i => i.DocumentType == "56" || i.DocumentType == "61", () =>
            {
                RuleFor(i => i.ReferencedDocuments).NotEmpty();
                RuleForEach(i => i.ReferencedDocuments).NotEmpty()
                    .Must(i => (short)i.ReferenceReason != (short)CorrectionCode.None);
            });
        }
    }
}
