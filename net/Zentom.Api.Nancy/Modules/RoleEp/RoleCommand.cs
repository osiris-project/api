﻿using System;
using System.Collections.Generic;

namespace Zentom.Api.Modules.RoleEp
{
    public class RoleCommand
        : Command
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public IList<Guid> Permissions { get; set; }

        public RoleCommand()
        {
            Description = "";
        }

        public override string ToString()
        {
            return $"Name: {Name}, DisplayName: {DisplayName}, Description: {Description}";
        }
    }
}