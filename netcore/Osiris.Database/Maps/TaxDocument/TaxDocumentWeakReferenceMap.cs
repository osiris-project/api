﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Database.Maps.TaxDocument
{
    public class TaxDocumentWeakReferenceMap
        : ClassMap<TaxDocumentWeakReference>
    {
        public TaxDocumentWeakReferenceMap()
        {
            Schema("taxation");
            Table("tax_document_weak_reference");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Date);
            Map(i => i.DocumentType);
            Map(i => i.Folio);
            Map(i => i.Reason);

            References(i => i.TaxDocument, "tax_document_id")
                .Not.Nullable()
                .Cascade.SaveUpdate();
        }
    }
}
