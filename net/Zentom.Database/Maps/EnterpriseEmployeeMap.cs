﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    class EnterpriseEmployeeMap
        : ClassMap<EnterpriseEmployee>
    {
        public EnterpriseEmployeeMap()
        {
            Table("organization.enterprise_employee");

            Id(i => i.Id).Column("id").Not.Nullable().GeneratedBy.GuidComb();
            Version(i => i.RowVersion).Column("row_version").Generated.Always();
            Map(i => i.IsActive).Column("is_active").Not.Nullable().Default("1");
            Map(i => i.IsConsultant).Column("is_consultant").Not.Nullable().Default("0");

            References(i => i.Employee, "employee_id")
                .Not.LazyLoad()
                .Fetch.Join()
                .UniqueKey("enterprise_employee_un")
                .Not.Nullable()
                .Cascade.SaveUpdate();

            References(i => i.Enterprise, "enterprise_id")
                .Not.LazyLoad()
                .Fetch.Join()
                .UniqueKey("enterprise_employee_un")
                .Not.Nullable()
                .Cascade.SaveUpdate();
        }
    }
}
