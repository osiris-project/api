﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using Dobelik.Utils.Identity.Attributes;
using Dobelik.Utils.Identity.Instances;
using Slf4Net;

namespace Dobelik.Utils.Identity {
    /// <summary>
    /// Factory to retrieve identity validators based on a CultureInfo object.
    /// </summary>
    public class IdentityLocator {
        private static readonly ILogger Log = LoggerFactory.GetLogger (typeof (IdentityLocator));

        private static IdentityLocator locator;

        private static IList<TypeInfo> decoratedTypes;

        private IdentityLocator () { }

        /// <summary>
        /// Retrieves an instance of this factory.
        /// </summary>
        /// <returns>This factory instance.</returns>
        public static IdentityLocator GetInstance () {
            if (locator != null) return locator;
            Log.Debug ("Creating identity locator instance.");
            locator = new IdentityLocator ();
            Init ();
            return locator;
        }

        /// <summary>
        /// Finds an Identity that matches the culture info object.
        /// </summary>
        /// <param name="culture">The culture to use.</param>
        /// <returns>An INationalIdentity instance that matches the culture info or
        /// the default implementation.</returns>
        public INationalIdentity Get (CultureInfo culture) {
            Log.Debug ("Entering Get.");
            Log.Info ("Retrieving instance for culture '{0}'.", culture);

            foreach (var typeInfo in decoratedTypes) {
                var attributes = typeInfo.GetCustomAttributes (true);
                Log.Debug ("typeinfo: {0}", typeInfo);

                if (attributes.OfType<Descriptor> ().Any (inst => inst.Culture.Equals (culture))) {
                    return (INationalIdentity) Activator.CreateInstance (typeInfo);
                }
            }
            Log.Debug ("Instance for culture '{0}' not found.", culture);
            Log.Warn ("Returning default instance of INationalIdentity.");
            Log.Warn ("This instance returns only default values and validates to false always.");
            Log.Debug ("Leaving Get.");
            return new Default ();
        }

        /// <summary>
        /// Finds an instance of INationalIdentity based on the current threads culture.
        /// </summary>
        /// <returns>An instance of INationalIdentity or a default implementation
        /// if none is found.</returns>
        public INationalIdentity Get () {
            Log.Debug ("Entering Get.");
            var culture = Thread.CurrentThread.CurrentCulture;
            Log.Info ("Retrieving instance for culture '{0}'.", culture);

            foreach (var typeInfo in decoratedTypes) {
                var attributes = typeInfo.GetCustomAttributes (true);
                Log.Debug ("typeinfo: {0}", typeInfo);

                if (attributes.OfType<Descriptor> ().Any (inst => inst.Culture.Equals (culture))) {
                    return (INationalIdentity) Activator.CreateInstance (typeInfo);
                }
            }
            Log.Debug ("Instance for culture '{0}' not found.", culture);
            Log.Warn ("Returning default instance of INationalIdentity.");
            Log.Warn ("This instance returns only default values and validates to false always.");
            Log.Debug ("Leaving Get.");
            return new Default ();
        }

        private static void Init () {
            Log.Debug ("Entering Init.");

            if (decoratedTypes == null) {
                Log.Debug ("Retrieving decorated types.");

                decoratedTypes = Assembly.GetExecutingAssembly ().DefinedTypes
                    .Where (i => i.GetInterface ("INationalIdentity") != null &&
                        i.IsDefined (typeof (Descriptor)))
                    .ToList ();

                if (Log.IsDebugEnabled ()) {
                    foreach (var i in decoratedTypes) {
                        Log.Debug ("Found: '{0}'", i);
                    }
                }
            }

            Log.Debug ("Leaving Init.");
        }
    }
}
