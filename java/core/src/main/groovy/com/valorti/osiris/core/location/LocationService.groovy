/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.location

import com.valorti.osiris.core.organization.Commune

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface LocationService {
    Commune retrieveCommune(String communeName, String cityName)
}
