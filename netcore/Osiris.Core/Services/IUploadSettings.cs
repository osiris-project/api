﻿namespace Osiris.Core.Services
{
    public interface IUploadSettings
    {
        string UploadPath { get; }
    }
}
