﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class ProductMap
        : SubclassMap<Product>
    {
        public ProductMap()
        {
            Schema("organization");
            Table("product");
            Abstract();
        }
    }
}
