/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class DocumentIssuerParserTests
    extends BaseParserTests {

    @Test
    void test_readIssuerSection() {
        println 'test_readIssuerSection'
        def path = this.buildEBill()
        assert path != null
        def emisor = path.DTE.Documento.Encabezado.Emisor

        def issuerParser = new DefaultDocumentIssuerParser(this.identity)
        def issuer = issuerParser.parse(emisor)
        assert issuer != null
        assert issuer.data.name == emisor.RznSoc.text().trim()
        assert issuer.data.taxId == this.identity.clean(emisor.RUTEmisor.text())
    }
}
