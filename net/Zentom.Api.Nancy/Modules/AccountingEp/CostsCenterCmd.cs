﻿using System;

namespace Zentom.Api.Modules.AccountingEp
{
    public class CostsCenterCreateCmd
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }

    public class CostsCenterUpdateCmd
        : CostsCenterCreateCmd
    {
        public Guid Id { get; set; }
    }
}