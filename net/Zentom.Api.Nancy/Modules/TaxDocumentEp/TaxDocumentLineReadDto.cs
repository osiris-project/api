﻿using System;

namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentLineReadVm
        : TaxDocumentLineVm
    {
        public Guid Id { get; set; }
    }
}