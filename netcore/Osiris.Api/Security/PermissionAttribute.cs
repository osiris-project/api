﻿using System;

namespace Osiris.Api.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionAttribute
        : Attribute
    {
        public string Permission { get; set; }

        public bool RequiresAdmin { get; set; }

        public bool RequiresLogin { get; set; }

        //public PermissionAttribute(string permission = "", bool requiresAdmin = false, bool requiresLogin = false)
        //{
        //    Permission = permission;
        //    RequiresAdmin = requiresAdmin;
        //    RequiresLogin = requiresLogin;
        //}

        public override string ToString()
        {
            return $"Permission: {Permission}, RequiresAdmin: {RequiresAdmin}, RequiresLogin: {RequiresLogin}";
        }
    }
}
