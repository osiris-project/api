/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core

import groovy.util.slurpersupport.GPathResult
import org.junit.Before
import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 */
class XmlValidationTests {
    GPathResult root

    @Before
    void setup() {
        def data = '''
    <root>
        <foo />
        <bar></bar>
        <baz>baz</baz>
    </root>
'''
        this.root = new XmlSlurper().parseText(data)
    }

    @Test
    void test_emptyness() {
        println 'test_emptyness'

        assert this.root != null
        if(this.root.foo.text()) {
            println 'Existing node; There is text'
        } else {
            println 'Existing node; No text'
        }
        assert this.root.foo.isEmpty() == false
        assert this.root.bar.isEmpty() == false
        assert this.root.baz.isEmpty() == false
        assert this.root.bar
        assert this.root.baz
        if(this.root.nonExistent) {
            println 'Exists'
        } else {
            println 'Does not exist'
        }
        assert this.root.nonExistent == ""
        assert this.root.nonExistent.text() == ""
        assert this.root.nonExistent.isEmpty() == true
    }
}
