﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Enterprise
{
    public class EnterpriseMap
        : ClassMap<Core.Entities.Enterprise>
    {
        public EnterpriseMap()
        {
            Schema("organization");
            Table("enterprise");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.BankAccountNumber).Length(15).Default("''");
            References(i => i.CostsCenter, "costs_center_id")
                .Nullable()
                .Cascade.SaveUpdate();
            Map(i => i.DisplayName).Length(100);
            Map(i => i.Email).Length(1000);
            Map(i => i.Relationship).CustomType(typeof(EnterpriseRelationship));

            HasMany(i => i.EnterpriseAddresses)
                .Cascade.All()
                .Inverse()
                .KeyColumn("enterprise_id");

            HasMany(i => i.EnterpriseComercialActivities)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("enterprise_id");

            HasMany(i => i.EnterpriseEconomicActivities)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("enterprise_id");

            Map(i => i.LegalName).Length(100).UniqueKey("enterprise_legal_name_un");
            Map(i => i.Phone).Length(50);
            Map(i => i.TaxId).Length(20).UniqueKey("enterprise_tax_id_un");

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("enterprise_legal_name_un")
                .UniqueKey("enterprise_tax_id_un");
        }
    }
}
