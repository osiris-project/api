﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    public class FolioRangeMap
        : ClassMap<FolioRange>
    {
        public FolioRangeMap()
        {
            Schema("taxation");
            Table("folio_range");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.CurrentFolio);
            Map(i => i.CreatedDate);
            Map(i => i.DocumentType).UniqueKey("folio_range_un");
            Map(i => i.IsCurrentRange);
            Map(i => i.RangeEnd);
            Map(i => i.RangeStart).UniqueKey("folio_range_un");
            Map(i => i.RequestDate);
            Map(i => i.Status).CustomType(typeof(RangeStatus));
            Map(i => i.UnusedFolios).CustomSqlType("VARCHAR(MAX)").Default("\"\"");
            //Map(i => i.UsagePercent).Formula("current_folio * 100 / (range_end - range_start + 1)").ReadOnly();
            
            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .Not.Nullable()
                .UniqueKey("folio_range_un");
        }
    }
}
