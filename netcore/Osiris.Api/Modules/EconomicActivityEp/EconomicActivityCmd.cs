﻿using System;

namespace Osiris.Api.Modules.EconomicActivityEp
{
    public class EconomicActivityCmd
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public Guid Parent { get; set; }

        public sbyte? TributaryCategory { get; set; }
    }
}
