﻿using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class TaxDocumentStateCmd
        : Command
    {
        public TaxDocumentStatus Status { get; set; }
    }
}