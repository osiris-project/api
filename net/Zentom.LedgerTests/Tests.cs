﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate;
using Zentom.Core.Entities;
using Zentom.Core.Entities.TaxDocument;
using Zentom.Database;

namespace Zentom.LedgerTests
{
    [TestClass]
    public class Tests
    {
        #region Properties

        // Estos son los tipos de documento

        /// <summary>
        /// Factura Electrónica afecta
        /// </summary>
        const string Bill = "33";

        /// <summary>
        /// Factura electrónica exenta
        /// </summary>
        const string ExemptBill = "34";

        /// <summary>
        /// Nota crédito electrónica
        /// </summary>
        const string CreditNote = "61";

        /// <summary>
        /// Nota débito electrónica
        /// </summary>
        const string DebitNote = "56";

        /// <summary>
        /// Sector exento de iva de la empresa de prueba
        /// </summary>
        const string exemptSector = "602220";
        /// <summary>
        /// Sector afecto a iva de la empresa de prueba
        /// </summary>
        const string subjectSector = "132010";

        /// <summary>
        /// Rut del cliente de prueba
        /// </summary>
        const string clientTaxId = "93083482";

        /// <summary>
        /// Rut del proveedor de prueba.
        /// </summary>
        const string providerTaxId = "96950918";

        /// <summary>
        /// La sesión de NHibernate
        /// </summary>
        private static ISession session;

        /// <summary>
        /// El responsable de enviar los documentos. Un empleado de pruebas.
        /// </summary>
        private Employee responsible;

        #endregion

        #region Init

        [TestInitialize]
        public void TestInitialize()
        {
            session = NHibernateHelper.SessionFactory.OpenSession();
            session.BeginTransaction();
            // Los ruts se guardan sin normalizar, o sea, sin puntos ni guión:
            responsible = session.QueryOver<Employee>()
                .Left.JoinQueryOver(i => i.Person)
                .Where(i => i.NationalId == "146408133")
                .Take(1)
                .SingleOrDefault();
        }

        #endregion

        #region Finalize

        [TestCleanup]
        public void Cleanup()
        {
            if (session == null) return;
            if (!session.Transaction.IsActive) return;
            try
            {
                session.Transaction.Commit();
            }
            catch
            {
                session.Transaction.Rollback();
            }
        }

        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            var provider = GetTenant(providerTaxId);
            var client = GetEnterprise(clientTaxId);

            // Hay varios documentos creados:
            // Facturas afectas - 1
            // Facturas exentas - 1
            // Para leer una factura y obtener un TaxDocument:
            var facturaAfecta = FindTaxDoc(1L, Bill, provider);
            var facturaExenta = FindTaxDoc(1L, ExemptBill, provider);
            // Habrán 4 notas de crédito con folios 1-3:
            var notaCredito = FindTaxDoc(1L, CreditNote, provider);
            // Habrán 2 notas de débito (folios 1 y 2):
            var notaDebito = FindTaxDoc(1L, DebitNote, provider);
        }

        #region Utils

        private static Enterprise GetEnterprise(string taxId)
        {
            return session.QueryOver<Enterprise>().Where(i => i.TaxId == taxId).Take(1)
.SingleOrDefault();
        }

        private static Tenant GetTenant(string taxId)
        {
            return session.QueryOver<Tenant>().Where(i => i.TaxId == taxId).Take(1)
.SingleOrDefault();
        }

        private static TaxDocument FindTaxDoc(long folio, string doctypeCode, Tenant enterprise)
        {
            return
                session.QueryOver<TaxDocument>()
                    .Where(i => i.FolioNumber == folio && i.DocumentType == doctypeCode && i.Tenant == enterprise)
                    .Take(1)
.SingleOrDefault();
        }

        #endregion
    }
}
