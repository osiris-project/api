﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Slf4Net;
using Nancy;
using Nancy.ModelBinding;
using Osiris.Api.Modules;
using Osiris.Api.Security;

namespace Osiris.Api.Extensions.Nancy
{
    public static class MethodHandler
    {
        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(MethodHandler));

        #region Run Methods

        public static object Run<T>(this NancyModule module, Func<T, object> handler)
        {
            Log.Debug("Entering Run<T> - No Args.");
            var authorization = AuthorizationHelper.IsAuthorized(module, handler);
            var response = UnauthorizedResponse(module, authorization);
            if (response != null)
            {
                return response;
            }

            T model;
            var result = Validate(module, out model);
            Log.Debug("Leaving Run<T>.");
            return result ?? handler(model);
        }

        private static object Run<T>(this NancyModule module, Func<T, dynamic, object> handler, dynamic args)
        {
            Log.Debug("Entering Run<T> - With args.");
            var authorization = AuthorizationHelper.IsAuthorized(module, handler.Method);
            var response = UnauthorizedResponse(module, authorization);
            if (response != null)
            {
                return response;
            }
            T model;
            var result = Validate(module, out model);
            Log.Debug("Leaving Run<T>");
            return result ?? handler(model, args);
        }

        private static async Task<object> RunAsync<T>(this NancyModule module, Func<T, CancellationToken, Task<object>> handler, CancellationToken ct)
        {
            T model;
            var result = Validate(module, out model);
            return result ?? await handler(model, ct);
        }

        private static async Task<object> RunAsync<T>(this NancyModule module, Func<T, dynamic, CancellationToken, Task<object>> handler, dynamic args, CancellationToken ct)
        {
            T model;
            var result = Validate(module, out model);
            return result ?? await handler(model, args, ct);
        }

        private static async Task<object> RunAsync(this NancyModule module,
            Func<dynamic, CancellationToken, Task<object>> handler, dynamic args, CancellationToken ct)
        {
            return await handler(args, ct);
        }

        private static object Run(this NancyModule module, Func<object> handler)
        {
            var authorization = AuthorizationHelper.IsAuthorized(module, handler.Method);
            var response = UnauthorizedResponse(module, authorization);
            if (response != null)
            {
                return response;
            }

            return handler();
        }

        private static object Run(this NancyModule module, Func<dynamic, object> handler, dynamic args)
        {
            var authorization = AuthorizationHelper.IsAuthorized(module, handler.Method);
            var response = UnauthorizedResponse(module, authorization);
            if (response != null)
            {
                return response;
            }
            return handler(args);
        }

        private static async Task<object> RunAsync(this NancyModule module, Func<Task<object>> handler)
        {
            return await handler();
        }

        private static async Task<object> RunAsync(this NancyModule module, Func<dynamic, Task<object>> handler,
            dynamic args)
        {
            return await handler(args);
        }

        #endregion

        #region Get Method

        public static void Get(this NancyModule module, string path, Func<object> handler)
        {
            module.Get(path, _ => Run(module, handler));
        }

        public static void Get(this NancyModule module, string path, Func<dynamic, object> handler)
        {
            module.Get(path, args => Run(module, handler, args));
        }

        public static void Get<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Get(path, _ => Run(module, handler));
        }

        public static void Get<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Get(path, args => Run(module, handler, args));
        }

        public static void Get<T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Get(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Get <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Get(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Get(this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Get(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        #endregion

        #region Post Method

        public static void Post(this NancyModule module, string path, Func<object> handler)
        {
            module.Post(path, _ => Run(module, handler));
        }

        public static void Post(this NancyModule module, string path, Func<dynamic, object> handler)
        {
            module.Post(path, args => Run(module, handler, args));
        }

        public static void Post<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Post(path, _ => Run(module, handler));
        }

        public static void Post<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Post(path, args => Run(module, handler, args));
        }

        public static void Post <T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Post(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Post <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Post(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Post (this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Post(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        #endregion

        #region Delete Method

        public static void Delete(this NancyModule module, string path, Func<object> handler)
        {
            module.Delete(path, _ => Run(module, handler));
        }

        public static void Delete<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Delete(path, _ => Run(module, handler));
        }

        public static void Delete<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Delete(path, args => Run(module, handler, args));
        }

        public static void Delete <T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Delete(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Delete <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Delete(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Delete (this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Delete(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        #endregion

        #region Put Method

        public static void Put(this NancyModule module, string path, Func<object> handler)
        {
            module.Put(path, _ => Run(module, handler));
        }

        public static void Put(this NancyModule module, string path, Func<dynamic, object> handler)
        {
            module.Put(path, args => Run(module, handler, args));
        }

        public static void Put<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Put(path, _ => Run(module, handler));
        }

        public static void Put<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Put(path, args => Run(module, handler, args));
        }

        public static void Put <T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Put(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Put <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Put(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Put (this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Put(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        #endregion

        #region Patch Method

        public static void Patch(this NancyModule module, string path, Func<object> handler)
        {
            module.Patch(path, _ => Run(module, handler));
        }

        public static void Patch(this NancyModule module, string path, Func<dynamic, object> handler)
        {
            module.Patch(path, args => Run(module, handler, args));
        }

        public static void Patch<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Patch(path, _ => Run(module, handler));
        }

        public static void Patch<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Patch(path, args => Run(module, handler, args));
        }

        public static void Patch <T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Patch(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Patch <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Patch(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Patch (this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Patch(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        #endregion

        #region Options Method

        public static void Options(this NancyModule module, string path, Func<object> handler)
        {
            module.Options(path, _ => Run(module, handler));
        }

        public static void Options<T>(this NancyModule module, string path, Func<T, object> handler)
        {
            module.Options(path, _ => Run(module, handler));
        }

        public static void Options<T>(this NancyModule module, string path, Func<T, dynamic, object> handler)
        {
            module.Options(path, args => Run(module, handler, args));
        }

        public static void Options <T>(this NancyModule module, string path, Func<T, CancellationToken, Task<object>> handler)
        {
            module.Options(path, async (args, ct) => await RunAsync(module, handler, ct));
        }

        public static void Options <T>(this NancyModule module, string path, Func<T, dynamic, CancellationToken, Task<object>> handler)
        {
            module.Options(path, async (args, ct) => await RunAsync(module, handler, args, ct));
        }

        public static void Options (this NancyModule module, string path, Func<dynamic, CancellationToken, Task<object>> handler)
        {
            module.Options(path, async (args, ct) => await RunAsync(module, handler, args));
        }

        #endregion

        #region Utils

        private static object Validate<T>(INancyModule module, out T model)
        {
            Log.Debug("Entering Validate<T>.");
            model = module.BindAndValidate<T>();
            if (!module.ModelValidationResult.IsValid)
            {
                return module.Negotiate.RespondWithValidationFailure(module.ModelValidationResult);
            }
            Log.Debug("Model Validation Succeeded.");

            Log.Debug("Leaving Validate<T>.");
            return null;
        }

        #endregion

        private static object UnauthorizedResponse(NancyModule module, AuthorizationHelper.AuthorizationResponse obj)
        {
            if (obj == null)
            {
                return null;
            }
            if (!string.IsNullOrWhiteSpace(obj.RequiredPermissions))
            {
                return module.Negotiate.WithUnauthorized(new
                {
                    permissions = obj.RequiredPermissions
                }, I18n.Error.Unauthorized);
            }

            if (!string.IsNullOrWhiteSpace(obj.RequiredModules))
            {
                return module.Negotiate.WithUnauthorized(new
                {
                    modules = obj.RequiredModules
                });
            }

            return null;
        }
    }
}
