﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class AuthorizedAccount
        : TenantEntity
    {
        #region Fields

        private Account account;

        private ISet<AccountRole> accountRoles = new HashSet<AccountRole>();

        #endregion

        #region Properties

        public virtual Account Account
        {
            get { return account; }
            set { account = value; }
        }

        public virtual ISet<AccountRole> AccountRoles
        {
            get { return accountRoles; }
            set { accountRoles = value; }
        }

        #endregion

        
        public AuthorizedAccount()
        {
        }

        public AuthorizedAccount(Tenant tenant, Account account)
            : base(tenant)
        {
            this.account = account;
        }
    }
}