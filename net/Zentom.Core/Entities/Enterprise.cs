﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zentom.Core.Entities
{
    public enum EnterpriseRelationship
        : short
    {
        Client,
        Provider,
        ClientProvider 
    }

    public class Enterprise
        : Company
    {
        #region

        private EnterpriseRelationship relationship;

        private CostsCenter costsCenter;
        
        private Tenant tenant;

        #endregion

        #region Properties

        public virtual Tenant Tenant
        {
            get { return tenant; }
            set { tenant = value; }
        }

        public virtual ISet<EnterpriseAddress> EnterpriseAddresses
        {
            get
            {
                if (Addresses == null)
                {
                    Addresses = new HashSet<EnterpriseAddress>();
                }
                return (ISet<EnterpriseAddress>) Addresses;
            }
            set { Addresses = value; }
        }

        public virtual EnterpriseRelationship Relationship
        {
            get { return relationship; }
            set { relationship = value; }
        }

        public virtual ISet<EnterpriseEconomicActivity> EnterpriseEconomicActivities
        {
            get
            {
                if (EconomicActivities == null)
                {
                    EconomicActivities = new HashSet<EnterpriseEconomicActivity>();
                }
                return (ISet<EnterpriseEconomicActivity>) EconomicActivities;
            }
            set { EconomicActivities = value; }
        }

        public virtual ISet<EnterpriseComercialActivity> EnterpriseComercialActivities
        {
            get
            {
                if (ComercialActivities == null)
                {
                    ComercialActivities = new HashSet<EnterpriseComercialActivity>();
                }
                return (ISet<EnterpriseComercialActivity>) ComercialActivities;
            }
            set { ComercialActivities = value; }
        }

        public virtual CostsCenter CostsCenter
        {
            get { return costsCenter; }
            set { costsCenter = value; }
        }

        #endregion

        public Enterprise() { }

        public Enterprise(string taxId, string displayName, string legalName, string bankAccountNumber, string email,
            string phone, IEnumerable<CompanyAddress> addresses,
            IEnumerable<EnterpriseComercialActivity> comercialActivities,
            IEnumerable<EnterpriseEconomicActivity> economicActivities, EnterpriseRelationship relationship, Tenant tenant)
            : base(
                taxId, displayName, legalName, bankAccountNumber, email, phone, addresses, comercialActivities,
                economicActivities)
        {
            this.relationship = relationship;
            this.tenant = tenant;
        }

        public virtual Guid AddAddress(string line1, string line2, Commune commune, IEnumerable<AddressType> types)
        {
            if (EnterpriseAddresses == null)
            {
                EnterpriseAddresses = new HashSet<EnterpriseAddress>();
            }

            var address = EnterpriseAddresses.SingleOrDefault(i => i.Line1 == line1 && i.Commune == commune);
            if (address != null)
                return address.Id;

            address = new EnterpriseAddress
            {
                Line1 = line1,
                Line2 = line2,
                Commune = commune,
                Enterprise = this
            };

            foreach (var eat in types.Select(type => new EnterpriseAddressType(address, type)))
            {
                address.EnterpriseAddressTypes.Add(eat);
            }

            EnterpriseAddresses.Add(address);
            return address.Id;
        }

        public virtual void SetComercialActivities(List<ComercialActivity> activities)
        {
            if (EnterpriseComercialActivities == null)
            {
                EnterpriseComercialActivities = new HashSet<EnterpriseComercialActivity>();
            }

            if (activities != null)
            {
                var len = EnterpriseComercialActivities.Count() - 1;

                for (var index = len; index >= 0; index--)
                {
                    var el = EnterpriseComercialActivities.ElementAt(index);
                    if (activities.All(i => i.Name != el.ComercialActivity.Name)) continue;
                    EnterpriseComercialActivities.Remove(el);
                }

                foreach (var comercialActivity in activities)
                {
                    EnterpriseComercialActivities.Add(new EnterpriseComercialActivity(this, comercialActivity));
                }
            }
            else
            {
                EnterpriseComercialActivities.Clear();
            }
        }

        public virtual void SetEconomicActivities(List<EconomicActivity> activities)
        {
            if (EconomicActivities == null)
            {
                EnterpriseEconomicActivities = new HashSet<EnterpriseEconomicActivity>();
            }
            if (activities != null)
            {
                var len = EnterpriseEconomicActivities.Count - 1;

                for (var index = len; index >= 0; index--)
                {
                    var el = EnterpriseEconomicActivities.ElementAt(index);
                    if (activities.All(i => i.Code != el.EconomicActivity.Code)) continue;
                    EnterpriseEconomicActivities.Remove(el);
                }

                foreach (var economicActivity in activities)
                {
                    EnterpriseEconomicActivities.Add(new EnterpriseEconomicActivity(this, economicActivity));
                }
            }
            else
            {
                EnterpriseEconomicActivities.Clear();
            }
        }

        public virtual void AddCostsCenter(CostsCenter costsCenter)
        {
            this.costsCenter = costsCenter;
        }
    }
}