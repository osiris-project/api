﻿using System;
using log4net;
using Nancy;
using Nancy.ModelBinding;
using NHibernate;
using Zentom.Api.Extensions.Nancy;
using Zentom.Api.Extensions.NHibernate;
using Zentom.Api.Security;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.ProductEp
{
    public class ProductModule
        : CRUDLModule<Command, ProductCommand, ProductCommand>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ProductModule));

        public ProductModule(ISessionFactory factory)
            : base("products", factory)
        {
            this.Get(BasePath + "/query", ProductBaseQuery);
            this.Post<ProductCommand>(BasePath + "/force-exception", ForceUniqueKeyException);
        }

        [Permission(Permission = "product-read")]
        public override object Read(Command cmd)
        {
            log.DebugFormat("Trying to locate product '{0}'.", cmd.Id);
            var prod = db.QueryOver<Product>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .ProjectToObject<Product, ProductDto>();

            if (prod == null)
                return Negotiate.RespondWithValidationFailure(
                    string.Format(I18n.Error.ProductNotFound, cmd.Id));

            return Negotiate.WithModel(prod)
                .WithStatusCode(HttpStatusCode.OK);
        }

        [Permission(Permission = "product-list")]
        public override object List(dynamic args)
        {
            log.DebugFormat("Entering GetProducts.");
            var page = Settings.PaginationSettings.Page;
            var rpp = Settings.PaginationSettings.ResultsPerPage;
            var bound = this.BindTo(new ProductQuery());
            var query = db.QueryOver<Product>();

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike(bound.Name);
            }

            var items = query
                .Where(i => i.Tenant == currentTenant)
                .ProjectInto<Product, ProductDto>()
                .PaginateResults<Product, ProductDto>(page, rpp);
            log.DebugFormat("Leaving GetProducts.");
            return Negotiate.WithOkAndList(items, Request.Url);
        }

        [Permission(Permission = "product-create")]
        public override object Create(ProductCommand cmd, dynamic args)
        {
            var exists = db.Exists<Product>(i =>
                i.Name == cmd.Name && i.Tenant == currentTenant);
            if (exists)
                return
                    Negotiate.RespondWithValidationFailure(string.Format(I18n.Error.ExistingItemName, I18n.Text.Product));
            var product = new Product(currentTenant, cmd.Name, cmd.UnitPrice, cmd.IsDiscontinued);
            db.Save(product);
            return Negotiate.WithCreated(Context.GetResourceUrl(product.Id), product.Id);
        }

        [Permission(Permission = "product-delete")]
        public override object Remove(Command cmd)
        {
            var found = db.QueryOver<Product>()
                .Where(i => i.Id == cmd.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            if (found?.Id == Guid.Empty)
                return Negotiate.WithNotFound();
            db.Delete(found);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        [Permission(Permission = "product-update")]
        public override object Update(ProductCommand product)
        {
            var found = db.QueryOver<Product>()
                .Where(i => i.Id == product.Id && i.Tenant == currentTenant)
                .Take(1)
                .SingleOrDefault();
            if (found?.Id == Guid.Empty)
                return Negotiate.WithNotFound();
            db.Update(product);
            return Negotiate.WithStatusCode(HttpStatusCode.OK);
        }

        [Permission(Permission = "product-read")]
        public object ProductBaseQuery(dynamic args)
        {
            var bound = this.BindTo(new ProductQuery());
            var query = db.QueryOver<ProductBase>()
                .Where(i => i.Tenant == currentTenant);

            if (!string.IsNullOrWhiteSpace(bound.Name))
            {
                query.WhereRestrictionOn(i => i.Name)
                    .IsInsensitiveLike("%" + bound.Name + "%");
            }

            var items = query.ProjectInto<ProductBase, ProductDto>()
                .PaginateResults<ProductBase, ProductDto>();
            return Negotiate.WithOkAndList(items, Request.Url);
        }

        [Permission(Permission = "product-create")]
        public object ForceUniqueKeyException(ProductCommand cmd)
        {
            var product = new Product(currentTenant, cmd.Name, cmd.UnitPrice, cmd.IsDiscontinued);
            db.Save(product);
            return Negotiate.WithCreated(Context.GetResourceUrl(product.Id), product.Id);
        }
    }
}