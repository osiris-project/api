﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace Osiris.Services
{
    public static class PasswordUtils
    {
        public static byte[] GenerateResetToken()
        {
            byte[] result;

            using (var rng = new RNGCryptoServiceProvider())
            {
                result = new byte[24];
                rng.GetBytes(result, 0, 24);
                rng.Dispose();
            }
            
            return result;
        }

        public static string ToStringToken(this byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        public static byte[] ToTokenBytes(this string token)
        {
            return Convert.FromBase64String(token);
        }

        public static bool CompareTo(this byte[] a, byte[] b)
        {
            if (a == null || b == null)
                return false;
            if (a.Length <= 0 || b.Length <= 0)
                return false;
            return a.SequenceEqual(b);
        }
    }
}
