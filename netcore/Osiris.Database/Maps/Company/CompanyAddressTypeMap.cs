﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Company
{
    public class CompanyAddressTypeMap
        : ClassMap<CompanyAddressType>
    {
        public CompanyAddressTypeMap()
        {
            UseUnionSubclassForInheritanceMapping();
            Id(i => i.Id);
            Version(i => i.RowVersion);
        }
    }
}
