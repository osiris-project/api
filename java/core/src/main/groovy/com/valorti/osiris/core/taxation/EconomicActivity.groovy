/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation

import com.valorti.osiris.core.ValidationBuilder

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.ValidationException

import javax.persistence.Transient

/**
 * Represents the economic activities for an enterprise or tenant.
 * This object is global, which means is filled and used by all tenants.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 *
 */

@Entity
@Table(schema = 'taxation')
class EconomicActivity extends UUIDEntity {

    enum TributaryCategory {
        OTHER(0),
        PRIMARY(1),
        SECONDARY(2)

        final int value

        TributaryCategory(int value) {
            this.value = value
        }

        static fromInt(int value) {
            values().each {
                if (it.value == value) {
                    return it
                }
            }
            return null
        }
    }

    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 110)
    String name

    @Column(nullable = false, length = 6, unique = true)
    String code

    @Column(nullable = false)
    boolean isSubjectToTax

    /**
     * TODO: this should be a map of constant values in the future.
     */
    @Column(nullable = false)
    TributaryCategory tributaryCategory

    @ManyToOne(optional = true)
    EconomicActivity parent

    protected EconomicActivity() {
    }

    /**
     *
     * @param name
     * @param code
     * @param isSubjectToTax
     * @param tributaryCategory
     * @param parent
     * @since 0.0.1
     */
    EconomicActivity(String name, String code, boolean isSubjectToTax, TributaryCategory tributaryCategory,
                     EconomicActivity parent) {
        def errors = []
        if (!name?.trim()) {
            errors << ValidationBuilder.empty('economicActivity.name')
        }
        if (!code?.trim()) {
            errors << ValidationBuilder.empty('economicActivity.code')
        }
        if (errors) {
            throw new ValidationException(errors)
        }
        this.name = name.trim()
        this.code = code.trim()
        this.isSubjectToTax = isSubjectToTax
        this.tributaryCategory = tributaryCategory
        this.parent = parent
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((code == null) ? 0 : code.hashCode())
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        result = prime * result + ((parent == null) ? 0 : parent.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true
        if (!super.equals(obj))
            return false
        if (!(obj instanceof EconomicActivity))
            return false
        EconomicActivity other = (EconomicActivity) obj
        if (code == null) {
            if (other.code != null)
                return false
        } else if (!code.equals(other.code))
            return false
        if (name == null) {
            if (other.name != null)
                return false
        } else if (!name.equals(other.name))
            return false
        if (parent == null) {
            if (other.parent != null)
                return false
        } else if (!parent.equals(other.parent))
            return false
        return true
    }
}
