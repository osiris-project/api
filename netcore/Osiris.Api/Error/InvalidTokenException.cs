﻿using System;

namespace Osiris.Api.Error
{
    public class InvalidTokenException
        : Exception
    {
        public InvalidTokenException()
            : base("The token is not valid. Someone may have tampered with it.")
        {
            
        }
    }
}
