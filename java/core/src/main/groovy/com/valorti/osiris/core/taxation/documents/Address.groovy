/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Embeddable

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Embeddable()
class Address {
    @Column(nullable = false, length = 150)
    final String line

    @Column(nullable = false, length = 50)
    final String commune

    @Column(nullable = false, length = 50)
    final String city

    protected Address() {
        this.line = ''
        this.commune = ''
        this.city = ''
    }

    /**
     * @param line The address line.
     * @param commune A commune this address is related to.
     * @param city The city this address is related to.
     * @throws IllegalArgumentException If the line or commune
     * are null.
     * @since 0.0.1
     */
    Address(String line, String commune, String city)
        throws IllegalArgumentException {
        def e = []
        if (!line.trim()) {
            throw new IllegalArgumentException('line')
        }
        if (!commune.trim()) {
            throw new IllegalArgumentException('commune')
        }
        this.line = line.trim()
        this.commune = commune.trim()
        this.city = city ? city.trim() : ''
    }
}
