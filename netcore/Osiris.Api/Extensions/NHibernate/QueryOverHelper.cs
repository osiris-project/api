﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Slf4Net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Criterion.Lambda;
using NHibernate.Transform;
using Osiris.Core.Entities;
using Osiris.Core.Pagination;

namespace Osiris.Api.Extensions.NHibernate
{
    public static class QueryOverHelper
    {
        private const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(QueryOverHelper));

        #region Pagination

        public static IPagedResult<TRoot> PageResults<TRoot, TSub>(this IQueryOver<TRoot, TSub> query,
            int page = 1, int pageSize = 15)
            where TRoot : Entity
        {
            var clone = query.Clone();
            clone.ClearOrders();
            var tmp = query.Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Future<TRoot>();
            var count = clone.Select(Projections.RowCount())
                .FutureValue<int>().Value;
            return new PagedResults<TRoot>(tmp, page, pageSize, count);
        }

        public static IPagedResult<TRoot> PageResults<TRoot>(this IQueryOver<TRoot, TRoot> query)
            where TRoot : Entity
        {
            return query.PageResults<TRoot, TRoot>();
        }

        public static IPagedResult<TOut> PaginateResults<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query,
            int page = 1, int pageSize = 15)
            where TRoot : Entity
        {
            return query.PaginateResults<TRoot, TRoot, TOut>(page, pageSize);
        }

        public static PagedResults<TOut> PaginateResults<TEntity, TSubType, TOut>(this IQueryOver<TEntity, TSubType> query,
            int page = 1, int pageSize = 15)
            where TEntity : Entity
        {
            var clone = query.Clone();
            clone.ClearOrders();
            var list = query.Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Future<TOut>();
            var count = clone.Select(Projections.RowCount())
                .FutureValue<int>().Value;
            return new PagedResults<TOut>(list, page, pageSize, count);
        }

        #endregion

        public static bool Exists<T>(this ISession session, Expression<Func<T, bool>> whereExpression)
            where T : Entity
        {
            return session.QueryOver<T>()
                .Where(whereExpression)
                .Take(1)
                .SingleOrDefault() != null;
        }

        /// <summary>
        /// Projects and transforms the root object (TRoot) into the output object (TOut).
        /// </summary>
        /// <typeparam name="TRoot">The base object that's going to be transformed.</typeparam>
        /// <typeparam name="TSub">Sub type object of the query.</typeparam>
        /// <typeparam name="TOut">The resulting object.</typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TSub> ProjectInto<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>();
        }

        /// <summary>
        /// Internally it calls <see cref="ProjectInto&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt; TRoot, TSub&gt;)"/>
        /// using the TRoot as TSub.
        /// </summary>
        /// <typeparam name="TRoot">The base object of the query.</typeparam>
        /// <typeparam name="TOut">The resulting object.</typeparam>
        /// <param name="query">IQueryOver object.</param>
        /// <returns>An IQueryOver object.</returns>
        public static IQueryOver<TRoot, TRoot> ProjectInto<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.ProjectInto<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Projects, transforms and executes the query, returning an instance of TOut
        /// or the default values.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static TOut ProjectToObject<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>()
                .SingleOrDefault<TOut>();
        }

        /// <summary>
        /// <see cref="ProjectToObject&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static TOut ProjectToObject<T, TOut>(this IQueryOver<T, T> query)
        {
            return query.ProjectToObject<T, T, TOut>();
        }

        /// <summary>
        /// Projects, transforms and generates an IEnumerable&lt;TOut&gt;.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns>An IEnumerable&lt;TOut&gt; on success.</returns>
        public static IEnumerable<TOut> ProjectToList<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.SelectAs<TRoot, TSub, TOut>()
                .TransformToBean<TRoot, TSub, TOut>()
                .List<TOut>();
        }

        /// <summary>
        /// <see cref="ProjectToList&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)"/>
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IEnumerable<TOut> ProjectToList<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.ProjectToList<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Projects a TRoot into a TOut object.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        private static IQueryOver<TRoot, TSub> SelectAs<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            Log.Debug("Converting from [{0}] to [{1}].", typeof(TRoot), typeof(TOut));
            var projectionBuilder = new QueryOverProjectionBuilder<TRoot>();
            var toTypeProps = typeof(TOut).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var fromTypeProps = typeof(TRoot).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var toProp in toTypeProps)
            {
                var fromProp = fromTypeProps.SingleOrDefault(i => i.Name == toProp.Name);
                //Log.Debug("To Property: {0}", toProp);
                //Log.Debug("From Property: {0}", fromProp);
                if (fromProp == null)
                {
                    //Log.Debug("Property {0} skipped.", toProp);
                    continue;
                }

                //Log.Debug("Projecting property: {0}", toProp);
                projectionBuilder.Select(Projections.Property(fromProp.Name).As(toProp.Name));
            }

            query.SelectList(i => projectionBuilder);
            return query;
        }

        public static IQueryOver<TRoot, TSub> SelectAs<TRoot, TSub>(this IQueryOver<TRoot, TSub> query, Type outType)
        {
            var projection = new QueryOverProjectionBuilder<TRoot>();
            var inType = typeof(TRoot);
            var toProps = outType.GetProperties(bindingFlags);
            var inProps = inType.GetProperties(bindingFlags);
            //Log.Debug("ToTypeProperties: {0}", toProps.Count());
            //Log.Debug("FromTypeProperties: {0}", inProps.Count());

            foreach (var property in toProps)
            {
                var foundProperty = inProps.SingleOrDefault(i => i.Name == property.Name);
                //Log.Debug("To Property: {0}", property);
                //Log.Debug("From Property: {0}", foundProperty);
                if (foundProperty == null) continue;
                //Log.Debug("Projecting property: {0}", property);
                projection.Select(Projections.Property(foundProperty.Name).As(property.Name));
            }

            query.SelectList(i => projection);
            return query;
        }

        /// <summary>
        /// <see cref="SelectAs&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)" />
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TRoot> SelectAs<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.SelectAs<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Shorcut method to transform a TRoot into a TOut.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TSub"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TSub> TransformToBean<TRoot, TSub, TOut>(this IQueryOver<TRoot, TSub> query)
        {
            return query.TransformUsing(Transformers.AliasToBean<TOut>());
        }

        /// <summary>
        /// <see cref="TransformToBean&lt;TRoot, TSub, TOut&gt;(IQueryOver&lt;TRoot, TSub&gt;)" />
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IQueryOver<TRoot, TRoot> TransformToBean<TRoot, TOut>(this IQueryOver<TRoot, TRoot> query)
        {
            return query.TransformToBean<TRoot, TRoot, TOut>();
        }

        /// <summary>
        /// Applies the "Order By" clause to the query based on string containing the column names and order.
        /// </summary>
        /// <typeparam name="TRoot"></typeparam>
        /// <param name="query"></param>
        /// <param name="orderBy">The order by columns and order.</param>
        /// <returns></returns>
        /// <example>An example of the orderBy string: "name:asc;lastName:asc", asuming that the entity has the columns Name and LastName,
        /// the query would be ordered by the "Name" in ascending order and then by "LastName" in ascending order.</example>
        public static IQueryOver<TRoot, TRoot> OrderBy<TRoot>(this IQueryOver<TRoot, TRoot> query, string orderBy)
        {
            Log.Debug("Entering ParseOrderBy.");
            if (string.IsNullOrWhiteSpace(orderBy))
                return query;

            Log.Debug("OrderBy: {0}", orderBy);
            var type = typeof(TRoot);
            var props = type.GetProperties(bindingFlags);
            try
            {
                var split = orderBy.Split(';');
                var count = 0;

                foreach (var element in split)
                {
                    var elementSplit = element.Split(':');
                    // Parse left value:
                    var prop = NormalizeProperty(props, elementSplit[0]);
                    if (string.IsNullOrWhiteSpace(prop)) continue;
                    count++;

                    var orderBuilder = count <= 0
                        ? query.OrderBy(Projections.Property(prop))
                        : query.ThenBy(Projections.Property(prop));

                    // Set order
                    var asc = true;

                    if (elementSplit.Length == 2)
                    {
                        asc = elementSplit[1].ToLower() == "asc";
                    }
                    query = asc ? orderBuilder.Asc() : orderBuilder.Desc();

                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to apply order to the query.", ex);
            }

            Log.Debug("Leaving ParseOrderBy.");
            return query;
        }

        /// <summary>
        /// Normalizes the parsed columns and checks if the properties exist one by one, using the conventions
        /// used in C#, were the property names are capitalized, but alias aren't (since they're scoped variables).
        /// </summary>
        /// <param name="properties">The properties to be checked.</param>
        /// <param name="toParse">The value to parse.</param>
        /// <returns>Returns null if the property does not exist or the normalized string representation.</returns>
        /// <example>
        /// Given the input propertyName.name, the result would be propertyName.Name. A real example would be:
        /// parentSector.name = parentSector.Name
        /// </example>
        private static string NormalizeProperty(IReadOnlyCollection<PropertyInfo> properties, string toParse)
        {
            Log.Debug("Entering NormalizeProperty");
            if (properties == null)
                return null;
            if (string.IsNullOrWhiteSpace(toParse))
                return null;

            string result = null;
            var currentProperties = properties;
            try
            {
                var split = toParse.Split('.');
                Log.Debug("Parsing: {0}", toParse);
                var isFirst = true;

                foreach (var propertyName in split)
                {
                    var currentProperty = currentProperties.SingleOrDefault(i => string.Equals(i.Name, propertyName, StringComparison.InvariantCultureIgnoreCase));
                    Log.Debug("CurrentProperty: {0}", currentProperty);

                    if (currentProperty == null)
                    {
                        break;
                    }

                    var current = currentProperty.Name;
                    Log.Debug("Current Property: {0}", current);

                    if (currentProperty.ReflectedType == null)
                    {
                        Log.Debug("No reflected type. IsFirst: {0}", isFirst);

                        if (isFirst)
                        {
                            result = current;
                            break;
                        }
                    }
                    else
                    {
                        Log.Debug("Reflected type not null.");
                        if (isFirst && currentProperty.PropertyType.BaseType == typeof(Entity))
                        {
                            current = char.ToLowerInvariant(current[0]) + current.Substring(1);
                        }
                        currentProperties = currentProperty.ReflectedType.GetProperties(bindingFlags);
                    }

                    result = isFirst ? current : string.Concat(result, ".", current);
                    Log.Debug("Current result: {0}", result);
                    isFirst = false;
                }
            }
            catch (Exception ex)
            {
                Log.Error("String to Property failed.", ex);
            }

            Log.Debug("Result: {0}", result);
            Log.Debug("Leaving StringToProperty");
            return result;
        }
    }
}
