﻿using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Interfaces.Integration.TributaryDocuments;
using Osiris.Integration.Artikos;

namespace Osiris.Api
{
    public class SigningProviderFactory
    {
        private readonly ArtikosFactory artikosFactory;

        public SigningProviderFactory(ArtikosFactory artikosFactory)
        {
            this.artikosFactory = artikosFactory;
        }

        public ITributaryDocumentFormatter<TaxDocument> LocateProvider(string signingProviderName, string documentType)
        {
            switch (signingProviderName)
            {
                case "artikos_cl":
                    return artikosFactory.Resolve(documentType);
            }

            return null;
        }
    }
}
