﻿namespace Zentom.Api.Modules.ProfileEp
{
    public class TenantQuery
    {
        public string TaxId { get; set; }

        public string LegalName { get; set; }
    }
}