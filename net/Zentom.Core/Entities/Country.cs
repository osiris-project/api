﻿using System.Collections.Generic;

namespace Zentom.Core.Entities
{
    public class Country
        : Entity
    {
        #region Fields

        private string _name;

        private string _code;

        private string _phoneCode;

        private ISet<CountryLocale> _locales;

        private ISet<City> _cities;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public virtual string PhoneCode
        {
            get { return _phoneCode; }
            set { _phoneCode = value; }
        }

        public virtual ISet<CountryLocale> Locales
        {
            get { return _locales; }
            set { _locales = value; }
        }

        public virtual ISet<City> Cities
        {
            get { return _cities; }
            set { _cities = value; }
        }

        #endregion

        public Country()
        {
            _locales = new HashSet<CountryLocale>();
            _cities = new HashSet<City>();
        }

        public Country(string name, string code, string phoneCode, ISet<CountryLocale> locales, ISet<City> cities)
            : this()
        {
            _name = name;
            _code = code;
            _phoneCode = phoneCode;

            if(locales != null)
                _locales = locales;

            if(cities != null)
                _cities = cities;
        }
    }
}
