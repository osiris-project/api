﻿namespace Zentom.Core.Entities
{
    public class TaxDocumentTotals
        : AbstractEntity
    {
        public virtual decimal NetAmount { get; set; }

        public virtual decimal ExemptAmount { get; set; }

        public virtual decimal VatAmount { get; set; }

        public virtual decimal TaxRate { get; set; }

        public virtual decimal Total { get; set; }

        public virtual decimal NonBillableAmount { get; set; }

        public virtual TaxDocument TaxDocument { get; set; }

        public TaxDocumentTotals() { }
    }
}
