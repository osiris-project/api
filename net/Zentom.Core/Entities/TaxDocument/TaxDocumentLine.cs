﻿using System;

namespace Zentom.Core.Entities.TaxDocument
{
    public enum TaxType
        : short
    {
        Subject = 0,
        Exempt = 1,
        PositiveNonBillable = 2,
        NegativeNonBillable = 6,
        ToLower = 5
    }

    public class TaxDocumentLine
        : Entity
    {
        #region Fields

        private decimal extraTaxAmount;

        private decimal totalTaxAmount;

        private int lineNumber;

        private string text;

        private string additionalGloss;

        private string measureUnit;

        private TaxDocument taxDocument;

        private decimal quantity;

        private decimal unitPrice;

        private decimal netAmount;

        private decimal taxAmount;

        private decimal lineTotal;

        private TaxType taxType;

        #endregion

        #region Persistent Properties

        public virtual int LineNumber
        {
            get { return lineNumber; }
            set { lineNumber = value; }
        }

        public virtual string Text
        {
            get { return text; }
            set { text = value; }
        }

        public virtual string AdditionalGloss
        {
            get { return additionalGloss; }
            set { additionalGloss = value; }
        }

        public virtual decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        public virtual decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        /// <summary>
        /// Total amount of taxes for this line, including any extra taxes.
        /// </summary>
        public virtual decimal TaxAmount
        {
            get { return taxAmount; }
            set { taxAmount = value; }
        }

        /// <summary>
        /// This is the total of extra tax amount of this line.
        /// </summary>
        public virtual decimal ExtraTaxAmount
        {
            get { return extraTaxAmount; }
            protected set { extraTaxAmount = value; }
        }

        /// <summary>
        /// Represents the total monetary amount of this line, including any tax
        /// and extra tax.
        /// </summary>
        public virtual decimal LineTotal
        {
            get { return lineTotal; }
            set { lineTotal = value; }
        }

        public virtual TaxDocument TaxDocument
        {
            get { return taxDocument; }
            set { taxDocument = value; }
        }

        public virtual bool HasExtraTaxes => ExtraTaxAmount > 0;

        public virtual string MeasureUnit
        {
            get { return measureUnit; }
            set { measureUnit = value; }
        }

        public virtual TaxType TaxType
        {
            get { return taxType; }
            set { taxType = value; }
        }

        #endregion

        #region Non persistent Properties
        
        public virtual decimal TotalTaxAmount
        {
            get { return totalTaxAmount; }
            protected set { totalTaxAmount = value; }
        }

        public virtual decimal NetAmount
        {
            get { return netAmount; }
            set { netAmount = value; }
        }

        #endregion

        public TaxDocumentLine()
        {
            lineNumber = 1;
            text = "";
            additionalGloss = "";
            measureUnit = "";
        }

        public TaxDocumentLine(int lineNumber, string text,
            TaxDocument taxDocument, decimal quantity = 1,
            string additionalGloss = "", decimal unitPrice = 0,
            string measureUnit = "", TaxType taxType = TaxType.Subject)
        {
            if (taxDocument == null)
                throw new ArgumentNullException(nameof(taxDocument));

            this.lineNumber = lineNumber;
            this.text = text;
            this.taxDocument = taxDocument;
            this.quantity = quantity;
            this.additionalGloss = additionalGloss;
            this.unitPrice = unitPrice;
            this.measureUnit = measureUnit;
            this.taxType = taxType;

            decimal taxRate, extraTaxRate;

            if (taxType == TaxType.Exempt)
            {
                extraTaxRate = taxRate = 0.0M;
            }
            else
            {
                taxRate = taxDocument.TaxRate;
                extraTaxRate = taxDocument.ExtraTaxRate;
            }

            netAmount = quantity * unitPrice;
            extraTaxAmount = (netAmount * extraTaxRate);
            taxAmount = (netAmount * taxRate);
            totalTaxAmount = taxAmount + extraTaxAmount;
            lineTotal = netAmount + taxAmount;
        }
    }
}