﻿namespace Osiris.Core.Entities
{
    public class EnterpriseAddressType
        : CompanyAddressType
    {
        #region Properties

        public virtual EnterpriseAddress EnterpriseAddress
        {
            get
            {
                if (CompanyAddress == null)
                {
                    CompanyAddress = new EnterpriseAddress();
                }
                return (EnterpriseAddress) CompanyAddress;
            }
            set { CompanyAddress = value; }
        }

        #endregion

        public EnterpriseAddressType() { }

        public EnterpriseAddressType(CompanyAddress companyAddress, AddressType addressType)
            : base(companyAddress, addressType)
        {
        }
    }
}
