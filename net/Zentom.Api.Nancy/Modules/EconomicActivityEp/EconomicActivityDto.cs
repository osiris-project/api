﻿using System;

namespace Zentom.Api.Modules.EconomicActivityEp
{
    public class EconomicActivityDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string ParentName { get; set; }

        public Guid ParentId { get; set; }

        public bool IsSubjectToTax { get; set; }
    }
}