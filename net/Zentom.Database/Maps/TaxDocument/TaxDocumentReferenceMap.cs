﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities.TaxDocument;

namespace Zentom.Database.Maps.TaxDocument
{
    public class TaxDocumentReferenceMap
        : ClassMap<TaxDocumentReference>
    {
        public TaxDocumentReferenceMap()
        {
            Schema("taxation");
            Table("tax_document_reference");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.ReferenceReason).Length(15);
            Map(p => p.ReferenceComment).Length(90);

            References(i => i.Parent, "parent_id")
                .UniqueKey("tax_document_reference_un")
                .Cascade.None();

            References(i => i.ReferencedDocument, "reference_id")
                .UniqueKey("tax_document_reference_un")
                .Cascade.None();
        }
    }
}
