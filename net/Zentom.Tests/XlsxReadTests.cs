﻿using System;
using System.IO;
using Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Zentom.Tests
{
    [TestClass]
    public class XlsxReadTests
    {
        private Stream stream;

        private IExcelDataReader excel;

        [TestInitialize]
        public void Init()
        {
            excel = GetDataReader(new FileInfo(@"D:\format.xlsx"));
        }

        [TestCleanup]
        public void Cleanup()
        {
            excel?.Close();
            if (stream == Stream.Null) return;
            stream.Close();
            stream.Dispose();
        }

        [TestMethod]
        public void ReadGlossSheet()
        {
            var dataset = excel.AsDataSet();
            Assert.IsNotNull(dataset);
            var gloss = dataset.Tables[0];
            Assert.IsNotNull(gloss);
            var i = 3;

            while (i < gloss.Rows.Count)
            {
                var row = gloss.Rows[i];
                var text = row[0];
                var value = row[1];
                Console.WriteLine(@"Text: {0}, Value: {1}", text, value);
                i++;
            }
        }

        private IExcelDataReader GetDataReader(FileSystemInfo fileInfo)
        {
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException();
            }

            stream = File.OpenRead(fileInfo.FullName);
            return fileInfo.Extension == ".xlsx"
                ? ExcelReaderFactory.CreateOpenXmlReader(stream)
                : ExcelReaderFactory.CreateBinaryReader(stream);
        }
    }
}