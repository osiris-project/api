/**
 * Copyright (c) Mar 13, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.BigIntegerEntity

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
@Entity
@Table(schema = 'taxation')
class GlobalCommission extends BigIntegerEntity {

    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false)
    boolean isCommission = true

    @Column(nullable = false, length = 60)
    String description

    @Column(precision = 4, scale = 3, nullable = false)
    double rate

    @Column(precision = 13, scale = 4, nullable = false)
    double netAmount = 0

    @Column(precision = 13, scale = 4, nullable = false)
    double taxFreeAmount = 0

    @Column(precision = 13, scale = 4, nullable = false)
    double other = 0

    @ManyToOne
    Document document

    /**
     * Default constructor.
     */
    protected GlobalCommission() {
    }

    /**
     * @param isCommission
     * @param description
     * @param rate
     * @param netAmount
     * @param taxFreeAmount
     * @param other
     * @param document
     */
    GlobalCommission(boolean isCommission, String description, double rate, double netAmount, double taxFreeAmount,
    double other, Document document) {
        this.isCommission = isCommission
        this.description = description
        this.rate = rate
        this.netAmount = netAmount
        this.taxFreeAmount = taxFreeAmount
        this.other = other
        this.document = document
    }
}
