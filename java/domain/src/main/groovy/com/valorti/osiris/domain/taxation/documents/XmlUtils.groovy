/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.domain.taxation.documents

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
class XmlUtils {
    private static final Logger LOG = LoggerFactory.getLogger(XmlUtils.class)

    static String dateFormat = 'YYYY-MM-dd'

    static LocalDate dateFromString(String str) {
        try {
            return LocalDate.parse(str, DateTimeFormatter.ISO_LOCAL_DATE)
        }
        catch (Exception ex) {
            LOG.error('Failed to parse date (yyyy-MM-dd): {}', str)
            LOG.error('', ex)
        }
        return null
    }
}
