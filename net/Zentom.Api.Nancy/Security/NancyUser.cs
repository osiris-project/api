﻿using System.Collections.Generic;
using Nancy.Security;
using Zentom.Core.Entities;

namespace Zentom.Api.Security
{
    public class NancyUser
        : IUserIdentity
    {
        #region Fields

        #endregion

        #region Properties

        #region Implementation of IUserIdentity

        public string UserName { get; }

        public IEnumerable<string> Claims => null;

        #endregion

        public Account Account { get; }

        #endregion

        public NancyUser(string userName, Account account)
        {
            UserName = userName;
            Account = account;
        }

    }
}