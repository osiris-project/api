﻿using FluentValidation;
using Osiris.Api.Modules.Validators.Guid;

namespace Osiris.Api.Modules.Validators
{
    public class EntityValidator
        : AbstractValidator<Command>
    {
        public EntityValidator()
        {
            RuleFor(i => i.Id).SetValidator(new GuidValidator());
        }
    }
}
