﻿using System;
using System.Collections.Generic;
using Osiris.Core.Pagination;

namespace Osiris
{
    [Serializable]
    public class PagedResults<TEntity>
        : IPagedResult<TEntity>
    {
        #region Properties

        public IList<TEntity> Items
        {
            get;
        }

        public long ResultCount
        {
            get;
        }

        public int CurrentPage
        {
            get;
        }

        public int ResultsPerPage
        {
            get;
        }

        public long PageCount
        {
            get;
        }

        public bool HasPrevious
        {
            get;
        }

        public bool HasNext
        {
            get;
        }

        #endregion

        public PagedResults(IEnumerable<TEntity> src, int page, int resultsPerPage, int total)
            : this(src, page, resultsPerPage, (long)total)
        { }

        public PagedResults(IEnumerable<TEntity> src, int page, int resultsPerPage, long total)
        {
            CurrentPage = page;
            ResultsPerPage = resultsPerPage;
            ResultCount = total;
            PageCount = ResultCount / ResultsPerPage;
            HasNext = (CurrentPage + 1) < ResultCount;
            HasPrevious = CurrentPage > 0;
            Items = new List<TEntity>(src);
        }

        public PagedResults(IList<TEntity> src, int page, int resultsPerPage, long total)
        {
            ResultsPerPage = resultsPerPage;
            ResultCount = total;
            PageCount = ResultCount / ResultsPerPage;
            HasNext = (CurrentPage + 1) < ResultCount;
            HasPrevious = CurrentPage > 0;
            CurrentPage = page;
            Items = src;
        }
    }
}
