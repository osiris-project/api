﻿using System.IO;
using Zentom.Core.Entities;

namespace Zentom.Core.Services
{
    public interface IReceivedDocumentValidationService
        : IService
    {
        void UpdateDocuments(Stream stream, Tenant tenant);
    }
}