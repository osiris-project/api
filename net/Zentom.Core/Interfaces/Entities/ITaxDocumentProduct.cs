﻿using System;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ITaxDocumentProduct
        : ITaxDocumentProductBase
    {
        IProduct Product { get; set; }
    }

    public interface ITaxDocumentProductId
        : ITaxDocumentProductBaseId
    {
        IProduct Product { get; set; }
    }
}
