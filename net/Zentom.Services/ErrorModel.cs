﻿using Zentom.Core.Interfaces.Validation;

namespace Zentom
{
    public class ErrorModel
        : IModelValidation
    {
        #region Implementation of IModelValidation

        public string MemberName { get; }

        public string ErrorMessage { get; }

        #endregion

        public ErrorModel(string memberName, string errorMessage)
        {
            MemberName = memberName;
            ErrorMessage = errorMessage;
        }
    }
}
