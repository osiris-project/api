/*
 * Copyright (c) 2017 ValorTI Limitada.
 * All rights reserved.
 */

package com.valorti.osiris.core.taxation.documents

import com.valorti.osiris.core.ValidationException

/**
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
interface DocumentParserService {

    /**
     * Parses a tax document from the specified path.
     *
     * @param filePath The path to the file.
     * @return True on success, false otherwise.
     * @throws ValidationException If there are any validation errors.
     * @throws IllegalArgumentException
     */
    boolean storeDocument(String filePath)
        throws ValidationException, IllegalArgumentException
}
