﻿using System;

namespace Zentom.Core.Interfaces.Data
{
    public interface ITransactionScopeWrapper
        : IDisposable
    {
        void Begin();

        void Complete();

        void Rollback();
    }
}
