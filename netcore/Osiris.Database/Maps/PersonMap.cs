﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class PersonMap
        : ClassMap<Person>
    {
        public PersonMap()
        {
            Schema("system");
            Table("person");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            HasOne(i => i.Account)
                .Fetch.Join()
                .Cascade.All();

            Map(i => i.BirthDate).Nullable().Default("null");
            Map(i => i.FirstName).Length(60);
            Map(i => i.FullName).Formula("CONCAT(last_name, ', ', first_name)").Generated.Always().Not.Insert().Not.Update();
            Map(i => i.Gender).Nullable().CustomType(typeof(Gender));
            Map(i => i.LastName).Length(60);
            Map(i => i.NationalId).Length(20).UniqueKey("person_un");
        }
    }
}
