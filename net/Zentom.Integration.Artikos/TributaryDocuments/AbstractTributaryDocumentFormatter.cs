﻿using System.Collections.Generic;
using System.IO;
using Dobelik.Utils.Identity;
using Zentom.Core.Interfaces.Integration.TributaryDocuments;
using Zentom.Core.Interfaces.Validation;

namespace Zentom.Integration.Artikos.TributaryDocuments
{
    internal abstract class AbstractTributaryDocumentFormatter<TEntity>
        : ITributaryDocumentFormatter<TEntity>
    {
        #region Fields

        protected INationalIdentity IdUtils;

        protected TEntity entity;

        /// <summary>
        /// The stream that holds the formatted content.
        /// </summary>
        protected StreamWriter StreamWriter;

        /// <summary>
        /// The total net amount of the document.
        /// </summary>
        protected decimal NetAmount = 0M;

        /// <summary>
        /// The exempt amount of the document (if any).
        /// </summary>
        protected decimal ExemptAmount = 0M;

        /// <summary>
        /// Represents the taxable amount of the document (if any).
        /// </summary>
        //protected decimal netTaxableAmount = 0M;

        protected decimal VatAmount = 0M;

        ///// <summary>
        ///// This is the total of the document. This includes the vat amount and the exempt amount.
        ///// </summary>
        //protected decimal total = 0M;

        #endregion

        #region ITributaryDocumentFormatter Properties

        public IList<IModelValidation> Errors { get; protected set; }

        public bool IsValid => Errors.Count <= 0;

        public string DateFormat { get; protected set; }

        #endregion

        #region Constructors
        /// <summary>
        /// </summary>
        /// <param name="idUtils"></param>
        protected AbstractTributaryDocumentFormatter(INationalIdentity idUtils)
        {
            IdUtils = idUtils;
            Errors = new List<IModelValidation>();
        }

        #endregion

        /// <summary>
        /// This method is called by the <see cref="Format(System.IO.StreamWriter, TEntity)"/>, which must be
        /// overrided by the sub-classes that need it.
        /// This method is the last called in the <see cref="Format(System.IO.StreamWriter, TEntity)"/>.
        /// </summary>
        protected abstract void Format();

        /// <summary>
        /// Used to perform the required class initialization.
        /// This function is called just before the Validation.
        /// </summary>
        protected abstract void Setup();

        #region ITributaryDocumentFormatter Implementation

        public virtual void Format(StreamWriter streamWriter, TEntity obj)
        {
            entity = obj;
            StreamWriter = streamWriter;
            Setup();
            Validate();
            Format();
        }

        public virtual void Dispose()
        {
            StreamWriter?.Dispose();
            Errors.Clear();
            Errors = null;
        }

        protected abstract void Validate();

        #endregion
    }
}
