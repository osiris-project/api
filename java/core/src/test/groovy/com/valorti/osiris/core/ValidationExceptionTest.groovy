/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

import org.junit.After
import org.junit.Before
import org.junit.Test

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since
 */
class ValidationExceptionTest {

    def errors = new ArrayList<ValidationError>()

    /**
     * @throws java.lang.Exception
     */
    @Before
    void setUp() throws Exception {
        println '------------'
        println 'setUp'
        errors.add(ValidationBuilder.empty('name'))
        errors.add(ValidationBuilder.empty('lastName', ''))
        errors.add(ValidationBuilder.empty('phone', ''))
        println errors
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    void tearDown() throws Exception {
        println '------------'
        println 'tearDown'
        errors = null
    }

    @Test
    void test_simpleValidationException() {
        println '------------'
        println 'test_simpleValidationException'
        def ex = new ValidationException(this.errors)
        assert ex != null
        assert ex.errors == this.errors
        assert ex.errors[0].property == 'name.empty'
        assert ex.errors[1].property == 'lastName.empty'
        assert ex.errors[2].property == 'phone.empty'
    }
}
