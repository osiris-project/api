﻿namespace Zentom.DAL.Models.Maps
{
    public class ReductionTypeMap : ClassMap<ReductionType>
    {
        public ReductionTypeMap()
        {
            Id(k => k.Id);

            Map(p => p.Id).Column("id")..HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Map(p => p.RowGuid).Column("row_guid");
            Map(p => p.RowVersion).Column("row_version")..IsConcurrencyToken(true).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Map(p => p.Code).Column("code").;
            Map(p => p.Description).Column("description")..Length(60);

            
        }
    }
}
