﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IAccount
        : IUnique<long>, IRowGuid, IVersioned
    {
        string Login { get; set; }

        byte[] Password { get; set; }

        bool IsActive { get; set; }

        IEmployee AccountCreator { get; set; }

        IEmployee AccountOwner { get; set; }

        ICollection<IAccountRole> AccountRoles { get; set; }
    }
}
