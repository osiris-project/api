﻿using System;
using System.Linq;
using Nancy;
using Nancy.Responses.Negotiation;
using NHibernate;
using Osiris.Api.Extensions.Nancy;
using Osiris.Api.Security;
using Osiris.Core.Services;
using Slf4Net;

namespace Osiris.Api.Modules.FileProcessorEp {
    public class FileProcessorModule : TenantApi {
        private readonly ILogger log = LoggerFactory.GetLogger (typeof (FileProcessorModule));

        private readonly IBulkImportTaxDocumentService bulkImportSvc;

        public FileProcessorModule (ISessionFactory factory, IBulkImportTaxDocumentService bulkImportSvc) : base ("file-processors", factory) {
            this.bulkImportSvc = bulkImportSvc;
            this.Post (BasePath + "/bulk-import-tax-documents/", BulkImportTaxDocuments);
        }

        [Permission (RequiresLogin = true)]
        public Negotiator BulkImportTaxDocuments () {
            if (!Request.Files.Any ()) {
                return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
            }

            var file = Request.Files.FirstOrDefault ();

            if (file == null) {
                return Negotiate.WithStatusCode (HttpStatusCode.BadRequest);
            }

            try {
                var result = bulkImportSvc.Process (file.Value);
                return Negotiate.WithOkAndModel (new {
                    result.Result,
                    errors = result.Errors
                });
            } catch (Exception ex) {
                log.Error ("Failed to process the uploaded bulk file.", ex);
            }

            return Negotiate.WithStatusCode (HttpStatusCode.BadRequest).WithModel (new {
                error = I18n.Error.BulkImportFailed
            });
        }
    }
}
