﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernate;
using NHibernate.Transform;
using Zentom.Core.Entities;
using Zentom.Database;

namespace Zentom.Nhibernate.Tests
{
    [TestClass]
    public class OrderTests
    {
        private static ISession session;
        [TestInitialize]
        public void Init()
        {
            log4net.Config.XmlConfigurator.Configure();
            session = NHibernateHelper.SessionFactory.OpenSession();
            session.BeginTransaction();
        }

        [TestCleanup]
        public void Cleanup()
        {
            try
            {
                session.Transaction.Commit();
            }
            catch
            {
                session.Transaction.Rollback();
                throw;
            }
            session.Dispose();
        }


        [TestMethod]
        public void SimpleOrderBy()
        {
            Dto dto = null;
            EconomicActivity parent = null;
            var query = session.QueryOver<EconomicActivity>()
                .Left.JoinAlias(i => i.Parent, () => parent)
                .SelectList(i => i
                    .Select(j => j.Id).WithAlias(() => dto.Id)
                    .Select(j => j.Name).WithAlias(() => dto.Name)
                    .Select(j => j.Code).WithAlias(() => dto.Code)
                    .Select(j => j.IsSubjectToTax).WithAlias(() => dto.IsSubjectToTax)
                    .Select(j => parent.Name).WithAlias(() => dto.ParentName))
                .TransformUsing(Transformers.AliasToBean<Dto>())
                .OrderBy("parent.name:asc;name");

            var list = query.List<Dto>();
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestAccentQuery()
        {
            var query = session.QueryOver<ComercialActivity>()
                .WhereRestrictionOn(i => i.Name)
                .IsInsensitiveLike("Generación Hidroeléctrica")
                .Take(1)
                .SingleOrDefault();

            Assert.IsNull(query);
        }

        public class Dto
        {
            public Guid Id { get; set; }

            public string Name { get; set; }

            public string Code { get; set; }

            public string ParentName { get; set; }

            public Guid ParentId { get; set; }

            public bool IsSubjectToTax { get; set; }
        }
    }
}