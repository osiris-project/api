﻿using FluentValidation;
using Zentom.Api.Modules.Validators.Guid;

namespace Zentom.Api.Modules.Auth
{
    public class PasswordResetValidator
        : AbstractValidator<PasswordResetCmd>
    {
        public PasswordResetValidator()
        {
            RuleFor(i => i.Id).SetValidator(new GuidValidator());
            RuleFor(i => i.Password).Length(6, 1000);
        }
    }
}