﻿namespace Zentom.Core.Entities
{
    public class TenantEconomicActivity
        : CompanyEconomicActivity
    {
        #region Properties

        public virtual Tenant Tenant
        {
            get { return (Tenant)Company; }
            set { Company = value; }
        }

        #endregion

        public TenantEconomicActivity()
        {
        }

        public TenantEconomicActivity(Company tenant, EconomicActivity economicActivity)
            : base(tenant, economicActivity)
        {
        }
    }
}