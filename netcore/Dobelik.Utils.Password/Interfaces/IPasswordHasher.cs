﻿namespace Dobelik.Utils.Password.Interfaces
{
    /// <summary>
    /// Utility interface used to hash passwords.
    /// </summary>
    /// <remarks>To hash a password, simply call the method <see cref="HashPassword(string, byte[])"/>.
    /// To generate a salt, call <see cref="GenerateSalt()"/>.
    /// Now, compare two password hashes, you must use <see cref="CompareHashes(byte[], string)"/>,
    /// <see cref="CompareHashes(byte[], byte[])"/> or <see cref="CompareHashes(byte[], string, byte[])"/>.
    /// </remarks>
    public interface IPasswordHasher
    {
        /// <summary>
        /// Reports information about this instance:
        /// Salt length, hash length, salt position, iteration count and salt separator.
        /// </summary>
        IHashInfo HashInformation { get; set; }

        /// <summary>
        /// When the <see cref="ExtractSalt(byte[])"/> method is used,
        /// this property contains the extracted Hash. Default is null.
        /// </summary>
        byte[] CurrentHash { get; }

        /// <summary>
        /// When the <see cref="ExtractSalt(byte[])"/> method is called,
        /// this property contains the extracted Salt. Default is null.
        /// </summary>
        byte[] CurrentSalt { get; }

        /// <summary>
        /// Compares two hashes bit by bit.
        /// </summary>
        /// <param name="hash1">The primary hash.</param>
        /// <param name="hash2">The second hash to be compared.</param>
        /// <returns>True if the hash2 is equal to hash1, false otherwise.</returns>
        bool CompareHashes(byte[] hash1, byte[] hash2);

        /// <summary>
        /// Compares two hashes bit by bit.
        /// This function takes the hashedPassword and tries to split
        /// the hash and
        /// salt from it, using the provided <see cref="HashInformation"/>.
        /// Once splitted, the plainTextPassword is hashed with the
        /// extracted salt and then compared.
        /// </summary>
        /// <param name="hashedPassword">The main password to compare.</param>
        /// <param name="plainTextPassword">The plain text password.</param>
        /// <returns>True if both hashes were the same, false otherwise.</returns>
        /// <exception cref="Exceptions.InvalidSaltPositionException">Thrown if the HashInformation.Saltposition == None.</exception>
        bool CompareHashes(byte[] hashedPassword,
            string plainTextPassword);

        /// <summary>
        /// Compares the hashedPassword with the hash generated from
        /// hashing the plainTextPassword with the salt.
        /// </summary>
        /// <param name="hashedPassword">The main hash to compare to the plainTextPassword.</param>
        /// <param name="plainTextPassword">A plain text password.</param>
        /// <param name="salt">The salt to hash the plainTextPassword.</param>
        /// <returns>true if the hashes are the same, false otherwise.</returns>
        /// <exception cref="Exceptions.InvalidSaltPositionException">Thrown if the HashInformation.Saltposition != None.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown if the salt is null or empty.</exception>
        /// <remarks>The hashedPassword must be only the hash, not the hash and the salt,
        /// otherwise the comparison will fail.</remarks>
        bool CompareHashes(byte[] hashedPassword,
            string plainTextPassword, byte[] salt);

        /// <summary>
        /// Concatenates two byte arrays (converting them to string) with the SaltSeparator.
        /// </summary>
        /// <param name="b1">The first array to add.</param>
        /// <param name="b2">The second array to add.</param>
        /// <returns>A concatenated representation of both arrays separated by the glue.</returns>
        string Concat(byte[] b1, byte[] b2);

        /// <summary>
        /// Generates a random salt to be used with/within a hashed password.
        /// </summary>
        /// <returns>The generated salt.</returns>
        byte[] GenerateSalt();

        /// <summary>
        /// Extracts the salt from the specified hash.
        /// </summary>
        /// <param name="hash">The hash from where to exctract the salt.</param>
        /// <returns>The byte array representing the extracted hash.</returns>
        /// <remarks>This method returns a byte array depending on the configuration used
        /// (the properties set). This means that, if the properties when the hash was generated were different
        /// than when this function is used, then the salt will not be the same!</remarks>
        byte[] ExtractSalt(byte[] hash);

        /// <summary>
        /// Hashes a password using PBKDF2. The length of the returned byte array will be HashLength + SaltLength, so the default would be 128.
        /// </summary>
        /// <param name="raw">The password to be hashed.</param>
        /// <param name="salt">The salt to be used.</param>
        /// <returns>A hash representation of the password.</returns>
        byte[] HashPassword(string raw, byte[] salt);
    }
}
