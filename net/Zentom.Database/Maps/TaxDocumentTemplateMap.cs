﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class TaxDocumentTemplateMap
        : ClassMap<TaxDocumentTemplate>
    {
        public TaxDocumentTemplateMap()
        {
            Schema("system");
            Table("tax_document_template");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Name).Length(255).UniqueKey("template_un");
            Map(i => i.FilePath).CustomSqlType("VARCHAR(MAX)");
            Map(i => i.DocumentType).Length(20);
            Map(i => i.CreatedAt);

            References(i => i.Account, "account_id")
                .Fetch.Join()
                .UniqueKey("template_un")
                .Cascade.None();

            References(i => i.Tenant)
                .Fetch.Join()
                .UniqueKey("template_un")
                .Cascade.None();
        }
    }
}
