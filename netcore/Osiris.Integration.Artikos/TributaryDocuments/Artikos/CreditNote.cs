﻿using System;
using System.Linq;
using Dobelik.Utils.Identity;
using log4net;
using Osiris.Core.Entities.TaxDocument;
using Osiris.Core.Enums;
using Osiris.I18n;

namespace Osiris.Integration.Artikos.TributaryDocuments.Artikos
{
    internal class CreditNote
        : AbstractArtikosFormatter
    {
        #region Fields

        private static readonly ILog Log = LogManager.GetLogger(typeof(CreditNote));

        protected CorrectionCode CorrectionCode;

        /// <summary>
        /// For credit notes, apply a discount. Defaults to false.
        /// </summary>
        protected bool CreditNoteDiscount;

        #endregion

        #region Ctors

        public CreditNote(INationalIdentity idUtils)
            : base(idUtils) { }

        #endregion

        protected override void Validate()
        {
            if (entity.ReferencedDocuments == null || entity.ReferencedDocuments.Count <= 0)
            {
                Errors.Add(new ModelValidation("ReferencedDocuments", Error.NoReferencedDocuments));
            }

            Log.DebugFormat("correctionCode = {0}", CorrectionCode.ToString());

            if (entity.TaxDocumentLines == null || entity?.TaxDocumentLines?.Count <= 0)
            {
                Errors.Add(new ModelValidation("CreditNote.NoDetail", Error.NoDetails));
            }

            if (entity.TaxDocumentLines == null || entity?.TaxDocumentLines?.Count <= 0)
            {
                Errors.Add(new ModelValidation("ExemptBill.NoDetail", Error.NoDetails));
            }

            if (CorrectionCode == CorrectionCode.AmendText)
            {
                
            }
        }

        protected override void Setup()
        {
            base.Setup();
            CorrectionCode = entity.CorrectionCode;
            CreditNoteDiscount = entity.CreditNoteDiscount;
            isExempt = entity.ExtraTaxRate <= 0 && entity.TaxRate <= 0;
        }

        protected override void Format()
        {
            CorrectionCode = entity.CorrectionCode;

            if (IsValid)
            {
                WriteHeader();
                WriteDocumentInfo();
                WriteIssuer();
                WriteReceiver();
                WriteDocumentReferences();

                if (CorrectionCode != CorrectionCode.AmendText)
                {
                    Log.DebugFormat("- Details -");
                    WriteDetails();
                    Log.DebugFormat("- END -");
                    WriteRetainedTaxes();
                }
                else
                {
                    SimpleDetails();
                }

                WriteTotals();
                WriteFinish();
            }
            else
            {
                Log.Error("The required fields are not present in the entity object.");
            }
        }

        protected override void WriteDocumentInfo()
        {
            Log.DebugFormat("- Document Info. -");
            var tmp = "";
            StreamWriter.Write("0100");

            // Punto de emisión
            StreamWriter.Write(CorrectString(entity.EmissionPoint, 10));
            Log.DebugFormat("User: {0}", entity.EmissionPoint);

            // Bill type Code
            StreamWriter.Write(CorrectString(entity.DocumentType, 3));
            Log.DebugFormat("Bill type Code:  {0}", entity.DocumentType);

            // Número de folio
            StreamWriter.Write(CorrectNumber(entity.FolioNumber.ToString(), 10));
            Log.DebugFormat("Folio number:  {0}", entity.FolioNumber);

            // Número de documento interno
            StreamWriter.Write(CorrectString(entity.DocumentNumber.ToString(), 15));
            Log.DebugFormat("Document number:  {0}", entity.DocumentNumber);

            // Fecha de emisión contable
            StreamWriter.Write(entity.AccountingDate.ToString(DateFormat));
            Log.DebugFormat("Date issued:  {0}", entity.AccountingDate.ToString(DateFormat));

            // Tipo de servicio
            StreamWriter.Write(serviceType);
            Log.DebugFormat("Service type Code:  {0}", serviceType);

            // Indicador de monto neto (sólo para boletas)
            if (entity.DocumentType == "39")
            {
                StreamWriter.Write("1");
                Log.DebugFormat("Net amount: 1");
            }
            else if (entity.DocumentType == "41")
            {
                StreamWriter.Write("0");
                Log.DebugFormat("Net amount: 0");
            }
            else
            {
                StreamWriter.Write(" ");
                Log.DebugFormat("Net amount: empty");
            }

            // Fecha de vencimiento del pago
            if (entity.ExpireDate != null)
            {
                StreamWriter.Write(entity.ExpireDate.Value.ToString(DateFormat));
                Log.DebugFormat("Payment expiration date:  {0}", entity.ExpireDate.Value.ToString(DateFormat));
            }
            else
            {
                StreamWriter.Write(GetSpaces(10));
            }


            // Tipo/forma de pago
            if (entity.DefaultPaymentType != null)
            {
                StreamWriter.Write(CorrectString(entity.DefaultPaymentType.Code, 3));
                Log.DebugFormat("Payment type:  {0}", tmp);
            }
            else
            {
                StreamWriter.Write(GetSpaces(3));
                Log.DebugFormat("Payment type: empty.");
            }

            // Formato de impresión
            StreamWriter.Write(CorrectNumber("", 5));

            // Cantidad de líneas de detalle (opcional - omitido).
            StreamWriter.Write(GetZeroes(3));

            // Clase de documento
            StreamWriter.Write(GetSpaces(3));

            // Código de envío (forma en que se despachan los documentos)
            if (entity.DispatchType != null)
            {
                StreamWriter.Write(CorrectString(entity.DispatchType.Code, 4));
                Log.DebugFormat("Dispatch Code:  {0}", entity.DispatchType.Code);
            }
            else
            {
                StreamWriter.Write(GetSpaces(4));
                Log.DebugFormat("Dispatch Code:  {0}", "none");
            }

            // Indicador de trasaldo - sólo para guías de despacho
            // Versión 1.0: no soportado.
            if (entity.DocumentType == "52")
            {
                throw new NotImplementedException("This type of document (type 52) is not supported yet.");
            }
            else
            {
                StreamWriter.Write("0");
            }

            tmp = "0";

            // Indicador de rebaja - sólo para notas de crédito
            if (!CreditNoteDiscount)
                tmp = "1";

            StreamWriter.Write(tmp);
            Log.DebugFormat("Discount indicator:  {0}", tmp == "1");

            // END
            StreamWriter.WriteLine();
            Log.DebugFormat("- END -");
            registryCount++;
        }

        protected override void WriteTotals()
        {
            Log.DebugFormat("- Totals -");
            // Start
            StreamWriter.Write("0301");

            if (CorrectionCode != CorrectionCode.AmendText)
            {
                var exempt = ReferencesExemptBills();
                string netAmountStr;
                string vatAmountStr;

                decimal total;

                if (exempt)
                {
                    total = ExemptAmount;
                    netAmountStr = GetZeroes(18);
                    vatAmountStr = GetZeroes(18);
                }
                else
                {
                    total = NetAmount + VatAmount;
                    netAmountStr = CorrectNumber(Convert.ToInt32(NetAmount).ToString(), 18);
                    vatAmountStr = CorrectNumber(Convert.ToInt32(VatAmount).ToString(), 18);
                }

                // Monto neto
                StreamWriter.Write(netAmountStr);
                Log.DebugFormat("Net Amount: {0}", netAmountStr);

                // Monto exento (boleta electrónica exenta)
                StreamWriter.Write(CorrectNumber(Convert.ToInt32(ExemptAmount).ToString(), 18));
                Log.DebugFormat("Exempt Amount: {0}", ExemptAmount);

                // Tasa de IVA
                StreamWriter.Write(GetZeroes(6));

                // Monto de IVA
                StreamWriter.Write(vatAmountStr);
                Log.DebugFormat("Tax amount: {0}", vatAmountStr);

                // Monto total (+ IVA)
                StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));
                Log.DebugFormat("Total + Tax: {0}", total);

                // Monto no facturable
                StreamWriter.Write(CorrectNumber(Convert.ToInt64(nonBillableAmount).ToString(), 18));

                // TODO - Total Periodo
                // TODO - Saldo anterior electrónico
                // TODO - Saldo anterior otras
                StreamWriter.Write(GetZeroes(54));

                // TODO - Valor a pagar total del documento
                StreamWriter.Write(CorrectNumber(Convert.ToInt64(total).ToString(), 18));

                // TODO - Saldo anterior total
                // TODO - Valor comisión neto
                // TODO - " exento
                // TODO - " IVA
                // TODO - Total comisión
                // TODO - Total venta a tercero
                // TODO - Iva propio o de terceros
                // TODO - Total venta terceros
                // TODO - Iva no retornable (IVANoRet)
                // TODO - CredEC
                // TODO - GrntDep
                StreamWriter.Write(GetZeroes(198));
            }
            else
            {
                StreamWriter.Write(GetZeroes(18));
                Log.DebugFormat("Net Amount: {0}", 0);

                // Monto exento (boleta electrónica exenta)
                StreamWriter.Write(GetZeroes(18));
                Log.DebugFormat("Exempt Amount: {0}", 0);

                // Tasa de IVA
                StreamWriter.Write(GetZeroes(6));

                // Monto de IVA
                StreamWriter.Write(GetZeroes(18));
                Log.DebugFormat("Tax amount: {0}", 0);

                // Monto total (+ IVA)
                StreamWriter.Write(GetZeroes(18));
                Log.DebugFormat("Total + Tax: {0}", 0);

                // Monto no facturable
                StreamWriter.Write(CorrectNumber(Convert.ToInt64(nonBillableAmount).ToString(), 18));

                // TODO - Total Periodo
                // TODO - Saldo anterior electrónico
                // TODO - Saldo anterior otras
                StreamWriter.Write(GetZeroes(72));

                // TODO - Valor a pagar total del documento
                StreamWriter.Write(GetZeroes(18));

                // TODO - Saldo anterior total
                // TODO - Valor comisión neto
                // TODO - " exento
                // TODO - " IVA
                // TODO - Total comisión
                // TODO - Total venta a tercero
                // TODO - Iva propio o de terceros
                // TODO - Total venta terceros
                // TODO - Iva no retornable (IVANoRet)
                // TODO - CredEC
                // TODO - GrntDep
                StreamWriter.Write(GetZeroes(198));
            }

            // END
            StreamWriter.WriteLine();
            Log.DebugFormat("- END -");
            registryCount++;
        }

        protected virtual bool ReferencesExemptBills()
        {
            return entity.ReferencedDocuments.Any(i =>
                i.ReferencedDocument.DocumentType == "34" ||
                i.ReferencedDocument.DocumentType == "32");
        }

        protected virtual void SimpleDetails()
        {
            foreach (var detail in entity.TaxDocumentLines)
            {
                StreamWriter.Write("0500");
                StreamWriter.Write(CorrectNumber(detail.LineNumber.ToString(), 4));
                WriteDetails(detail);
                StreamWriter.WriteLine();
                registryCount++;

                if (detail.AdditionalGloss.Length > 0)
                    AddGloss(detail.LineNumber, detail.AdditionalGloss);
            }
        }

        protected virtual void SimpleDetails(TaxDocumentLine item)
        {
            StreamWriter.Write(GetSpaces(45));
            StreamWriter.Write("0");
            StreamWriter.Write(CorrectString(item.Text, 80));
            StreamWriter.Write(CorrectNumber("1", 18));
            StreamWriter.Write(GetSpaces(4));
            StreamWriter.Write(GetZeroes(72));
            StreamWriter.Write(GetSpaces(21));
            StreamWriter.Write(GetZeroes(8));
            StreamWriter.Write(GetSpaces(45));
        }
    }
}
