﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using log4net;
using Nancy;
using Zentom.Api.Modules;
using Zentom.Core.Entities;

namespace Zentom.Api.Security
{
    public static class AuthorizationHelper
    {
        private readonly static ILog Log = LogManager.GetLogger(typeof(AuthorizationHelper));

        private static readonly char[] trim = { ',' };

        public static AuthorizationResponse IsAuthorized<T>(NancyModule module, Func<T, object> handler)
        {
            return IsAuthorized(module, handler.Method);
        }

        public static AuthorizationResponse IsAuthorized(NancyModule module, MethodInfo method)
        {
            Log.DebugFormat("Entering IsAuthorized.");
            Log.InfoFormat("[Authorization] Requesting access to '{0}'.", method);
            if (!(module is TenantApi))
            {
                Log.WarnFormat("Module is not of type TenantApi. Received type: {0}", method.DeclaringType);
                return null;
            }
            var permissionAttrs = GetMethodAttrs(method, typeof(PermissionAttribute));
            var moduleAttrs = GetMethodAttrs(method, typeof (ModuleAttribute), false);
            
            var mod = (TenantApi)module;
            var permissions = CheckPermissions(permissionAttrs, mod.Account.AuthorizedBy.SingleOrDefault(i => i.Tenant == mod.Tenant));
            var modules = CheckModules(moduleAttrs, mod.Tenant);
            var response = new AuthorizationResponse(permissions, modules);
            Log.DebugFormat("[Authorization] Response: {0}", response);
            return response;
        }

        private static string CheckPermissions(IEnumerable<object> attributes, AuthorizedAccount account)
        {
            var requiredPermissions = new StringBuilder();

            foreach (var attribute in attributes.OfType<PermissionAttribute>().Where(attribute => attribute != null))
            {
                Log.DebugFormat("[Authorization] PermissionAttribute: {0}", attribute);
                if (attribute.Permission == Constants.ANONYMOUS)
                    return null;
                if (attribute.RequiresLogin && account != null)
                    return null;
                if (attribute.RequiresAdmin && account.IsAdmin())
                    return null;
                if (account.HasPermission(attribute.Permission))
                    return null;
                requiredPermissions.Append(attribute.Permission).Append(",");
            }

            return requiredPermissions.ToString().TrimEnd(trim);
        }

        private static string CheckModules(IEnumerable<object> attributes, Tenant tenant)
        {
            if (attributes == null)
            {
                return null;
            }
            var requiredModules = new StringBuilder();

            foreach (var attr in attributes.OfType<ModuleAttribute>().Where(i => i != null))
            {
                Log.DebugFormat("[Authorization] ModuleAttribute: {0}", attr);
                var found = tenant.Modules.SingleOrDefault(i => i.Module.Name == attr.Name);

                if (found != null)
                    return null;
                requiredModules.Append(attr.Name).Append(",");
            }

            return requiredModules.ToString().TrimEnd(trim);
        }

        private static IEnumerable<object> GetMethodAttrs(MethodInfo method, Type type = null, bool throwWhenNotDefined = true)
        {
            var clazz = method.DeclaringType;
            var attrs = type != null ? method.GetCustomAttributes(type, false) : method.GetCustomAttributes(false);
            if (attrs.Length > 0) return attrs;
            Log.Debug("[Authorization] No method attributes");
            if (clazz == null)
            {
                if (throwWhenNotDefined)throw new Exception($"No permissions declared for method '{method.Name}'");
                return null;
            }
            attrs = clazz.GetCustomAttributes(false);
            Log.DebugFormat("[Authorization] Class attribute count: {0}", attrs.Length);
            if (attrs.Length > 0) return attrs;
            Log.DebugFormat("[Authorization] No class attributes.");
            if(throwWhenNotDefined)
            throw new Exception(
                $"No permissions specified for class {clazz.FullName} nor method {method.Name}");
            return null;
        }

        public class AuthorizationResponse
        {
            public string RequiredPermissions { get; }

            public string RequiredModules { get; }

            public AuthorizationResponse(string requiredPermissions, string requiredModules)
            {
                RequiredPermissions = requiredPermissions;
                RequiredModules = requiredModules;
            }

            public override string ToString()
            {
                return $"RequiredPermissions: {RequiredPermissions}, RequiredModules: {RequiredModules}";
            }
        }
    }
}