﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    public class TenantSectorMap
        : ClassMap<TenantSector>
    {
        public TenantSectorMap()
        {
            Schema("taxation");
            Table("tenant_sector");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            References(i => i.Tenant, "tenant_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_sector_un");

            References(i => i.Sector, "sector_id")
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_sector_un");
        }
    }
}
