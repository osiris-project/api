﻿using System;
using System.Collections.Generic;

namespace Zentom.Api.Modules.AccountingEp
{
    public class CreateVoucherCmd
    {
        public DateTime Date { get; set; }

        public string Description { get; set; }

        public IList<VoucherDetailCmd> Lines { get; set; }

        public IEnumerable<Guid> Documents { get; set; }
    }

    public class VoucherDetailCmd
    {
        public AccountingAccountCmd AccountingAccount { get; set; }

        public CostsCenterCmd CostsCenter { get; set; }

        public string DocumentType { get; set; }

        public long FolioNumber { get; set; }

        public string Description { get; set; }

        public DateTime EmissionDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public string ReferencedDocumentType { get; set; }

        public long ReferencedDocumentFolio { get; set; }

        public decimal Debit { get; set; }

        public decimal Asset { get; set; }

        public decimal NetAmount { get; set; }

        public decimal NetFixedAsset { get; set; }

        public decimal TotalExemptAmount { get; set; }

        public decimal TotalTaxAmount { get; set; }

        public decimal TotalUnrecoverableAmount { get; set; }

        public decimal Total { get; set; }

        public string CustomField1 { get; set; }

        public string CustomField2 { get; set; }

        public string CustomField3 { get; set; }

        public ProviderCmd Provider { get; set; }
    }

    public class AccountingAccountCmd
    {
        public Guid Id { get; set; }

        public string Code { get; set; }
    }

    public class CostsCenterCmd
    {
        public Guid Id { get; set; }

        public string Code { get; set; }
    }

    public class ProviderCmd
    {
        public Guid Id { get; set; }

        public string TaxId { get; set; }
    }
}