﻿using System;

namespace Zentom.Api.Error
{
    public class TokenExpiredException
        : Exception
    {
        public TokenExpiredException()
            : base("The token has expired.")
        {
            
        }
    }
}