﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IPaymentType
        : IUnique<int>, IVersioned, IRowGuid
    {
        string Code { get; set; }

        string Description { get; set; }

        ICollection<IPayment> Payments { get; set; }
        
        ICollection<ITaxDocument> TaxDocuments { get; set; }
    }
}
