/**
 * Copyright (c) Mar 7, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core

/**
 * Exception thrown when an entity already exists in the applicable context.
 *
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 */
class DuplicateEntityException extends Exception {
    private static final long serialVersionUID = 1L

    /**
     * The duplicate entity type
     */
    final Class clazz

    /**
     * Throws an exception with a generic message using the {@link Class#getName()}
     *
     * @param clazz The duplicate entity type.
     */
    DuplicateEntityException(Class clazz) {
        super('Duplicate entity of type ' + clazz.name)
        this.clazz = clazz
    }

    /**
     * Builds an exception using the entity type and a message.
     *
     * @param clazz The duplicate entity type.
     * @param msg The exception message.
     */
    DuplicateEntityException(Class clazz, String msg) {
        super(msg)
        this.clazz = clazz
    }

    /**
     * Builds an exception using the entity type, a message and an exception.
     *
     * @param clazz The duplicate entity type
     * @param msg The message that the exception will contain.
     * @param ex The inner exception
     */
    DuplicateEntityException(Class clazz, String msg, Throwable ex) {
        super(msg, ex)
        this.clazz = clazz
    }
}
