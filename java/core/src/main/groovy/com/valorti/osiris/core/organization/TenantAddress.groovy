/**
 * Copyright (c) Mar 8, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.organization

import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Entity
@Table(schema = 'organization')
class TenantAddress extends CompanyAddress {
    @Transient
    private static final long serialVersionUID = 1L

    @ManyToOne
    Tenant tenant

    protected TenantAddress() {
    }

    /**
     * @param line1
     * @param line2
     * @param type
     * @param commune
     * @param tenant
     */
    TenantAddress(String line1, String line2, AddressType type, Commune commune, Tenant tenant) {
        super(line1, line2, type, commune)
        if(!tenant) {
            throw new IllegalArgumentException('No tenant provided')
        }
        this.tenant = tenant
    }
}
