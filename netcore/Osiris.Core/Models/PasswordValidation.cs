namespace Osiris.Core.Models {
    public class PasswordValidation {
        public byte[] PasswordHash { get; }

        public byte[] HashMeta { get; }

        public string Password { get; }

        /// <summary>
        /// Builds as password validation data model.
        /// </summary>
        /// <param name="passwordHash">Hashed password to check.</param>
        /// <param name="hashMeta">Salt data to be used to hash the raw password.</param>
        /// <param name="password">Raw password to be validated.</param>
        public PasswordValidation (byte[] passwordHash, byte[] hashMeta, string password) {
            PasswordHash = passwordHash;
            HashMeta = hashMeta;
            Password = password;
        }
    }
}
