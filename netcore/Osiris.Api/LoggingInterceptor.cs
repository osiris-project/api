using LightInject.Interception;
using Slf4Net;

namespace Osiris.Api {
    /// <summary>
    /// Intercepts every method call to log the entry and exit of each.
    /// </summary>
    public class LoggingInterceptor : IInterceptor {
        public object Invoke (IInvocationInfo invocationInfo) {
            var log = LoggerFactory.GetLogger (invocationInfo.Proxy.ToString ());
            log.Debug ("Entering {1}.{0}()", invocationInfo.Method.Name, invocationInfo.Proxy);
            var retval = invocationInfo.Proceed ();
            log.Debug ("Leaving {1}.{0}()", invocationInfo.Method.Name, invocationInfo.Proxy);
            return retval;
        }
    }
}
