﻿using System;
using System.Collections.Generic;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IProduct : IBaseProduct
    {
        ICollection<ITaxDocumentProduct> TaxDocumentProducts { get; set; }

        ICollection<IEnterpriseProduct> EnterpriseProducts { get; set; }
    }
}
