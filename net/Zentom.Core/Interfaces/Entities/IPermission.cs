﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IPermission
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Description { get; set; }

        ICollection<IOptionPermission> OptionPermissions { get; set; }

        ICollection<IRolePermission> RolePermissions { get; set; }
    }
}
