﻿using Osiris.Core.Interfaces.Validation;

namespace Osiris.Integration.Artikos
{
    internal class ModelValidation
        : IModelValidation
    {
        public ModelValidation(string memberName, string errorMessage)
        {
            MemberName = memberName;
            ErrorMessage = errorMessage;
        }

        public string MemberName { get; protected set; }

        public string ErrorMessage { get; protected set; }

        public override string ToString()
        {
            return string.Concat("MemberName=", MemberName, "Error=", ErrorMessage);
        }
    }
}
