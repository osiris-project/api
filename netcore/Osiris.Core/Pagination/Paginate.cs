﻿using System.Collections.Generic;

namespace Osiris.Core.Pagination
{
    public class Paginate<T>
        : IPagedResult<T>
    {
        #region Properties

        public IList<T> Items
        {
            get;
        }

        public long ResultCount
        {
            get;
        }

        public int CurrentPage
        {
            get;
        }

        public int ResultsPerPage
        {
            get;
        }

        public long PageCount
        {
            get;
        }

        public bool HasPrevious
        {
            get;
        }

        public bool HasNext
        {
            get;
        }

        #endregion

        public Paginate(IEnumerable<T> src, int page, int resultsPerPage, int total)
            : this(src, page, resultsPerPage, (long)total)
        { }

        public Paginate(IEnumerable<T> src, int page, int resultsPerPage, long total)
        {
            CurrentPage = page;
            ResultsPerPage = resultsPerPage;
            ResultCount = total;
            PageCount = ResultCount / ResultsPerPage;
            HasNext = (CurrentPage + 1) < ResultCount;
            HasPrevious = CurrentPage > 0;
            Items = new List<T>(src);
        }

        public Paginate(IList<T> src, int page, int resultsPerPage, long total)
        {
            ResultsPerPage = resultsPerPage;
            ResultCount = total;
            PageCount = ResultCount / ResultsPerPage;
            HasNext = (CurrentPage + 1) < ResultCount;
            HasPrevious = CurrentPage > 0;
            CurrentPage = page;
            Items = src;
        }
    }
}
