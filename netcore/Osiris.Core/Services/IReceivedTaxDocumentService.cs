﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Osiris.Core.Entities;
using Osiris.Core.Entities.TaxDocument;

namespace Osiris.Core.Services
{
    public interface IReceivedTaxDocumentService
        : IService
    {
        IList<ReceivedTaxDocument> ReceivedTaxDocuments { get; }

        void ReceiveDocument(Stream stream, Tenant t);

        Task ReceiveDocumentAsync(Stream stream, Tenant t);

        Task SaveDocumentAsync(string path);

        void SaveDocument(string path);
    }
}
