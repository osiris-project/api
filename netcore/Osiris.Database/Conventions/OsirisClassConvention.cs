﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Osiris.Database.Conventions
{
    public class OsirisClassConvention
        : IClassConvention
    {
        #region Implementation of IConvention<IClassInspector,IClassInstance>

        public void Apply(IClassInstance instance)
        {

            instance.OptimisticLock.Version();
        }

        #endregion
    }
}
