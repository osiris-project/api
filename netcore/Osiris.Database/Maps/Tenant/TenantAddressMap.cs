﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps.Tenant
{
    public class TenantAddressMap
        : ClassMap<TenantAddress>
    {
        public TenantAddressMap()
        {
            Schema("organization");
            Table("tenant_address");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(i => i.Line1).Length(70).UniqueKey("tenant_address_un");
            Map(i => i.Line2).Length(150);

            References(i => i.Commune, "commune_id")
                .Not.Nullable()
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_address_un");

            References(i => i.Tenant, "tenant_id")
                .Not.Nullable()
                .Cascade.SaveUpdate()
                .UniqueKey("tenant_address_un");

            HasMany(i => i.TenantAddressTypes)
                .Cascade.AllDeleteOrphan()
                .Inverse()
                .KeyColumn("tenant_address_id");
        }
    }
}
