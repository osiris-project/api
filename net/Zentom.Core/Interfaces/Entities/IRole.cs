﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface IRole
        : IUnique<long>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        string Description { get; set; }

        ICollection<IAccountRole> AccountRoles { get; set; }

        ICollection<IRolePermission> RolePermissions { get; set; }
    }
}
