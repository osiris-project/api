﻿using Dobelik.Utils.Password.Enums;
using Dobelik.Utils.Password.Interfaces;
using Slf4Net;
using System;
using System.Runtime.Serialization;

namespace Dobelik.Utils.Password
{
    /// <summary>
    /// Represents that data associated to the password hasher instance.
    /// </summary>
    /// <remarks>This class can be serialized for later storage.</remarks>
    [Serializable]
    public class HashInfo
        : IHashInfo
    {
        #region Constants

        ///// <summary>
        ///// The character that is used to separate the data when the fields are concatenated.
        ///// </summary>
        ///// <remarks>The character is ";". To escape it, use a backslash (\).</remarks>
        //const string DATA_SEPARATOR = ";";

        #endregion

        #region Fields

        private static readonly ILogger Log = LoggerFactory.GetLogger(typeof(HashInfo));

        #endregion

        #region Properties
            
        public int TotalHashLength { get; }

        public int HashLength { get; }

        public int SaltLength { get; }

        public int IterationCount { get; set; }

        public string SaltSeparator { get; set; }

        public SaltPosition SaltPosition { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates an instance of this class with the default values.
        /// </summary>
        public HashInfo()
            : this(64, 64, 50) { }

        /// <summary>
        /// Builds an instance of the class with some default values.
        /// </summary>
        /// <param name="hashLength">The length of the hash part.</param>
        /// <param name="saltLength">The length of the salt part.</param>
        /// <param name="saltPosition">The salt position in the resulting hash.</param>
        /// <param name="saltSeparator">When a string representation (instead of a byte array) is needed,
        /// this is the character that splits the hash from the salt parts.</param>
        /// <param name="iterationCount">How many times to iterate the encryption.</param>
        /// <returns>An instance of implementing class.</returns>
        /// <remarks>When specifying the hash and salt length, please, take into account that
        /// the final length will be the salt length + hash length. This is important if it's going to
        /// be stored on a database, where the length of the column is static.</remarks>
        public HashInfo(int hashLength, int saltLength, int iterationCount,
            SaltPosition saltPosition = SaltPosition.Append,
            string saltSeparator = "|")
        {
            if (hashLength % 2 != 0)
                hashLength -= 1;

            if (saltLength % 2 != 0)
                saltLength -= 1;

            if (hashLength == 0)
                hashLength = 1;

            if (saltLength <= 0)
                saltLength = 1;
            HashLength = hashLength;
            SaltLength = saltLength;
            TotalHashLength = HashLength + SaltLength;
            SetProperties(iterationCount, saltPosition, saltSeparator);
        }

        public HashInfo(SerializationInfo info, StreamingContext context)
        {
            HashLength = info.GetInt32("hl");
            SaltLength = info.GetInt32("sl");
            TotalHashLength = HashLength + SaltLength;
            IterationCount = info.GetInt32("ic");
            SaltSeparator = info.GetString("ss");
            SaltPosition = (SaltPosition)info.GetInt32("sp");
            Log.Debug("HashInfo CTOR. Deserialized: {0}, {1}, {2}, {3}, {4}, {5}",
                HashLength, SaltLength, TotalHashLength, IterationCount, SaltSeparator, SaltPosition);
        }

        #endregion

        #region IHashInfo
        
        public void SetProperties(int iterationCount, SaltPosition saltPosition = SaltPosition.Append,
            string saltSeparator = "|")
        {
            IterationCount = iterationCount;
            SaltPosition = saltPosition;

            if(saltSeparator?.Length > 0)
            {
                SaltSeparator = saltSeparator;
            }
        }

        #endregion

        #region Serialization

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Log.Debug("Entering GetObjectData.");
            info.AddValue("hl", HashLength, typeof(int));
            info.AddValue("sl", SaltLength, typeof(int));
            info.AddValue("ic", IterationCount, typeof(int));
            info.AddValue("ss", SaltSeparator, typeof(string));
            info.AddValue("sp", (int)SaltPosition, typeof(int));
            Log.Debug("Deserialized data: {0}", ToString());
            Log.Debug("Leaving GetObjectData.");
        }

        public override bool Equals(object obj)
        {
            var t = obj as HashInfo;
            if (ReferenceEquals(null, t))
                return false;
            if (ReferenceEquals(this, t))
                return true;
            return (HashLength == t.HashLength &&
                SaltLength == t.SaltLength &&
                IterationCount == t.IterationCount);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = GetType().GetHashCode();
                hash = (hash * 31) ^ HashLength.GetHashCode();
                hash = (hash * 31) ^ SaltLength.GetHashCode();

                return hash;
            }
        }

        #endregion

        #region Object

        public override string ToString()
        {
            return
                $"TotalHashLength: {TotalHashLength}, HashLength: {HashLength}, SaltLength: {SaltLength}, IterationCount: {IterationCount}, SaltSeparator: {SaltSeparator}, SaltPosition: {SaltPosition}";
        }

        #endregion

        #region Utilities

        //protected void Split()
        //{
        //    log.Trace("<!-- Entering Split");

        //    if (!string.IsNullOrEmpty(this.serializedData))
        //    {
        //        string[] split = splitRegex.Split(this.serializedData);

        //        foreach (string s in split)
        //        {
        //            if (s.StartsWith("hl"))
        //                this.HashLength = Convert.ToInt32(s.Replace("hl", "").Trim());

        //            if (s.StartsWith("sl"))
        //                this.SaltLength = Convert.ToInt32(s.Replace("sl", "").Trim());

        //            if (s.StartsWith("ic"))
        //                this.IterationCount = Convert.ToInt32(s.Replace("ic", "").Trim());

        //            if (s.StartsWith("ss"))
        //                this.SaltSeparator = s.Replace("ss", "").Trim();

        //            if (s.StartsWith("sp"))
        //            {
        //                SaltPosition sp = SaltPosition.Append;

        //                if (!Enum.TryParse<SaltPosition>(s.Replace("sp", "").Trim(), out sp))
        //                    sp = SaltPosition.Append;
        //            }
        //        }
        //    }


        //    log.Trace("HashInfo [HashLength = {0}, SaltLength = {1}, TotalHashLength = {2}, IterationCount = {3}, SaltSeparator = {4}, SaltPosition = {5}",
        //        HashLength, SaltLength, TotalHashLength, IterationCount, SaltSeparator, SaltPosition.ToString());
        //    log.Trace("Split -->");
        //}

        //protected void Join()
        //{
        //    log.Trace("<!-- Entering Join");
        //    GenerateRandomValues();
        //    this.serializedData = string.Concat("hl", this.HashLength, DATA_SEPARATOR);
        //    this.serializedData = string.Concat(this.serializedData, "sl", this.SaltLength, DATA_SEPARATOR);
        //    this.serializedData = string.Concat(this.serializedData, "ic", this.IterationCount, DATA_SEPARATOR);
        //    this.serializedData = string.Concat(this.serializedData, "ss", this.SaltSeparator, DATA_SEPARATOR);
        //    this.serializedData = string.Concat(this.serializedData, "sp", (int)this.SaltPosition, DATA_SEPARATOR);
        //    log.Trace("Join -->");
        //}

        //protected void GenerateRandomValues()
        //{
        //    log.Trace("<!-- Entering GeneratedRandomValues");
        //    this.IterationCount = rand.Next(minIterationCount, Convert.ToInt32((minIterationCount * 0.10) + minIterationCount));
        //    Array values = Enum.GetValues(typeof(SaltPosition));
        //    this.SaltPosition = (SaltPosition)values.GetValue(rand.Next(values.Length));
        //    log.Trace("GeneratedRandomValues -->");
        //}

        #endregion
    }
}
