﻿namespace Zentom.Api.Modules.ServiceEp
{
    public class ServiceDto
        : Command
    {
        public string Name { get; set; }

        public decimal UnitPrice { get; set; }

        public bool IsDiscontinued { get; set; }
    }
}