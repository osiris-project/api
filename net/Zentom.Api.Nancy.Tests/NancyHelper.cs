﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CsQuery.Utility;
using Nancy.Testing;
using Browser = Nancy.Testing.Browser;

namespace Zentom.Api.Nancy.Tests
{
    public static class NancyHelper
    {
        public const string AUTH_URL = "/auth";

        public static BrowserContext DefaultBrowserContext(this BrowserContext context, Browser browser, bool privileged = true)
        {
            var token = privileged ? browser.GetToken() : browser.GetUnprivToken();
            context.Header("x-tenant-id", browser.RetrieveTenant(token).ToString());
            context.DefaultBrowserContextWithoutAuth();
            context.Header("Authorization", token);
            return context;
        }

        public static BrowserContext DefaultBrowserContextWithoutAuth(this BrowserContext context)
        {
            context.HttpRequest();
            context.Header("Accept", "application/json");
            context.Header("Accept-Language", "es-cl");
            context.Header("Remote Address", "127.0.0.1");
            return context;
        }

        public static string GetToken(this Browser browser)
        {
            var result = browser.Post(AUTH_URL, with =>
            {
                with.HttpRequest();
                with.FormValue("loginName", "146408133");
                with.FormValue("password", "nomeacuerdo");
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "es-cl");
            });
            return result.Headers["Authorization"];
        }

        public static string GetUnprivToken(this Browser browser)
        {
            var result = browser.Post(AUTH_URL, with =>
            {
                with.HttpRequest();
                with.FormValue("loginName", "146408133");
                with.FormValue("password", "nomeacuerdo");
                with.Header("Accept", "application/json");
                with.Header("Accept-Language", "es-cl");
            });
            return result.Headers["Authorization"];
        }

        public static object RetrieveTenant(this Browser browser, string token)
        {
            var result = browser.Get("/resources/profile/tenants/", with =>
            {
                with.DefaultBrowserContextWithoutAuth();
                with.Header("Authorization", token);
            });

            result.Log();
            var response = result.Body.AsString().TrimStart(@")]}',\n".ToCharArray());
            Log("Clean Response: {0}", response);
            var tenant = JSON.ParseJSON<List<TenantDto>>(response)[0];
            return tenant.id;
        }

        public static void Log(this BrowserResponse response)
        {
            Log("Response Body: {0}", response.Body.AsString());
            Log("Headers:");
            foreach (var item in response.Context.Response.Headers)
            {
                Log("'{0}' => {1}", item.Key, item.Value);
            }
        }

        private static void Log(string msg, params object[] args)
        {
            Console.WriteLine(msg, args);
        }
    }
}
