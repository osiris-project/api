/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.Transient

import com.valorti.osiris.core.UUIDEntity
import com.valorti.osiris.core.organization.Tenant

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @since 0.0.1
 */
@Entity
@Table(schema = 'system')
class Role extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @Column(nullable = false, length = 50, unique = true)
    String name

    @Column(nullable = false, length = 100)
    String description

    @ManyToOne
    def permissions = new HashSet<RolePermission>()

    @OneToOne
    Tenant tenant

    /**
     *
     */
    protected Role() {
    }

    /**
     * @param name
     * @param description
     * @param tenant
     * @since 0.0.1
     */
    Role(String name, String description, Tenant tenant) {
        this.name = name
        this.description = description
        this.tenant = tenant
    }

    /**
     * Adds a single permission to this role.
     *
     * @param permission The permission to add.
     * @throws IllegalArgumentException if the permission is null
     * @since 0.0.1
     */
    void addPermission(Permission permission) {
        if(!permission) {
            throw new IllegalArgumentException('No permission provided!')
        }
        def found = false
        def rp = new RolePermission(this, permission)
        this.permissions.each {
            if(it == rp) {
                found = true
                return
            }
        }
        if(found) {
            return
        }
        this.permissions.add(rp)
    }

    /**
     * Removes a single permission.
     *
     * @param permission
     * @return true if a permission was removed, false otherwise.
     * @throws IllegalArgumentException if the permission is null
     * @since 0.0.1
     */
    boolean removePermission(Permission permission) {
        if(!permission) {
            throw new IllegalArgumentException('No permission provided')
        }
        return this.permissions.removeIf {
            if(it.permission == permission) {
                return true
            }
            return false
        }
    }

    /**
     * Sets the rolePermission collection from a list of permissions.
     * If the permissions list is empty or null, then the current
     * list of rolePermissions is cleared.
     *
     * @param permissions The permission list.
     * @since 0.0.1
     */
    void includePermissions(List<Permission> permissions) {
        if(permissions == null) {
            return
        }
        if(permissions.size() > 0) {
            // 1. Remove permissions from the actual list that
            // aren't in the new list:
            this.permissions.removeAll {
                // Find the existing item and remove from the new list, or
                def indexOf = permissions.indexOf(it.permission)
                if(indexOf > -1) {
                    permissions.remove(indexOf)
                    return false
                }
                // Remove the element from the actual list
                return true
            }
            // 2. Add the new permissions.
            permissions.forEach {
                def rp = new RolePermission(this, it)
                this.permissions.add(rp)
            }
        } else {
            this.permissions.clear()
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((name == null) ? 0 : name.hashCode())
        result = prime * result + ((tenant == null) ? 0 : tenant.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof Role)) {
            return false
        }
        final other = (Role) obj
        if(!Objects.equals(this.tenant, other.tenant)) {
            return false
        }
        return Objects.equals(this.name, other.name)
    }


}
