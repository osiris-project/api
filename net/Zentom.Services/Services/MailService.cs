﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Zentom.Services
{
    internal class MailService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof (MailService));

        private const string NoReplyEmail = "no-reply@zentom.valorti.com";

        public bool SendResetLink(string toEmail, string firstName, string resetUrl, string ipv4, string supportEmail = "support@zentom.valorti.com")
        {
            log.DebugFormat("Entering SendResetLink.");
            const string subject = "[Zentom - Password Reset]";
            var templates = GetMailTemplates(firstName, resetUrl, ipv4, "reset-password", supportEmail);

            try
            {
                dynamic sendgrid =
                    new SendGridAPIClient("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");
                var mail = new Mail
                {
                    Subject = subject,
                    From = new Email(NoReplyEmail, "Zentom")
                };
                var personalization = new Personalization();
                personalization.AddTo(new Email(toEmail, firstName));
                mail.AddPersonalization(personalization);
                mail.AddContent(new Content("text/plain", templates[0]));
                mail.AddContent(new Content("text/html", templates[1]));
                ParseResponse(sendgrid.client.mail.send.post(requestBody: mail.Get()));
            }
            catch (Exception ex)
            {
                log.Error("Failed to send reset password mail.", ex);
            }
            
            log.DebugFormat("Leaving SendResetLink.");
            return true;
        }

        public bool SendChangeSuccess(string toEmail, string firstName, string resetUrl, string ipv4, string supportEmail = "support@zentom.valorti.com")
        {
            log.DebugFormat("Entering SendResetLink.");
            const string subject = "[Zentom - Password Change]";
            var templates = GetMailTemplates(firstName, resetUrl, ipv4, "password-reset", supportEmail);

            try
            {
                var sendgrid =
                    new SendGridAPIClient("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");
                var mail = new Mail
                {
                    Subject = subject,
                    From = new Email(NoReplyEmail, "Zentom")
                };
                var personalization = new Personalization();
                personalization.AddTo(new Email(toEmail, firstName));
                mail.AddPersonalization(personalization);
                mail.AddContent(new Content("text/plain", templates[0]));
                mail.AddContent(new Content("text/html", templates[1]));
                ParseResponse(sendgrid.client.mail.send.post(requestBody: mail.Get()));
            }
            catch (Exception ex)
            {
                log.Error("Failed to send reset password mail.", ex);
            }

            log.DebugFormat("Leaving SendResetLink.");
            return true;
        }

        private static void ParseResponse(dynamic response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                log.Debug("Reset token sent.");
            }
            else
            {
                log.DebugFormat("Response body: {0}", response.Body.ReadAsStringAsync().Result);
            }
        }
       
        private static string[] GetMailTemplates(string firstName, string resetUrl, string ipv4,
            string templatePrefix = "reset-password", string supportEmail = "support@valorti.com")
        {
            log.DebugFormat("Current Thread: {0}", Thread.CurrentThread.ManagedThreadId);
            log.DebugFormat("Current IsoCode: {0}", Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
            var templates = new[] { resetUrl, resetUrl };
            var isocode = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            var folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "templates");
            // HTML template
            var templateName = Path.Combine(folderPath, templatePrefix + "_" + isocode + ".html");
            string result;

            if (!File.Exists(templateName))
            {
                log.ErrorFormat("The HTML reset password template wasn't found at '{0}'.", templateName);
            }
            else
            {
                result = File.ReadAllText(templateName);
                result =
                    result.Replace("${user}", firstName)
                        .Replace("${resetUrl}", resetUrl)
                        .Replace("${ip}", ipv4)
                        .Replace("${support}", supportEmail);
                templates[1] = result;
            }


            // TXT template
            templateName = Path.Combine(folderPath, templatePrefix + "_" + isocode + ".txt");

            if (!File.Exists(templateName))
            {
                log.ErrorFormat("The raw reset password template wasn't found at '{0}'.", templateName);
            }
            else
            {
                result = File.ReadAllText(templateName);
                result =
                    result.Replace("${user}", firstName)
                        .Replace("${resetUrl}", resetUrl)
                        .Replace("${ip}", ipv4)
                        .Replace("${support}", supportEmail);
                templates[0] = result;
            }

            return templates;
        }
    }
}
