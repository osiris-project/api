﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class EmployeeContactInfoMap
        : SubclassMap<EmployeeContactInfo>
    {
        public EmployeeContactInfoMap()
        {
            Abstract();
            Schema("organization");
            Table("employee_contact_info");

            References(i => i.Employee, "employee_id")
                .Cascade.All()
                .Unique();
        }
    }
}
