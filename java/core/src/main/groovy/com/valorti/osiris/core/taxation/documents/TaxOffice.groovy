/**
 * Copyright (c) Mar 9, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.taxation.documents

import javax.persistence.Column
import javax.persistence.Embeddable

import com.valorti.osiris.core.ValidationException

/**
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 * @version 0.0.1
 * @since 0.0.1
 */
@Embeddable
class TaxOffice {
    @Column(nullable = false, length = 50)
    final String name

    @Column(nullable = false, length = 6)
    final String code

    protected TaxOffice() {
        this.name = ''
        this.code = ''
    }

    /**
     * @param name
     * @param code
     */
    TaxOffice(String name, String code) {
        def errors = []
        if(!name.trim()) {
            errors << ValidationBuilde.empty('taxOffice.name')
        }
        if(!code.trim()) {
            errors << ValidationBuilde.empty('taxOffice.code')
        }
        if(errors) {
            throw new ValidationException(errors)
        }
        this.name = name.trim()
        this.code = code.trim()
    }
}
