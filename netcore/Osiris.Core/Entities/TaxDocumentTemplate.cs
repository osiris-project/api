﻿using System;
using System.IO;

namespace Osiris.Core.Entities
{
    public class TaxDocumentTemplate
        : TenantEntity
    {
        #region Fields

        private string name;

        private string filePath;

        private string documentType;

        private DateTime createdAt;

        private string fileContents;

        private Account account;

        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual DateTime CreatedAt
        {
            get { return createdAt; }
            set { createdAt = value; }
        }

        public virtual string FileContents
        {
            get
            {
                if (string.IsNullOrWhiteSpace(filePath))
                    fileContents = "";
                else
                {
                    if (File.Exists(filePath))
                    {
                        fileContents = File.ReadAllText(filePath);
                    }
                }

                return fileContents;
            }
        }

        public virtual Account Account
        {
            get { return account; }
            set { account = value; }
        }

        #endregion

        public TaxDocumentTemplate()
        {
        }

        public TaxDocumentTemplate(string filePath, string name, string documentType, DateTime createdAt, Account account)
        {
            this.filePath = filePath;
            this.name = name;
            this.documentType = documentType;
            this.createdAt = createdAt;
            this.account = account;
        }

        public virtual void Save(string jsonContents)
        {
            File.WriteAllText(filePath, jsonContents);
        }

        public virtual string Read()
        {
            if (string.IsNullOrWhiteSpace(filePath))
                return "";
            return !File.Exists(filePath)
                ? ""
                : File.ReadAllText(filePath);
        }

        public virtual void RemoveFile()
        {
            if (!File.Exists(filePath))
                return;
            File.Delete(filePath);
        }
    }
}
