﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ICity : IUnique<int>, IVersioned, IRowGuid
    {
        string Name { get; set; }

        ICountry Country { get; set; }

        ICollection<ICommune> Communes { get; set; }
    }
}
