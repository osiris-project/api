﻿using System;
using Zentom.Api.Modules.EmployeeEp;
using Zentom.Core.Entities;

namespace Zentom.Api.Modules.ManagementEp
{
    public class AccountCmd
        : AccountCreateCmd
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalId { get; set; }

        public DateTime? BirthDate { get; set; }

        public Gender Gender { get; set; }
    }
}