﻿using System;
using System.Collections.Generic;
using Osiris.Api.Modules.RoleEp;

namespace Osiris.Api.Modules.EmployeeEp
{
    public class RoleCommand
    {
        public Guid AccountId { get; set; }

        public IList<RoleDto> Roles { get; set; }
    }
}
