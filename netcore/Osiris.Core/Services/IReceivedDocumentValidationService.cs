﻿using System.IO;
using Osiris.Core.Entities;

namespace Osiris.Core.Services
{
    public interface IReceivedDocumentValidationService
        : IService
    {
        void UpdateDocuments(Stream stream, Tenant tenant);
    }
}
