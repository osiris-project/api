﻿using System;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Entities
{
    public abstract class Entity
        : VersionedEntity, IUnique<Guid>
    {
        private int? oldHashCode;

        #region Properties

        public virtual Guid Id { get; protected set; }

        public virtual bool IsTransient => Id == Guid.Empty;

        #endregion
        
        #region Object

        public override bool Equals(object obj)
        {
            var other = obj as Entity;

            if (other == null)
                return false;

            if (IsTransient && other.IsTransient)
                return ReferenceEquals(this, other);

            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            if (oldHashCode.HasValue)
                return oldHashCode.Value;

            if (IsTransient)
            {
                oldHashCode = base.GetHashCode();
                return oldHashCode.Value;
            }

            return Id.GetHashCode();
        }

        #endregion

        #region Operators

        public static bool operator ==(Entity obj1, Entity obj2)
        {
            return Equals(obj1, obj2);
        }

        public static bool operator != (Entity obj1, Entity obj2)
        {
            return !(obj1 == obj2);
        }

        #endregion


    }
}
