﻿namespace Osiris.Core.Interfaces.Data
{
    public interface IVersioned
    {
        byte[] RowVersion { get; }
    }
}
