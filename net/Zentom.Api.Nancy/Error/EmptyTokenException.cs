﻿using System;

namespace Zentom.Api.Error
{
    public class EmptyTokenException
        : Exception
    {
        public EmptyTokenException()
            : base(I18n.Error.EmptyToken)
        {
        }
    }
}