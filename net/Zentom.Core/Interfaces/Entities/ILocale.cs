﻿using System.Collections.Generic;
using Zentom.Core.Interfaces.Data;

namespace Zentom.Core.Interfaces.Entities
{
    public interface ILocale
        : IUnique<int>, IVersioned, IRowGuid
    {
        string Code { get; set; }

        string FullName { get; set; }
        
        ICollection<ICountryLocale> CountryLocales { get; set; }
    }
}
