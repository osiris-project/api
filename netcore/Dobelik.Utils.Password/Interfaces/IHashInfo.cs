﻿using Dobelik.Utils.Password.Enums;
using System.Runtime.Serialization;

namespace Dobelik.Utils.Password.Interfaces
{
    public interface IHashInfo
        : ISerializable
    {
        /// <summary>
        /// The length of the hash + the length of the salt, which
        /// should be a constant value for the entire application
        /// (unless the hash it's stored on files, with variable data length).
        /// </summary>
        int TotalHashLength { get; }

        /// <summary>
        /// The byte length of the hash. Please, use an even value, otherwise it will reset to 64.
        /// Defaults to 64.
        /// </summary>
        int HashLength { get; }

        /// <summary>
        /// The byte lenght of the generated salt.
        /// Use even numbers, otherwise the value will be reset to 64.
        /// Defatuls to 64.
        /// </summary>
        int SaltLength { get; }

        /// <summary>
        /// This is the number of iterations to hash the password.
        /// More is better, but slower (for the hardware and the attacker).
        /// Defaults to 50.
        /// </summary>
        int IterationCount { get; set; }

        /// <summary>
        /// A separator for the salt. Only used when a string representation of the hash is used,
        /// otherwise is ignored.
        /// Defaults to "|".
        /// </summary>
        string SaltSeparator { get; set; }

        /// <summary>
        /// Whether the salt will be added to the hash or not, and where.
        /// See <see cref="Password.SaltPosition"/> for more info.
        /// Defaults to SaltPosition.Append.
        /// </summary>
        SaltPosition SaltPosition { get; set; }

        void SetProperties(int iterationCount, SaltPosition saltPosition = SaltPosition.Append,
            string saltSeparator = "|");
    }
}
