﻿using NHibernate;
using NHibernate.Criterion;
using Zentom.Core.Pagination;

namespace Zentom.Extensions.NHibernate
{
    public static class NHibernateExternsions
    {
        public static IPagedResult<TEntity> PagedResults<TEntity>(this ICriteria criteria, int page = 0, int resultsPerPage = 15)
        {
            var clone = (ICriteria)criteria.Clone();
            clone.ClearOrders();

            var results = criteria.SetFirstResult((page - 1) * resultsPerPage)
                .SetMaxResults(resultsPerPage)
                .Future<TEntity>();
            var count = clone.SetProjection(Projections.RowCount())
                .FutureValue<int>().Value;

            return new PagedResults<TEntity>(results, page, resultsPerPage, count);
        }

        public static ICriteria Sort(this ICriteria criteria, string field, bool asc)
        {
            return criteria.AddOrder(asc ? Order.Asc(field) : Order.Desc(field));
        }
    }
}
