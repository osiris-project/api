﻿using FluentValidation;
using Zentom.Api.Modules.Validators.Guid;

namespace Zentom.Api.Modules.Validators
{
    public class EntityValidator
        : AbstractValidator<Command>
    {
        public EntityValidator()
        {
            RuleFor(i => i.Id).SetValidator(new GuidValidator());
        }
    }
}