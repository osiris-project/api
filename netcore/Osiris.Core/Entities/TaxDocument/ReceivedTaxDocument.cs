﻿using System;
using System.Collections.Generic;
using System.Linq;
using Osiris.Core.Entities.Accounting;
using Osiris.Core.Enums;

namespace Osiris.Core.Entities.TaxDocument
{
    public enum ReceptionStatus
        : short
    {
        Pending = 0,
        Received,
        NotReceived,
        Missing
    }

    public class ReceivedTaxDocument
        : TenantEntity
    {
        #region Fields

        private string documentType;

        private long folioNumber;

        private DateTime accountingDate;

        private DateTime? expiryDate;

        private string comments;

        private string filePath;

        private decimal taxRate;

        private DateTime receptionTime;

        private short extraTaxCode;

        private decimal extraTaxRate;

        private ReceptionStatus receptionStatus;

        private bool acceptedForAccounting;

        // Totals

        private decimal netAmount;

        private decimal exemptAmount;

        private decimal retainedTaxAmount;

        private decimal taxAmount;

        private decimal subjectAmount;

        private decimal total;

        // Provider Data

        private string providerTaxId;

        private string providerLegalName;

        private string providerPhone;

        private string providerBankAccountNumber;

        private string providerComercialActivity;

        private string providerEconomicActivity;

        private string providerAddresLine1;

        private string providerAddressLine2;

        private string providerAddressCommune;

        private string providerAddressCity;

        // Receptor Data

        private string receptorTaxId;

        private string receptorLegalName;

        private string receptorComercialActivity;

        private string receptorAddress;

        private string receptorCommune;

        private ISet<ReceivedTaxDocumentLine> taxDocumentLines;

        private Enterprise enterprise;

        private AccountingVoucher voucher;

        private ISet<ReceivedTaxDocumentReference> references = new HashSet<ReceivedTaxDocumentReference>();

        private ISet<ReceivedTaxDocumentReference> referencedBy;

        private ISet<ReceivedTaxDocumentWeakReference> weakReferences = new HashSet<ReceivedTaxDocumentWeakReference>();
        
        #endregion

        #region Properties

        public virtual string DocumentType
        {
            get { return documentType; }
            set { documentType = value; }
        }

        public virtual long FolioNumber
        {
            get { return folioNumber; }
            set { folioNumber = value; }
        }

        public virtual DateTime AccountingDate
        {
            get { return accountingDate; }
            set { accountingDate = value; }
        }

        public virtual DateTime? ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public virtual string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        public virtual string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public virtual decimal TaxRate
        {
            get { return taxRate; }
            set { taxRate = value; }
        }

        public virtual DateTime ReceptionTime
        {
            get { return receptionTime; }
            set { receptionTime = value; }
        }

        public virtual ReceptionStatus ReceptionStatus
        {
            get { return receptionStatus; }
            set { receptionStatus = value; }
        }

        public virtual bool AcceptedForAccounting
        {
            get { return acceptedForAccounting; }
            set { acceptedForAccounting = value; }
        }

        public virtual short ExtraTaxCode
        {
            get { return extraTaxCode; }
            set { extraTaxCode = value; }
        }

        public virtual decimal ExtraTaxRate
        {
            get { return extraTaxRate; }
            set { extraTaxRate = value; }
        }

        public virtual string ProviderTaxId
        {
            get { return providerTaxId; }
            set { providerTaxId = value; }
        }

        public virtual string ProviderLegalName
        {
            get { return providerLegalName; }
            set { providerLegalName = value; }
        }

        public virtual string ProviderPhone
        {
            get { return providerPhone; }
            set { providerPhone = value; }
        }

        public virtual string ProviderBankAccountNumber
        {
            get { return providerBankAccountNumber; }
            set { providerBankAccountNumber = value; }
        }

        public virtual string ProviderComercialActivity
        {
            get { return providerComercialActivity; }
            set { providerComercialActivity = value; }
        }

        public virtual string ProviderEconomicActivity
        {
            get { return providerEconomicActivity; }
            set { providerEconomicActivity = value; }
        }

        public virtual string ProviderAddresLine1
        {
            get { return providerAddresLine1; }
            set { providerAddresLine1 = value; }
        }

        public virtual string ProviderAddressLine2
        {
            get { return providerAddressLine2; }
            set { providerAddressLine2 = value; }
        }

        public virtual string ProviderAddressCommune
        {
            get { return providerAddressCommune; }
            set { providerAddressCommune = value; }
        }

        public virtual string ProviderAddressCity
        {
            get { return providerAddressCity; }
            set { providerAddressCity = value; }
        }

        public virtual string ReceptorTaxId
        {
            get { return receptorTaxId; }
            set { receptorTaxId = value; }
        }

        public virtual string ReceptorLegalName
        {
            get { return receptorLegalName; }
            set { receptorLegalName = value; }
        }

        public virtual string ReceptorComercialActivity
        {
            get { return receptorComercialActivity; }
            set { receptorComercialActivity = value; }
        }

        public virtual string ReceptorAddress
        {
            get { return receptorAddress; }
            set { receptorAddress = value; }
        }

        public virtual string ReceptorCommune
        {
            get { return receptorCommune; }
            set { receptorCommune = value; }
        }

        public virtual ISet<ReceivedTaxDocumentLine> TaxDocumentLines
        {
            get { return taxDocumentLines; }
            set { taxDocumentLines = value; }
        }

        public virtual Enterprise Enterprise
        {
            get { return enterprise; }
            set { enterprise = value; }
        }

        public virtual AccountingVoucher Voucher
        {
            get { return voucher; }
            set { voucher = value; }
        }

        public virtual ISet<ReceivedTaxDocumentReference> References
        {
            get { return references; }
            set { references = value; }
        }

        public virtual ISet<ReceivedTaxDocumentReference> ReferencedBy
        {
            get { return referencedBy; }
            set { referencedBy = value; }
        }

        public virtual ISet<ReceivedTaxDocumentWeakReference> WeakReferences
        {
            get { return weakReferences; }
            set { weakReferences = value; }
        }

        // Totals

        public virtual decimal NetAmount
        {
            get { return netAmount; }
            set { netAmount = value; }
        }

        public virtual decimal ExemptAmount
        {
            get { return exemptAmount; }
            set { exemptAmount = value; }
        }

        public virtual decimal SubjectAmount
        {
            get { return subjectAmount; }
            set { subjectAmount = value; }
        }

        public virtual decimal RetainedTaxAmount
        {
            get { return retainedTaxAmount; }
            set { retainedTaxAmount = value; }
        }

        public virtual decimal TaxAmount
        {
            get { return taxAmount; }
            set { taxAmount = value; }
        }

        public virtual decimal Total
        {
            get { return total; }
            set { total = value; }
        }

        #endregion

        #region Ctors.

        public ReceivedTaxDocument()
        {
            taxDocumentLines = new HashSet<ReceivedTaxDocumentLine>();
            references = new HashSet<ReceivedTaxDocumentReference>();
            referencedBy = new HashSet<ReceivedTaxDocumentReference>();
        }

        public ReceivedTaxDocument(Tenant tenant, string documentType, long folioNumber, DateTime accountingDate,
            string comments, string filePath, decimal taxRate, DateTime receptionTime, short extraTaxCode,
            decimal extraTaxRate, string providerTaxId, string providerLegalName,
            string providerPhone, string providerBankAccountNumber, string providerComercialActivity, string providerEconomicActivity,
            string providerAddresLine1, string providerAddressLine2, string providerAddressCommune,
            string providerAddressCity, string receptorTaxId, string receptorLegalName, string receptorComercialActivity,
            string receptorAddress, string receptorCommune, ISet<ReceivedTaxDocumentLine> taxDocumentLines,
            Enterprise enterprise, ISet<ReceivedTaxDocumentReference> references, ISet<ReceivedTaxDocumentReference> referencedBy, DateTime? expiryDate = null)
            : base(tenant)
        {
            this.documentType = documentType;
            this.folioNumber = folioNumber;
            this.accountingDate = accountingDate;
            this.expiryDate = expiryDate;
            this.comments = comments;
            this.filePath = filePath;
            this.taxRate = taxRate;
            this.receptionTime = receptionTime;
            this.extraTaxCode = extraTaxCode;
            this.extraTaxRate = extraTaxRate;
            this.providerTaxId = providerTaxId;
            this.providerLegalName = providerLegalName;
            this.providerPhone = providerPhone;
            this.providerBankAccountNumber = providerBankAccountNumber;
            this.providerComercialActivity = providerComercialActivity;
            this.providerEconomicActivity = providerEconomicActivity;
            this.providerAddresLine1 = providerAddresLine1;
            this.providerAddressLine2 = providerAddressLine2;
            this.providerAddressCommune = providerAddressCommune;
            this.providerAddressCity = providerAddressCity;
            this.receptorTaxId = receptorTaxId;
            this.receptorLegalName = receptorLegalName;
            this.receptorComercialActivity = receptorComercialActivity;
            this.receptorAddress = receptorAddress;
            this.receptorCommune = receptorCommune;
            this.taxDocumentLines = taxDocumentLines;
            this.enterprise = enterprise;
            this.references = references;
            this.referencedBy = referencedBy;
        }

        #endregion

        #region References

        public virtual void AddReference(ReceivedTaxDocument referenced, CorrectionCode correctionCode, string reason, DateTime? referenceDate = null)
        {
            if (references.Any(i => i.ReferencedDocument.Id == referenced.Id))
                return;

            var reference = new ReceivedTaxDocumentReference(correctionCode.ToString(), reason, referenceDate, this, referenced);
            references.Add(reference);
        }

        public virtual void AddReference(long folio, string doctype, DateTime date, string reason)
        {
            var reference = new ReceivedTaxDocumentWeakReference(folio, doctype, date, reason, this);

            if (weakReferences.Any(i => i.Folio == folio && i.DocumentType == doctype))
            {
                return;
            }
            weakReferences.Add(reference);
        }

        #endregion
    }
}
