﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class CommuneMap : ClassMap<Commune>
    {
        public CommuneMap()
        {
            Schema("location");
            Table("commune");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            Map(i => i.Name).Length(20).UniqueKey("commune_un");
            Map(i => i.PostCode).Length(7).Default("''");

            HasMany(x => x.EnterpriseAddresses)
                .Cascade.SaveUpdate()
                .Inverse();

            References(i => i.City, "city_id")
                .UniqueKey("commune_un")
                .Cascade.SaveUpdate();
        }
    }
}
