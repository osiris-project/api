﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class DispatchTypeMap : ClassMap<DispatchType>
    {
        public DispatchTypeMap()
        {
            Schema("taxation");
            Table("dispatch_type");

            Id(i => i.Id);
            Version(i => i.RowVersion);

            Map(p => p.Code).Length(10).Unique();
            Map(p => p.Description).Length(70).Default("''");

            HasMany(x => x.TaxDocuments)
                .Cascade.SaveUpdate()
                .Inverse()
                .KeyColumn("dispatch_type_id");
        }
    }
}
