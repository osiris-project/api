﻿namespace Osiris.Core.Entities
{
    public abstract class CompanySector
        : CompanyEntity
    {
        #region Fields

        private Sector sector;

        #endregion

        public virtual Sector Sector
        {
            get { return sector; }
            set { sector = value; }
        }

        protected CompanySector()
        {
        }

        protected CompanySector(Company enterprise, Sector sector) : base(enterprise)
        {
            this.sector = sector;
        }
    }
}
