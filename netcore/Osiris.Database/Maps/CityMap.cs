﻿using FluentNHibernate.Mapping;
using Osiris.Core.Entities;

namespace Osiris.Database.Maps
{
    public class CityMap : ClassMap<City>
    {
        public CityMap()
        {
            Schema("location");
            Table("city");

            Id(i => i.Id);
            Version(i => i.RowVersion);
            Map(i => i.Name).Length(100).UniqueKey("city_un");

            HasMany(x => x.Communes)
                .Inverse()
                .Cascade.SaveUpdate();

            References(i => i.Country, "country_id")
                .Unique()
                .Cascade.SaveUpdate();
        }
    }
}
