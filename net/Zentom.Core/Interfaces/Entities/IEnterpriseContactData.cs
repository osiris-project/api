﻿namespace Zentom.Core.Interfaces.Entities
{
    public interface IEnterpriseContactData : IContactData
    {
        IEnterprise Enterprise { get; set; }
    }
}
