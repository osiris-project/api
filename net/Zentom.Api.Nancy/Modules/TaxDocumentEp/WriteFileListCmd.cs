﻿using System;

namespace Zentom.Api.Modules.TaxDocumentEp
{
    public class WriteFileListCmd
    {
        public Guid[] Documents { get; set; } 
    }
}