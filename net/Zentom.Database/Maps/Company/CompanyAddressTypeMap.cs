﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps.Company
{
    public class CompanyAddressTypeMap
        : ClassMap<CompanyAddressType>
    {
        public CompanyAddressTypeMap()
        {
            UseUnionSubclassForInheritanceMapping();
            Id(i => i.Id);
            Version(i => i.RowVersion);
        }
    }
}
