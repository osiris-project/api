﻿using System;
using System.Collections.Generic;
using Zentom.Api.Modules.RoleEp;

namespace Zentom.Api.Modules.EmployeeEp
{
    public class RoleCommand
    {
        public Guid AccountId { get; set; }

        public IList<RoleDto> Roles { get; set; }
    }
}