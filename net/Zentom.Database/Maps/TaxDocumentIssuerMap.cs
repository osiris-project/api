﻿using FluentNHibernate.Mapping;
using Zentom.Core.Entities;

namespace Zentom.Database.Maps
{
    public class TaxDocumentIssuerMap
        : SubclassMap<TaxDocumentIssuer>
    {
        public TaxDocumentIssuerMap()
        {
            Table("tax_document_issuer");

            Map(i => i.SectorCode).Column("sector_code").Length(6).Not.Nullable();
            Map(i => i.VatOwnerTaxationId).Column("vat_owner_taxation_id").Length(20).Not.Nullable().Default("''");
            Map(i => i.IssuerName).Column("issuer_name").Length(100).Not.Nullable();
        }
    }
}
