/**
 * Copyright (c) Mar 6, 2017 ValorTI Limitada.
 * All rights reserved.
 */
package com.valorti.osiris.core.system

import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Transient
import javax.persistence.UniqueConstraint

import com.valorti.osiris.core.UUIDEntity

/**
 * Represents a link between a role and a permission.
 * @author Nicolas Mancilla <nmancilla@valorti.com>
 *
 */
@Entity
@Table(uniqueConstraints = [
    @UniqueConstraint(
    columnNames = [ 'permission_id', 'role_id' ]
    )],
schema = 'system')
class RolePermission extends UUIDEntity {
    @Transient
    private static final long serialVersionUID = 1L

    @OneToMany
    Role role

    @OneToMany
    Permission permission

    /**
     *
     */
    protected RolePermission() {
    }

    /**
     * Links a role with a permission.
     *
     * @param role
     * @param permission
     * @since 0.0.1
     */
    RolePermission(Role role, Permission permission) {
        if(!role) {
            throw new IllegalArgumentException('No role provided!')
        }
        if(!permission) {
            throw new IllegalArgumentException('No role provided!')
        }
        this.role = role
        this.permission = permission
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31
        int result = super.hashCode()
        result = prime * result + ((permission == null) ? 0 : permission.hashCode())
        result = prime * result + ((role == null) ? 0 : role.hashCode())
        return result
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    boolean equals(Object obj) {
        if(this.is(obj)) {
            return true
        }
        if(!(obj instanceof RolePermission)) {
            return false
        }
        final other = (RolePermission) obj
        if(!Objects.equals(this.permission, other.permission)) {
            return false
        }
        return Objects.equals(this.role, other.role)
    }
}
